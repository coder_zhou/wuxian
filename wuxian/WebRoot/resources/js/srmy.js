$(function(){
    $(".srmy_menu>li").click(function(){
        $(this).css({borderBottom:"2px solid #43a5de"}).siblings().css({border:"none"});
    });

    $(".tab_menu li").click(function(){
        $(this).css({backgroundColor:"#5abbf4"}).siblings().css({backgroundColor:"#bbbbbb"});
        var v1=$(this).index();
        $(".tab_content>div").eq(v1).removeClass("content_hide").siblings().addClass("content_hide");
    });
});