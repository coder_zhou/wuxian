/// <reference path="jquery-1.8.2.js" />
//



$(function () {


    $(".srmy_menu>li").click(function(){
        $(this).css({borderBottom:"2px solid #43a5de"}).siblings().css({border:"none"});
    });


    $(".tab_menu li").click(function(){
        $(this).css({backgroundColor:"#5abbf4"}).siblings().css({backgroundColor:"#bbbbbb"});
        $(".tab_menu li.choose_ceng").css({backgroundColor:"#5abbf4"});
        var v1=$(this).index();
        $(".srmy_tab_content>div").eq(v1).removeClass("content_hide").siblings().addClass("content_hide");
    });


    $(".tab_menu li.jisi").click(function(){
        $(".jisi_show").css({display:"block"});
        $(".buzhi_show").css({display:"none"});
        $("#buzhi_corpus_bg,#buzhi_corpus_content").css({display:"none"});

    });

    $(".tab_menu li.buzhi").click(function(){
        $(".buzhi_show").css({display:"block"});
        $(".jisi_show").css({display:"none"});
        $("#corpus_bg,#corpus_content").css({display:"none"});
    });


    //素材---点击素材类---图片和文字的效果
    $(".sacrifice li").click(function(){
        $(this).children('img').css({border:"2px solid #fd844e"});
        $(this).siblings().children('img').css({border:"none"});
        $(this).children('div').css({color:"#fd844e"});
        $(this).siblings().children('div').css({color:"#2a95d4"});
        $("#bg,#show").css({display:"block"});
    });


});


$(function(){
    $(".goods_imgtext").hide();
    $(".content_heng_first>div").hover(function(){
        $(".goods_imgtext",this).slideToggle(0);
    });


// 显示选中状态
    $(".content_img").toggle(function(){
        $(this).children('.pitch_on').css({display:"block"});
    },function(){
        $(this).children('.pitch_on').css({display:"none"});
    });


// tab页切换
    $(".kind_title li").click(function(){
        $(this).css({backgroundColor:"#ffffff",color:"#2a95d4"}).siblings().css({backgroundColor:"#c0e7ff",color:"#777777"});
        var tab_i=$(this).index();
        $(".tab_content .content_first").eq(tab_i).removeClass("content_hide").siblings().addClass("content_hide");
    });



//  选取商品，点击确定，弹出下一页
    $(".tab_btn").click(function(){
        $(".bg_confirm,.show_confirm").css({display:"block"});
    });



//补充福币
    $(".fill_money").click(function(){
        $(".bg_prompt,.prompt_infor").css({display:"block"});
    });

//福币充足时，确定按钮，直接进入墓园
    $(".affirm").click(function(){
        $("#show,#bg,.bg_prompt,.prompt_infor,.bg_confirm,.show_confirm").css({display:"none"});
        $(".footer a input").css({backgroundColor:"#bbbbbb"});

        //保存按钮
        $(".footer_save_btn").css({display:"block"});

        //添加图片到页面
        for (var i = 1; i <= change_num; i++) {
            html_img = '<img id=div' + (real_num + i) + " " + 'data-name=img' + (real_num + i)
            + ' data-id=url' + (real_num + i) + " " + 'src=' + arr[i - 1] + " " + 'name=1 class=srmy_FixedAdorn>';

            //添加图片对应的名称
            img_title='<li'+" "+'id=list'+ (real_num + i) +" " +'class=cc draggable=true '+
            'data-itemidx=' + (real_num - 1 + i) + '>' + arr_title[i-1]+ '<span class=buy_user>' + '匿名者祭祀' + '<img src=images/srmy_materisl_big.png class=srmy_sucai_big><img src=images/srmy_materisl_reduce.png class=srmy_sucai_reduce><img src=images/srmy_materisl_hidden.png class=srmy_sucai_hidden><img src=images/srmy_materisl_delete.png class=srmy_sucai_delete></span>'+'</li>';

            SiteFunc.AddAdorn(html_img);

            SiteFunc.AddAdorn_title(img_title);
        }
    });

// 付费页--取消按钮--取消付费页
    $(".pay_cancel").click(function(){
        $(".bg_confirm,.show_confirm").css({display:"none"});
    });


//“×”----×关闭当前页
    $(".close_window").click(function(){
        $("#show,#bg").css({display:"none"});
        $("#corpus_content,#corpus_bg").css({display:"none"});
    });

    $(".prompt_close").click(function(){
        $(".bg_prompt,.prompt_infor").css({display:"none"});
    });


    //原有图片的数量
    var real_num = $(".srmy_main img").length;

    //点击之后要添加的数量;
    var change_num = $(".module_goods").length;

    //原有"图片名称"的条数
    var title_num = $(".house li").length;

    //图片的src地址
    var arr = new Array();
    $(".module_goods").each(function() {
        arr.push($(this).attr("src"));
    });

    //图片对应的标题名
    var arr_title=new Array();
    $(".module_title").each(function() {
        arr_title.push($(this).text());
    });

    //提示框上的确定
    //确认购买
    $(".accomplish").click(function () {

        $("#show,#bg,.bg_prompt,.prompt_infor,.bg_confirm,.show_confirm").css({display:"none"});
        $(".footer a input").css({backgroundColor:"#bbbbbb"});

        //保存按钮
        $(".footer_save_btn").css({display:"inline"});

        //添加图片到页面
        for (var i = 1; i <= change_num; i++) {
            html_img = '<img id=div' + (real_num + i) + " " + 'data-name=img' + (real_num + i)
            + ' data-id=url' + (real_num + i) + " " + 'src=' + arr[i - 1] + " " + 'name=1 class=srmy_FixedAdorn style=z-index:0>';

            //添加图片对应的名称
            img_title='<li'+" "+'id=list'+ (real_num + i) +" " +'class=cc draggable=true '+
            'data-itemidx=' + (real_num - 1 + i) + '>' + arr_title[i-1]+ '<span class=buy_user>' + '匿名者祭祀' + '<img src=images/srmy_materisl_big.png class=srmy_sucai_big><img src=images/srmy_materisl_reduce.png class=srmy_sucai_reduce><img src=images/srmy_materisl_hidden.png class=srmy_sucai_hidden><img src=images/srmy_materisl_delete.png class=srmy_sucai_delete></span>'+'</li>';


            SiteFunc.AddAdorn(html_img);

            SiteFunc.AddAdorn_title(img_title);


        }

    });


    //点击“墓园布置”、“选择图层”的时候，页面上的“保存”按钮显示
    $(".sacrifice img,.sacrifice div,.srmy_fix_up li").click(function(){
        $(".footer a input").css({backgroundColor:"#bbbbbb"});
        $(".footer_save_btn").css({display:"inline"});
    });


//    点击整个页面上的保存按钮之后的效果
    $(".footer_save_btn").click(function(){
        //“保存”按钮隐藏
        $(this).css({display:"none"});
        //功能按钮显示
        $(".footer a input").css({backgroundColor:"#5eb7f9"});
    });


});



//墓园核心部分
var $thisImg;
//拖拽
$.extend($.fn, {
    //拖拽 支持pc and mobile 暂时没怎么测试 用 chrome测下来没什么问题


    Drag: function (opt) {

        //事件类型
        var _EventType = {
                Mousedown: ("ontouchend" in document) ? "touchstart" : "mousedown",
                Mousemove: ("ontouchend" in document) ? "touchmove" : "mousemove",
                Mouseup: ("ontouchend" in document) ? "touchend" : "mouseup"
            },
        //拖动配置

            _opt = $.extend({
                //拖动元素的父亲
                dragParent: $("body"),
                //开始拖动
                onStart: function () { },
                //拖动过程中
                onMove: function () { },
                //拖动结束
                onEnd: function () { }
            }, opt),
        //当前拖拽的对象
            _dragObj = null,

        //元素拖动时候的位置
            _initPoint = {},

        //开始拖动手指或者鼠标的位置
            _startPoint = {};



        function _getMousePoint(ev) {
            //获取当前鼠标或者手指的点

            ev = ("ontouchend" in document) ? event.touches[0] : ev;
            return { 'x': ev.pageX, 'y': ev.pageY};
        }

        $(this).off(_EventType.Mousedown).on(_EventType.Mousedown, _dragStartHandle);
        //拖动开始事件句柄
        function _dragStartHandle(e) {

            e.preventDefault();
            e.stopPropagation();


            //记录当前拖拽的对象
            _dragObj = $(this);

            //起始点
            _startPoint = _getMousePoint(e);
            //初始点
            _initPoint = _dragObj.position();

            //开始拖动的事件
            _opt.onStart.call(_dragObj, _startPoint);

            //绑定移动事件
            $(_opt.dragParent).off(_EventType.Mousemove).on(_EventType.Mousemove, _dragMoveHandle);
            e=event||window.event;


            function _dragMoveHandle(e) {

                var iTop = parseInt($("#srmy_main").css("height"))-parseInt(_dragObj.css("height"));
                var iLeft = parseInt($("#srmy_main").css("width"))-parseInt(_dragObj.css("width"));
                //this.limit && (iTop < 0 && (iTop = 0), iLeft < 0 && (iLeft = 0), iTop > this.maxTop && (iTop = this.maxTop), iLeft > this.maxLeft && (iLeft = this.maxLeft));


                //移动事件
                e.preventDefault();
                e.stopPropagation();

                var _movePoint = _getMousePoint(e);
                //if(parseInt(_dragObj.css("left"))<0){
                //    _dragObj.css({left:"0"});
                //}
                //if(parseInt(_dragObj.css("left"))>iLeft){
                //    _dragObj.css({left:"iLeft"});
                //}
                //if(parseInt(_dragObj.css("top"))<0){
                //    _dragObj.css({top:"0"});
                //}
                //if(parseInt(_dragObj.css("top"))>iTop){
                //    _dragObj.css({top:"iTop"});
                //}
                //
                //if(  parseInt(_dragObj.css("left"))>=0  &&  parseInt(_dragObj.css("left"))<=iLeft  &&  parseInt(_dragObj.css("top"))>=0  && parseInt(_dragObj.css("top"))<=iTop ){
                    _opt.onMove.call(_dragObj.css({left:(_initPoint.left + _movePoint.x - _startPoint.x) + "px",top:(_initPoint.top + _movePoint.y - _startPoint.y) + "px"}), _movePoint);
                    return false;
                //}



            }

            $("body").off(_EventType.Mouseup).on(_EventType.Mouseup, _dragEndHandle);


            function _dragEndHandle(e) {
                $(window).off(_EventType.Mouseup);
                $(_opt.dragParent).off(_EventType.Mousemove);
                _initPoint = { left: 0, top: 0 };
                _opt.onEnd.call(_dragObj);
            }
        }

    },

    //设置css3属性
    SetCss3: function (name, value) {
        var _self = this;
        if (typeof arguments[0] == "object" && arguments.length == 1) {
            $.each(arguments[0], function (key,item) {
                _setCss3($(_self)[0], key, item);
            });
        }
        else {
            _setCss3($(_self)[0], name, value);
        }

        function _setCss3(obj,name, value) {
            obj.style['Webkit' + name.charAt(0).toUpperCase() + name.substring(1)] = value;
            obj.style['Moz' + name.charAt(0).toUpperCase() + name.substring(1)] = value;
            obj.style['ms' + name.charAt(0).toUpperCase() + name.substring(1)] = value;
            obj.style['O' + name.charAt(0).toUpperCase() + name.substring(1)] = value;
            obj.style[name] = value;
        }

    },


    setOptions : function (options)
    {
        this.options =
        {
            handle:		this.drag, //事件对象
            limit:			true, //锁定范围

            maxContainer:	document.getElementById('srmy_main'),//指定限制容器
            onStart:		function () {}, //开始时回调函数
            onMove:			function () {}, //拖拽时回调函数
            onStop:			function () {}  //停止时回调函数
        };
        for (var p in options) this.options[p] = options[p]
    }

});
//本地存储操作
var LocalStorage = {
    Set: function (key,val) {
        localStorage && localStorage.setItem(key, JSON.stringify(val));
    },
    Get: function (key) {
        return !!localStorage ? ((!!localStorage.getItem(key))?localStorage.getItem(key):"") : "";
    }
};

var SiteFunc = {
    //本地缓存数据的key
    LocalCacheDataKey: "SrmyMainKey",
    LocalCacheTitleDataKey:"SrmyTitleKey",

    //当前装饰
    CurAdorn: null,
    CurTitleAdorn:null,

    //添加的装饰集合
    AdornList:[],
    AdornTitleList:[],


    //初始化墓园
    InitSrmyMain: function () {

        //获取本地添加的那些墓园装饰
        var _data = LocalStorage.Get(this.LocalCacheDataKey);
        var _dataTitle = LocalStorage.Get(this.LocalCacheTitleDataKey);

        //是否有“墓园图片”缓存数据
        if (_data) {
            _data = $.parseJSON(_data);
            //console.log(_data.length);
            for (var _i = 0; _i < _data.length; _i++) {
                $(".srmy_main").append($(_data[_i]));
            }
            this.AdornList = _data;
        }

        //是否有“图片名称”的缓存数据
        if (_dataTitle) {
            _dataTitle = $.parseJSON(_dataTitle);
            //console.log(_dataTitle.length);
            for (var _i = 0; _i < _dataTitle.length; _i++) {
                $(".fix_up").append($(_dataTitle[_i]));
            }
            this.AdornTitleList = _dataTitle;
        }

        //先初始化装饰
        $(".srmy_main .srmy_FixedAdorn").each(function () {
            var _adornProp = LocalStorage.Get($(this).attr("id"));

            if (_adornProp) {
                _adornProp = $.parseJSON(_adornProp.toString());
                $(this).css({  position: "absolute",left: _adornProp.x, top: _adornProp.y ,"z-index":_adornProp.zIndex,width:_adornProp.width,height:_adornProp.height})
                    //.SetCss3({ "transform": "scale(" + _adornProp.scale + ")","transform-origin":"0 0" }
                ;
                $(this).attr("imgwidth",parseInt(_adornProp.width));
                $(this).attr("imgheight",parseInt(_adornProp.height));
            }
            //w="<input type='hidden' id='hidden_area' value='"+$("#"+_adornProp).css()

        });

        $(".house li").each(function () {
            var _adornPropTitle = LocalStorage.Get($(this).attr("id"));
            if (_adornPropTitle) {
                _adornPropTitle = $.parseJSON(_adornPropTitle.toString());
                $(this).attr("data-itemidx",_adornPropTitle);
            }
        });
    },

    BindDrag: function () {
        //给墓园里面所有的装饰绑定拖拽事件
        var _zIndex =LocalStorage.Get("adornMaxIndex")|| 0;



        $(".srmy_main .srmy_FixedAdorn").Drag({

            onStart: function () {

                var zIndex_id=$(this).attr("id");

                zIndex_id_list=zIndex_id.substring(zIndex_id.lastIndexOf("v")+1);
                var list_id="list"+zIndex_id_list;

                //document.getElementById("SortContaint").scrollTop=document.getElementById(list_id).offsetTop;

                $("#"+list_id).css({backgroundColor:"#f1f8fe",color:"#5abbf4",border:"1px solid #5abbf4"}).siblings().css({border:"1px solid #eeeeee",backgroundColor:"#ffffff",color:"#000000"});

                zIndex_old=document.getElementById(zIndex_id).style.zIndex;
                $(".btn").css({ display: "block" });
                $(this).children('img').css({"border": "1px #000 dotted"});
                $(this).siblings('div').children('img').css({border:"none"});
                $(this).css({ position: "absolute", "zIndex": ++_zIndex });

                $(this).children('div').css({display:"block"});
                $(this).siblings('div').children('div').css({display:"none"});
                $(".foot_cao_zuo,.cao_zuo_btn").css({display:"block",border:"1px solid #5abbf4"});
                $(this).children('div').css({display:"block"});
                $(this).siblings('div').children('div').css({display:"none"});


            },
            onEnd: function () {
                //alert("a")
                //$(this).css({ "border": "none" });
                $(this).css({zIndex:zIndex_old});
                //可以选择在这里去同步存储拖拽状态 也可以在其他时间点 看具体需求
                //LocalStorage.Set($(this).attr("id"), {
                //    x: $(this).position().left,
                //    y: $(this).position().top,
                //    zIndex:$(this).css("z-index"),
                //    scale: $(this).attr("scale"),
                //    width:$(this).width(),
                //    height:$(this).height()
                //});

                w=$(this).css("width");
                h=$(this).css("height");


                //LocalStorage.Set("adornMaxIndex", _zIndex);
            }

        });



        //图片标题drag时，其对应的图片的z-index变为最大
        var arrDemo = new Array();
        $(".srmy_main .srmy_FixedAdorn").each(function(){
            arrDemo.push($(this).css('zIndex'));
        });
        arrDemo.reverse();



        //图片上的放缩按钮
        $(".big").click(function(){

            var _dataPhotoId=$(this).parent('div').parents('div').attr("id");

            var w=$("#"+_dataPhotoId).attr("imgwidth");
            var h=$("#"+_dataPhotoId).attr("imgweight");


            var v1 = $("#"+_dataPhotoId).attr("name");//倍数
            var v1 = Number(v1);

            if (v1 > 2) {
                return;
            }

            $("#"+_dataPhotoId).children('img').css({
                "width":parseInt(parseInt(w)*(v1+0.1)),"height":parseInt(parseInt(h)*(v1+0.1))
            });
            v1 = v1 + 0.1;
            v1 = v1.toFixed(2);

            $("#"+_dataPhotoId).attr("name", v1);

        });
        $(".small").click(function(){
            var _dataPhotoId=$(this).parent('div').parents('div').attr("id");

            var w=$("#"+_dataPhotoId).attr("imgwidth");
            var h=$("#"+_dataPhotoId).attr("imgweight");

            var v1 = $("#"+_dataPhotoId).attr("name");
            v1 = Number(v1);
            var v2 = v1 - 0.1;
            if (v2 < 0.5) {
                return;
            }
            $("#"+_dataPhotoId).children('img').css({
                "width":(parseInt(w)*v2),"height":(parseInt(h)*v2)
            });
            v1 = v1 - 0.1;
            v1 = v1.toFixed(2);
            $("#"+_dataPhotoId).attr("name", v1);

        });

        $(".before").click(function(){
            var _dataPhotoId=$(this).parent('div').parents('div').attr("id");
            imgZIndex=document.getElementById(_dataPhotoId).style.zIndex;
            imgZIndex=parseInt(imgZIndex)+1;
            $("#"+_dataPhotoId).css("zIndex",imgZIndex);

        });


        $(".after").click(function(){
            var _dataPhotoId=$(this).parent('div').parents('div').attr("id");
            imgZIndex=document.getElementById(_dataPhotoId).style.zIndex;
            imgZIndex=parseInt(imgZIndex)-1;
            $("#"+_dataPhotoId).css("zIndex",imgZIndex);

        });




        //墓园底部的放缩按钮
        $(".srmy_main .srmy_FixedAdorn").click(function () {
            $(this).attr("class", "check");
            $(this).siblings('div').attr("class", " ");
            $thisImg=$(this);
            imgZIndex=document.getElementById($thisImg.attr("id")).style.zIndex
        });
        //底部放大按钮
        $(".foot_big").click(function(){

            var w=$(".check").attr("imgwidth");
            var h=$(".check").attr("imgweight");

            var v1 = $(".check").attr("name");

            var v1 = Number(v1);
            if (v1 > 2) {
                return;
            }
            $(".check").children('img').css({
                //"-webkit-transform-origin": "0 0", "-moz-transform-origin": "0 0", "-ms-transform-origin": "0 0", "-o-transform-origin": "0 0",
                //"-webkit-transform": "scale(" + (v1 + 0.1) + ")", "-moz-transform": "scale(" + (v1 + 0.1) + ")", "-ms-transform": "scale(" + (v1 + 0.1) + ")", "-o-transform": "scale(" + (v1 + 0.1) + ")"
                "width":(parseInt(w)*v2),"height":(parseInt(h)*v2)
            });
            v1 = v1 + 0.1;
            v1 = v1.toFixed(2);
            $(".check").attr("name", v1);
        });

        //底部缩小按钮
        $(".foot_small").click(function(){

            var w=$(".check").attr("imgwidth");
            var h=$(".check").attr("imgweight");

            var v1 = $(".check").attr("name");
            v1 = Number(v1);
            var v2 = v1 - 0.1;
            if (v2 < 0.5) {
                return;
            }
            $(".check").children('img').css({
                //"-webkit-transform-origin": "0 0", "-moz-transform-origin": "0 0", "-ms-transform-origin": "0 0", "-o-transform-origin": "0 0",
                //"-webkit-transform": "scale(" + v2 + ")", "-moz-transform": "scale(" + v2 + ")", "-ms-transform": "scale(" + v2 + ")", "-o-transform": "scale(" + v2 + ")"
                "width":(parseInt(w)*v2),"height":(parseInt(h)*v2)
            });
            v1 = v1 - 0.1;
            v1 = v1.toFixed(2);
            $(".check").attr("name", v1);
        });

        //底部隐藏按钮
        $(".foot_hidden").toggle(function(){
            $(this).css({border:"none"});
            $(this).attr("src","images/srmy_materisl_hidden.png");
            $thisImg.css({display:"none"});
        },function(){
            $(this).css({border:"none"});
            $(this).attr("src","images/srmy_materisl_show.png");
            $thisImg.css({display:"inline"});
        });


        //底部删除按钮
        $(".foot_delete").click(function(){
            $(this).css({border:"none"});
            //获取对应图片
            var img_id=$thisImg.attr("id");
            var _datalistId=img_id.substring(img_id.lastIndexOf("v")+1);
            var _dataPhotoId="list"+_datalistId;

            //对图片进行删除
            document.getElementById("SortContaint").removeChild(document.getElementById(_dataPhotoId));
            document.getElementById("srmy_main").removeChild(document.getElementById(img_id));
        });

        //提前
        $(".buzhi_before").click(function(){

            imgZIndex=parseInt(imgZIndex)+1;
            $thisImg.css("zIndex",imgZIndex);
            LocalStorage.Set($thisImg.attr("id"), {
                x: $thisImg.position().left,
                y: $thisImg.position().top,
                zIndex:imgZIndex
            });
        });


        //置后
        $(".buzhi_after").click(function(){
            imgZIndex=parseInt(imgZIndex)-1;
            $thisImg.css("zIndex",imgZIndex);
            LocalStorage.Set($thisImg.attr("id"), {
                x: $thisImg.position().left,
                y: $thisImg.position().top,
                zIndex:imgZIndex
            });
        });




        //右侧放缩按钮
        //点击放大按钮，放大元素
        $(".fix_up li span img.srmy_sucai_big").click(function(){
            Object.getPrototypeOf=false;
            $(this).css({border:"none"});
            var img_eq=$(this).parent().parent().attr("id");
            var _datalistId=img_eq.substring(img_eq.lastIndexOf("t")+1);

            //找寻名称对应的图片的id
            var _dataPhotoId=("div"+_datalistId);
            var v1 = $("#"+_dataPhotoId).attr("name");//倍数

            var w=$("#"+_dataPhotoId).attr("imgwidth");
            var h=$("#"+_dataPhotoId).attr("imgweight");

            var v1 = Number(v1);
            if (v1 > 2) {
                return;
            }

            $("#"+_dataPhotoId).children('img').css({
                //"-webkit-transform-origin": "0 0", "-moz-transform-origin": "0 0", "-ms-transform-origin": "0 0", "-o-transform-origin": "0 0",
                //"-webkit-transform": "scale(" + (v1 + 0.1) + ")", "-moz-transform": "scale(" + (v1 + 0.1) + ")", "-ms-transform": "scale(" + (v1 + 0.1) + ")", "-o-transform": "scale(" + (v1 + 0.1) + ")"
                "width":(parseInt(w)*v2),"height":(parseInt(h)*v2)
            });

            v1 = v1 + 0.1;
            v1 = v1.toFixed(2);

            $("#"+_dataPhotoId).attr("name", v1);

            LocalStorage.Set(_dataPhotoId, {
                x: parseInt(document.getElementById(_dataPhotoId).style.left),
                y:  parseInt(document.getElementById(_dataPhotoId).style.top),
                //zIndex:(parseInt(_dataSibImgId)+1),
                //scale:$(".srmy_main img").attr("id",_dataPhotoId).scale,
                width:parseInt(parseInt(document.getElementById(_dataPhotoId).width)*v1),
                height:parseInt(parseInt(document.getElementById(_dataPhotoId).height)*v1)
            });

        });

        //点击缩小按钮，对图片进行缩小
        $(".fix_up li span img.srmy_sucai_reduce").click(function(){
            Object.getPrototypeOf=false;
            $(this).css({border:"none"});
            var img_eq=$(this).parent().parent().attr("id");
            var _datalistId=img_eq.substring(img_eq.lastIndexOf("t")+1);

            //找寻名称对应的图片的id
            var _dataPhotoId=("div"+_datalistId);

            //document.getElementById(_dataPhotoId).setAttribute("class","check");

            var v1 = $("#"+_dataPhotoId).attr("name");

            var w=$("#"+_dataPhotoId).attr("imgwidth");
            var h=$("#"+_dataPhotoId).attr("imgweight");

            v1 = Number(v1);
            var v2 = v1 - 0.1;
            if (v2 < 0.5) {
                return;
            }
            $("#"+_dataPhotoId).children('img').css({
                //"-webkit-transform-origin": "0 0", "-moz-transform-origin": "0 0", "-ms-transform-origin": "0 0", "-o-transform-origin": "0 0",
                //"-webkit-transform": "scale(" + v2 + ")", "-moz-transform": "scale(" + v2 + ")", "-ms-transform": "scale(" + v2 + ")", "-o-transform": "scale(" + v2 + ")"
                "width":(parseInt(w)*v2),"height":(parseInt(h)*v2)
            });
            v1 = v1 - 0.1;
            v1 = v1.toFixed(2);
            $("#"+_dataPhotoId).attr("name", v1);
            //var _scale=LocalStorage.Set(document.getElementById(_dataPhotoId).getAttribute("data-name"), { "scale": v1 });
            LocalStorage.Set(_dataPhotoId, {
                x: parseInt(document.getElementById(_dataPhotoId).style.left),
                y:  parseInt(document.getElementById(_dataPhotoId).style.top),
                //zIndex:(parseInt(_dataSibImgId)+1),
                //scale:_scale,
                width:parseInt(parseInt(document.getElementById(_dataPhotoId).width)*v1),
                height:parseInt(parseInt(document.getElementById(_dataPhotoId).height)*v1)
            });
        });

        //点击隐藏，对图片进行隐藏
        $(".fix_up li span img.srmy_sucai_hidden").toggle(function(){
            $(this).css({border:"none"});
            $(this).attr("src","images/srmy_materisl_hidden.png");

            //获取对应图片
            var img_eq=$(this).parent().parent().attr("id");
            var _datalistId=img_eq.substring(img_eq.lastIndexOf("t")+1);
            var _dataPhotoId="div"+_datalistId;

            //对图片进行隐藏
            document.getElementById(_dataPhotoId).style.display="none";


        },function(){
            $(this).css({border:"none"});
            $(this).attr("src","images/srmy_materisl_show.png");

            //获取对应图片
            var img_eq=$(this).parent().parent().attr("id");
            var _datalistId=img_eq.substring(img_eq.lastIndexOf("t")+1);
            var _dataPhotoId="div"+_datalistId;

            //对图片进行显示
            document.getElementById(_dataPhotoId).style.display="inline";
        });

        //点击删除，对图片进行删除
        $(".fix_up li span img.srmy_sucai_delete").click(function(){

            $(this).css({border:"none"});
            //获取对应图片
            var img_eq=$(this).parent().parent().attr("id");
            var _datalistId=img_eq.substring(img_eq.lastIndexOf("t")+1);

            var _dataPhotoId="div"+_datalistId;
            //对图片进行删除
            document.getElementById("SortContaint").removeChild(document.getElementById(img_eq));
            document.getElementById("srmy_main").removeChild(document.getElementById(_dataPhotoId));
        });

        $(".fix_up li").dblclick(function(){

            //获取当前元素的id
            var _dataId=$(this).attr("id");
            $(this).css({border:"1px solid #5abbf4",color:"#5abbf4"}).siblings().css({border:"1px solid #eeeeee",color:"black"});
            $(this).children('span').css({border:"none",color:"#5abbf4"});
            $(this).siblings('li').children('span.buy_user_name').css({color:"black"});

            _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);

            var _dataImgId="div"+_dataId;
            $("#"+_dataImgId).children('img').css({border:"1px solid blue"});
            $("#"+_dataImgId).siblings('div').children('img').css({border:"none"});
            arrDemo[0]=parseInt(arrDemo[0])+1;
            document.getElementById(_dataImgId).style.zIndex=parseInt(parseInt(_zIndex)+parseInt(arrDemo[0]));
        });

        //点击“选择图层”中的列表标签，执行列表点击和拖拽
        var list_id;
        var list_zIndex;
        $(".fix_up").dragsort({

            dragSelectorExclude :function(){

                var _dataId=$(this).attr("id");
                $(this).css({border:"1px solid #5abbf4",color:"#5abbf4"}).siblings().css({border:"1px solid #eeeeee",color:"black"});

                list_id =_dataId;

                var _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);
                //_dataId=_dataId.slice("t");
                //alert(_dataId);
                var _dataImgId="div"+_dataId;
                $("#"+_dataImgId).children('div').css({display:"block"});
                $("#"+_dataImgId).siblings('div').children('div').css({display:"none"});
                $("#"+_dataImgId).children('img').css({border:"1px solid blue"});
                $("#"+_dataImgId).siblings('div').children('img').css({border:"none"});

                if(list_id==null) {
                    //获取当前元素的id
                    var _dataId=$(this).attr("id");
                    list_id =_dataId;

                    _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);

                    var _dataImgId="div"+_dataId;

                    var _dataOddZindex=document.getElementById(_dataImgId).style.zIndex;
                    list_zIndex =_dataOddZindex;

                    //arrDemo[0]=parseInt(arrDemo[0])+1;
                    //document.getElementById(_dataImgId).style.zIndex=(parseInt(_zIndex)+(arrDemo[0]));

                }
                else if(($(this).attr("id"))!=list_id){

                    //$(this).css({border:"1px solid #5abbf4",color:"#5abbf4"}).siblings().css({border:"1px solid #eeeeee",color:"black"});
                    $("#"+('div'+(list_id.substring(list_id.lastIndexOf("t")+1)))).css("z-index",list_zIndex);

                    //获取当前元素的id
                    var  _dataId=$(this).attr("id");
                    list_id=_dataId;

                    var _dataOddZindex=document.getElementById(_dataImgId).style.zIndex;
                    list_zIndex=_dataOddZindex;

                    _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);

                    //找寻名称对应的图片的id
                    _dataImgId=("div"+_dataId);

                }

            },

            dragEnd: function() {

                ////当前名称对应的图片的相关信息
                //获取当前元素的id
                var _dataId=$(this).attr("id");
                $("#"+_dataId).css({border:"1px solid #5abbf4",color:"#5abbf4"}).siblings().css({border:"1px solid #eeeeee",color:"black"});

                //列表中"图片名称"的id的数字（list4 === 4）
                _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);


                //找寻名称对应的图片的id
                var _dataImgId="div"+_dataId;

                //设置相应图片的zIndex值
                var _data_itemidx=$(this).attr("data-itemidx");
                var li_length=parseInt($(".fix_up li").length)-1;

                if(_data_itemidx==0){
                    //获取第二个标签的ID
                    var before_list=$(".fix_up li").eq(1).attr("id");
                    before_list=before_list.substring(before_list.lastIndexOf('t')+1);
                    //第二个标签对应的图片ID
                    var before_img="div"+before_list;
                    //第二个标签对应的图片的zIndex
                    var before_zIndex=document.getElementById(before_img).style.zIndex;
                    //当前标签对应的图片的zIndex为相邻图片层减1
                    document.getElementById(_dataImgId).style.zIndex=parseInt(parseInt(before_zIndex)-1);
                }else{
                    //当前图片名称的相邻上一个的名称的相关信息
                    //获取当前拖动元素的data-itemidx值
                    var _datazIndex=$(this).attr("data-itemidx");

                    //获取当前拖动元素的上一个相邻的元素的id值
                    var dataSiblingId=$(".fix_up li").eq(parseInt(_datazIndex)-1).attr("id");

                    //列表中"上一个图片名称"的id的数字（list3 === 3）
                    _dataSiblingId=dataSiblingId.substring(dataSiblingId.lastIndexOf("t")+1);

                    //寻"上一个图片名称"对应的图片的id
                    var _dataSibImgId="div"+_dataSiblingId;

                    //获取"上一个图片名称"的zIndex值
                    _dataSibImgId=document.getElementById(_dataSibImgId).style.zIndex;


                    //当前名称对应的图片的相关信息
                    //获取当前元素的id
                    var _dataId=$(this).attr("id");


                    //列表中"图片名称"的id的数字（list4 === 4）
                    _dataId=_dataId.substring(_dataId.lastIndexOf("t")+1);

                    //找寻名称对应的图片的id
                    var _dataImgId="div"+_dataId;

                    document.getElementById(_dataImgId).style.zIndex=(parseInt(_dataSibImgId)+1);
                    for(var i=(parseInt(_data_itemidx)+1);i<(li_length+1);i++){
                        var list_n_siblings=$(".fix_up li").eq(i).attr("id");
                        var list_n_siblings_img=list_n_siblings.substring(list_n_siblings.lastIndexOf('t')+1);
                        var siblings_img_id="div"+list_n_siblings_img;
                        var siblings_zIndex=document.getElementById(siblings_img_id).style.zIndex;
                        document.getElementById(siblings_img_id).style.zIndex=parseInt(parseInt(siblings_zIndex)+1);
                    }
                }



                //LocalStorage.Set($(this).attr("id"), ($(this).attr("data-itemidx")));

                //将拖动后的名称对应的图片的层进行存储
                //LocalStorage.Set(_dataImgId, {
                //    x: document.getElementById(_dataImgId).style.left,
                //    y: document.getElementById(_dataImgId).style.top,
                //    zIndex:(parseInt(_dataSibImgId)+1),
                //    width:document.getElementById(_dataImgId).width,
                //    height:document.getElementById(_dataImgId).height
                //});
                //
                //LocalStorage.Set($(this).attr("id"), {
                //    dataEq: $(this).attr("data-itemidx")
                //});
            }
        });

    },


    //为“墓园图片”添加装饰
    AddAdorn: function (imageHtml) {

        var _adorn = $(imageHtml);
        if (_adorn.length <= 0) { return;}
        //添加到墓园
        $(".srmy_main").append(_adorn);
        console.log(this.AdornList.length);
        this.AdornList.push(imageHtml);

        LocalStorage.Set(this.LocalCacheDataKey, this.AdornList);

        this.BindDrag();
    },

    //为“图片标题”添加装饰
    AddAdorn_title:function(titleHtml){
        var _adornTitle = $(titleHtml);
        if (_adornTitle.length <= 0) { return;}
        //添加到墓园
        $(".house").append(_adornTitle);
        console.log(this.AdornTitleList.length);
        this.AdornTitleList.push(titleHtml);

        LocalStorage.Set(this.LocalCacheTitleDataKey, this.AdornTitleList);

        this.BindDrag();
    },

    //缩小当前的装饰
    NarrowCurAdorn: function () {

    },
    //放大当前的装饰
    EnlargeCurAdorn: function () {

    }
};


$(function () {

    //初始化墓园
    SiteFunc.InitSrmyMain();

    //绑定墓园装饰的拖拽
    SiteFunc.BindDrag();

    $(".srmy_main .srmy_FixedAdorn").each(function () {

        var   _keyScale = $(this).attr("data-name"),
            _size = LocalStorage.Get(_keyScale);

        if (_size != "") {
            _size = $.parseJSON(_size.toString());
            $(this).css({
                "-webkit-transform-origin": "0 0",
                "-moz-transform-origin": "0 0",
                "-ms-transform-origin": "0 0",
                "-o-transform-origin": "0 0",
                "-webkit-transform": "scale(" + _size.scale + ")",
                "-moz-transform": "scale(" + _size.scale + ")",
                "-ms-transform": "scale(" + _size.scale + ")",
                "-o-transform": "scale(" + _size.scale + ")"
            });
        }
    });

});
