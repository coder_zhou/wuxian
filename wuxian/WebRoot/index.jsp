<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title></title>
<link href="/resources/index/css/bootstrap.min.css" rel="stylesheet">
<link href="/resources/index/css/index.css" rel="stylesheet">
<script src="/resources/index/js/jquery-1.7.2.min.js"></script>

<script src="/resources/index/js/jquery.min.js"></script>
<script src="/resources/index/js/bootstrap.min.js"></script>
<!--<script src="/resources/index/js/jquery-1.4.4.min.js"></script>-->
<!--<script src="/resources/index/js/slides.min.jquery.js"></script>-->

<script src="/resources/index/js/slide.js"></script>
<script src="/resources/index/js/index.js"></script>

<!--[if lt IE 9]>
    <script src="/resources/index/js/html5shiv.min.js"></script>
    <script src="/resources/index/js/respond.min.js"></script>
<![endif]-->

</head>
<body>
	<div class="main">


		<!--  引入头部  -->
		<jsp:include page="/jsp/common/head.jsp"></jsp:include>


		<div class="ca1_slide">
			<div id="slider-wrapper">
				<div id="slider-bg">
					<div id="slider-photos">
						<div id="slides">
							<div class="slides_container">
								<div class="slide">
									<img src="/resources/index/images/banner_index.png">
									<div class="carousel-caption">
										<div class="sy_banner_biaoyu">
											<img src="/resources/index/images/banner_biaoyu.png">
										</div>
										<div class="sy_banner_ewm">
											<div>扫描二维码下载手机客户端</div>
											<ul>
												<li><img src="/resources/index/images/banner_ios.png"></li>
												<li><input type="button" value="i o s 下载"></li>
											</ul>
											<ul>
												<li><img src="/resources/index/images/banner_android.png"></li>
												<li><input type="button" value="Android下载"></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="/resources/index/images/banner_index.png">
								</div>
								<div class="slide">
									<img src="/resources/index/images/banner_index.png">
								</div>
								<div class="slide">
									<img src="/resources/index/images/banner_index.png">
								</div>
								<div class="slide">
									<img src="/resources/index/images/banner_index.png">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="content">
			<!-- 大型祭祀祈福活动即将开启-->
			<div class="sy_action">
				<div class="sy_action_title">在此与你相守，让我重温幸福的感受</div>
				<div class="action_title_small">
					全球祭祀祈福网<span><a href="#">QJQW.com</a></span>让生命没有终点
				</div>


				 <div class="cont_kind">
                <div class="kind_srmy">
                    <div class="kind_img"><img src="/resources/index/images/sy_srmy.png"></div>
                    <div class="kind_name">私人墓园</div>
                    <div class="kind_jian_tou"><img src="/resources/index/images/sy_mk_bottom.png"></div>
                </div>

                <div class="kind_zczp">
                    <div class="kind_img"><img src="/resources/index/images/sy_zczp.png"></div>
                    <div class="kind_name">宗祠族谱</div>
                    <div class="kind_jian_tou"><img src="/resources/index/images/sy_mk_top.png"></div>
                </div>

                <div class="kind_bflf">
                    <div class="kind_img"><img src="/resources/index/images/sy_bflf.png"></div>
                    <div class="kind_name">拜佛礼佛</div>
                    <div class="kind_jian_tou"><img src="/resources/index/images/sy_mk_top.png"></div>
                </div>

                <div class="kind_xys">
                    <div class="kind_img"><img src="/resources/index/images/sy_xys.png"></div>
                    <div class="kind_name">许愿树</div>
                    <div class="kind_jian_tou"><img src="/resources/index/images/sy_mk_top.png"></div>
                </div>

            </div>
            <div class="cont_kind_content">


                <!-- 私人墓园 -->
                <div class="container_infor">

                    <!--香火排行-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;香火排行</div>
                            <div class="pai_hang_shun_xu">
                                <div class="ph_click_active">周</div>
                                <div>月</div>
                            </div>
                        </div>
                        <!-- 每一块内容-->
                    
 		<c:choose>
 				<c:when test="${ not empty qqph}">
 				<c:forEach items="${qqph }" var="var" varStatus="vs">
 				    <ul>
                            <li class="ph_cont_1"><img src="/resources/index/images/user_infor_dengji.png"></li>
                            <li class="ph_cont_2"><a href=" /webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.cemetery_id }">${var.cemetery_name }</a></li>
                            <li class="ph_cont_3"><img src="/resources/index/images/srmy_infor_ico_04.png"></li>
                            <li class="ph_cont_4">${var.cemetery_family/10000 }w</li>
                        </ul>
 				</c:forEach>
 				</c:when>
 				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 最新祭祀-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;最新祭祀</div>
                        </div>
                        <!-- 每一块内容-->
                   
                   		<c:choose>
                   				<c:when test="${ not empty zxjs}">
                   				<c:forEach items="${zxjs }" var="var" varStatus="vs">
                   				     <ul>
                            <li class="js_cont_1"><img src="/resources/index/images/create_tree_goods.png"></li>
                            <li class="js_cont_2"><a href=" /webusersettiongs/queryToUserSettiongs?user_myid=${var.sacrifice_user_id }">${var.sacrifice_nick_name }</a></li>
                            <li class="js_cont_3"><a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.cemetery_id }">${var.sacrifice_materials }</a></li>
                        </ul>
                   				</c:forEach>
                   				</c:when>
                   				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 优秀墓园-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;优秀墓园</div>
                        </div>
                        <!-- 每一块内容-->
            
                      		<c:choose>
                      				<c:when test="${ not empty djph}">
                      				<c:forEach items="${ djph}" var="var" varStatus="vs">
                      				            <ul>
                            <li class="yx_cont_1"><a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.cemetery_id }">${var.cemetery_name }</a></li>
                            <li class="yx_cont_2"><a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.cemetery_id }">园号：${var.cemetery_number }</a></li>
                        </ul>
                      				</c:forEach>
                      				</c:when>
                      				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 新建墓园-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;新建墓园</div>
                        </div>
                        <!-- 每一块内容-->
                        
              		<c:choose>
              				<c:when test="${ not empty xjmy}">
              				<c:forEach items="${xjmy }" var="var" varStatus="vs">
              				<ul>
                            <li class="new_cont_1"><a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.cemetery_id }">${var.cemetery_name }</a></li>
                            <li class="new_cont_2">
 								<c:set target="${myDate}" property="time" value="${var.cemetery_add_time*1000}"/>
 								<fmt:formatDate pattern="yy-MM-dd" value="${myDate}" type="both"/> 
                        </li>
                        </ul>
              				</c:forEach>
              				</c:when>
              				</c:choose>
                    </div>

                </div>

                <!-- 宗祠族谱 -->
                <div class="container_infor  content_hidden">

                    <!--兴旺排行-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;兴旺排行</div>
                        </div>
                        <!-- 每一块内容-->
              
                        		<c:choose>
                        				<c:when test="${ not empty 兴旺排行}">
                        				<c:forEach items="${兴旺排行 }" var="var" varStatus="vs">
                        				          <ul>
                            <li class="xwph_cont_1"><img src="/resources/images/zongci/xwph_img_1.png"></li>
                            <li class="xwph_cont_2"><a href=" /webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var._id }">${var.ancestral_hall_surname }氏宗祠</a></li>
                            <li class="xwph_cont_3">族人：${var.ancestral_hall_people }人</li>
                        </ul>
                        				</c:forEach>
                        				</c:when>
                        				</c:choose>
                        
                    </div>
                    <div class="line"></div>

                    <!-- 最新祭祀-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;最新祭祀</div>
                        </div>
                        <!-- 每一块内容-->
            
          
                    		<c:choose>
                    				<c:when test="${ not empty 宗祠祭祀}">
                    				<c:forEach items="${宗祠祭祀 }" var="var" varStatus="vs">
                    				            <ul>
                            <li class="js_cont_1"><img src="/resources/index/images/create_tree_goods.png"></li>
                            <li class="js_cont_2"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.sacrifice_user_id }">${var.sacrifice_nick_name }</a></li>
                            <li class="js_cont_3"><a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_hall_id }">${var.sacrifice_materials }</a></li>
                        </ul>
                    				</c:forEach>
                    				</c:when>
                    				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 修缮日志-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;修缮日志</div>
                        </div>
                        <!-- 每一块内容-->
                       
                          		<c:choose>
                          				<c:when test="${ not empty 修缮日志}">
                          				<c:forEach items="${修缮日志 }"  var="var" varStatus="vs">
                          				 <ul>
                            <li class="xsrz_cont_1"><a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_hall_id }">${var.repair_surname }氏宗祠</a></li>
                            <li class="xsrz_cont_2"><a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_hall_id }">${var.repair_materials }</a></li>
                            <li class="xsrz_cont_3">
                            		${var.repair_time_str}
                            </li>
                        </ul>
                          				</c:forEach>
                          				</c:when>
                          				</c:choose>
                          				
                    </div>
                    <div class="line"></div>

                    <!-- 新录族人-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;新录族人</div>
                        </div>
                        <!-- 每一块内容-->
                  
                      		<c:choose>
                      				<c:when test="${ not empty 新录族人}">
                      				<c:forEach items="${新录族人 }" var="var" varStatus="vs">
                      				      <ul>
                            <li class="new_zr_cont_1"><a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_hall_myid }">${var.genealogy_name }</a></li>
                            <li class="new_zr_cont_2">加入<a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_hall_myid }">${var.am.ancestral_hall_surname }氏宗祠</a></li>
                        </ul>
                      				</c:forEach>
                      				</c:when>
                      				</c:choose>
                    </div>

                </div>


                <!-- 拜佛礼佛 -->
                <div class="container_infor  content_hidden">

                    <!--最新礼佛-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;最新礼佛</div>
                        </div>
                        <!-- 每一块内容-->
                     
                     		<c:choose>
                     				<c:when test="${ not empty 最新礼佛}">
                     				<c:forEach items="${最新礼佛 }" var="var" varStatus="vs">
                     				   <c:if test="${vs.index<4}">
                     				
                     				   <ul>
                            <li class="zxlf_cont_1"><img src="/resources/index/images/create_tree_goods.png"></li>
                            <li class="zxlf_cont_2"><a href=" /webLifo/queryFoPubWorshipForBz?fo_id=${var.fo_id }">${var.fo_name }</a></li>
                            <li class="zxlf_cont_3"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }">${var.user_name }</a></li>
                            <li class="zxlf_cont_4"><a href="/webLifo/queryFoPubWorshipForBz?fo_id=${var.fo_id }">${var.materials_name }</a></li>
                        </ul>
                     				   </c:if>
                     				</c:forEach>
                     				</c:when>
                     				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 许愿还愿-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;许愿还愿</div>
                        </div>
                        <!-- 每一块内容-->
                  
                          		<c:choose>
                          				<c:when test="${ not empty 许愿还愿}">
                          				<c:forEach items="${ 许愿还愿}" var="var" varStatus="vs">
                          				      <c:if test="${vs.index<4}">
                          				   
                          				      <ul>
                            <li class="xyhy_cont_1"><a href="/webLifo/queryFoPubWorshipForBz?fo_id=${var.fo_id }">[拜]</a></li>
                            <li class="xyhy_cont_2"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }">${var.user_name }</a></li>
                            <li class="xyhy_cont_3">    
                             <c:if test="${var.wish_type==2}">
                    <img src="/resources/images/huan_wish.png">
                    </c:if>
                     <c:if test="${var.wish_type==1}">
                    <img src="/resources/images/no_huan_wish.png">
                    </c:if></li>
                            <li class="xyhy_cont_4">${var.wish_come_true_price }福币</li>
                        </ul>
                          				   </c:if>
                          				</c:forEach>
                          				</c:when>
                          				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 最新供奉-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;最新供奉</div>
                        </div>
                        <!-- 每一块内容-->
                 
                     		<c:choose>
                     				<c:when test="${ not empty 最新供奉}">
                     				<c:forEach items="${最新供奉 }" var="var" varStatus="vs">
                     				       <c:if test="${vs.index<4}">
                     				    
                     				       <ul>
                            <li class="zxgf_cont_1"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.personal_worship_user_id }">${var.user_name }</a></li>
                            <li class="zxgf_cont_2"><a href="/webLifo/queryFoPersonalWorshipForBz?personal_worship_id=${var.personal_worship_id }">${var.personal_worship_fo_name }</a></li>
                            <li class="zxgf_cont_3">	<c:set target="${myDate}" property="time" value="${var.personal_worship_add_time*1000}"/>
                                    <fmt:formatDate pattern="yy-MM-dd" value="${myDate}" type="both"/> </li>
                        </ul>
                     				   </c:if>
                     				</c:forEach>
                     				</c:when>
                     				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 功德簿-->
                    <div class="list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;功德簿</div>
                        </div>
                        <!-- 每一块内容-->
                
                        		<c:choose>
                        				<c:when test="${ not empty 功德簿}">
                        				<c:forEach items="${功德簿 }" var="var" varStatus="vs">
                        				        <c:if test="${vs.index<4}">
                        				        <ul>
                            <li class="gdb_cont_1">
                            <a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }">${var.user_name }</a>
                            </li>
                            <li class="gdb_cont_2"><img src="/resources/index/images/srmy_infor_ico_04.png"></li>
                            <li class="gdb_cont_3">${var.donation_price }</li>
                            <li class="gdb_cont_4">
                            <c:choose>  
                                <c:when test="${fn:length(var.donation_info) > 4}">  
                                <c:out value="${fn:substring(var.donation_info, 0, 4)}..." />  
                             </c:when>  
                           <c:otherwise>  
                            <c:out value="${var.donation_info}" />  
                           </c:otherwise>  
                            </c:choose>
                            </li>
                        </ul>
                        				</c:if>
                        				</c:forEach>
                        				</c:when>
                        				</c:choose>
                    </div>

                </div>

                <!-- 许愿树 -->
                <div class="wish_container_infor  content_hidden">

                    <!--最新愿望-->
                    <div class="xys_list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;最新愿望</div>
                        </div>
                        <!-- 每一块内容-->
                 
                     		<c:choose>
                     				<c:when test="${ not empty 最新愿望}">
                     				<c:forEach items="${最新愿望 }" var="var" varStatus="vs">
                     				       <ul  <c:if test="${vs.index==0}">class="zxyw_ul"  </c:if>  >
                            <li class="zxyw_cont_1"><img src="/resources/index/images/create_tree_goods.png"></li>
                            <li class="zxyw_cont_2"><a href="/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${var.wish_tree_id }">
                            <c:choose>  
         <c:when test="${fn:length(var.wish_title) > 18}">  
             <c:out value="${fn:substring(var.wish_title, 0, 20)}..." />  
         </c:when>  
        <c:otherwise>  
           <c:out value="${var.wish_title}" />  
         </c:otherwise>  
     </c:choose>
                            <span>[${var.wish_type == 1 ? '公' : '私'}]</span><img src="/resources/index/images/my_wish_open.png"></a></li>
                            <li class="zxyw_cont_3"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }">${var.user_nick_name }</a></li>
                        </ul>
                     				</c:forEach>
                     				</c:when>
                     				</c:choose>
                    </div>
                    <div class="line"></div>

                    <!-- 私人许愿树-->
                    <div class="xys_list_first">
                        <!-- 每一块标题-->
                        <div class="list_title">
                            <div class="title_name">&nbsp;&nbsp;私人许愿树</div>
                        </div>
                  
                        		<c:choose>
                        				<c:when test="${ not empty 私人许愿树}">
                        				<c:forEach items="${私人许愿树 }" var="var" varStatus="vs">
                        				      <!-- 每一块内容-->
                        <ul   <c:if test="${vs.index==0}">class="zxyw_ul"  </c:if>  >
                            <li class="xys_cont_1"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }">${var.user_nick_name }</a></li>
                            <li class="xys_cont_2"><a href="/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${var.private_wish_tree_id }">${var.private_wish_tree_name}</a></li>
                            <li class="xys_cont_3">${var.private_wish_tree_add_time_str}</li>
                        </ul>
                        				
                        				</c:forEach>
                        				</c:when>
                        				</c:choose>
                        
                    </div>
                </div>

            </div>
								
			</div>

			<!-- 文章祈福-->
			<div class="sy_wzqf sy_action">
				<div class="sy_action_title">
					<img src="/resources/index/images/sy_wzqf_book.png">&nbsp;文章资讯
				</div>
				<div class="sy_wzqf_small action_title_small">
					<a href="/textForIndex">追思留言</a>
				</div>
				<div class="row wzqf_row01">
							<c:choose>
									<c:when test="${ not empty wjzs}">
									<c:forEach items="${wjzs }" var="var" varStatus="vs">
									<c:if test="${vs.index==0 or vs.index==2}">
										<div class="col-lg-4">
						<h3>${var.article_name }</h3>
						<span>
<c:choose>  
         <c:when test="${fn:length(var.article_info) > 40}">  
             <c:out value="${fn:substring(var.article_info, 0, 40)}..." />  
         </c:when>  
        <c:otherwise>  
           <c:out value="${var.article_info}" />  
         </c:otherwise>  
     </c:choose>
						</span>
						<a href="/cemetery/viewArticle?artId=${var._id }" target="_blank">
						<div class="text">
							<div class="imgtext">
								<div>作者：${var.user_name }</div>
								<div>点击次数：${var.article_clicks }次</div>
								<div>拥抱：${var.article_praise }次</div>
							</div>
						</div>
						</a>
					</div>
									</c:if>
									<c:if test="${vs.index==1 or vs.index==3}">
											<div class="col-lg-2">
						<h3>${var.article_name }</h3>
						<span>
<c:choose>  
         <c:when test="${fn:length(var.article_info) > 20}">  
             <c:out value="${fn:substring(var.article_info, 0, 20)}..." />  
         </c:when>  
        <c:otherwise>  
           <c:out value="${var.article_info}" />  
         </c:otherwise>  
     </c:choose>
						</span>
							<a href="/cemetery/viewArticle?artId=${var._id }" target="_blank">
						<div class="text text_zsly">
							<div class="imgtext imgtext_zsly">
								<div>作者：${var.user_name }</div>
								<div>点击次数：${var.article_clicks }次</div>
								<div>拥抱：${var.article_praise }次</div>
							</div>
						</div>
						</a>
					</div>	
									</c:if>
									
									</c:forEach>
									</c:when>
									</c:choose>
					
					
				
				</div>

				<div class="row wzqf_row01 wzqf_row02">
										<c:choose>
									<c:when test="${ not empty wjzs}">
									<c:forEach items="${wjzs }" var="var" varStatus="vs">
									<c:if test="${vs.index==4 or vs.index==6}">
										<div class="col-lg-4">
						<h3>${var.article_name }</h3>
						<span>
<c:choose>  
         <c:when test="${fn:length(var.article_info) > 40}">  
             <c:out value="${fn:substring(var.article_info, 0, 40)}..." />  
         </c:when>  
        <c:otherwise>  
           <c:out value="${var.article_info}" />  
         </c:otherwise>  
     </c:choose>
						</span>
							<a href="/cemetery/viewArticle?artId=${var._id }" target="_blank">
						<div class="text">
							<div class="imgtext">
								<div>作者：${var.user_name }</div>
								<div>点击次数：${var.article_clicks }次</div>
								<div>拥抱：${var.article_praise }次</div>
							</div>
						</div>
						</a>
					</div>
									</c:if>
									<c:if test="${vs.index==5 or vs.index==7}">
											<div class="col-lg-2">
						<h3>${var.article_name }</h3>
						<span>
<c:choose>  
         <c:when test="${fn:length(var.article_info) > 20}">  
             <c:out value="${fn:substring(var.article_info, 0, 20)}..." />  
         </c:when>  
        <c:otherwise>  
           <c:out value="${var.article_info}" />  
         </c:otherwise>  
     </c:choose>
						</span>
							<a href="/cemetery/viewArticle?artId=${var._id }" target="_blank">
						<div class="text text_zsly">
							<div class="imgtext imgtext_zsly">
								<div>作者：${var.user_name }</div>
								<div>点击次数：${var.article_clicks }次</div>
								<div>拥抱：${var.article_praise }次</div>
							</div>
						</div>
						</a>
					</div>	
									</c:if>
									
									</c:forEach>
									</c:when>
									</c:choose>
				</div>
			</div>

			<!-- 特殊纪念-->
			<div class="sy_tsjn sy_action">
				<div class="sy_action_title">特殊纪念·名人事件</div>
				<div class="panel-group" id="accordion">
							<c:choose>
									<c:when test="${ not empty mrsj}">
									<c:forEach items="${mrsj }" var="var" varStatus="vs">
									<div class="panel panel-default">
						<div class="panel-heading" rel="${vs.index }">
							<h4 class="panel-title">
								<a  href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${var.banner_cemetery_id }">
									<h4>${var.banner_name }</h4> <img src="/goods/${var.banner_img }">
								</a>
							</h4>
						</div>
			<!-- 	<div id="collapseOne" class="panel-collapse">
							<div class="photo_jj"></div>
							<div class="panel-body">
								<h2>翁美玲</h2>
								<ul>
									<li>出生日期：1959-05-07</li>
									<li>出生地区：香港</li>
									<li>墓地地址：中国香港</li>
									<li>中国香港已故著名女演员</li>
								</ul>
								<ul class="tsjn_ul_02">
									<li>逝世日期：1985-05-14</li>
									<li>籍贯：安徽省</li>
								</ul>
								<div>
									<input type="button" value="追 思 纪 念">
								</div>
							</div>
							 
						</div>
						-->
					</div>
									</c:forEach>
									</c:when>
									</c:choose>
					
					
					

			

				</div>
			</div>


			<!--友情链接-->
			<div class="footer_top_hr"></div>
			<div class="sy_footer sy_action">
				<div class="footer_title">友情链接</div>
				<div class="footer_center">
					<div class="row">
			             		<c:choose>
			             				<c:when test="${ not empty yqlj}">
			             				<c:forEach items="${yqlj }" var="var" varStatus="vs">
			             							<c:if test="${vs.index<9}">
			             						
			             							<div class="col-lg-1">
						         	<a href="${var.links_url }">${var.links_name }</a>
				                  	</div>
				                  		</c:if>
			             				</c:forEach>
			             				</c:when>
			             				</c:choose>
					</div>

					<div class="row footer_row_02">
						  		<c:choose>
			             				<c:when test="${ not empty yqlj}">
			             				<c:forEach items="${yqlj }" var="var" varStatus="vs">
			             							<c:if test="${vs.index>8}">
			             						
			             							<div class="col-lg-1">
						         	<a href="${var.links_url }">${var.links_name }</a>
				                  	</div>
				                  		</c:if>
			             				</c:forEach>
			             				</c:when>
			             				</c:choose>
					</div>
				</div>
				<div class="row footer_row_03">
					<div class=" col-lg-4">
						<img src="/resources/index/images/sy_yqlj_service.png">
						<div class="footer_foot_tel">
							<div>
								<img src="/resources/index/images/sy_yqlj_tel.png">&nbsp;400-0240-535
							</div>
							<div>
								<img src="/resources/index/images/sy_yqlj_message.png">&nbsp;502772160@qq.com
							</div>

						</div>

					</div>
					<div class=" col-lg-7 footer_foot_mzsm">
						<div>
							<span><a href="">关于我们</a></span> <span><a href="">客户服务</a></span> <span><a href="">人才招募</a></span> <span><a href="">免责声明</a></span> <span><a href="">版权声明</a></span> <span><a href="">友情链接</a></span> <span><a href="">网站地图</a></span>
						</div>
						<div>
							<span>哈尔滨祈福科技有限公司Copyright 2007-2015 All rights reserved 黑ICP备07002329号</span> <span>网站统计</span>
						</div>
					</div>
					<div class=" col-lg-1 footer_foot_ewm">
						<img src="/resources/index/images/sy_yqlj_ewm.png">
					</div>
				</div>
			</div>

		</div>
	</div>

</body>
</html>