<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
<meta content="height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" name="viewport" />
<title>许愿树</title>
<link href="<%=path%>/resources/css/category.css" rel="stylesheet" />
<link href="<%=path%>/resources/css/wishingtree/appwish_tree.css" rel="stylesheet" />
<script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>

<script src="<%=path%>/resources/js/jquery/jquery.promptu-menu.js"></script>
<script src="<%=path%>/resources/js/second_menu.js"></script>
<script src="<%=path%>/resources/js/wishingtree/appwish_tree.js"></script>
    
<script type="text/javascript">
	
		var isfirst=1;
		var ios=0;
		function fun_meta(h,issed){
			if(ios==1){
				var initial_scale=h/600;
				var minimum_scale=h/600;
				var maximum_scale=h/600;
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
				document.body.scrollLeft=(initial_scale*960/3);	
				screenH=h;
			}
			else{
				if(isfirst==1 || issed==1){
					var initial_scale=h/600;
					var minimum_scale=h/600;
					var maximum_scale=h/600;
					document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
					document.body.scrollLeft=(initial_scale*960/3);	
					screenH=h;
					isfirst=2;
				}
	 			else{
	 				
	 				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
	 			}
			}	
 		}		
		function showSec(){
			if(ios==1){
				
				fun_meta(screenH);
			}
			else{
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
			}
			
		}
	
	
	$(document).ready(function(){
	        
        //loadUrlTrue() 加载成功   loadUrlFalse() 加载失败
        var u = navigator.userAgent;
		var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
		var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
		if(isiOS){
			ios=1;
			loadUrlTrue();//ios用
		}
		else{
			window.qifu.loadUrlTrue();//Android用
		}
        
	});
		
    //全部保存
    function onSave(){
    	$("#tabflag").val(0);//0 首次加载 1祭祀建园 2 布置
   		var wish_tree_id = $("#input_wish_tree_id").val();
		var arr = "";
		var arrTab=$('.inputs_materials_id');
      		for(var i=0;i<arrTab.length;i++){
      			var imgId = $(arrTab[i]).val();
      			var cemetery_materials_id = $(arrTab[i]).val();
	    	var zindex = $(arrTab[i]).attr("z-index");
	    	var x = $(arrTab[i]).attr("left");
	    	var y = $(arrTab[i]).attr("top");
	    	var width = $(arrTab[i]).attr("width");
	    	var height = $(arrTab[i]).attr("height");
	    	
	    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
	    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
      		}
		    
		var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="wish_tree_id" type="hidden" value="' + wish_tree_id + '" />';
		
		$("#ff").append(tempstr);
		document.ff.action = "<%=path%>/webWishingTree/saveWishTreeMaterialsForUpd";
		$("#ff").ajaxSubmit(function(result){
			location.href="<%=path%>/webWishingTree/queryprivateWishTreeForAppBz?private_wish_tree_id="+$("#input_wish_tree_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
		});
		fun_meta(screenH);
   	}
    	
   	//删除素材
   	function onDelMaterials(){
   		var wish_tree_materials_id = $("#ff_wish_tree_materials_id").val();
   		var wish_tree_id = $("#ff_wish_tree_id").val();
           if(wish_tree_materials_id != null && wish_tree_materials_id != ''){
    		$.ajax({
				type: "POST",
				url: '/webWishingTree/delWishTreeMaterialsByWishTreeMaterialsId',
		    	data: {wish_tree_materials_id : wish_tree_materials_id},
				dataType:'json',
				cache: false,
				success: function(data){
					 if(data == 0){
					 	$(".check").remove();
						var namelist = $("li[class='cc']");
						for(var i=0; i<namelist.length; i++){
						 	if($(namelist[i]).attr("materials_info") == wish_tree_materials_id){
						 		namelist[i].remove();
						 	}
						}
					}else{
						alert('操作失败，请联系管理员！');
					}
				}
			});
		}else{
   			alert("请选择图层！");
   		}
   	}
    	
   	//弹出祭祀布置一级菜单
	function onShowJisiBz(flag){//1布置 2祭祀
		$("#tabflag").val(1);//0 首次加载 1祭祀建园 2 布置
		$("#back_flag").val(1);
		if(flag == 1){//布置
			$(".box_category_bz").css({display:"block"});
			$(".box_category_jisi").css({display:"none"});
		}else{//祭祀
			$(".box_category_jisi").css({display:"block"});
			$(".box_category_bz").css({display:"none"});	
		}
		
		$("#main").css({display:"none"});
		
		if(flag == 1){
			$("#ff_type_flag").val(3);
		}else{
			$("#ff_type_flag").val(2);
		}
		showSec();
	}
		
	//弹出祭祀布置二级菜单
	function onShowMaterialsClass2(materials_class_id){
	
		var src = "/webWishingTree/queryMaterialsListForApp?materials_class_id=" + materials_class_id;
		$("#iframe_materials").attr("src",src);
       	$(".second_menu").css({display:"block"});
       	
       	$("#back_flag").val(2);
	}    
	
	function onShowMaterialsDiv(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id, materials_small_img, parent_id){
		var vipcount = $('#vipcount').val();
		var str = '<input type="hidden" id="materials_id_1" value="' + materials_id + '" />';
		var str = str + '<input type="hidden" id="materials_name_1" value="' + materials_name + '" />';
		var str = str + '<input type="hidden" id="img_url_1" value="' + img_url + '" />';
		var str = str + '<input type="hidden" id="materials_family_1" value="' + materials_family + '" />';
		var str = str + '<input type="hidden" id="materials_wealth_1" value="' + materials_wealth + '" />';
		var str = str + '<input type="hidden" id="materials_measured_1" value="' + materials_measured + '" />';
		var str = str + '<input type="hidden" id="materials_price_1" value="' + materials_price + '" />';
		var str = str + '<input type="hidden" id="materials_class_id_1" value="' + materials_class_id + '" />';
		var str = str + '<input type="hidden" id="parent_id_1" value="' + parent_id + '" />';
		var str = str + '<input type="hidden" id="materials_small_img_1" value="' + materials_small_img + '" />';
		var str = str + '<input type="hidden" id="materials_measured_value_1" value="1" />';
		var str = str + '<input type="hidden" id="materials_count_1" value="1" />';
		var str = str + '<input type="hidden" id="price_1" value="' + materials_price + '" />';
		str = str + '<div class="box_top">';
		str = str + materials_name + '<img src="<%=path%>/resources/images/image1/X.png" class="end" onclick="onHideThree()">';
        str = str + '</div>';
        str = str + '<ul class="content">';
        str = str + '<li class="box_img"><img src="<%=path%>/goods' + materials_small_img + '"></li>';
        str = str + '<li class="box_text">';
        str = str + '<ul>';
           
        str = str + '<li>价格';
        
        if(materials_measured == 1){
        	str = str + '<span>' + materials_price + '福币/天</span>';
        }else if(materials_measured == 2){
         	str = str + '<span>' + materials_price + '福币/月</span>';
        }else if(materials_measured == 3){
         	str = str + '<span>' + materials_price + '福币/年</span>';
        }else{
         	str = str + '<span>' + materials_price + '福币/-</span>';
        }
           
        str = str + '</li>';
        
        if(parent_id != 3){
			str = str + '<li>时效';
			if(materials_measured == 1){
            	str = str + '<span>天</span>';
            }else if(materials_measured == 2){
             	str = str + '<span>月</span>';
            }else if(materials_measured == 3){
             	str = str + '<span>年</span>';
            }else{
             	str = str + '<span>-</span>';
            }
            
            str = str + '<button class="number" onclick="onAddDate1(1)">+</button>';
            str = str + '<button class="number" id="div_date_count_1">1</button>';
            str = str + '<button class="number" onclick="onAddDate1(-1)">-</button>';
            str = str + '</li>';
        }
           
		str = str + '<li>数量';
        str = str + '<span>个</span>';
        str = str + '<button class="number" onclick="onAddCount1(1)" >+</button>';
        str = str + '<button class="number" id="div_number_sum_1">1</button>';
        str = str + '<button class="number" onclick="onAddCount1(-1)">-</button>';
        str = str + '</li>';
        str = str + '<li>原价<span id="span_pay_price_1">' + materials_price + '福币</span></li>';
        
        str = str + '<li>会员价<span id="span_pay_vipprice_1">' + Math.floor(materials_price * vipcount) + '福币</span></li>';
        
        
        str = str + '</ul>';
        str = str + '</li>';
        str = str + '<div class="clear"></div>';
        str = str + '</ul>';
        str = str + '<div class="box_button">';
        str = str + '<button class="button" class="quxiao" onclick="onHideThree()" >取消</button>';
        str = str + '<button class="button" onClick="onSaveMaterial();">确定</button>';
        str = str + '</div>';
		$("#Layer2").html(str);
		
		$(".second_menu2").css({display:"block"});
		$(".bg_content,.box_content").css({display:"block"});
		
		$("#back_flag").val(3);
		showSec();
	}
		
	function onSaveMaterial(){
		//背景单选
		var arrTab=$('.class_materials');
		var materials_class_id = $("#materials_class_id_1").val();
   		if(materials_class_id == 37){
   			for(var i=0;i<arrTab.length;i++){
				if($(arrTab[i]).attr("materials_class_id") == 37 ){
					$(arrTab[i]).remove();
				}
            }
   		}
		var temp = $(".class_materials");
        $("#Form", parent.document).append('<input type="hidden" name="materials_id" class="class_materials" '+
        'id="input_materials_id_'+temp.length+'_' + $('#materials_id_1').val() + '" '+
        'value="' + $('#materials_id_1').val() + '" '+
        'materials_name="' + $('#materials_name_1').val() + '" '+
        'img_url="' + $('#img_url_1').val() + '" '+
        'materials_family="' + $('#materials_family_1').val() + '" '+
        'materials_wealth="' + $('#materials_wealth_1').val() + '" '+
        'materials_measured="' + $('#materials_measured_1').val() + '" '+
        'materials_measured_value="' + $('#materials_measured_value_1').val() + '" '+
        'materials_count="' + $('#materials_count_1').val() + '" '+
        'materials_price_sum="' + $('#materials_price_1').val() + '" '+
        'materials_price="' + $('#materials_price_1').val() + '" '+
        'ifCheck="1" materials_class_id="' + $('#materials_class_id_1').val() + '" '+
        'parent_id = "' + $('#parent_id_1').val() + '" />' );
		var class_materials = $(".class_materials");
		
// 		$(".jisuan").text("结算(" + class_materials.length + ")");
		$(".second_menu2").css({display:"none"});
		$(".bg_content,.box_content").css({display:"none"});
		
		//直接调结算方法
   		onSaveMaterials();
  	}
   		
  	//弹出结算素材弹出窗
   	function onShowJs(){
   		$.ajax({
			type: "POST",
			url: '/webCemetery/queryUserFb',
			dataType:'json',
			cache: false,
			success: function(data){
				onShowJsDiv(data);
			}
		});
		showSec();
   	}
    	
   	//显示结算弹出窗
   	function onShowJsDiv(user_fb){
   	
   		$('.jisuan').hide();
   		
   		var arrTab=$('.class_materials');
   		var str = '';
   		var fb_sum = 0 * 1;
   		str = str + '<ul>';
   		
   		for(var i=0;i<arrTab.length;i++){
   		
   			if($(arrTab[i]).attr("ifCheck") == 1){
   				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") * 1;
   			}
   			
   			str = str + '<li>';
            str = str + '<div class="check_btn" ><img src="<%=path%>/resources/images/check_pre.png" class="img_check" img_flag_info = "1" onclick="onChooseCheck(this)" img_materials_price_sum_info = '+$(arrTab[i]).attr("materials_price_sum")+'></div>';
            str = str + '<div class="check_img"><img src="' + $(arrTab[i]).attr("img_url") + '"></div>';
            str = str + '<div class="img_message">';
            str = str + '<div class="img_name">' + $(arrTab[i]).attr("materials_name") + '</div>';
            str = str + '<div class="img_money" id="div_sum_price_' + $(arrTab[i]).val() + '">小计' + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") + '福币</div>';
            str = str + '</div>';
            str = str + '<div class="img_time">';
            if($(arrTab[i]).attr("parent_id") != 1){
              
                str = str + '<div class="img_time_first">';
                str = str + '<span class="time_title">时效</span>';
                str = str + '<span onclick="onAddDate(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
                str = str + '<span id="div_date_count_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</span>';
                str = str + '<span onclick="onAddDate(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
                if($(arrTab[i]).attr("materials_measured") == 1){
                	str = str + '<span class="time_title">天</span>';
                }else if($(arrTab[i]).attr("materials_measured") == 2){
                	str = str + '<span class="time_title">月</span>';
                }else if($(arrTab[i]).attr("materials_measured") == 3){
                	str = str + '<span class="time_title">年</span>';
                }else{
                	str = str + '<span class="time_title"> - </span>';
                }
                str = str + '</div>';
            }
               
            str = str + '<div class="img_time_cecond">';
            str = str + '<span class="time_title">数量</span>';
            
            str = str + '<span onclick="onAddCount(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
            str = str + '<span id="div_number_sum_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</span>';
            if($(arrTab[i]).attr("materials_class_id") == 37){//背景
            	str = str + '<span onclick="alert(\'背景只能买一个！\');">+</span>';
            }else{
            	str = str + '<span onclick="onAddCount(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
            }
            
            str = str + '<span class="time_title">只</span>';
            str = str + '</div>';
            str = str + '</div>';
            str = str + '</li>';
        }
           
        fb_sum = fb_sum * 1

        str = str + '</ul>';
		str = str + '<div class="sum_foot">';
        str = str + '<div class="accounts_footer"></div>';
        str = str + '<div class="accounts_footer_cont">';
        str = str + '<div class="footer_first">';
        str = str + '<div class="all_check"><img src="<%=path%>/resources/images/footer_check_pre.png" onclick="onChooseAllCheck(this);" img_all_flag_info = "1"></div>';
        str = str + '<div>全选</div>';
        str = str + '</div>';
        str = str + '<div class="footer_second">总计：</div>';
        str = str + '<div class="footer_money" id="div_footer_money">' + fb_sum + '</div>';
        str = str + '<div class="footer_danwei">福币</div>';
        if(user_fb < fb_sum){
        	str = str + '<div class="footer_queding">补充福币</div>';
        }else{
        	str = str + '<div class="footer_queding" onClick="onSaveMaterials();">确定</div>';
        }
           
        str = str + '<div class="footer_number" id="div_footer_number">共计' + arrTab.length + '件</div>';
        str = str + '</div>';
        str = str + '</div>';
        
        $("#div_confirm_container_js").html(str);
   		//显示弹出窗
		$(".sum_accounts,.sum_bg,.sum_content").attr("style","display: block; ");
		$("#back_flag").val(4);
		showSec();
   	}
   	
   	//添加减少数量
	function onAddCount1(flag){
		var materials_class_id = $("#materials_class_id_1").val();
   		if(materials_class_id == 37){
   			alert("背景只能买一个！");
   		}else{
   			var materials_count = $("#div_number_sum_1").text();
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#div_number_sum_1").text(materials_count);
    		$("#materials_count_1").val(materials_count);
    		
    		var div_date_count_1 = $("#div_date_count_1").text();
    		if(div_date_count_1 == null || div_date_count_1 == '' || div_date_count_1 == 'undefined'){
    			div_date_count_1 = 1;
    		}
    		$("#span_pay_price_1").text((materials_count * $("#price_1").val() * div_date_count_1)+'福币');
    		$("#span_pay_vipprice_1").text( Math.floor((materials_count * $("#price_1").val() * div_date_count_1) * $('#vipcount').val())+'福币');
    	
   		}
   	}
   	
   	//时效更改
   	function onAddDate1(flag){
   		var materials_measured_value = $("#div_date_count_1").text();
   		if(flag == -1 && materials_measured_value == 1){
   			return false;
   		}else{
   			materials_measured_value = materials_measured_value * 1 + flag * 1;
   		}
   		$("#div_date_count_1").text(materials_measured_value);
   		$("#materials_measured_value_1").val(materials_measured_value);
   		$("#span_pay_price_1").text((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text())+'福币');
   		$("#span_pay_vipprice_1").text( Math.floor((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text()) * $('#vipcount').val())+'福币');
   		
   	}
   	
   	//添加减少时效
   	function onAddDate(flag, i, materials_id, user_fb){
   		var materials_measured_value = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value");
   		if(flag == -1 && materials_measured_value == 1){
   			return false;
   		}else{
   			materials_measured_value = materials_measured_value * 1 + flag * 1;
   		}
   		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value", materials_measured_value);
   		$("#input_materials_id_"+i+"_"+ materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
   		onShowJsDiv(user_fb);
   	}
    	
   	//添加减少数量
   	function onAddCount(flag, i, materials_id, user_fb){
   		var materials_count = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count");
   		if(flag == -1 && materials_count == 1){
   			return false;
   		}else{
   			materials_count = materials_count * 1 + flag * 1;
   		}
   		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_count", materials_count);
   		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
   		onShowJsDiv(user_fb);
   	}
    	
   	//结算
   	function onSaveMaterials(){
   		var arrTab=$('.class_materials');
   		var fb_sum = 0;
   		var str = '<input type="hidden" name="private_wish_tree_id" value="' + $("#input_wish_tree_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#ff_type_flag").val() + '"/><input name="wish_type" type="hidden" value="2" />';
   		for(var i=0;i<arrTab.length;i++){
   			if($(arrTab[i]).attr("ifCheck") == 1){
   				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
   			}
   		}
   		$("#ff").html(str);
   		
   		document.ff.action = '<%=path%>/webWishingTree/saveWishTreeMaterials';
		$("#ff").ajaxSubmit(function(result){
			if(result.flag == 0){
				location.href="<%=path%>/webWishingTree/queryprivateWishTreeForAppBz?private_wish_tree_id="+$("#input_wish_tree_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
			}else{
				alert("余额不足，跳转支付页面！");
			}
		});
   	}
    	
   	//弹出信物弹出窗
   	function onShowXinWu(materials_small_img, materials_measured, materials_price, materials_id, materials_name){
   		$("#xinwu_wish_title").val('');
   		$("#xinwu_wish_password").val('');
   		$("#xinwu_wish_info").val('');
		//时效
        if(materials_measured == 1){
        	$("#make_wish_content_time_time").text("天");
        }else if(materials_measured == 1){
        	$("#make_wish_content_time_time").text("月");
        }else if(materials_measured == 1){
        	$("#make_wish_content_time_time").text("年");
        }else{
        	$("#make_wish_content_time_time").text("-");
        }
		//价格
        $("#make_wish_content_price_price").text("价格 " +materials_price + "福币");
        $("#make_wish_content_price_price_vip").text("会员价 " + Math.floor(materials_price * $('#vipcount').val()) + "福币");
		//信物名称
        $("#xinwu_name").text(materials_name);
		//总计
        $("#make_wish_content_tree_sum_amount").text(materials_price + "福币");
   		
		//图片路径
   		$("#make_wish_content_wish_img").attr("src","<%=path%>/goods"+materials_small_img);
   		
   		$("#xinwu_materials_id").val(materials_id);
   		$(".make_wish_content,.wish_content").css({display:"block"});
   		$("#back_flag").val(7);
    	$("#main").css({display:"none"});
    	showSec();
   	}
    	
   	//重置信物弹出窗
   	function onCloseXinwu(){
   		$("#xinwu_wish_title").val('');
   		$("#xinwu_wish_password").val('');
   		$("#xinwu_wish_info").val('');
   		$(".make_wish_content,.wish_content").css({display:"none"});
   		showSec();
   	}
   	
   	//保存信物
   	function onSaveXinwu(){
   		var xinwu_wish_info = $("#xinwu_wish_info").val();
		var wish_title = $("#xinwu_wish_title").val();//愿望标题
		
		if(wish_title == null || wish_title == ''){
			alert("请填写愿望标题！");
		}else if(wish_title.length > 30){
		    alert('愿望标题不得超过30字！');
		}else if(xinwu_wish_info != null && xinwu_wish_info.length > 200){
	   		alert('愿望限输入 200 个文字！');
	   	}else{
			document.xinwuForm.action = "<%=path%>/webWishingTree/saveXinwu";
			$("#xinwuForm").ajaxSubmit(function(result){
				//alert(result);
    			if(result == 2){//未登陆
    				alert("请先登录！");
	    			location.href="<%=path%>/login_toLogin";
    			}else if(result == 1){
    				alert("跳转充值页面");
    			}else{
					location.href="<%=path%>/webWishingTree/queryprivateWishTreeForAppBz?private_wish_tree_id="+$("#input_wish_tree_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
    			}
			});
		}
		fun_meta(screenH);
   	}
   	
   	//关闭结算
   	function onCloseSumContent(){
   		$(".sum_bg,.sum_content").attr("style","display: none; ");
   		$(".jisuan").css({display:"block"});
   		$("#back_flag").val(2);
   	}
   	
   	//隐藏第三层
   	function onHideThree(){
   		$(".second_menu2").css({display:"none"});
		$(".bg_content,.box_content", parent.document).css({display:"none"});
		$("#back_flag").val(2);
   	}
   	
   	//APP端返回
   	function onCloseJisiBz(){//1有弹出层 0没有弹出层
		var back_flag = $("#back_flag").val();
		if(back_flag == 1){//一层
			$(".box_category_bz").css({display:"none"});
			$(".box_category_jisi").css({display:"none"});
			$(".jisuan").css({display:"none"});
			$("#main").css({display:"block"});
			$("#back_flag").val(0);

			fun_meta(screenH,1);
		}else if(back_flag == 2){//二层
			$(".second_menu").css({display:"none"});
			$("#back_flag").val(1);
			fun_meta(screenH);
		}else if(back_flag == 3){//三层
			$(".second_menu2").css({display:"none"});
			$(".bg_content,.box_content").css({display:"none"});
			$("#back_flag").val(2);
			fun_meta(screenH);
		}else if(back_flag == 4){//结算小结层
			$(".sum_bg,.sum_content").attr("style","display: none; ");
			$("#back_flag").val(2);
			fun_meta(screenH);
		}else if(back_flag == 5){//文字层
			$("#show").attr("style","display: none; ");
			$(".bg_wz,.show_wz").attr("style","display: none; ");

			$("#back_flag").val(1);
			fun_meta(screenH);
		}else if(back_flag == 7){//许愿
			$(".make_wish_content,.wish_content").css({display:"none"});
			$("#back_flag").val(1);
			fun_meta(screenH);
		}else{
			back_flag = 0;
			$("#back_flag").val(0);
			
			var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				back();//ios用，用于返回原生页面
			}
			else{
				window.qifu.back();//Android用，用于返回原生页面
			}
			showSec();
		}
	}
	
	//跳转布置，所有素材都能拖
   	function onqueryBz(){
   		$("#tabflag").val(2);//0 首次加载 1祭祀建园 2 布置
   		location.href="<%=path%>/webWishingTree/queryprivateWishTreeForAppBz?private_wish_tree_id="+$("#input_wish_tree_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
   		fun_meta(screenH);
   	}
    	
</script>
    	
</head>
<body>
<input type="hidden" id="vipcount" value="${vipcount == null ? 1 : vipcount}" />
<input type="hidden" id="tabflag" value="${tabflag}" /> <!-- 0 首次加载 1祭祀建园 2 布置  -->
<input type="hidden" id="back_flag" value="0"/>
<form method="post" name="fff" id="fff">
  <input type="hidden" name="wz" id="id_wz" value=""/>
  <input type="hidden" name="materials_id" id="id_materials_id" value=""/>
  <input type="hidden" name="wish_tree_id" id="id_wish_tree_id" value=""/>
  <input type="hidden" name="wish_tree_materials_id" id="id_wish_tree_materials_id" value=""/>
</form>
<input type="hidden" id="input_wish_tree_id" value="${private_wish_tree_id}"/>
<form method="post" name="saveForm" id="saveForm">
  
</form>
<form method="post" name="Form" id="Form">
  
</form>
<form method="post" name="ff" id="ff">
  <input type="hidden" id="ff_wish_tree_materials_id"  />
  <input type="hidden" id="ff_wish_tree_id"  />
  <input type="hidden" id="ff_materials_id"  />
</form>
<input type="hidden" name="type_flag" id="ff_type_flag"  />

<div class="main" id="main">
  <div id="header"></div>

  <!-- 刚进入墓园时，显示各种 值  -->
  <div class="srmy_infor_look" id="srmy_infor_look">
    <div class="srmy_infor_ico_01">
      <div>浏览</div>
      <img src="<%=path%>/resources/images/srmy_look.png" id="srmy_look" />
      <div>${privateWishTree.private_wish_tree_clicks}</div>
    </div>
    <div class="srmy_infor_ico_02">
      <div>愿望</div>
      <img src="<%=path%>/resources/images/wish_small_log.png" id="srmy_jisi" />
      <div>${privateWishTree.private_wish_count}</div>
    </div>
  </div>

  <!-- 0 首次加载 1祭祀建园 2 布置  -->
  <c:choose>
    <c:when test="${tabflag == 1 || tabflag == 2 }">
      <div class="js_and_save"> 
        <div class="yk_jisi" onclick="onShowJisiBz(2);">许愿</div> 
     	<c:if test="${is_user_tree_flag > 0}">
     	  <div class="zr_jianyuan" onclick="onShowJisiBz(1);">建园</div>
          <div class="zr_buzhi" onclick="onqueryBz();">布置</div>
     	</c:if>
        <div class="yk_save" onclick="onSave();">保存</div> 
      </div> 
    </c:when>
    <c:otherwise>
      <div class="menu"></div>
      <div class="you_ke_menu">
        <div class="zr_jisi" onclick="onShowJisiBz(2);">许愿</div>
        <c:if test="${is_user_tree_flag > 0}">
          <div class="zr_jianyuan" onclick="onShowJisiBz(1);">建园</div>
          <div class="zr_buzhi" onclick="onqueryBz();">布置</div>
        </c:if>
<!--         <div class="zr_buzhi" >许愿</div> -->
      </div>
    </c:otherwise>
  </c:choose>

  <!-- 布置墓园时，左侧的图层选项-->
  <div class="buzhi_left_list" ${tabflag == 2 ? 'style="display: block;"' : '' }> 
    <div class="buzhi_left_menu" ${tabflag == 2 ? 'style="display: block;"' : '' }>
      <div class="left_menu_title">
<!--       物品 -->
<!--         <div id="srmy_menu_jiantou"><img src="<%=path%>/resources/images/ct_goods_jt_top.png" id="goods_jian_tou" /></div> -->
      </div>
    </div>
    <div class="srmy_fix_up" id="srmy_fix_up">
      <ul id="SortContaint" class="fix_up">
        <c:forEach var="obj" items="${movelist}" varStatus="status"> 
          <c:if test="${obj.materials_class_id != 10}"> 
            <li id="list${obj.wish_tree_materials_id}" 
            	class="cc" 
            	draggable="true" 
                materials_info="${obj.wish_tree_materials_id}" 
                cemetery_info="${obj.wish_tree_id}"  
                materials_class_info = "${obj.materials_class_id}" 
                materials_id_info = "${obj.materials_id}" > 
                  
              ${fn:length(obj.materials_name) > 5 ? fn:substring(obj.materials_name, 0, 5) : obj.materials_name}${fn:length(obj.materials_name) > 5 ? '...' : ''} 
            </li> 
          </c:if> 
   		</c:forEach> 
      </ul>
    </div>
  </div>

  <div class="srmy_main" id="srmy_main">
    <c:forEach var="obj" items="${sclist}" varStatus="status">
	  <c:choose>
	    <c:when test="${obj.materials_class_id eq '37'}"><%--  背景 素材分分类id ==10    --%>
	      <img id="div${obj.wish_tree_materials_id}" materials_info="${obj.wish_tree_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:0px;top:0px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	    </c:when>
	    <c:otherwise>
	      <img id="div${obj.wish_tree_materials_id}"  
	      	   width_info="${obj.wish_tree_materials_width}" 
	      	   height_info="${obj.wish_tree_materials_height}" 
		       ${((obj.wish_user_id == user_id && obj.materials_limit != 3) || (is_user_tree_flag == 1 && tabflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
		       materials_info="${obj.wish_tree_materials_id}" 
		       data-name="img${status.index+1}" 
		       data-id="url${status.index+1}" 
		       title="${obj.materials_name}-${obj.materials_class_id}" 
		       style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:${obj.wish_tree_materials_x}px;top:${obj.wish_tree_materials_y}px;width:${obj.wish_tree_materials_width}px;height:${obj.wish_tree_materials_height}px;${obj.wish_user_id == user_id && obj.materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
		       src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	    </c:otherwise>
	  </c:choose>
    </c:forEach>
  </div>
  
  <!-- 布置墓园时，显示功能条和菜单选项  -->
  <div class="foot_bg"></div>
  <div class="buzhi_top_menu" id="buzhi_top_menu">
    <div class="buzhi_big" id="div_buzhi_big">
      <img src="<%=path%>/resources/images/buzhi_big.png" />
      <div>放大</div>
    </div>
    <div class="buzhi_small" id="div_buzhi_small">
      <img src="<%=path%>/resources/images/buzhi_small.png" />
      <div>缩小</div>
    </div>
    <div class="buzhi_before">
      <img src="<%=path%>/resources/images/buzhi_before.png" />
      <div>提前</div>
    </div>
    <div class="buzhi_after">
      <img src="<%=path%>/resources/images/buzhi_after.png" />
      <div>置后</div>
    </div>
    <div class="buzhi_delete" onclick="onDelMaterials();">
      <img src="<%=path%>/resources/images/buzhi_delete.png" />
      <div>删除</div>
    </div>
  </div>
  
  
      <!-- 半全景切换图-->
   
    <div class="qj_img">
        <img src="<%=path%>/goods/${privateWishTree.private_wish_tree_img}">
    </div>
     <div class="quan_jing_btn"></div>
    <div class="xiang_xi_btn"></div>
    
    
    
 </div>
  
<!-- 一级菜单-->
<div class="box_category_jisi" id="box_category_jisi">
  <ul class="category first_menu">
    <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
      <li onclick="onShowXinWu('${var.materials_small_img}','${var.materials_measured}','${var.materials_price}','${var.materials_id}','${var.materials_name}');">
        <div>
          <img src="<%=path%>/goods${var.materials_small_img}" class="img" />
          <span class="span">${var.materials_name}</span>
        </div>
      </li>
    </c:forEach>
    <div class="clear"></div>
  </ul>
</div>

<!-- 一级菜单-->
  <div class="box_category_bz">
    <ul class="category first_menu">
      <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
        <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
          <div>
            <img src="<%=path%>/goods/${var.materials_class_img}" class="img" />
            <span class="span">${var.materials_class_name}</span>
          </div>
        </li>
      </c:forEach>
      <div class="clear"></div>
    </ul>
  </div>
  
<!-- 许愿-->
<form  method="post" name="xinwuForm" id="xinwuForm">
  <input type="hidden" name="materials_id" id="xinwu_materials_id" />
  <input type="hidden" name="wish_tree_id" id="xinwu_wish_tree_id" value="${private_wish_tree_id}" />
  <input type="hidden" name="wish_type" id="xinwu_wish_type" value="2" />  <!--  1表示公共许愿树的素材或愿望 2表示私人的许愿树和愿望   -->
  <div class="make_wish_content">
    <div class="wish_content">
      <div class="wish_good_infor">
        <div class="good_name" id="xinwu_name">信物名称</div>
        <div class="good_price" id="make_wish_content_price_price">价格  200福币/月</div>
        <div class="good_price" id="make_wish_content_price_price_vip">会员价  200福币/月</div>
        <div class="good_img"><img src="<%=path%>/resources/images/image2/muyangquan.png" id="make_wish_content_wish_img" /></div>
      </div>
      <div class="wish_second">
        <div class="wish_title">愿望标题
          <input type="text" name="wish_title" id="xinwu_wish_title" value="" class="wish_cont" />
        </div>
      </div>
      <div class="wish_third">
        <div class="wish_title">查看密码<span>(选填)</span>
          <input type="password" name="wish_password" id="xinwu_wish_password" onkeyup="value=value.replace(/[^\d]/g,'') " value="请输入查看密码" class="wish_cont wish_cont_pwd" />
        </div>
      </div>
      <div class="wish_forth">
        <div class="wish_title" >愿望内容</div>
        <textarea name="wish_info" id="xinwu_wish_info"></textarea>
                    &nbsp;限输入 <span id="count1">200</span> 个文字
      </div>
    </div>
    <div class="wish_btn">
      <div class="wish_cancel" onclick="onCloseXinwu();">取消</div>
      <div class="wish_queren" onclick="onSaveXinwu();">确认</div>
    </div>
  </div>
</form>
  
<!--  祭祀 -> 二级菜单  -->
<div class="second_menu">
  <div id="category2">
    <iframe src="" scrolling="no" iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
  </div>
</div>
  
<div class="second_menu2">
  <div id="category3">
    <div id="ly" class="bg_content"></div>
    <div id="Layer2" class="box_content"></div>
  </div>
</div>    

<!-- 总结算页面-->
<div class="sum_accounts">
  <div class="sum_bg"></div>
  <div class="sum_content">
    <div class="kind_title">物品</div>
    <img src="<%=path%>/resources/images/image1/X.png" class="kind_close" onclick="onCloseSumContent();" />
    <div class="kind_content" id="div_confirm_container_js"></div>
  </div>
</div>

<div class="jisuan" onclick="onShowJs();">结算</div>
</body>
</html>