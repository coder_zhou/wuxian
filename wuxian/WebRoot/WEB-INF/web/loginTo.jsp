<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="/resources/css/deng_lu_win.css" rel="stylesheet">
    <script src="/resources/js/jquery-1.8.2.js"></script>
    <script src="/resources/js/deng_lu_win.js"></script>
    <script type="text/javascript">
		function denglu(){
			
			
			if(check()){
			$("#loginForm").submit();
		}
		}
		function quxiao(){
			$("#loginname").val('');
			$("#password").val('');
		}
	</script>
	<c:if test="${error != null && error != '' }">
	<script type="text/javascript">
		alert("${error}");
	</script>
</c:if>
</head>
<body>

<!-- 登录 -->
<div class="loading_bg"></div>
<div class="loading_content">
    <div class="loading_logo"><img src="/resources/images/logo.png"></div>
	<form id="loginForm" action="tologin" method="post">
    <div class="loading_title">用户登录</div>
    <div class="loading_name">
        <div><img src="/resources/images/denglu_user.png"></div>
        <div><input type="text"  name="userAccount" id="userAccount" placeholder="请输入用户名" maxlength="20" onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\@\.]/g,'')"></div>
    </div>
    <div class="loading_pwd">
        <div><img src="/resources/images/denglu_pwd.png"></div>
        <div><input  type="password" name="userPwd" id="userPwd" placeholder="请输入密码" onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\@\.]/g,'')"></div>
    </div>
    <div class="loaging_btn" onclick="denglu();">登&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;录</div>
    <div class="loading_choose">
    <!--     <div class="use_fu_ka">
            <div><input type="checkbox"></div>
            <div>使用福卡</div>
        </div>
         -->
        <div class="forget_pwd"><a href="">忘记密码？</a></div>
        <div class="zhu_ce_message">还没有云祈福账号？</div>
        <div class="feel_zhu_ce">免费注册 > </div>
    </div>
    <div class="line"></div>
    <div class="use_third_load">使用第三方账号登录</div>
    <div class="third_img">
        <img src="/resources/images/loading_wb.png">
        <img src="/resources/images/loading_qq.png">
        <img src="/resources/images/loading_dou.png">
        <img src="/resources/images/loading_rr.png">
    </div>
    <input type="hidden" name="type" value="1"></input>
    <input type="hidden" name="from" value="${pd.from}">
		<input type="hidden" name="token" value="${token}" />
					</form>
</div>

<!-- 免费注册 -->
<div class="zhu_ce_bg"></div>
<div class="zhu_ce_content">
    <div class="loading_logo"><img src="/resources/images/logo.png"></div>

    <div class="loading_title">免费注册</div>
    <div class="loading_name">
        <div><img src="/resources/images/denglu_user.png"></div>
        <div><input type="text" name="userAccount" id="em" placeholder="请输入手机号" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" onblur="chau();"></div>
    </div>
    <div class="loading_pwd">
        <div><img src="/resources/images/denglu_pwd.png"></div>
        <div><input type="password" id="pwd1" value="" name="userPwd" placeholder="设置密码" maxlength="50"></div>
    </div>
    <div class="loading_pwd">
        <div><img src="/resources/images/denglu_pwd.png"></div>
        <div><input type="password" id="pwd2" value="" placeholder="确认密码" maxlength="50"></div>
    </div>

    <div class="pan_zheng_ma">
        <div class="input_tel"><input type="text" name="code" id="code"></div>
        <div class="input_send"><input type="button" value="免费获取验证码" class="hq_btn"  id="send_again_btn" ></div>
    </div>

    <div class="loaging_btn">注&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;册</div>
    <div class="loading_choose zhu_ce_choose">
        <div class="use_fu_ka">
            <div><input type="checkbox"></div>
            <div>使用福卡</div>
        </div>
        <div class="return_loading">返回登录 > </div>
        <div class="have_num">已有账号？</div>

    </div>
</div>

<script src="/resources/js/jquery-1.8.2.js"></script>

	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
	<script type="text/javascript">
		var errInfo = "${errInfo}";
		$(document).ready(function(){
			if(errInfo!=""){
				if(errInfo.indexOf("验证码")>-1){
					
					$("#code").tips({
						side:1,
			            msg:errInfo,
			            bg:'#FF5080',
			            time:5
			        });
					
					$("#code").focus();
				}else{
					$("#userAccount").tips({
						side:1,
			            msg:errInfo,
			            bg:'#FF5080',
			            time:5
			        });
				}
			}
		
			$("#userAccount").focus();
		

		});
		
		$(document).keyup(function(event){
			  if(event.keyCode ==13){
			//    $("#dl").trigger("click");
				  denglu();
			  }
			});
	
		function genTimestamp(){
			var time = new Date();
			return time.getTime();
		}
	
		
		
		function check(){
			
			if($("#userAccount").val()==""){

				$("#userAccount").tips({
					side:2,
		            msg:'用户名不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#userAccount").focus();
				return false;
			}else
			
			if($("#userPwd").val()==""){

				$("#userPwd").tips({
					side:2,
		            msg:'密码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#userPwd").focus();
				return false;
			}
			else{
			$("#loginbox").tips({
				side:1,
	            msg:'正在登录 , 请稍后 ...',
	            bg:'#68B500',
	            time:1000
	        });
			
			return true;
			}
		}
		function checkR(){
		
		    var pattern = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;  
		/*    if (!pattern.test($("#em").val())) {  
		    	$("#em").tips({
					side:2,
		            msg:'請輸入正確的郵箱地址',
		            bg:'#AE81FF',
		            time:3
		        });
		        $("#em").focus(); 
		        return false;  
		    }*/  
			chau();
			if($("#em").val()==""){

				$("#em").tips({
					side:2,
		            msg:'用户名不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#em").focus();
				return false;
			}else{
				
				$("#em").val(jQuery.trim($('#em').val()));
			}
			
			if($("#pwd1").val()==""){

				$("#pwd1").tips({
					side:2,
		            msg:'密码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd1").focus();
				return false;
			}else if($("#pwd1").val().length<6){
				$("#pwd1").tips({
					side:2,
		            msg:'密碼太短了',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd1").focus();
				return false;
			}
			if($("#pwd2").val()==""){

				$("#pwd2").tips({
					side:2,
		            msg:'请输入确认密码',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd2").focus();
				return false;
			}
			if($("#pwd2").val()!=$("#pwd1").val()){

				$("#pwd2").tips({
					side:2,
		            msg:'两次密码不一致',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd2").focus();
				return false;
			}
			if($("#code").val()==""){

				$("#code").tips({
					side:1,
		            msg:'验证码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });

				$("#code").focus();
				return false;
			}
			
			$("#loginbox").tips({
				side:1,
	            msg:'正在登录 , 请稍后 ...',
	            bg:'#68B500',
	            time:1000
	        });
			
			return true;
		}
		/*
		function savePaw(){
			if(!$("#saveid").attr("checked")){
				$.cookie('userAccount', '', { expires: -1 });
				$.cookie('userPwd', '', { expires: -1 });
				$("#userAccount").val('');
				$("#userPwd").val('');
			}
		}
		*/
		/*
		jQuery(function(){
			var userAccount = $.cookie('userAccount');
			var userPwd = $.cookie('userPwd');
			if(typeof(userAccount) != "undefined" && typeof(userPwd) != "undefined"){
				$("#userAccount").val(userAccount);
				$("#userPwd").val(userPwd);
				$("#saveid").attr("checked",true);
				$("#code").focus();
			}
		});
		*/
		function chau(){
			if(checkMobile()){
				$.ajax({
					type: "POST",
					url: '/getUser?username='+$("#em").val(),
					//beforeSend: validateData,
					success: function(data){
						
						if(data=="此帐号已存在")
							{
							$("#em").tips({
								side:1,
					            msg:data,
					            bg:'#AE81FF',
					            time:3
					        });
							return false;
							}else{
								$("#em").tips({
									side:1,
						            msg:"此号码可以注册",
						            bg:'#AE81FF',
						            time:3
						        });
							    document.getElementById("send_again_btn").removeAttribute("disabled");
							}
					}
				});
			}else{
				$("#em").tips({
					side:2,
		            msg:'请输入正确的手机号码',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#em").focus();
				return false;
			}
			
		}
		</script>

	<script type="text/javascript">
		    var wait=60;
		    function time(o) {
		        if (wait == 0) {
		            o.removeAttribute("disabled");
		            o.value="免费获取验证码";
		            o.style.backgroundColor="#4bacf1";
		            wait = 60;
		        } else {
		           $("#huo_qu_ma").attr("disabled", "true");
		   //         o.value="重发验证码(" + wait + ")";
		            $("#huo_qu_ma").val("重发验证码(" + wait + ")");
		            $("#huo_qu_ma").css("background-color","#999");
		                       
		   //         o.style.backgroundColor="#999";
		            wait--;
		            setTimeout(function() {
		                        time(o)
		                    },
		                    1000)
		        }
		       
		    }
		    document.getElementById("send_again_btn").onclick=function(){

				if($("#em").val()==""){

					$("#em").tips({
						side:2,
			            msg:'请输入手机号',
			            bg:'#AE81FF',
			            time:3
			        });
					
					$("#em").focus();
					return false;
				}else if(!checkMobile()){       
					$("#em").tips({
						side:2,
			            msg:'请输入正确的手机号码',
			            bg:'#AE81FF',
			            time:3
			        });
					
					$("#em").focus();
					return false;
				}
		    	 else{
		    	var  a=$("#em").val();
			var url = "/sendSms?phone="+a;
		    $.get(url,function(data){
		     if(data=="success"){
		    	 alert("已发送验证码");
		    	 time(this);
		     }
		    }
		    );}
		    }
		    //验证电话  
		    function checkMobile() {  
		        var s=$("#em").val();
		        if (trim(s) != "") {  
		            var regu = /^[1][3-9][0-9]{9}$/;  
		            var re = new RegExp(regu);  
		            if (re.test(s)) {  
		                return true;  
		            } else {  
		                return false;  
		            }  
		        }  
		    } 
		    
		    function trim(str) { //删除左右两端的空格  
		        return str.replace(/(^\s*)|(\s*$)/g, "");  
		    }  
		    
</script>

<script type="text/javascript">
    var sab = document.getElementById("send_again_btn").disabled=false;
    var wait=60;
    function time(o) {

        if (wait == 0) {
            o.disabled=false;
            o.value="免费获取验证码";
            o.style.backgroundColor="#4bacf1";
            wait = 60;
        } else {
            o.disabled=true;
            o.value="重发验证码(" + wait + ")";
            o.style.backgroundColor="#999";
            wait--;
            window.setTimeout(function() {
                        time(o)
                    },
                    1000);
        }

    }

    function test(){
        var aa= document.getElementById("send_again_btn");
        time(aa);
    }

</script>

</body>
</html>