<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
<meta content="height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" name="viewport" />
	<link href="<%=path%>/resources/css/category.css" rel="stylesheet" />
	<link href="<%=path%>/resources/css/appZhuan/zong_ci.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/zongci/zhuan.css" rel="stylesheet" />
    
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
     
	<script src="<%=path%>/resources/js/zongci/zhuan.js"></script>
	<script src="<%=path%>/resources/js/appzongci/zong_ci.js?a=121314"></script>

</head>
<body>	
<div id="jzdiv">
  <c:forEach items="${list}" var="obj" varStatus="j">
    <ul> 
      <li class="list_1"><img src="<%=path%>/resources/${obj.donation_info.donation_nick_photo}"></li> 
      <li class="list_2"> 
        <div class="jz_people_name">${obj.donation_info.donation_nick_name}</div> 
        <div class="jz_time">${obj.donation_time}</div> 
      </li> 
      <li class="list_3">${obj.donation_info.donation_fb}福币</li> 
    </ul> 
  </c:forEach>
  <c:if test="${list != null && fn:length(list) > 0}"> 
    <div class="pagination" style="padding-top: 0px;margin-top: 0px;height:30px;">
 	  <ff:page mhFrom="ff" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="webAncestralBz/queryJzListForApp"  />
	</div>
                
    <form id="ff" name="ff" method="post">
      <input type="hidden" name="ancestral_hall_obid" value="${ancestral_hall_obid}"/>
      <input type="hidden" name="pageNo" value="${page.pageNo}" />
      <input type="hidden" name="pageSize" value="${page.pageSize}" />
      <input type="hidden" name="pageCount" value="${page.pageCount}" />
    </form>
  </c:if>
</div>
</body>
</html>

