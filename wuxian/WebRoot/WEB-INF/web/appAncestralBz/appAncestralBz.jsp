<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
<meta content="height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" name="viewport" />
    <title>全球祭祀祈福网-宗祠词院</title>
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="<%=path%>/resources/css/category.css" rel="stylesheet" />
	<link href="<%=path%>/resources/css/appZhuan/zong_ci.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/zongci/zhuan.css" rel="stylesheet" />
    
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
     
	<script src="<%=path%>/resources/js/zongci/zhuan.js"></script>
	<script src="<%=path%>/resources/js/appzongci/zong_ci.js?a=121312"></script>
	
    <!--[if lt IE 9]>
    	<script src="/resources/index/js/html5shiv.min.js"></script>
    	<script src="/resources/index/js/respond.min.js"></script>
	<![endif]-->
	
	<script type="text/javascript">
		var isfirst=1;
		var ios=0;
		function fun_meta(h,issed){
			if(ios==1){
				var initial_scale=h/600;
				var minimum_scale=h/600;
				var maximum_scale=h/600;
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
				document.body.scrollLeft=(250);	
				screenH=h;
			}
			else{
			
				if(isfirst==1 || issed==1){
					var initial_scale=h/600;
					var minimum_scale=h/600;
					var maximum_scale=h/600;
					document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
					document.body.scrollLeft=(initial_scale*960/3);	
					screenH=h;
					isfirst=3;
				}
	 			else{
	 				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
	 			}
			}	
 		}		
		function showSec(){
			if(ios==1){
				fun_meta(screenH);
			}
			else{
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
			}
			
		}

		$(document).ready(function(){
	        
	        //loadUrlTrue() 加载成功   loadUrlFalse() 加载失败
	        var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				ios=1;
				loadUrlTrue();//ios用
			}
			else{
				window.qifu.loadUrlTrue();//Android用
			}
	        
		});
		
		//跳转布置，所有素材都能拖
    	function onqueryBz(){
    		$("#tabflag").val(2);//0 首次加载 1祭祀建园 2 布置
    		location.href="<%=path%>/webAncestralBz/queryAncestralBzOutsideForApp?ancestral_hall_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
    		fun_meta(screenH);
    	}
    	
    	//保存
    	function onSave(){
    		$("#tabflag").val(0);//0 首次加载 1祭祀建园 2 布置
    		var cemetery_id = $("#input_cemetery_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="ancestral_hall_id" type="hidden" value="' + cemetery_id + '" />';
			
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webAncestralBz/saveAncestralMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
    			location.href="<%=path%>/webAncestralBz/queryAncestralBzOutsideForApp?ancestral_hall_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
			});
			fun_meta(screenH);
    	}
    	
    	//弹出祭祀布置一级菜单
		function onShowJisiBz(flag){//1布置 2祭祀
			$("#tabflag").val(1);//0 首次加载 1祭祀建园 2 布置
			
			$("#back_flag").val(1);
			if(flag == 1){//布置
				$(".box_category_bz").css({display:"block"});
				$(".box_category_jisi").css({display:"none"});
			}else{//祭祀
				$(".box_category_jisi").css({display:"block"});
				$(".box_category_bz").css({display:"none"});
			}
// 			$(".jisuan").css({display:"block"});
			$("#main").css({display:"none"});
			
			if(flag == 1){
				$("#ff_type_flag").val(3);
			}else{
				$("#ff_type_flag").val(2);
			}
			showSec();
		}
		
		//弹出祭祀布置二级菜单
		function onShowMaterialsClass2(materials_class_id){
			var src = "/webCemetery/queryMaterialsListForApp?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);
        	$(".second_menu").css({display:"block"});
        	
        	$("#back_flag").val(2);
		}    
		
		function onShowMaterialsDiv(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id, materials_small_img, parent_id){
			var vipcount = $('#vipcount').val();
			var str = '<input type="hidden" id="materials_id_1" value="' + materials_id + '" />';
			var str = str + '<input type="hidden" id="materials_name_1" value="' + materials_name + '" />';
			var str = str + '<input type="hidden" id="img_url_1" value="' + img_url + '" />';
			var str = str + '<input type="hidden" id="materials_family_1" value="' + materials_family + '" />';
			var str = str + '<input type="hidden" id="materials_wealth_1" value="' + materials_wealth + '" />';
			var str = str + '<input type="hidden" id="materials_measured_1" value="' + materials_measured + '" />';
			var str = str + '<input type="hidden" id="materials_price_1" value="' + materials_price + '" />';
			var str = str + '<input type="hidden" id="materials_class_id_1" value="' + materials_class_id + '" />';
			var str = str + '<input type="hidden" id="parent_id_1" value="' + parent_id + '" />';
			var str = str + '<input type="hidden" id="materials_small_img_1" value="' + materials_small_img + '" />';
			var str = str + '<input type="hidden" id="materials_measured_value_1" value="1" />';
			var str = str + '<input type="hidden" id="materials_count_1" value="1" />';
			var str = str + '<input type="hidden" id="price_1" value="' + materials_price + '" />';
			str = str + '<div class="box_top">';
			str = str + materials_name + '<img src="<%=path%>/resources/images/image1/X.png" class="end" onclick="onHideThree()">';
            str = str + '</div>';
            str = str + '<ul class="content">';
            str = str + '<li class="box_img"><img src="<%=path%>/goods' + materials_small_img + '"></li>';
            str = str + '<li class="box_text">';
            str = str + '<ul>';
            
            str = str + '<li>价格';
            
            if(materials_measured == 1){
            	str = str + '<span>' + materials_price + '福币/天</span>';
            }else if(materials_measured == 2){
             	str = str + '<span>' + materials_price + '福币/月</span>';
            }else if(materials_measured == 3){
             	str = str + '<span>' + materials_price + '福币/年</span>';
            }else{
             	str = str + '<span>' + materials_price + '福币/-</span>';
            }
            
            str = str + '</li>';
            
            if(parent_id != 2){
				str = str + '<li>时效';
				if(materials_measured == 1){
	            	str = str + '<span>天</span>';
	            }else if(materials_measured == 2){
	             	str = str + '<span>月</span>';
	            }else if(materials_measured == 3){
	             	str = str + '<span>年</span>';
	            }else{
	             	str = str + '<span>-</span>';
	            }
	            str = str + '<button class="number" onclick="onAddDate1(1)">+</button>';
	            str = str + '<button class="number" id="div_date_count_1">1</button>';
	            str = str + '<button class="number" onclick="onAddDate1(-1)">-</button>';
	            str = str + '</li>';
            }
			str = str + '<li>数量';
            str = str + '<span>个</span>';
            str = str + '<button class="number" onclick="onAddCount1(1)" >+</button>';
            str = str + '<button class="number" id="div_number_sum_1">1</button>';
            str = str + '<button class="number" onclick="onAddCount1(-1)">-</button>';
            str = str + '</li>';
            str = str + '<li>原价<span id="span_pay_price_1">' + materials_price + '福币</span></li>';
            
            
            
            str = str + '<li>会员价<span id="span_pay_vipprice_1">' + Math.floor(materials_price * vipcount) + '福币</span></li>';
            
            
            
            str = str + '</ul>';
            str = str + '</li>';
            str = str + '<div class="clear"></div>';
            str = str + '</ul>';
            str = str + '<div class="box_button">';
            str = str + '<button class="button" class="quxiao" onclick="onHideThree()" >取消</button>';
            str = str + '<button class="button" onClick="onSaveMaterial();">确定</button>';
            str = str + '</div>';
			$("#Layer2").html(str);
			
			$(".second_menu2").css({display:"block"});
			$(".bg_content,.box_content").css({display:"block"});
			
			$("#back_flag").val(3);
			showSec();
		}
		
		//添加减少数量
		function onAddCount1(flag){
			var materials_class_id = $("#materials_class_id_1").val();
    		if(materials_class_id == 10){
    			alert("背景只能买一个！");
    		}else{
    			var materials_count = $("#div_number_sum_1").text();
	    		if(flag == -1 && materials_count == 1){
	    			return false;
	    		}else{
	    			materials_count = materials_count * 1 + flag * 1;
	    		}
	    		$("#div_number_sum_1").text(materials_count);
	    		$("#materials_count_1").val(materials_count);
	    		
	    		var div_date_count_1 = $("#div_date_count_1").text();
	    		if(div_date_count_1 == null || div_date_count_1 == '' || div_date_count_1 == 'undefined'){
	    			div_date_count_1 = 1;
	    		}
	    		$("#span_pay_price_1").text((materials_count * $("#price_1").val() * div_date_count_1)+'福币');
	    		$("#span_pay_vipprice_1").text( Math.floor((materials_count * $("#price_1").val() * div_date_count_1) * $('#vipcount').val())+'福币');
	    	
    		}
    	}
    	
    	//时效更改
    	function onAddDate1(flag){
    		var materials_measured_value = $("#div_date_count_1").text();
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#div_date_count_1").text(materials_measured_value);
    		$("#materials_measured_value_1").val(materials_measured_value);
    		$("#span_pay_price_1").text((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text())+'福币');
    	    $("#span_pay_vipprice_1").text( Math.floor((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text()) * $('#vipcount').val())+'福币');
    	}
   
   		function onSaveMaterial(){
   			//背景单选
   			var arrTab=$('.class_materials');
   			var materials_class_id = $("#materials_class_id_1").val();
    		if(materials_class_id == 10){
    			for(var i=0;i<arrTab.length;i++){
					if($(arrTab[i]).attr("materials_class_id") == 10 ){
						$(arrTab[i]).remove();
					}
	            }
    		}
   			var temp = $(".class_materials");
            $("#Form", parent.document).append('<input type="hidden" name="materials_id" class="class_materials" '+
            'id="input_materials_id_'+temp.length+'_' + $('#materials_id_1').val() + '" '+
            'value="' + $('#materials_id_1').val() + '" '+
            'materials_name="' + $('#materials_name_1').val() + '" '+
            'img_url="' + $('#img_url_1').val() + '" '+
            'materials_family="' + $('#materials_family_1').val() + '" '+
            'materials_wealth="' + $('#materials_wealth_1').val() + '" '+
            'materials_measured="' + $('#materials_measured_1').val() + '" '+
            'materials_measured_value="' + $('#materials_measured_value_1').val() + '" '+
            'materials_count="' + $('#materials_count_1').val() + '" '+
            'materials_price_sum="' + $('#materials_price_1').val() + '" '+
            'materials_price="' + $('#materials_price_1').val() + '" '+
            'ifCheck="1" materials_class_id="' + $('#materials_class_id_1').val() + '" '+
            'parent_id = "' + $('#parent_id_1').val() + '" />' );
   			var class_materials = $(".class_materials");
//    			$(".jisuan").text("结算(" + class_materials.length + ")");
   			$(".second_menu2").css({display:"none"});
   			$(".bg_content,.box_content").css({display:"none"});
   			
   			//直接调结算方法
   			onSaveMaterials();
   		}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    	
    		$('.jisuan').hide();
    		
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<ul>';
    		
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") * 1;
    			}
    			str = str + '<li>';
                str = str + '<div class="check_btn" ><img src="<%=path%>/resources/images/check_pre.png" class="img_check" img_flag_info = "1" onclick="onChooseCheck(this)" img_materials_price_sum_info = '+$(arrTab[i]).attr("materials_price_sum")+'></div>';
                str = str + '<div class="check_img"><img src="' + $(arrTab[i]).attr("img_url") + '"></div>';
                str = str + '<div class="img_message">';
                str = str + '<div class="img_name">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<div class="img_money" id="div_sum_price_' + $(arrTab[i]).val() + '">小计' + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") + '福币</div>';
                str = str + '</div>';
                str = str + '<div class="img_time">';
                alert($(arrTab[i]).attr("parent_id"));
                if($(arrTab[i]).attr("parent_id") != 2){
	                str = str + '<div class="img_time_first">';
	                str = str + '<span class="time_title">时效</span>';
	                str = str + '<span onclick="onAddDate(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
	                str = str + '<span id="div_date_count_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</span>';
	                str = str + '<span onclick="onAddDate(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<span class="time_title">天</span>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<span class="time_title">月</span>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<span class="time_title">年</span>';
	                }else{
	                	str = str + '<span class="time_title"> - </span>';
	                }
	                str = str + '</div>';
                }
                
                str = str + '<div class="img_time_cecond">';
                str = str + '<span class="time_title">数量</span>';
                
                str = str + '<span onclick="onAddCount(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
                str = str + '<span id="div_number_sum_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</span>';
                if($(arrTab[i]).attr("materials_class_id") == 10){//背景
                	str = str + '<span onclick="alert(\'背景只能买一个！\');">+</span>';
                }else{
                	str = str + '<span onclick="onAddCount(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
                }
                
                str = str + '<span class="time_title">只</span>';
                str = str + '</div>';
                str = str + '</div>';
                str = str + '</li>';
            }
            fb_sum = fb_sum * 1
            str = str + '</ul>';
			str = str + '<div class="sum_foot">';
            str = str + '<div class="accounts_footer"></div>';
            str = str + '<div class="accounts_footer_cont">';
            str = str + '<div class="footer_first">';
            str = str + '<div class="all_check"><img src="<%=path%>/resources/images/footer_check_pre.png" onclick="onChooseAllCheck(this);" img_all_flag_info = "1"></div>';
            str = str + '<div>全选</div>';
            str = str + '</div>';
            str = str + '<div class="footer_second">总计：</div>';
            str = str + '<div class="footer_money" id="div_footer_money">' + fb_sum + '</div>';
            str = str + '<div class="footer_danwei">福币</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="footer_queding">补充福币</div>';
            }else{
            	str = str + '<div class="footer_queding" onClick="onSaveMaterials();">确定</div>';
            }
            str = str + '<div class="footer_number" id="div_footer_number">共计' + arrTab.length + '件</div>';
            str = str + '</div>';
            str = str + '</div>';
            
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
			$(".sum_bg,.sum_content").attr("style","display: block; ");
			$("#back_flag").val(4);
			showSec();
    	}
    	
    	//弹出结算素材弹出窗
    	function onShowJs(){
    		$.ajax({
				type: "POST",
				url: '/webCemetery/queryUserFb',
				dataType:'json',
				cache: false,
				success: function(data){
					onShowJsDiv(data);
				}
			});
			showSec();
    	}
    
    	//添加减少时效
    	function onAddDate(flag, i, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_"+i+"_"+ materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    		showSec();
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, i, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    		showSec();
    	}
    	
    	//选择按钮
    	function onChooseCheck(bg){
    		var img_materials_price_sum_info = 0;
    		var img_flag_info = $(bg).attr("img_flag_info");
    		if(img_flag_info == 1){
    			$(bg).attr("src","<%=path%>/resources/images/check_n.png");
    			$(bg).attr("img_flag_info", "0");
    		}else{
    			$(bg).attr("src","<%=path%>/resources/images/check_pre.png");
    			$(bg).attr("img_flag_info", "1");
    		}
    		var img_check = $(".img_check");
    		var count = 0;
    		for(var i=0;i<img_check.length;i++){
    			if($(img_check[i]).attr("img_flag_info") == 1){
    				count = count + 1;
    				img_materials_price_sum_info = img_materials_price_sum_info + $(img_check[i]).attr("img_materials_price_sum_info") * 1;
    			}
    		}
    		$("#div_footer_number").text("共计"+count+"件");
    		$("#div_footer_money").text(img_materials_price_sum_info);
    	}
    	
    	//全选
    	function onChooseAllCheck(bg){
    		var img_all_flag_info = $(bg).attr("img_all_flag_info");
    		var img_check = $(".img_check");
    		if(img_all_flag_info == 1){
    			$(bg).attr("img_all_flag_info", 0);
    			$(bg).attr("src","<%=path%>/resources/images/footer_check_n.png");
    			$("#div_footer_number").text("共计0件");
    			$("#div_footer_money").text("0");
    			for(var i=0;i<img_check.length;i++){
	    			$(img_check[i]).attr("img_flag_info", 0);
	    			$(img_check[i]).attr("src","<%=path%>/resources/images/check_n.png");
	    		}
    		}else{
    			$(bg).attr("img_all_flag_info", 1);
    			$(bg).attr("src","<%=path%>/resources/images/footer_check_pre.png");
    			$("#div_footer_number").text("共计"+img_check.length+"件");
    			var img_materials_price_sum_info = 0;
    			for(var i=0;i<img_check.length;i++){
	    			$(img_check[i]).attr("img_flag_info", 1);
	    			$(img_check[i]).attr("src","<%=path%>/resources/images/check_pre.png");
	    			img_materials_price_sum_info = img_materials_price_sum_info + $(img_check[i]).attr("img_materials_price_sum_info") * 1;
	    		}
	    		$("#div_footer_money").text(img_materials_price_sum_info);
    		}
    	}
    	
    	//关闭结算
    	function onCloseSumContent(){
    		$(".sum_bg,.sum_content").attr("style","display: none; ");
    		$(".jisuan").css({display:"block"});
    		$("#back_flag").val(2);
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="ancestral_hall_id" value="' + $("#input_cemetery_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#ff_type_flag").val() + '"/><input type="hidden" name="hall_materials_type" value="1"/>';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webAncestralBz/saveAncestralHallMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
    			location.href="<%=path%>/webAncestralBz/queryAncestralBzOutsideForApp?ancestral_hall_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	//APP端返回
    	function onCloseJisiBz(){//1有弹出层 0没有弹出层
			var back_flag = $("#back_flag").val();
			if(back_flag == 1){//一层
				$(".box_category_bz").css({display:"none"});
				$(".box_category_jisi").css({display:"none"});
				$(".jisuan").css({display:"none"});
				$("#main").css({display:"block"});
				$("#back_flag").val(0);
				fun_meta(screenH,1);
			}else if(back_flag == 2){//二层
				$(".second_menu").css({display:"none"});
				$("#back_flag").val(1);
				fun_meta(screenH);
			}else if(back_flag == 3){//三层
				$(".second_menu2").css({display:"none"});
				$(".bg_content,.box_content").css({display:"none"});
				$("#back_flag").val(2);
				fun_meta(screenH);
			}else if(back_flag == 4){//结算小结层
				$(".sum_bg,.sum_content").attr("style","display: none; ");
				$("#back_flag").val(2);
				fun_meta(screenH);
			}else if(back_flag == 5){//文字层
				$("#show").attr("style","display: none; ");
				$(".bg_wz,.show_wz").attr("style","display: none; ");
				$("#back_flag").val(1);
				fun_meta(screenH);
			}else if(back_flag == 10){//捐赠层
				$("#main").css({display:"block"});
	    		$(".juan_zeng1").hide();
				$("#back_flag").val(1);
				fun_meta(screenH,1);
			}else{
				back_flag = 0;
				$("#back_flag").val(0);
				
				var u = navigator.userAgent;
				var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
				var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
				if(isiOS){
					back();//ios用，用于返回原生页面
				}
				else{
					window.qifu.back();//Android用，用于返回原生页面
				}
				showSec();
			}
		}
		
    	//弹出买砖弹出窗
    	function onShowMaizhuan(){
    		$(".choose_zuo_wei").css({display:"block"});
        	$(".footer_menu,.main").css({display:"none"});
        	showSec();
    	}
    	
    	//相册
	    function onShowXC(ancestral_name, ancestral_hall_obid, isPermission){
	    	var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				anc_photo(ancestral_name, ancestral_hall_obid, isPermission);//ios用
			}
			else{
				window.qifu.anc_photo(ancestral_name, ancestral_hall_obid, isPermission);//Android用
			}
	    }
	    
	    //档案（家族历史）
	    function onShowDA(surname_origin, family_instructions, big_event, ancestral_hall_obid, isPermission){
	   // alert(surname_origin+"--"+family_instructions+"--"+ big_event+"--"+ ancestral_hall_obid+"--"+ isPermission);
	    	var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			
			if(isiOS){
				anc_history(surname_origin, family_instructions, big_event, ancestral_hall_obid, isPermission);//ios用
			}
			else{
				window.qifu.anc_history(surname_origin, family_instructions, big_event, ancestral_hall_obid, isPermission);//Android用
			}
	    }
	        
	    //族谱
	    function onShowZP(name, _id, isPermission, myid){
	    	var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				anc_hall(name, _id, isPermission, myid);//ios用
			}
			else{
				window.qifu.anc_hall(name, _id, isPermission, myid);//Android用
			}
	    }
	    
	    //弹出捐赠
	    function onqueryJuanZeng(){
// 		    $("#jzIframe").attr("src","<%=path%>/webAncestralBz/queryJzListForApp?ancestral_hall_obid=${ancestralHall._id}");
	    	$("#back_flag").val(10);
 			$("#jzform_donation_fb").val(0);
	    	$("#main").css({display:"none"});
	    	$(".juan_zeng1").show();
	    }
	    
	    //保存捐赠
	    function onSaveJuanZeng(){
	    	var jzform_donation_fb = $("#jzform_donation_fb").val();
	    	if(jzform_donation_fb == 0 || jzform_donation_fb == '' || jzform_donation_fb == null){
	    		alert("请填写捐赠福币数！");
	    	}else{
	    		document.jzform.action = '<%=path%>/webAncestralBz/saveAncestralHallDonationApp';
				$("#jzform").ajaxSubmit(function(result){
					if(result == 1){
						$("#main").css({display:"block"});
	    				$(".juan_zeng1").hide();
					}else{
						alert("余额不足，跳转支付页面！");
					}
				});
	    	}
	    	fun_meta(screenH,1);
	    }
	    
	    //隐藏第三层
    	function onHideThree(){
    		$(".second_menu2").css({display:"none"});
			$(".bg_content,.box_content", parent.document).css({display:"none"});
			$("#back_flag").val(2);
			fun_meta(screenH);
    	}

    	
    </script>

</head>
<body>

<input type="hidden" id="vipcount" value="${vipcount == null ? 1 : vipcount}" />
<input type="hidden" id="choose_img_cemetery_materials_id" value=""/>
<input type="hidden" id="choose_img_materials_id" value=""/>
<input type="hidden" id="tabflag" value="${tabflag}" /> <!-- 0 首次加载 1祭祀建园 2 布置  -->
<input type="hidden" id="input_cemetery_id" value="${ancestral_hall_id}"/>
<input type="hidden" id="back_flag" value="0"/>
<form method="post" name="saveForm" id="saveForm">
</form>
<form method="post" name="Form" id="Form"> 
</form>
<form method="post" name="ff" id="ff">
  <input type="hidden" id="ff_cemetery_materials_id"  />
  <input type="hidden" id="ff_cemetery_id"  />
  <input type="hidden" id="ff_cemetery_materials_show"  /><!-- 素材显示隐藏 -->
  <input type="hidden" id="ff_materials_id"  />
</form>
<div class="main" id="main">
    <!-- 墓园部分-->
    <div class="wdmy_wrap">
      <!-- 墓园放置物品-->
      <div class="srmy_main" id="srmy_main">
		<c:forEach var="obj" items="${sclist}" varStatus="status">
		  <c:if test="${obj.hall_materials_show == 0}">
			<img id="div${obj.hall_materials_id}"  
	        	 width_info="${obj.hall_materials_width}" 
	        	 height_info="${obj.hall_materials_height}" 
		         materials_info="${obj.hall_materials_id}"
		         ${((obj.sacrifice_user_id == user_id && obj.hall_materials_limit != 3) || (is_user_ancestral_flag == 1 && tabflag == 2) ) ? 'class="srmy_FixedAdorn"' : ''} 		         
		         data-name="img${status.index+1}" 
		         data-id="url${status.index+1}" 
		         title="${obj.materials_name}-${obj.materials_class_id}" 
		         style="position:absolute;z-index:${obj.hall_materials_z_index};left:${obj.hall_materials_x}px;top:${obj.hall_materials_y}px;width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;${obj.sacrifice_user_id == user_id && obj.hall_materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
		         src="<%=path%>/goods/${obj.materials_big_img}" name="1" 
		         materials_id_info = "${obj.materials_id}"/>
			     
		  </c:if>
		</c:forEach>
      </div>
      <div id="zc_goods" style="width: 100%;">
        <img src="/resources/images/zong_ci_home.png" id="zc_house" style="width: 100%" />
      </div>
      <div class="zhuan_content">
        <c:choose >
      	  <c:when test="${not empty ancestralHall.gold_brick}">
      		<c:forEach items="${ ancestralHall.gold_brick}" var="golds" varStatus="vs">
      		  <c:if test="${golds.gold_brick_type==1 }">
             	<img src="<%=path%>/resources/images/img_zhuan/odd_${golds.gold_brick_number }.png" id="zhuan${golds.gold_brick_number }" ${tabflag == 2 ? 'onclick="onShowMaizhuan();"' : ''}/>
      		  </c:if>
      		  <c:if test="${golds.gold_brick_type==2 }">
             	<img src="<%=path%>/resources/images/img_zhuan/new_${golds.gold_brick_number }.png" id="zhuan${golds.gold_brick_number }" ${tabflag == 2 ? 'onclick="onShowMaizhuan();"' : ''}/>
      		  </c:if>
      		</c:forEach>
      	  </c:when>
      	</c:choose>
      </div>
    </div>
    
    
    


<c:choose>
  <c:when test="${tabflag == 1 || tabflag == 2 }">
    <div class="js_and_save"> 
  	  <div class="ji_si" onclick="onShowJisiBz(2);">祭祀</div>
      <c:if test="${is_user_ancestral_flag > 0}">
    	<div class="ji_yuan" onclick="onShowJisiBz(1);">建园</div>
    	<div class="bu_zhi" onclick="onqueryBz();">布置</div>
      </c:if>
      <div class="yk_save" onclick="onSave();">保存</div> 
    </div>
  </c:when>
  <c:otherwise>
    <div class="footer_menu"></div>
    <div class="gn_menu">
      <div onclick="onShowXC('${ancestralHall.ancestral_hall_surname}','${ancestralHall._id}','${is_user_ancestral_flag}');">相册</div>
      <c:if test="${is_user_ancestral_flag == 0}">
        <div class="dang_an" onclick="onShowDA('${ancestralHall.surname_origin}', '${ancestralHall.family_instructions}', '${ancestralHall.big_event}', '${ancestralHall._id}', '${is_user_ancestral_flag}');">档案</div>
      </c:if>
      <div class="zu_pu" onclick="onShowZP('${ancestralHall.ancestral_hall_surname}', '${ancestralHall._id}', '${is_user_ancestral_flag}', ${ancestral_hall_id});">族谱</div>
      <div class="ji_si" onclick="onShowJisiBz(2);">祭祀</div>
      <c:if test="${is_user_ancestral_flag > 0}">
        <div class="ji_yuan" onclick="onShowJisiBz(1);">建园</div>
        <div class="bu_zhi" onclick="onqueryBz();">布置</div>
      </c:if>
      <div class="juan_zeng" onclick="onqueryJuanZeng();">捐赠</div>
<!--       <div class="gong_de_lu">功德录</div> -->
<!--       <div class="ri_zhi">日志</div> -->
    </div>
  </c:otherwise>
</c:choose>
     
<!-- 布置墓园时，显示功能条和菜单选项  -->
  <div class="foot_bg"></div>
  <div class="buzhi_top_menu" id="buzhi_top_menu">
    <div class="buzhi_big" id="div_buzhi_big">
      <img src="<%=path%>/resources/images/buzhi_big.png" />
      <div>放大</div>
    </div>
    <div class="buzhi_small" id="div_buzhi_small">
      <img src="<%=path%>/resources/images/buzhi_small.png" />
      <div>缩小</div>
    </div>
    <div class="buzhi_before">
      <img src="<%=path%>/resources/images/buzhi_before.png" />
      <div>提前</div>
    </div>
    <div class="buzhi_after">
      <img src="<%=path%>/resources/images/buzhi_after.png" />
      <div>置后</div>
    </div>
    <div class="buzhi_delete" >
      <img src="<%=path%>/resources/images/buzhi_delete.png" />
      <div>删除</div>
    </div>
  </div>
  
  <div class="toggle_btn"><a href="<%=path%>/Ancestral/queryAncestralBzInsideForApp?ancestral_hall_id=${ancestral_hall_id}&user_id=${user_id}"></a></div>
  
  <!-- 布置墓园时，左侧的图层选项-->
  <div class="buzhi_left_list" ${tabflag == 2 ? 'style="display: block;"' : '' }> 
    <div class="buzhi_left_menu" ${tabflag == 2 ? 'style="display: block;"' : '' }>
      <div class="left_menu_title"></div>
    </div>
    <div class="srmy_fix_up">
      <ul id="SortContaint" class="fix_up">
        <c:forEach var="obj" items="${movelist}" varStatus="status"> 
          <li id="list${obj.hall_materials_id}" 
          	  class="cc" 
          	  draggable="true" 
              materials_info="${obj.hall_materials_id}" 
              cemetery_info="${obj.ancestral_hall_id}"  
              flag_info = '${obj.hall_materials_show}' 
              materials_class_info = "${obj.materials_class_id}" 
              materials_id_info = "${obj.materials_id}" > 
                  
            ${fn:length(obj.materials_name) > 5 ? fn:substring(obj.materials_name, 0, 5) : obj.materials_name}${fn:length(obj.materials_name) > 5 ? '...' : ''} 
          </li> 
   		</c:forEach> 
      </ul>
    </div>
  </div>
  
  <!-- 半全景切换图-->
    
    <div class="qj_img">
        <img src="<%=path%>/goods/${ancestralHall.ancestral_hall_external_img}" />
    </div>
    <div class="quan_jing_btn"></div>
    <div class="xiang_xi_btn"></div>
  
 </div> 
  <!-- 一级菜单-->
  <div class="box_category_jisi" id="box_category_jisi">
    <ul class="category first_menu">
      <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
        <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
          <div>
            <img src="<%=path%>/goods/${var.materials_class_img}" class="img" />
            <span class="span">${var.materials_class_name}</span>
          </div>
        </li>
      </c:forEach>
      <div class="clear"></div>
    </ul>
  </div>

  <div class="box_category_bz">
    <ul class="category first_menu">
      <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
        <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
          <div>
            <img src="<%=path%>/goods/${var.materials_class_img}" class="img" />
            <span class="span">${var.materials_class_name}</span>
          </div>
        </li>
      </c:forEach>
<!--       <li onclick = "onShowWZ();"><div><img src="<%=path%>/resources/images/text.jpg" class="img" /><span class="span">文字</span></div></li> -->
      <div class="clear"></div>
    </ul>
  </div>
  <!--  祭祀 -> 二级菜单  -->
  <div class="second_menu">
    <div id="category2">
      <iframe src=""  iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
    </div>
  </div>

  <div class="second_menu2">
    <div id="category3">
      <div id="ly" class="bg_content"></div>
      <div id="Layer2" class="box_content"></div>
    </div>
  </div>    


<!--捐赠-->
<div class="juan_zeng1">
    <div class="juan_zeng_money">
      <form method="post" name="jzform" id="jzform">
        <input type="hidden" name="ancestral_hall_obid" value="${ancestralHall._id}" />
        <input type="hidden" name="user_id" value="${user_id}" />
        <div class="juan_zeng_text"><input type="number" name="donation_fb" id="jzform_donation_fb" value="0" placeholder="&nbsp;请输入" /></div>
        <div class="juan_zeng_btn" onclick="onSaveJuanZeng();">捐 赠</div>
      </form>
    </div>
    
    <div class="jz_title">捐赠金额单位为福币</div>
    <iframe  src="/webAncestralBz/queryJzListForApp?ancestral_hall_obid=${ancestralHall._id}" id="jzIframe" name="jzIframe"  style="width:100%;height:600px;background-color:white;"  iframeborder="no" ></iframe>
</div>



  <!--弹出购买 砖 层-->
  <div class="choose_zuo_wei" id="choose_zuo_wei">
    <div class="jz_money">
      <div class="jz_money_title">捐赠金额：</div>
      <div class="jz_momey_num"><span id="zongshu">${ancestralHall.ancestral_hall_fb_balance }</span>福币</div>
    </div>
    <div class="pay_sum">
      <div class="pay_title">消费总计：</div>
      <div class="pay_num"><span id="sumfb">0</span>福币</div>
    </div>
    <div class="cont_title">请选择您要装饰的砖块 <span>（1000福币/块）</span></div>
    <div class="cont_intro">
      <div>
        <div class="intro_first"></div>
        <div>&nbsp;&nbsp;可选</div>
      </div>
      <div>
        <div class="intro_second"><img src="<%=path%>/resources/images/zongci/zc_choose.png"></div>
        <div>&nbsp;&nbsp;已选</div>
      </div>
      <div>
        <div class="intro_third"></div>
        <div>&nbsp;&nbsp;已装饰</div>
      </div>
    </div>

    <div class="cont_container_title">祠&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;堂</div>

    <div class="all_zhuan">
      <c:choose >
      <c:when test="${not empty ancestralHall.gold_brick}">
      <c:forEach items="${ ancestralHall.gold_brick}" var="golds" varStatus="vs">
      <c:if test="${golds.gold_brick_number==1 }">
         <div class="zhuan_hang_1 zhuan_hang">
      </c:if>
      <c:if test="${golds.gold_brick_number==11 }">
          </div>
          <div class="zhuan_hang_2 zhuan_hang">
        </c:if>
        <c:if test="${golds.gold_brick_number==22 }">
          </div>
          <div class="zhuan_hang_3 zhuan_hang">
        </c:if>
        <c:if test="${golds.gold_brick_number==32 }">
          </div>
          <div class="zhuan_hang_4 zhuan_hang">
        </c:if> 
        <c:if test="${golds.gold_brick_number==43 }">
          </div>
          <div class="zhuan_hang_5 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_number==53 }">
          </div>
          <div class="zhuan_hang_6 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_number==64 }">
          </div>
          <div class="zhuan_hang_7 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_number==74 }">
          </div>
          <div class="zhuan_hang_8 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_number==85 }">
          </div>
          <div class="zhuan_hang_9 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_number==95 }">
          </div>
          <div class="zhuan_hang_10 zhuan_hang">
        </c:if> 
        <c:if test="${golds.gold_brick_number==106 }">
          </div>
          <div class="zhuan_hang_11 zhuan_hang">
        </c:if> 
        <c:if test="${golds.gold_brick_number==116 }">
          </div>
          <div class="zhuan_hang_12 zhuan_hang">
        </c:if> 
        <c:if test="${golds.gold_brick_number==125 }">
          </div>
          <div class="zhuan_hang_13 zhuan_hang">
        </c:if> 
        <c:if test="${golds.gold_brick_number==133 }">
          </div>
          <div class="zhuan_hang_14 zhuan_hang">
        </c:if>    
        <c:if test="${golds.gold_brick_number==142 }">
          </div>
          <div class="zhuan_hang_15 zhuan_hang">
        </c:if>  
        <c:if test="${golds.gold_brick_type==1 }">
          <div id="zz${golds.gold_brick_number }"  class="zh"></div>
        </c:if>
        <c:if test="${golds.gold_brick_type==2 }">
          <div id="zz${golds.gold_brick_number }" ></div>
        </c:if>
        <c:if test="${golds.gold_brick_number==149 }">
          </div>
        </c:if>  
      </c:forEach>
    </c:when>
  </c:choose>
  
  
  <div class="buy_zhuan_btn"   onclick="submitJinZhuan();">购买</div>

  <form action="/webAncestralBz/jinzhuanForApp" id="jinzhuan" method="post">
    <input type="hidden" name="zongjine"  id="zongjine"/>
    <input type="hidden" name="ancestral_hall_id" value="${ancestral_hall_id}"/>
    <input type="hidden" name="user_id" value="${user_id}"/>
  </form>
</div>






</body>
</html>
