<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
<meta content="height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" name="viewport" />
    <title>全球祭祀祈福网-私人墓园</title>
    
    <link href="<%=path%>/resources/css/category.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/cemetery/appsrmy.css" rel="stylesheet" />
    
    
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script src="<%=path%>/resources/js/jquery/jquery.promptu-menu.js"></script>
    <script src="<%=path%>/resources/js/second_menu.js"></script>
    <script src="<%=path%>/resources/js/cemetery/appsrmy.js"></script>
    
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	
	<script type="text/javascript">	
		var isfirst=1;
		var ios=0;
		function fun_meta(h,issed){
			if(ios==1){
				var initial_scale=h/600;
				var minimum_scale=h/600;
				var maximum_scale=h/600;
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
				document.body.scrollLeft=(initial_scale*960/3);	
				screenH=h;
			}
			else{
				if(isfirst==1 || issed==1){
					var initial_scale=h/600;
					var minimum_scale=h/600;
					var maximum_scale=h/600;
					document.getElementsByName("viewport")[0].content = "width=10,initial-scale="+initial_scale+",minimum-scale="+minimum_scale+",maximum-scale="+maximum_scale+", user-scalable=no";
					document.body.scrollLeft=(initial_scale*960/3);	
					screenH=h;
					isfirst=2;
				}
	 			else{
	 				
	 				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
	 			}
			}	
 		}		
		function showSec(){
			if(ios==1){
				fun_meta(screenH);
			}
			else{
				document.getElementsByName("viewport")[0].content = "width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no";
			}
			
		}
        $(document).ready(function(){
        
	        $("#div_buzhi_change").hide();
	        
	        //loadUrlTrue() 加载成功   loadUrlFalse() 加载失败
	        var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				ios=1;
				loadUrlTrue();//ios用
			}
			else{
				window.qifu.loadUrlTrue();//Android用
			}
	        
		});
        
		//弹出祭祀布置一级菜单
		function onShowJisiBz(flag){//1布置 2祭祀
			
			$("#tabflag").val(1);//0 首次加载 1祭祀建园 2 布置
			
			$("#back_flag").val(1);
			
			if(flag == 1){//布置
				$(".box_category_bz").css({display:"block"});
				$(".box_category_jisi").css({display:"none"});
			}else{//祭祀
				$(".box_category_jisi").css({display:"block"});
				$(".box_category_bz").css({display:"none"});
			}
			$("#main").css({display:"none"});
			
			if(flag == 1){
				$("#ff_type_flag").val(3);
			}else{
				$("#ff_type_flag").val(2);
			}
			showSec();
		}
		
		//APP端返回
		function onCloseJisiBz(){//1有弹出层 0没有弹出层
			var back_flag = $("#back_flag").val();
			
			if(back_flag == 1){//一层
				$(".box_category_bz").css({display:"none"});
				$(".box_category_jisi").css({display:"none"});
				$(".jisuan").css({display:"none"});
				$("#main").css({display:"block"});
				$("#back_flag").val(0);
				fun_meta(screenH,1);
			}else if(back_flag == 2){//二层
				$(".second_menu").css({display:"none"});
				$("#back_flag").val(1);
				fun_meta(screenH);
			}else if(back_flag == 3){//三层
				$(".second_menu2").css({display:"none"});
				$(".bg_content,.box_content").css({display:"none"});
				$("#back_flag").val(2);
				fun_meta(screenH);
			}else if(back_flag == 4){//结算小结层
				$(".sum_bg,.sum_content").attr("style","display: none; ");
				$("#back_flag").val(2);
				fun_meta(screenH);
			}else if(back_flag == 5){//文字层
				$("#show").attr("style","display: none; ");
				$(".bg_wz,.show_wz").attr("style","display: none; ");
				$("#back_flag").val(1);
				fun_meta(screenH);
			}else{
				back_flag = 0;
				$("#back_flag").val(0);
				
				var u = navigator.userAgent;
				var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
				var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
				if(isiOS){
					back();//ios用，用于返回原生页面
				}else{
					window.qifu.back();//Android用，用于返回原生页面
				}
				showSec();
			}
		}
		
		
		//弹出祭祀布置二级菜单
		function onShowMaterialsClass2(materials_class_id){
			var src = "/webCemetery/queryMaterialsListForApp?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);			
			
        	$(".second_menu").css({display:"block"});
        	
        	$("#back_flag").val(2);

		}    
		function onShowMaterialsDiv(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id, materials_small_img, parent_id){
			var vipcount = $('#vipcount').val();
			var str = '<input type="hidden" id="materials_id_1" value="' + materials_id + '" />';
			var str = str + '<input type="hidden" id="materials_name_1" value="' + materials_name + '" />';
			var str = str + '<input type="hidden" id="img_url_1" value="' + img_url + '" />';
			var str = str + '<input type="hidden" id="materials_family_1" value="' + materials_family + '" />';
			var str = str + '<input type="hidden" id="materials_wealth_1" value="' + materials_wealth + '" />';
			var str = str + '<input type="hidden" id="materials_measured_1" value="' + materials_measured + '" />';
			var str = str + '<input type="hidden" id="materials_price_1" value="' + materials_price + '" />';
			var str = str + '<input type="hidden" id="materials_class_id_1" value="' + materials_class_id + '" />';
			var str = str + '<input type="hidden" id="parent_id_1" value="' + parent_id + '" />';
			var str = str + '<input type="hidden" id="materials_small_img_1" value="' + materials_small_img + '" />';
			var str = str + '<input type="hidden" id="materials_measured_value_1" value="1" />';
			var str = str + '<input type="hidden" id="materials_count_1" value="1" />';
			var str = str + '<input type="hidden" id="price_1" value="' + materials_price + '" />';
			str = str + '<div class="box_top">';
			str = str + materials_name + '<img src="<%=path%>/resources/images/image1/X.png" class="end" onclick="onHideThree()">';
            str = str + '</div>';
            str = str + '<ul class="content">';
            str = str + '<li class="box_img"><img src="<%=path%>/goods' + materials_small_img + '"></li>';
            str = str + '<li class="box_text">';
            str = str + '<ul>';
            
            str = str + '<li>价格';
            
            if(materials_measured == 1){
            	str = str + '<span>' + materials_price + '福币/天</span>';
            }else if(materials_measured == 2){
             	str = str + '<span>' + materials_price + '福币/月</span>';
            }else if(materials_measured == 3){
             	str = str + '<span>' + materials_price + '福币/年</span>';
            }else{
             	str = str + '<span>' + materials_price + '福币/-</span>';
            }
            
            str = str + '</li>';
            
            
            
            if(parent_id != 1){
				str = str + '<li>时效';
				if(materials_measured == 1){
	            	str = str + '<span>天</span>';
	            }else if(materials_measured == 2){
	             	str = str + '<span>月</span>';
	            }else if(materials_measured == 3){
	             	str = str + '<span>年</span>';
	            }else{
	             	str = str + '<span>-</span>';
	            }
	            
	            str = str + '<button class="number" onclick="onAddDate1(1)">+</button>';
	            str = str + '<button class="number" id="div_date_count_1">1</button>';
	            str = str + '<button class="number" onclick="onAddDate1(-1)">-</button>';
	            str = str + '</li>';
            }
			str = str + '<li>数量';
            str = str + '<span>个</span>';
            str = str + '<button class="number" onclick="onAddCount1(1)" >+</button>';
            str = str + '<button class="number" id="div_number_sum_1">1</button>';
            str = str + '<button class="number" onclick="onAddCount1(-1)">-</button>';
            str = str + '</li>';
            str = str + '<li>原价<span id="span_pay_price_1">' + materials_price + '福币</span></li>';
            
             
            str = str + '<li>会员价<span id="span_pay_vipprice_1">' + Math.floor(materials_price * vipcount) + '福币</span></li>';
             
             
            str = str + '</ul>';
            str = str + '</li>';
            str = str + '<div class="clear"></div>';
            str = str + '</ul>';
            str = str + '<div class="box_button">';
            str = str + '<button class="button" class="quxiao" onclick="onHideThree()" >取消</button>';
            str = str + '<button class="button" onClick="onSaveMaterial();">确定</button>';
            str = str + '</div>';
			$("#Layer2").html(str);
			
			$(".second_menu2").css({display:"block"});
			$(".bg_content,.box_content").css({display:"block"});
			
			$("#back_flag").val(3);
			showSec();
		}
		
		//添加减少数量
		function onAddCount1(flag){
			var materials_class_id = $("#materials_class_id_1").val();
    		if(materials_class_id == 10){
    			alert("背景只能买一个！");
    		}else{
    			var materials_count = $("#div_number_sum_1").text();
	    		if(flag == -1 && materials_count == 1){
	    			return false;
	    		}else{
	    			materials_count = materials_count * 1 + flag * 1;
	    		}
	    		$("#div_number_sum_1").text(materials_count);
	    		$("#materials_count_1").val(materials_count);
	    		
	    		var div_date_count_1 = $("#div_date_count_1").text();
	    		if(div_date_count_1 == null || div_date_count_1 == '' || div_date_count_1 == 'undefined'){
	    			div_date_count_1 = 1;
	    		}
	    		$("#span_pay_price_1").text((materials_count * $("#price_1").val() * div_date_count_1)+'福币');
	    		$("#span_pay_vipprice_1").text( Math.floor((materials_count * $("#price_1").val() * div_date_count_1) * $('#vipcount').val())+'福币');
	    	
    		}
    	}
    	
    	//时效更改
    	function onAddDate1(flag){
    		var materials_measured_value = $("#div_date_count_1").text();
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#div_date_count_1").text(materials_measured_value);
    		$("#materials_measured_value_1").val(materials_measured_value);
    		$("#span_pay_price_1").text((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text())+'福币');
    		$("#span_pay_vipprice_1").text( Math.floor((materials_measured_value * $("#price_1").val() * $("#div_number_sum_1").text()) * $('#vipcount').val())+'福币');
    	}
   
   		function onSaveMaterial(){
   			//背景单选
   			var arrTab=$('.class_materials');
   			var materials_class_id = $("#materials_class_id_1").val();
    		if(materials_class_id == 10){
    			for(var i=0;i<arrTab.length;i++){
					if($(arrTab[i]).attr("materials_class_id") == 10 ){
						$(arrTab[i]).remove();
					}
	            }
    		}
    		
   			var temp = $(".class_materials");
            $("#Form", parent.document).append('<input type="hidden" name="materials_id" class="class_materials" '+
            'id="input_materials_id_'+temp.length+'_' + $('#materials_id_1').val() + '" '+
            'value="' + $('#materials_id_1').val() + '" '+
            'materials_name="' + $('#materials_name_1').val() + '" '+
            'img_url="' + $('#img_url_1').val() + '" '+
            'materials_family="' + $('#materials_family_1').val() + '" '+
            'materials_wealth="' + $('#materials_wealth_1').val() + '" '+
            'materials_measured="' + $('#materials_measured_1').val() + '" '+
            'materials_measured_value="' + $('#materials_measured_value_1').val() + '" '+
            'materials_count="' + $('#materials_count_1').val() + '" '+
            'materials_price_sum="' + $('#materials_price_1').val() + '" '+
            'materials_price="' + $('#materials_price_1').val() + '" '+
            'ifCheck="1" materials_class_id="' + $('#materials_class_id_1').val() + '" '+
            'parent_id = "' + $('#parent_id_1').val() + '" />' );
   			var class_materials = $(".class_materials");
    		
//    			$(".jisuan").text("结算(" + class_materials.length + ")");
   			$(".second_menu2").css({display:"none"});
   			$(".bg_content,.box_content").css({display:"none"});
   			
   			//直接调结算方法
   			onSaveMaterials();
   		}
   		
   		//删除素材
    	function onDelMaterials(){
    		var cemetery_materials_id = $("#ff_cemetery_materials_id").val();
    		var cemetery_id = $("#ff_cemetery_id").val();
            if(cemetery_materials_id != null && cemetery_materials_id != ''){
	    		$.ajax({
					type: "POST",
					url: '/webCemetery/delCemeteryMaterialsByCemeteryMaterialsId',
			    	data: {cemetery_materials_id : cemetery_materials_id, cemetery_id : cemetery_id},
					dataType:'json',
					cache: false,
					success: function(data){
						 if(data == 0){
						 	$(".check").remove();
							var namelist = $("li[class='cc']");
							for(var i=0; i<namelist.length; i++){
							 	if($(namelist[i]).attr("materials_info") == cemetery_materials_id){
							 		namelist[i].remove();
							 	}
							}
						}else{
							alert('操作失败，请联系管理员！');
						}
					}
				});
			}else{
    			alert("请选择图层！");
    		}
			
    	}
    	
    	
    	//控制素材显示隐藏
    	function onUpdMaterialsShow(){
    		var cemetery_materials_id = $("#ff_cemetery_materials_id").val();
    		var cemetery_id = $("#ff_cemetery_id").val();
    		var cemetery_materials_show = $("#ff_cemetery_materials_show").val();
    		
    		if(cemetery_materials_id != null && cemetery_materials_id != ''){
    			if(cemetery_materials_show == 1){//隐藏图标
	    			cemetery_materials_show = 0;
	    			$("#img_buzhi_hidden").attr("src", "<%=path%>/resources/images/buzhi_hidden.png");
	     			$("#div_buzhi_hidden").text("隐藏");
	    		}else{//显示图标
	    			cemetery_materials_show = 1;
	    			$("#img_buzhi_hidden").attr("src", "<%=path%>/resources/images/srmy_look.png");
	     			$("#div_buzhi_hidden").text("显示");
	    		}
	    		
	    		$.ajax({
					type: "POST",
					url: '/webCemetery/updateCemeteryMaterialsForCemeteryMaterialsShow',
			    	data: {cemetery_materials_id : cemetery_materials_id, cemetery_id : cemetery_id, cemetery_materials_show : cemetery_materials_show},
					dataType:'json',
					cache: false,
					success: function(data){
						if(data == 0){
							if(cemetery_materials_show == 0 ){//需要显示，的刷新页面
								 location.href="<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id=" + cemetery_id+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
								 
							}else{
								 $(".check").remove();
							}
							$("#ff_cemetery_materials_show").val(cemetery_materials_show);
							$("#div"+cemetery_materials_id).attr("flag_info", cemetery_materials_show);
						}else{
							alert('操作失败，请联系管理员！');
						}
					}
				});
    		}else{
    			alert("请选择图层！");
    		}
    	}
        
    	//保存
    	function onSave(){
    		$("#tabflag").val(0);//0 首次加载 1祭祀建园 2 布置
    		var cemetery_id = $("#input_cemetery_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			    
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="cemetery_id" type="hidden" value="' + cemetery_id + '" />';
			
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webCemetery/saveCemeteryMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
				location.href="<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
			});
			fun_meta(screenH);
    	}
    	
    	//显示弹出窗
    	function onShowWZ(){
    		
    		$("#show").attr("style","display: block; ");
			$(".bg_wz,.show_wz").attr("style","display: block; ");
			$("#back_flag").val(5);
 			showSec();
    	}
    	
    	//保存文字信息
    	function onQdWz(wz){
			$("#id_wz").val(wz);
			$("#id_cemetery_id").val("${cemetery_id}");
			document.fff.action = "<%=path%>/webCemetery/saveCemeteryMaterialsForWz";
			$("#fff").ajaxSubmit(function(result){
				location.href="<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
			});
    	}
    	
    	//隐藏第三层
    	function onHideThree(){
    		$(".second_menu2").css({display:"none"});
			$(".bg_content,.box_content", parent.document).css({display:"none"});
			$("#back_flag").val(2);
			fun_meta(screenH);
    	}
    	
    	//修改文字信息
    	function onChangeWz(){
    		var cemetery_materials_id = $("#ff_cemetery_materials_id").val();
    		var cemetery_id = $("#ff_cemetery_id").val();
    		var materials_id = $("#ff_materials_id").val();
    		
			$("#id_cemetery_id").val(cemetery_id);
			$("#id_materials_id").val(materials_id);
			$("#id_cemetery_materials_id").val(cemetery_materials_id);
			document.fff.action = "<%=path%>/webCemetery/queryWzByCemeteryMaterialsId";
			$("#fff").ajaxSubmit(function(result){
	    		$("#show").attr("style","display: block; ");
				$(".bg_wz,.show_wz").attr("style","display: block; ");
				$(window.parent.document).contents().find("#iframe_wz_id")[0].contentWindow.setWz(result.materials_big_img); 
				$("#id_materials_id").val(materials_id);
				
			});
    	}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    	
    		$('.jisuan').hide();
    		
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<ul>';
    		
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") * 1;
    			}
    			
    			str = str + '<li>';
                str = str + '<div class="check_btn" ><img src="<%=path%>/resources/images/check_pre.png" class="img_check" img_flag_info = "1" onclick="onChooseCheck(this)" img_materials_price_sum_info = '+$(arrTab[i]).attr("materials_price_sum")+'></div>';
                str = str + '<div class="check_img"><img src="' + $(arrTab[i]).attr("img_url") + '"></div>';
                str = str + '<div class="img_message">';
                str = str + '<div class="img_name">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<div class="img_money" id="div_sum_price_' + $(arrTab[i]).val() + '">小计' + $(arrTab[i]).attr("materials_price") * $(arrTab[i]).attr("materials_count") * $(arrTab[i]).attr("materials_measured_value") + '福币</div>';
                str = str + '</div>';
                  str = str + '<div class="img_time">';
                if($(arrTab[i]).attr("parent_id") != 1){
	              
	                str = str + '<div class="img_time_first">';
	                str = str + '<span class="time_title">时效</span>';
	                str = str + '<span onclick="onAddDate(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
	                str = str + '<span id="div_date_count_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</span>';
	                str = str + '<span onclick="onAddDate(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<span class="time_title">天</span>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<span class="time_title">月</span>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<span class="time_title">年</span>';
	                }else{
	                	str = str + '<span class="time_title"> -- </span>';
	                }
	                str = str + '</div>';
                }
                
                str = str + '<div class="img_time_cecond">';
                str = str + '<span class="time_title">数量</span>';
                
                
                str = str + '<span onclick="onAddCount(-1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</span>';
                str = str + '<span id="div_number_sum_i_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</span>';
                if($(arrTab[i]).attr("materials_class_id") == 10){//背景
                	str = str + '<span onclick="alert(\'背景只能买一个！\');">+</span>';
                }else{
                	str = str + '<span onclick="onAddCount(1, '+i+', ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</span>';
                }
                
                str = str + '<span class="time_title">只</span>';
                str = str + '</div>';
                str = str + '</div>';
                str = str + '</li>';
            }
            
            fb_sum = fb_sum * 1

            str = str + '</ul>';
			str = str + '<div class="sum_foot">';
            str = str + '<div class="accounts_footer"></div>';
            str = str + '<div class="accounts_footer_cont">';
            str = str + '<div class="footer_first">';
            str = str + '<div class="all_check"><img src="<%=path%>/resources/images/footer_check_pre.png" onclick="onChooseAllCheck(this);" img_all_flag_info = "1"></div>';
            str = str + '<div>全选</div>';
            str = str + '</div>';
            str = str + '<div class="footer_second">总计：</div>';
            str = str + '<div class="footer_money" id="div_footer_money">' + fb_sum + '</div>';
            str = str + '<div class="footer_danwei">福币</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="footer_queding">补充福币</div>';
            }else{
            	str = str + '<div class="footer_queding" onClick="onSaveMaterials();">确定</div>';
            }
            
            str = str + '<div class="footer_number" id="div_footer_number">共计' + arrTab.length + '件</div>';
            str = str + '</div>';
            str = str + '</div>';
            
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
			$(".sum_bg,.sum_content").attr("style","display: block; ");
			$("#back_flag").val(4);
 			showSec();
    	}
    	
    	//弹出结算素材弹出窗
    	function onShowJs(){
    		$.ajax({
				type: "POST",
				url: '/webCemetery/queryUserFb',
				dataType:'json',
				cache: false,
				success: function(data){
					onShowJsDiv(data);
				}
			});
 			showSec();
    	}
    
    	//添加减少时效
    	function onAddDate(flag, i, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_"+i+"_"+ materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, i, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_"+i+"_" + materials_id).attr("materials_price_sum", $("#input_materials_id_"+i+"_" + materials_id).attr("materials_count") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_price") * $("#input_materials_id_"+i+"_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//选择按钮
    	function onChooseCheck(bg){
    		var img_materials_price_sum_info = 0;
    		var img_flag_info = $(bg).attr("img_flag_info");
    		if(img_flag_info == 1){
    			$(bg).attr("src","<%=path%>/resources/images/check_n.png");
    			$(bg).attr("img_flag_info", "0");
    		}else{
    			$(bg).attr("src","<%=path%>/resources/images/check_pre.png");
    			$(bg).attr("img_flag_info", "1");
    		}
    		
    		var img_check = $(".img_check");
    		var count = 0;
    		for(var i=0;i<img_check.length;i++){
    			if($(img_check[i]).attr("img_flag_info") == 1){
    				count = count + 1;
    				img_materials_price_sum_info = img_materials_price_sum_info + $(img_check[i]).attr("img_materials_price_sum_info") * 1;
    			}
    		}
    		
    		$("#div_footer_number").text("共计"+count+"件");
    		$("#div_footer_money").text(img_materials_price_sum_info);
    	}
    	
    	//全选
    	function onChooseAllCheck(bg){
    		var img_all_flag_info = $(bg).attr("img_all_flag_info");
    		var img_check = $(".img_check");
    		if(img_all_flag_info == 1){
    			$(bg).attr("img_all_flag_info", 0);
    			$(bg).attr("src","<%=path%>/resources/images/footer_check_n.png");
    			$("#div_footer_number").text("共计0件");
    			$("#div_footer_money").text("0");
    			for(var i=0;i<img_check.length;i++){
	    			$(img_check[i]).attr("img_flag_info", 0);
	    			$(img_check[i]).attr("src","<%=path%>/resources/images/check_n.png");
	    		}
    		}else{
    			$(bg).attr("img_all_flag_info", 1);
    			$(bg).attr("src","<%=path%>/resources/images/footer_check_pre.png");
    			$("#div_footer_number").text("共计"+img_check.length+"件");
    			var img_materials_price_sum_info = 0;
    			for(var i=0;i<img_check.length;i++){
	    			$(img_check[i]).attr("img_flag_info", 1);
	    			$(img_check[i]).attr("src","<%=path%>/resources/images/check_pre.png");
	    			img_materials_price_sum_info = img_materials_price_sum_info + $(img_check[i]).attr("img_materials_price_sum_info") * 1;
	    		}
	    		$("#div_footer_money").text(img_materials_price_sum_info);
    		}
    	}
    	
    	//关闭结算
    	function onCloseSumContent(){
    		$(".sum_bg,.sum_content").attr("style","display: none; ");
    		$(".jisuan").css({display:"block"});
    		$("#back_flag").val(2);
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="cemetery_id" value="' + $("#input_cemetery_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#ff_type_flag").val() + '"/>';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webCemetery/saveCemeteryMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
					location.href="<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	//关闭文字
    	function onCloseWz(){
    		$(".bg_wz,.show_wz").attr("style","display: none; ");
    		$("#back_flag").val(1);
    		
    	}
    	
    	//跳转布置，所有素材都能拖
    	function onqueryBz(){
    		$("#tabflag").val(2);//0 首次加载 1祭祀建园 2 布置
    		location.href="<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val();
    		
//     		refresh("<%=path%>/webCemetery/queryCemeteryInfoForAppBz?cemetery_id="+$("#input_cemetery_id").val()+"&user_id=${user_id}&tabflag="+$("#tabflag").val());
    		
    		fun_meta(screenH);
    	}
    	
    	//显示放大缩小提前之后div(素材)
    	function onShowSDiv(bg){
    		$(".foot_bg,#buzhi_top_menu").show();
    		$("#div_buzhi_big").show();
    		$("#div_buzhi_small").show();
    		$("#div_buzhi_change").hide();
    	}
    	
    	//显示放大缩小提前之后div（文字）
    	function onShowSDivForWz(bg){
    		$(".foot_bg,#buzhi_top_menu").show();
    		$("#div_buzhi_big").hide();
    		$("#div_buzhi_small").hide();
    		$("#div_buzhi_change").show();
    		$("#buzhi_top_menu>div").css({width:"24%"});
    	}
	        
	    //相册
	    function onShowXC(cemetery_name, _id, isPermission){
	    	var u = navigator.userAgent;
			var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器
			var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
			if(isiOS){
				cem_photo(cemetery_name, _id, isPermission);//ios用
			}
			else{
				window.qifu.cem_photo(cemetery_name, _id, isPermission);//Android用
			}
	    }
	    
	    //然送
    	function onRanSong(cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		
    		$("#ransongform_cemetery_materials_id").val(cemetery_materials_id);
    		document.ransongform.action = "<%=path%>/webCemetery/updateCemeteryMaterialsForRansong";
			$("#ransongform").ajaxSubmit(function(result){
				$("#srmy_main").append("<img id='hire_"+cemetery_materials_id+"' style='position:absolute;z-index:"+(cemetery_materials_z_index+1)+";left:360px;top:330px;width:100px;height:180px;' src='/resources/images/hire02.gif' />");
	    		$("#div_ransong_"+cemetery_materials_id).remove();
	    		var count = 0.8;
	    		setTimeout("onChangeRansong1('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
			});
    	}
    	function onChangeRansong1(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.6;
    		setTimeout("onChangeRansong2('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong2(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.4;
    		setTimeout("onChangeRansong3('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong3(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.2;
    		setTimeout("onChangeRansong4('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong4(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		setTimeout("onChangeRansong5('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong5(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    		$("#hire_"+cemetery_materials_id).remove();
    	}
    	
    	//显示扫墓弹出窗
    	function onShowCleanCemetery(){
    		$(".sao_mu_content, .sao_mu_person").css({display: "block"});
    	}
    	//关闭扫墓弹出窗
    	function onCloseCleanCemetery(){
    		$(".sao_mu_content, .sao_mu_person").css({display: "none"});
    		fun_meta(screenH,1);
    	}
    	
    	//扫墓
    	function toCleanCemetery(){
    		var sex = $("input[name='sex']:checked").val();
    		
    		document.cleanform.action = "<%=path%>/webCemetery/updateCemeteryForCleanCemeteryApp";
			$("#cleanform").ajaxSubmit(function(result){
				if(result == 1){
					if(sex == 1){//男
						$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: 0px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_boy.gif' width='149' height='186' /></DIV>");
					}else{//女
						$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: 0px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_gril.gif' width='149' height='186' /></DIV>");
					}
					//关闭扫墓弹出窗
    				onCloseCleanCemetery()
    				//移动扫墓小人
					moveCleanPeople(sex);
					//清扫落叶
					$(".clean").remove();
				}else{
					alert('扫墓失败，请联系管理员！');
				}
			});
    	}
    	//移动扫墓小人
    	function moveCleanPeople(sex){
    		
    		left = 17;	
			setTimeout(moveCleanPeopleleft(left, sex), 600);
    	}
    	function moveCleanPeopleleft(left, sex){
    		
    		$("#cleanPeople").remove();
    		if(sex == 1){//男
    		    $("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: " + left + "px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_boy.gif' width='149' height='186' /></DIV>");
    		}else{//女
    			$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: " + left + "px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_gril.gif' width='149' height='186' /></DIV>");
    		}
    		if(left > 940){
    			left=-100;
    		}
    		left = left + 17;
    		setTimeout("moveCleanPeopleleft("+left+","+sex+")", 600);
    	}
    </script>
 
</head>
<body> 

<input type="hidden" id="vipcount" value="${vipcount}" />
<form action="" method="post" name="ransongform" id="ransongform">
  <input type="hidden" name="cemetery_id" value="${cemetery_id}"/>
  <input type="hidden" name="cemetery_materials_id" id="ransongform_cemetery_materials_id" value=""/>
</form>
<input type="hidden" id="tabflag" value="${tabflag}" /> <!-- 0 首次加载 1祭祀建园 2 布置  -->
<input type="hidden" id="back_flag" value="0"/>
<form method="post" name="fff" id="fff">
  <input type="hidden" name="wz" id="id_wz" value=""/>
  <input type="hidden" name="materials_id" id="id_materials_id" value=""/>
  <input type="hidden" name="cemetery_id" id="id_cemetery_id" value=""/>
  <input type="hidden" name="cemetery_materials_id" id="id_cemetery_materials_id" value=""/>
</form>
<input type="hidden" id="input_cemetery_id" value="${cemetery_id}"/>
<form method="post" name="saveForm" id="saveForm">
  
</form>
<form method="post" name="Form" id="Form">
  
</form>
<form method="post" name="ff" id="ff">
  <input type="hidden" id="ff_cemetery_materials_id"  />
  <input type="hidden" id="ff_cemetery_id"  />
  <input type="hidden" id="ff_cemetery_materials_show"  /><!-- 素材显示隐藏 -->
  <input type="hidden" id="ff_materials_id"  />

</form>
<input type="hidden" name="type_flag" id="ff_type_flag"  />
<div class="main" id="main">

  <!--刚进入墓园时，显示各种 值  -->
  <div class="srmy_infor_look" id="srmy_infor_look">
    <div class="srmy_infor_ico_01">
      <div>浏览</div>
      <img src="<%=path%>/resources/images/srmy_look.png" id="srmy_look" />
      <div>${cemetery.cemetery_clicks}</div>
    </div>
    <div class="srmy_infor_ico_02">
      <div>祭祀</div>
      <img src="<%=path%>/resources/images/srmy_jisi.png" id="srmy_jisi" />
      <div>${cemetery.cemetery_sacrifice}</div>
    </div>
    <div class="srmy_infor_ico_03">
      <div>亲情</div>
      <img src="<%=path%>/resources/images/srmy_qinqing.png" id="srmy_qinqing" />
      <div>${cemetery.cemetery_family}</div>
    </div>
    <div class="srmy_infor_ico_04">
      <div>财富</div>
      <img src="<%=path%>/resources/images/srmy_caifu.png" id="srmy_caifu" />
      <div>${cemetery.cemetery_wealth}</div>
    </div>
  </div>

  
    <!-- 0 首次加载 1祭祀建园 2 布置  -->
  <c:choose>
    <c:when test="${tabflag == 1 || tabflag == 2 }">
      <div class="js_and_save"> 
        <div class="yk_jisi" onclick="onShowJisiBz(2);">祭祀</div> 
     	<c:if test="${is_user_cemetery_flag > 0}">
     	  <div class="zr_jianyuan" onclick="onShowJisiBz(1);">建园</div>
          <div class="zr_buzhi" onclick="onqueryBz();">布置</div>
     	</c:if>
        <div class="yk_save" onclick="onSave();">保存</div> 
      </div> 
    </c:when>
    <c:otherwise>
      <div class="menu"></div>
      <div class="you_ke_menu">
        <div class="zr_jisi" onclick="onShowJisiBz(2);">祭祀</div>
        <c:if test="${is_user_cemetery_flag > 0}">
          <div class="zr_jianyuan" onclick="onShowJisiBz(1);">建园</div>
          <div class="zr_buzhi" onclick="onqueryBz();">布置</div>
        </c:if>
        <div class="zr_dangan" onclick="onShowXC('${cemetery.cemetery_name}','${cemetery._id}','${is_user_cemetery_flag}');">相册</div>
        <div class="zr_wenji">文集</div>
        <div class="zr_saomu" onclick="onShowCleanCemetery();">扫墓</div>
<!--         <div class="zr_shoucang">收藏</div> -->
      </div>
    </c:otherwise>
  </c:choose>
  
  
  <!-- 布置墓园时，左侧的图层选项-->
  <div class="buzhi_left_list" ${tabflag == 2 ? 'style="display: block;"' : '' }> 
    <div class="buzhi_left_menu" ${tabflag == 2 ? 'style="display: block;"' : '' }>
      <div class="left_menu_title">
<!--       物品 -->
<!--         <div id="srmy_menu_jiantou"><img src="<%=path%>/resources/images/ct_goods_jt_top.png" id="goods_jian_tou" /></div> -->
      </div>
    </div>
    <div class="srmy_fix_up">
      <ul id="SortContaint" class="fix_up">
        <c:forEach var="obj" items="${movelist}" varStatus="status"> 
          <c:if test="${obj.materials_class_id != 10}"> 
            <li id="list${obj.cemetery_materials_id}" 
            	class="cc" 
            	draggable="true" 
                materials_info="${obj.cemetery_materials_id}" 
                cemetery_info="${obj.cemetery_id}"  
                flag_info = '${obj.cemetery_materials_show}' 
                materials_class_info = "${obj.materials_class_id}" 
                materials_id_info = "${obj.materials_id}" > 
                  
              ${fn:length(obj.materials_name) > 5 ? fn:substring(obj.materials_name, 0, 5) : obj.materials_name}${fn:length(obj.materials_name) > 5 ? '...' : ''} 
            </li> 
          </c:if> 
   		</c:forEach> 
      </ul>
    </div>
  </div>
  

  
  <div class="srmy_main" id="srmy_main">
    <c:forEach var="obj" items="${sclist}" varStatus="status">
	  <c:if test="${obj.cemetery_materials_show == 0 && obj.cemetery_materials_id != 210452}">
	    <c:choose>
	      <c:when test="${obj.materials_class_id eq '10'}"><%--  背景 素材分分类id ==10    --%>
	        <img id="div${obj.cemetery_materials_id}" materials_info="${obj.cemetery_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:0;left:0px;top:0px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	      </c:when>
	      <c:when test="${obj.materials_class_id eq '8888'}"><!-- 是文字信息   -->
	        <div id="div${obj.cemetery_materials_id}" 
	        	 width_info="${obj.cemetery_materials_width}" 
	        	 height_info="${obj.cemetery_materials_height}" 
		         materials_info="${obj.cemetery_materials_id}" 
		         name="1"
 				 ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1 && tabflag == 2) ) ? 'class="srmy_FixedAdorn"' : ''} 		         data-name="img${obj.cemetery_materials_id}" 
		         data-id="url${obj.cemetery_materials_id}" 
		         ${(tabflag == 1 || tabflag == 2) ? 'onclick="onShowSDivForWz(this);"' : ''}
		         style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:auto;height:auto;${obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit == 0 ? 'border:1px dotted black;' : '' }">
		            
		      ${obj.materials_big_img}
	        </div>
	      </c:when>
	      <c:when test="${obj.materials_class_id eq '74'}">
	            <img  title="${obj.materials_name}-${obj.materials_class_id}"  id="ransong_${obj.cemetery_materials_id}"
		            	style="opacity:1;position:absolute;z-index:${obj.cemetery_materials_z_index};left:360px;top:400px;width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;"
		            	src="<%=path%>/goods/${obj.materials_big_img}" />
	            <div id="div_ransong_${obj.cemetery_materials_id}" style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:380px;top:430px;" onclick="onRanSong('${obj.cemetery_materials_id}','${obj.cemetery_materials_z_index}','${obj.cemetery_materials_width}','${obj.cemetery_materials_height}','${obj.materials_big_img}');">燃送</div>
	      </c:when>
	      <c:otherwise>
	        <img id="div${obj.cemetery_materials_id}"  
	        	 width_info="${obj.cemetery_materials_width}" 
	        	 height_info="${obj.cemetery_materials_height}" 
		         materials_info="${obj.cemetery_materials_id}"
		         ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1 && tabflag == 2) ) ? 'class="srmy_FixedAdorn"' : ''}  
		         data-name="img${status.index+1}" 
		         data-id="url${status.index+1}" 
		         title="${obj.materials_name}-${obj.materials_class_id}" 
		         style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;${obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
		         ${(tabflag == 1 || tabflag == 2) ? 'onclick="onShowSDiv(this);"' : ''}
		         
		         src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	      </c:otherwise>
	    </c:choose>
	  </c:if>
    </c:forEach>
    
    <c:forEach var="obj" items="${leafsArray}" varStatus="status">
		${obj}
	</c:forEach>
  </div>

  <!-- 布置墓园时，显示功能条和菜单选项  -->
  <div class="foot_bg"></div>
  <div class="buzhi_top_menu" id="buzhi_top_menu">
    <div class="buzhi_big" id="div_buzhi_big">
      <img src="<%=path%>/resources/images/buzhi_big.png" />
      <div>放大</div>
    </div>
    <div class="buzhi_small" id="div_buzhi_small">
      <img src="<%=path%>/resources/images/buzhi_small.png" />
      <div>缩小</div>
    </div>
    <div class="buzhi_change" id="div_buzhi_change" onclick="onChangeWz();">
      <img src="<%=path%>/resources/images/editwordyellow.png" />
      <div>修改</div>
    </div>
    <div class="buzhi_before">
      <img src="<%=path%>/resources/images/buzhi_before.png" />
      <div>提前</div>
    </div>
    <div class="buzhi_after">
      <img src="<%=path%>/resources/images/buzhi_after.png" />
      <div>置后</div>
    </div>
    <div class="buzhi_delete" onclick="onDelMaterials();">
      <img src="<%=path%>/resources/images/buzhi_delete.png" />
      <div>删除</div>
    </div>
  </div>
  
  <!-- 半全景切换图-->
    
    <div class="qj_img">
        <img src="<%=path%>/goods/${cemetery.cemetery_img }" />
    </div>
    <div class="quan_jing_btn"></div>
    <div class="xiang_xi_btn"></div>
  
</div>

<!-- 一级菜单-->
<div class="box_category_jisi" id="box_category_jisi">
<div id="aa"></div>
  <ul class="category first_menu">
    <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
      <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
        <div>
          <img src="<%=path%>/goods/${var.materials_class_img}" class="img" />
          <span class="span">${var.materials_class_name}</span>
        </div>
      </li>
    </c:forEach>
    <div class="clear"></div>
  </ul>
</div>

<div class="box_category_bz">
  <ul class="category first_menu">
    <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
      <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
        <div>
          <img src="<%=path%>/goods/${var.materials_class_img}" class="img" />
          <span class="span">${var.materials_class_name}</span>
        </div>
      </li>
    </c:forEach>
    <li onclick = "onShowWZ();"><div><img src="<%=path%>/resources/images/text.jpg" class="img" /><span class="span">文字</span></div></li>
    <div class="clear"></div>
  </ul>
</div>
  
<!--  祭祀 -> 二级菜单  -->
<div class="second_menu">
  <div id="category2">
    <iframe src="" iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
  </div>
</div>

<div class="second_menu2">
  <div id="category3">
    <div id="ly" class="bg_content"></div>
    <div id="Layer2" class="box_content"></div>
  </div>
</div>    
    
<div id="show"></div>
<div class="bg_wz"></div>
<div class="show_wz" id="showWz">
  <img src="<%=path%>/resources/images/close_window.png" class="prompt_close" onclick="onCloseWz();" />
  <div class="prompt_title">提 示</div>
  <div class="wz_content">
    <iframe src="<%=path%>/webCemetery/queryAppWz" scrolling="no" iframeborder="no" name="iframe_wz_name" id="iframe_wz_id"></iframe>
  </div>
</div>
     
<!-- 总结算页面-->
<div class="sum_accounts">
  <div class="sum_bg"></div>
  <div class="sum_content">
    <div class="kind_title">物品</div>
    <img src="<%=path%>/resources/images/image1/X.png" class="kind_close" onclick="onCloseSumContent();" />
    <div class="kind_content" id="div_confirm_container_js"></div>
  </div>
</div>
    
<div class="jisuan" onclick="onShowJs();">结算</div>


<!-- 聘用扫墓人-->
 <form action="" method="post" name="cleanform" id="cleanform">
 <input type="hidden" name="cemetery_id" value="${cemetery.cemetery_id}" />
 <input type="hidden" name="user_id" value="${user_id}" />
    <div class="sao_mu_person"></div>
    <div class="sao_mu_content">
        <div class="kind_title">请选择扫墓人性别</div>
        <img src="<%=path%>/resources/images/close_window.png" class="kind_close  window_close" onclick="onCloseCleanCemetery();"/>
        <div class="smr_window_content">
            <div class="smr_sex">
                <div class="choose_title">请选择扫墓人性别：</div>
                <label for="man"><input type="radio" id="man" name="sex" value="1" checked="checked" />男</label>
                <label for="woman"><input type="radio" id="woman" name="sex" value="0" />女</label>
            </div>
            <div class="smr_message">提示:每扫墓一次需收取<span>50福币</span>，您确定要扫墓吗?</div>
            <div class="smr_btn">
                <div class="smr_qd" onclick="toCleanCemetery();">确定</div>
                <div class="smr_qx" onclick="onCloseCleanCemetery();">取消</div>
            </div>
        </div>
    </div>
</form>

</body>
</html>