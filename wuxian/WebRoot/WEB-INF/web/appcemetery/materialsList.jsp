<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=10,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no" />
    <title>全球祭祀祈福网-私人墓园</title>
    
    <link href="<%=path%>/resources/css/category.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/cemetery/appsrmy.css" rel="stylesheet" />
    
    
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script src="<%=path%>/resources/js/zepto.min.js"></script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery/jquery.promptu-menu.js"></script>
    <script src="<%=path%>/resources/js/second_menu.js"></script>
    <script src="<%=path%>/resources/js/cemetery/appsrmy.js"></script>
    
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">

		//切换tab
        function onShowMaterials(i,count){
        	for(var k=0; k<count; k++){
        		var arrTab=$('.li_'+k);
        		for(var j=0; j<arrTab.length; j++){
        			$(arrTab[j]).attr("style","display: none; ");
        		}
        	}
        	var arrTab=$('.li_'+i);
       		for(var j=0; j<arrTab.length; j++){
       			$(arrTab[j]).attr("style","display: block; ");
       		}
        }
        
        function onShowChooseMaterials(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id, materials_small_img, parent_id){
        	
        	window.parent.onShowMaterialsDiv(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id, materials_small_img, parent_id);
    	}
	        
    </script>
 
</head>
<body>
<div class="box_nav">
  <div>
    <ul class="promptu-menu2 nav">
      <c:choose>
        <c:when test="${flag == 0}"><!-- 没有三级素材类别列表，直接查询素材列表 -->
          <li><a class="show">${materials_class_name}</a></li>
        </c:when>
        <c:otherwise><!-- 有三级素材类别列表，先查询三级素材类别列表，在查询默认第一个素材三级类别下的素材列表 -->
          <c:forEach items="${materialsClassList}" var="var" varStatus="i">
            <c:choose>
      	      <c:when test="${i.index == 0}">
      	        <li onclick="onShowMaterials('${i.index}','${fn:length(materialsClassList)}')"><a class="show" href="#">${var.materials_class_name}</a></li>
      	      </c:when>
      	      <c:otherwise>
      	        <li onclick="onShowMaterials('${i.index}','${fn:length(materialsClassList)}')"><a href="#">
      	          ${var.materials_class_name}
      	        </a></li>
      	      </c:otherwise>
      	    </c:choose>
          </c:forEach>
        </c:otherwise>
      </c:choose>
    </ul>
  </div>
</div>

<div class="box_category2">
  <ul class="category2">
    <c:choose>
      <c:when test="${flag == 0}"><!-- 没有三级素材类别列表，直接查询素材列表 -->
        <c:forEach items="${materialsList}" var="var" varStatus="i">
      	  <li onclick="onShowChooseMaterials('${var.materials_id}','${var.materials_name}','<%=path%>/goods${var.materials_small_img}','${var.materials_family}','${var.materials_wealth}','${var.materials_measured}', '${var.materials_price}', '${var.materials_class_id}', '${var.parent_id}','${var.materials_small_img}','${var.parent_id}');">
      		<img src="<%=path%>/goods${var.materials_small_img}" class="img" />
      		<span class="span">${fn:length(var.materials_name) > 4 ? fn:substring(var.materials_name, 0, 3) : var.materials_name}${fn:length(var.materials_name) > 4 ? '...' : ''}</span>
      		<div>${var.materials_price}福币/
              <c:choose>
	            <c:when test="${var.materials_measured == 1}">天</c:when>
	            <c:when test="${var.materials_measured == 2}">月</c:when>
	            <c:when test="${var.materials_measured == 3}">年</c:when>
	          </c:choose>
	        </div>
	      </li>
   	    </c:forEach>
	  </c:when>
      <c:otherwise><!-- 有三级素材类别列表，先查询三级素材类别列表，在查询默认第一个素材三级类别下的素材列表 -->
        <c:forEach items="${materialsLists}" var="var" varStatus="j">
          <c:forEach items="${var}" var="var1" varStatus="i">
            <li onclick="onShowChooseMaterials('${var1.materials_id}','${var1.materials_name}','<%=path%>/goods${var1.materials_small_img}','${var1.materials_family}','${var1.materials_wealth}','${var1.materials_measured}', '${var1.materials_price}', '${var1.materials_class_id}', '${var1.parent_id}','${var1.materials_small_img}','${var1.parent_id}');" 
                class="li_${i.index}" ${i.index == 0 ? '' : 'style="display: none; "' }>
              <img src="<%=path%>/goods${var1.materials_small_img}" class="img" />
              <span class="span">${fn:length(var.materials_name) > 4 ? fn:substring(var.materials_name, 0, 3) : var.materials_name}${fn:length(var.materials_name) > 4 ? '...' : ''}</span>
              <div>${var1.materials_price}福币/
                <c:choose>
	              <c:when test="${var1.materials_measured == 1}">天</c:when>
	              <c:when test="${var1.materials_measured == 2}">月</c:when>
	              <c:when test="${var1.materials_measured == 3}">年</c:when>
	            </c:choose>
	          </div>
	        </li>
          </c:forEach>
        </c:forEach>
      </c:otherwise>
    </c:choose>
    <div class="clear"></div>
  </ul>
</div>
    
</body>
</html>