<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="<%=path%>/resources/css/lifo/appli_fo.css" rel="stylesheet">
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
<script src="<%=path%>/resources/js/lifo/appli_fo.js" charset="UTF-8"></script>

	<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>

</head>
<body>	
  <div class="wish_container" id="wish_container">
<form id="gg" name="gg" method="post">








        <div class="gd_list">
            <table cellspacing="0">
                <tr>
                    <td class="list_first">捐赠人</td>
                    <td class="list_second">金额</td>
                    <td class="list_third">祈愿内容</td>
                </tr>
                <c:forEach items="${list}" var="obj" varStatus="j">
                           <tr >
                                <td class="list_first">${obj.user_name}</td>
                                <td class="list_second">${obj.donation_price}</td>
                                <td class="list_third">
                                ${fn:length(obj.donation_info) > 10 ? fn:substring(obj.donation_info, 0, 10) : obj.donation_info}${fn:length(obj.donation_info) > 10 ? '...' : ''}
                                </td>
                            </tr>
                           </c:forEach>
                
            </table>
        </div>
               </form>        
      
              <c:if test="${list != null && fn:length(list) > 0}"> 
                <div class="pagination" style="padding-top: 0px;margin-top: 0px;height:30px;">
			  	  <ff:page mhFrom="ff" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="webLifo/gdxListForApp"  />
				</div>
                
			  	  <form id="ff" name="ff" method="post">
			  	     <input type="hidden" name="fo_id" value="${fo_id}"/>
			  	     <input type="hidden" name="pageNo" value="${page.pageNo}" />
			  	     <input type="hidden" name="pageSize" value="${page.pageSize}" />
			  	     <input type="hidden" name="pageCount" value="${page.pageCount}" />
			  	  </form>
              </c:if>
      </div>   
</body>
</html>

