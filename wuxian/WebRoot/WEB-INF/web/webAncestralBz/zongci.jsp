<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网站-祠堂</title>
    <link href="<%=path%>/resources/css/ci_tang.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/zongci/zhuan.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/zongci/zong_ci.js"></script>
<!--     <script src="<%=path%>/resources/js/zongci/ci_tang.js"></script> -->
    <script src="<%=path%>/resources/js/zongci/zhuan.js"></script>
        <c:if test="${ not empty  ancestralHall.ancestral_hall_music_url }">
<!--    <audio src="${ancestralHall.ancestral_hall_music_url }" autoplay="true" loop="true">
 您的浏览器不支持 audio 标签。
    </audio>
     -->
    </c:if>
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <!--[if lt IE 9]>
	    <script src="/resources/index/js/html5shiv.min.js"></script>
	    <script src="/resources/index/js/respond.min.js"></script>
	<![endif]-->
    
    
	<script type="text/javascript">
		//切换墓园祭祀、墓园布置
    	function onChooseJisiOrBuzhi(flag, cemetery_id,chooseflag, is_user_cemetery_flag){
    		
			if(flag == 1){
    			$("#div_jisi").attr("class","");
    			$(".jisi").css({backgroundColor:"#fd844e"});
    			
    			if(is_user_cemetery_flag > 0 ){
    				$(".buzhi").css({backgroundColor:"#069dd5"});
    			}
   				$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
			if(flag == 2){
				$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#fd844e"});
	    		}
    			$("#div_buzhi").attr("class","");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
    		if(flag == 3){
    			$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#069dd5"});
	    		}
	    		$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","");
    			$(".choose_ceng").css({backgroundColor:"#fd844e"});
    		}
    		
    		$("#tabflag").val(flag);
    	}
    	
    	//弹出二级素材
    	function onShowMaterialsClass2(materials_class_id){
    		var src = "/webAncestralBz/queryMaterialsList?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);
    		$("#showMaterialsList").attr("style","display: block; ");
    	}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    		var vipcount = $('#vipcount').val();
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<div class="confirm_container" >';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price_sum") * 1;
    			}
    			
				if(i % 2 == 0){
					str = str + '<div class="module_first">';
				}else{
					str = str + '<div class="module_second">';
				}
				
				if($(arrTab[i]).attr("ifCheck") == 1){
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" checked="checked" >';
				}else{
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" >';
				}
                str = str + '<div class="module_content">';
                str = str + '<div class="module_title">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<img src="' + $(arrTab[i]).attr("img_url") + '" class="module_goods">';
                str = str + '<div class="module_goods_infor">';
                if($(arrTab[i]).attr("parent_id") != 2){
                	str = str + '<div class="aging">';
	                str = str + '<div class="aging_first">时效</div>';
	                str = str + '<div class="reduce" onclick="onAddDate(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >-</div>';
	                str = str + '<div class="number_sum" id="div_date_count_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</div>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<div>天 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<div>月 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<div>年</div>';
	                }else{
	                	str = str + '<div> - </div>';
	                }
	                str = str + '<div class="add" onclick="onAddDate(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >+</div>';
	                str = str + '</div>';
                }
                
                str = str + '<div class="number">';
                str = str + '<div class="number_first">数量</div>';
                str = str + '<div class="reduce" onclick="onAddCount(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</div>';
                str = str + '<div class="number_sum" id="div_number_sum_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</div>';
                str = str + '<div>个</div>';
                
                if($(arrTab[i]).attr("materials_class_id") == 10){//背景,不能增加数量
                	str = str + '<div class="add" onclick="alert(\'背景只能买一个！\');">+</div>';
                }else{
                	str = str + '<div class="add" onclick="onAddCount(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</div>';
                }
                
                str = str + '</div>';
                str = str + '<div class="sum">';
                str = str + '<div class="sum_first">小计</div>';
                str = str + '<div class="sum_price" id="div_sum_price_' + $(arrTab[i]).val() + '"><span>' + $(arrTab[i]).attr("materials_price_sum") + '</span>福币</div>';
                str = str + '</div>';
                str = str + '</div>';
                
//                 str = str + '<div class="line"></div>';
//                 str = str + '<div class="kind_price">';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/kinship_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_family") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_wealth") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>200</span>';
//                 str = str + '</div>';
//                 str = str + '</div>';
                str = str + '</div>';
                str = str + '</div>';
            }
            
            fb_sum = fb_sum * 1
            str = str + '</div>';
            str = str + '<div class="pay">';
            str = str + '<div class="pay_sum">总计：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + fb_sum + '</span>福币</div>';
            
            
            str = str + '<div class="pay_sum">会员价：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + Math.floor(fb_sum * vipcount) + '</span>福币</div>';
            
            
            str = str + '</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="balance">';
	            str = str + '<div class="pay_sum">余额不足：</div>';
	            str = str + '<div class="pay_price"><span>' + (user_fb - fb_sum) + '</span>福币</div>';
	            str = str + '</div>';
            }
            str = str + '<div class="pay_btn">';
            if(user_fb < fb_sum && ($('#userr_user_id').val() == null || $('#userr_user_id').val() == '')){
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >补充福币</div>';
            }else{
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >确定</div>';
            }
			
            str = str + '<div class="pay_cancel" onclick="onClear();">取消</div>';
            str = str + '</div>';
            
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
    		$("#show").attr("style","display: block; ");
			$(".bg_confirm,.show_confirm").attr("style","display: block; ");
    	}
    	
    	function onChooseJsCheckbox(user_fb, materials_id){
    		var ifcheck = $("#checkbox_" + materials_id).attr("checked");
    		if(ifcheck == "checked"){
    			$("#input_materials_id_" + materials_id).attr("ifCheck", 1);
    		}else{
    			$("#input_materials_id_" + materials_id).attr("ifCheck", 0);
    		}
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少时效
    	function onAddDate(flag, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="ancestral_hall_id" value="' + $("#input_cemetery_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#input_type_flag").val() + '"/><input type="hidden" name="hall_materials_type" value="2"/>';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webAncestralBz/saveAncestralHallInsideMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
					location.href="<%=path%>/Ancestral/queryAncestralBzInside?ancestral_hall_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();;
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	//显示放大缩小提前之后div(素材)
    	function onShowSDiv(bg){
    		$(".foot_cao_zuo,.cao_zuo_btn").css({display:"block",border:"1px solid #5abbf4"});
    		$("#div_foot_big").show();
    		$("#div_foot_small").show();
    		$("#div_foot_edit").hide();
    		$(".cao_zuo").css({width:"100px"});
    		$(".foot_cao_zuo,.cao_zuo_btn").css({width:"300px"});
    		$(bg).children('div').css({display:"block"});
    	}
    	
    	//保存
    	function save(){
    		var cemetery_id = $("#input_cemetery_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="ancestral_hall_id" type="hidden" value="' + cemetery_id + '" />';
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webAncestralBz/saveAncestralMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
				location.href="<%=path%>/Ancestral/queryAncestralBzInside?ancestral_hall_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();;
			});
    	}
    	
    	//清楚勾选记录
    	function onClear(){
//     		if(confirm('关闭将清空勾选记录，是否确认关闭？')){
    			$('#Form', parent.document).empty();
    			$(".bg_confirm, .show_confirm").attr("style","display: none; ");
    			$("#showMaterialsList", parent.document).attr("style","display: none; ");
//     		}
    	}
	</script>

</head>
<body>
<input type="hidden" id="vipcount" value="${vipcount == null ? 1 : vipcount}" />
<input type="hidden" id="choose_img_cemetery_materials_id" value=""/>
<input type="hidden" id="choose_img_materials_id" value=""/>
<input type="hidden" id="input_cemetery_id" value="${ancestral_hall_id}"/>
<input type="hidden" id="input_type_flag" value="2"/>
<input type="hidden" id="tabflag" value="${flag}"/>
<form action="/qjqwmain/cemetery/list" method="post" name="ff" id="ff">
</form>
<form action="/qjqwmain/cemetery/list" method="post" name="Form" id="Form">
  
</form>
<form action="/qjqwmain/cemetery/list" method="post" name="saveForm" id="saveForm">
</form>
<div class="main">
  <div class="header">
    <div class="srmy_wzgg">公告：纪念512汶川地震7周年</div>
    <img src="<%=path%>/resources/images/logo.png" class="srmy_logo" />
    <!-- 菜单-->
    <ul class="srmy_menu">
      <li class="active"><a href="#">宗祠园区</a></li>
      <li><a href="<%=path%>/">网站首页</a></li>
      <li><a href="<%=path%>/userManage">个人后台</a></li>
    </ul>
    <div class="srmy_infor_bg"></div>
    <div class="srmy_infor">
      <div class="top_infor_first">${ancestralHall.ancestral_hall_surname }氏宗祠&nbsp;<span>（族谱：${ancestralHall.ancestral_hall_genealogy }  堂号：${ancestralHall.ancestral_hall_family }）</span></div>
        <div class="top_infor_second">创  建  人 ：<span>${ancestralHall.ancestral_user_nick_name }</span></div>
        <div class="top_infor_third">族    长：<span>${zongci_admin_nick_name }</span><a href="">留言</a></div>
        <div class="srmy_infor_addr">创建时间：<span>${ancestralHall.ancestral_hall_addtime_str }</span></div>
        <div class="srmy_infor_look">
          <div class="srmy_infor_ico_01">
            <div>墓园</div>
            <img src="<%=path%>/resources/images/srmy_infor_ico_00.png" />
            <div>${ancestralHall.cemetery_number }</div>
          </div>
          <div class="srmy_infor_ico_02">
            <div>浏览</div>
            <img src="<%=path%>/resources/images/srmy_infor_ico_01.png" />
            <div>${ancestralHall.ancestral_hall_clicks }</div>
          </div>
          <div class="srmy_infor_ico_03">
            <div>祭祀</div>
            <img src="<%=path%>/resources/images/srmy_infor_ico_02.png" />
            <div></div>
          </div>
          <div class="srmy_infor_ico_04">
            <div>香火</div>
            <img src="<%=path%>/resources/images/srmy_infor_ico_xh.png" />
            <div>${ancestralHall.ancestral_hall_succeed }</div>
          </div>
        </div>
      </div>
    </div>
    
 	<!-- 素材部分-->
  	<div class="srmy_sc">
      <div class="srmy_suocai">
        <ul class="tab_menu">
          <li class="jisi" onclick="onChooseJisiOrBuzhi('1', '${ancestral_hall_id}', ${chooseflag}, '${is_user_ancestral_flag }');" ${flag == 1 ? 'style="background-color:#fd844e"' : ''} >祭祀物品</li>
          <c:choose>
            <c:when test="${is_user_ancestral_flag > 0}"><!-- 是墓园建造者，显示墓园布置列表 -->
              <li class="buzhi" onclick="onChooseJisiOrBuzhi('2','${ancestral_hall_id}',${chooseflag}, '${is_user_ancestral_flag }');" ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>建园物品</li>
            </c:when>
            <c:otherwise>
              <li class="buzhi" style='background-color: #bbb;' ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>建园物品</li>
            </c:otherwise>
          </c:choose>
          <li class="choose_ceng" onclick="onChooseJisiOrBuzhi('3','${ancestral_hall_id}',${chooseflag}, '${is_user_ancestral_flag }');" ${flag == 3 ? 'style="background-color:#fd844e"' : ''} >移动布置</li>
        </ul>
  		<div class="srmy_tab_content">
          <!-- 墓园祭祀-->
          <div id="div_jisi" class="sacrifice ${flag == 1 ? '' : 'content_hide'}">
            <ul>
              <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
                <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
                  <img src="<%=path%>/goods/${var.materials_class_img }" style="height: 29px;width: 29px;" />
                  <div>${var.materials_class_name }</div>
                </li>
              </c:forEach>
            </ul>
		  </div>
          <!-- 墓园布置-->
          <div class="sacrifice ${flag == 2 ? '' : 'content_hide'}" id="div_buzhi">
            <ul>
              <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
                <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
                  <img src="<%=path%>/goods/${var.materials_class_img}" style="height: 29px;width: 29px;" />
                  <div>${var.materials_class_name}</div>
                </li>
              </c:forEach>
            </ul>
          </div>
          <!-- 选择图层-->
          <div ${flag == 3 ? '' : 'class="content_hide"'} id="div_tuceng">
            <div class="srmy_bg">
              <c:forEach var="obj" items="${bglist}" varStatus="status">
                <c:if test="${status.index == (fn:length(bglist) - 1) }">
                  <li id="list${status.index+1}" class="cc" draggable="true">${obj.materials_name}
    			    <span class="buy_user_name">${obj.user_nick_name}祭祀</span>
                    <span class="buy_user"> </span>
                  </li>
                </c:if>
              </c:forEach>
            </div>
            <ul id="SortContaint" class="house fix_up">
              <c:forEach var="obj" items="${movelist}" varStatus="status">
                <c:if test="${obj.materials_class_id != 10}">
	    		  <li id="list${obj.hall_materials_id}" class="cc" draggable="true">
	    			${fn:length(obj.materials_name) > 10 ? fn:substring(obj.materials_name, 0, 10) : obj.materials_name}${fn:length(obj.materials_name) > 10 ? '...' : ''}
	    			<span class="buy_user_name">${obj.user_nick_name}祭祀</span>
	                <span class="buy_user">
	                  <c:choose>
	                    <c:when test="${obj.materials_class_id == 8888 }"><!-- 文字信息   -->
	                      <a href="#" onclick="onChangeWz('${obj.materials_id}','${cemetery_id}', '${obj.hall_materials_id}');">
							<img src="<%=path%>/resources/images/editwordblue.png" class="srmy_sucai_big" />
						  </a>
	                    </c:when>
	                    <c:otherwise>
	                      <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="srmy_sucai_big" />
	                      <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="srmy_sucai_reduce" />
	                    </c:otherwise>
	                  </c:choose>
	                  <img name="eye_${obj.hall_materials_id}" src="<%=path%>/resources/images/srmy_materisl_${obj.cemetery_materials_show == 0 ? 'show' : 'hidden'}.png" class="srmy_sucai_hidden" onclick="onUpdMaterialsShow(${obj.hall_materials_id}, ${obj.cemetery_id},this);" flag_info = '${obj.cemetery_materials_show}' />
	                  <img src="<%=path%>/resources/images/srmy_materisl_delete.png" class="srmy_sucai_delete" onclick="onDelMaterials(${obj.hall_materials_id}, ${obj.cemetery_id});" />
	                </span>
	              </li>
                </c:if>
  			  </c:forEach>
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <div class="wdzc_wrap">
      <div class="ci_tang_bg">
        <!-- 祠堂 顶部  堂沿-->
        <div class="ct_header">
          <div class="ct_pai">
            <div class="ct_pai_wen_zi"></div>
          </div>
        </div>
        <!-- 背景  墙壁画-->
        <div class="bg_img"></div>
        <!-- 排位下的红色区域-->
        <div class="ct_red_bg"></div>
        <div class="ct_red_pai_wei" >
          <div style="  width: 534px; height: 260px; position: absolute; z-index:3;" id="father">
            ${ht}
		  </div>
        </div>

        <div class="heng_zhe_1"></div>
        <div class="heng_zhe_2"></div>
        <div class="heng_zhe_3"></div>
        <div class="heng_zhe_4"></div>

        <!-- 堂内部的墙-->
        <div class="nei_qiang_left"></div>
        <div class="nei_qiang_right"></div>

        <!-- 堂内 堂联-->
        <div class="ci_lian_left"></div>
        <div class="ci_lian_right"></div>

        <div class="chuang_lian_left_1"></div>
        <div class="chuang_lian_left_2"></div>
        <div class="chuang_lian_left_3"></div>

        <div class="chuang_lian_right_1"></div>
        <div class="chuang_lian_right_2"></div>
        <div class="chuang_lian_right_3"></div>

        <!-- 地-->
        <div class="di"></div>

        <!-- 墙-->
        <div class="qiang_left"></div>
        <div class="qiang_right"></div>

        <!-- 柱子-->
        <div class="zhu_left"></div>
        <div class="zhu_right"></div>

        <div class="gn_btn">
          <img src="/resources/images/zongci/gn_top2.png" id="to" class="top_btn"/>
          <img src="/resources/images/zongci/gn_bottom2.png" id="bu" class="bottom_btn" />
          <img src="/resources/images/zongci/gn_left2.png" id="le" class="left_btn" />
          <img src="/resources/images/zongci/gn_right2.png" id="ri" class="right_btn"/>
        </div>

        <div class="srmy_main" id="srmy_main">
          <c:forEach var="obj" items="${sclist}" varStatus="status">
		    <c:if test="${obj.hall_materials_show == 0}">
	          <div id="div${obj.hall_materials_id}" 
	               ${((obj.sacrifice_user_id == user_id && obj.hall_materials_limit != 3) || (is_user_ancestral_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
	               data-name="img${status.index+1}" 
	               data-id="url${status.index+1}" 
	               style="position:absolute;z-index:${obj.hall_materials_z_index};left:${obj.hall_materials_x}px;top:${obj.hall_materials_y}px;width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;"
	               name="1"
	               width_info="${obj.hall_materials_width}" 
	               height_info="${obj.hall_materials_height}" 
		           materials_info="${obj.hall_materials_id}" 
		           imgwidth="${obj.hall_materials_width}" 
		           imgweight="${obj.hall_materials_height}"
		           onclick="onShowSDiv(this);"
		           materials_id_info = "${obj.materials_id}">
                <div class="cao_zuo">
                  <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                  <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                  <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                  <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                </div>
	            <img title="${obj.materials_name}-${obj.materials_class_id}" 
		             style="width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;${obj.sacrifice_user_id == user_id && obj.hall_materials_limit == 0 ? 'border:1px dotted black;' : '' }"
		             src="<%=path%>/goods/${obj.materials_big_img}" />
		      </div>
	        </c:if>
  		  </c:forEach>
        </div>
   		
        <div class="foot_cao_zuo"></div>
      	<div class="cao_zuo_btn">
          <div id="div_foot_big" class="foot_big">
            <div><img src="<%=path%>/resources/images/srmy_materisl_big.png" /></div>
            <div>放大</div>
          </div>
          <div id="div_foot_small" class="foot_small">
            <div><img src="<%=path%>/resources/images/srmy_materisl_reduce.png" /></div>
            <div>缩小</div>
          </div>
          <div class="buzhi_before">
            <div><img src="<%=path%>/resources/images/srmy_materisl_before.png" /></div>
            <div>置前</div>
          </div>
          <div class="buzhi_after">
            <div><img src="<%=path%>/resources/images/srmy_materisl_after.png" /></div>
            <div>置后</div>
          </div>
          <div class="foot_delete">
            <div><img src="<%=path%>/resources/images/srmy_materisl_delete.png" /></div>
            <div>删除</div>
          </div>
        </div>

     	<!--商品类别详细信息-->
      	<div id="bg"></div>
        <div id="showMaterialsList">
      	  <iframe src="" scrolling="no" iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
        </div>
        <div id="show"></div>
      
        <!--结算购物车-->
        <div class="bg_confirm"></div>
        <div class="show_confirm">
          <div class="confirm_content" id="div_confirm_container_js">
          </div>
        </div>
      </div>
	
	
	
      <!-- 宗祠信息-->
            <div class="zc_infor_bg  bg"></div>
            <div class="zc_infor_content">
                <div class="window_title">宗 祠 信 息</div>
                <img src="/resources/images/close_window.png" class="zcxx_window_close  window_close">
                <div class="zcxx_window_content">
                    <div class="zcxx_hang_first">
                        <div>宗祠名称</div>
                        <div><input type="text"   value="${ancestralHall.ancestral_hall_genealogy }"   readonly="readonly"></div>
                        <div class="must_write">（4字以内&nbsp;必填）</div>
                    </div>

                    <div class="zcxx_hang_second">
                        <div>宗谱名称</div>
                        <div><input type="text" readonly="readonly"   value="${ancestralHall.ancestral_hall_surname}">   </div>
                    </div>

                    <div class="zcxx_hang_third">
                        <div>堂&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</div>
                        <div><input type="text"   value="${ancestralHall.ancestral_hall_family}" readonly="readonly"></div>
                        <div class="must_write">（5字以内）</div>
                    </div>

                    <div class="zcxx_hang_fourth">
                        <div>所在地址</div>
                        <div>
                            <input type="text"   value="${ancestralHall.ancestral_hall_province}  ${ancestralHall.ancestral_hall_city}" readonly="readonly">
                        </div>
                        <div class="must_write">（必填）</div>
                    </div>

                    <div class="zcxx_que_ding">确定</div>
                </div>
            </div>


      






        <!-- 关联墓园-->
        <div class="guan_lian_my_bg"></div>
        <div class="guan_lian_my_content">
            <div class="window_title">关 联 墓 园</div>
            <img src="images/close_window.png" class="glmy_window_close  window_close">
            <div class="glmy_window_content">
                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

                <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="images/bjpw_photo.png"></li>
                    <li class="cont_ul_second">李白</li>
                    <li class="cont_ul_third">字太白</li>
                    <li class="cont_ul_fourth">号青莲居士</li>
                    <li class="cont_ul_fifth">墓园号：12345678</li>
                    <li class="cont_ul_sixth">
                        <div class="bjpw_shen_he">申请中</div>
                        <div class="bjpw_my">关联墓园</div>
                        <div class="bjpw_create_btn">创建墓园</div>
                    </li>
                </ul>

            </div>
        </div>
        <!-- 关联墓园   搜索墓园框-->
        <div class="search_my_num_bg"></div>
        <div class="search_my_num_content">
            <div class="glmy_infor_title">
                <div class="glmy_infor__name">墓 园 号 </div>
                <input type="text" placeholder="  请输入">
                <div class="search_btn">搜索</div>
            </div>
            <div class="yst_title">绑定后可加入宗祠议事堂</div>

            <div class="glmy_center_cont">
                <!-- 缩略图-->
                <div class="cont_left"><img src="images/"></div>
                <div class="cont_center">
                    <div class="center_hang_1">刘启华墓园</div>
                    <div class="center_hang_2">
                        <div>
                            <div>建园</div>
                            <div class="create_yuan_name">刘某</div>
                        </div>
                        <div>
                            <div>墓地</div>
                            <div class="mu_di_addr">中国-北京</div>
                        </div>

                    </div>
                    <div class="center_hang_3">
                        <div>
                            <div>生于</div>
                            <div class="create_yuan_name">1961-05-29</div>
                        </div>
                        <div>
                            <div>卒于</div>
                            <div class="mu_di_addr">2009-06-05</div>
                        </div>
                    </div>
                    <div class="center_hang_4">
                        <div>
                            <img src="images/srmy_infor_ico_01.png">
                            <div>312</div>
                        </div>
                        <div>
                            <img src="images/srmy_infor_ico_02.png">
                            <div>312</div>
                        </div>
                        <div>
                            <img src="images/srmy_infor_ico_03.png">
                            <div>312</div>
                        </div>
                        <div>
                            <img src="images/srmy_infor_ico_04.png">
                            <div>312</div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="my_foot_btn">
                <div class="my_foot_qx">取消</div>
                <div class="my_foot_qd">确定</div>
            </div>

        </div>



      
	
	
	  <div class="footer">

		        <div><a href="/Ancestral/viewAncestral?id=${ancestralHall.ancestral_myid }" target="_blank" >宗氏族谱</a></div>
		        <div class="zc_infor">宗祠信息</div>
		        <div class="history_photo" onclick="getPhoto();">家族相册</div>
		        <div class="family_history" onclick="history();">家族历史</div>
		        <div class="te_shu_date" onclick="getSpecialDay();">特殊祭日</div>
		        <c:if test="${is_user_ancestral_flag==1}">

		        <div class="bei_jing_music" onclick="getAncestrallMusic();">背景音乐</div>
		        <div class="guan_lian_mu_yuan"   onclick="guanlian();">关联账号</div>
		        <div class="bian_ji_pai_wei" onclick="EditPaiWei();">编辑牌位</div>
		        		        
		        		        </c:if>
		      <div class="juan_zeng_jin_e" onclick="tojuanzeng();">捐赠金额</div>
		        <div><a href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${ancestralHall.ancestral_myid }&chooseflag=1&flag=1"  >回到院落</a></div>

        <!-- 保存按钮-->
<!--         <input type="button" value="保存"  style="background-color:#bbbbbb"   class="footer_save_btn"   id="sava_id" onclick="save()" /> -->
            <input type="button" value="保存" id="sava_id" style="background-color:#bbbbbb" class="footer_save_btn"  />
    </div>
	
	
	
	</div>
<script type="text/javascript">
	$(document).ready(function(){
	  	$("#le").click(function(){
	    	$("#father").animate({left: $('#father').position().left-83});
	  	});
	   	$("#to").click(function(){
	    	$("#father").animate({top:$('#father').position().top-83});
	  	});
	   	$("#ri").click(function(){
	    	$("#father").animate({left:$('#father').position().left+83});
	  	});
	   	$("#bu").click(function(){
	    	$("#father").animate({top:$('#father').position().top+83});
	  	});
	});
</script>
<script type="text/javascript">
function getPhoto(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="家族相册";
	 diag.URL = '/webAncestralBz/getAncestralPhoto?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
}
function history(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="家族历史";
	 diag.URL = '/webAncestralBz/history?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 720;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
}
function getAncestrallMusic(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="背景音乐";
	 diag.URL = '/webAncestralBz/getMusic?url=${ancestralHall.ancestral_hall_music_url}&ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 
function getAncestrallLog(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="祭祀日志";
	 diag.URL = '/webAncestralBz/getAncestrallLog?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 
function getSpecialDay(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="特殊祭日";
	 diag.URL = '/webAncestralBz/getSpecialDay?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 780;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 
function guanlian(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="关联账号";
	 diag.URL = '/webAncestralBz/guanlian?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 
function EditPaiWei(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="编辑排位";
	 diag.URL = '/webAncestralBz/EditPaiWei?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 
function tojuanzeng(){
	 var diag = new Dialog();
	 diag.Drag=true;
	 diag.Title ="捐赠金额";
	 diag.URL = '/Ancestral/tojuanzeng?ancestral_hall_obid=${ancestralHall._id}';
	 diag.Width = 700;
	 diag.Height = 550;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
} 

</script>


</body>
</html>