<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网-宗祠词院</title>
    <link href="<%=path%>/resources/css/zongci/zong_ci.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/zongci/zhuan.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/zongci/zong_ci.js"></script>
    <script src="<%=path%>/resources/js/zongci/zhuan.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    <c:if test="${ not empty  ancestralHall.ancestral_hall_music_url }">
<!--    <audio src="${ancestralHall.ancestral_hall_music_url }" autoplay="true" loop="true">
 您的浏览器不支持 audio 标签。
    </audio>
     -->
    </c:if>
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript">
	</script>
</head>

<body>

<!--   <div class="content"> -->
    <!-- 墓园部分-->
    <div class="wdmy_wrap" style="left:0px;">
      <!-- 墓园放置物品-->
      <div class="srmy_main" id="srmy_main">
        <c:forEach var="obj" items="${sclist}" varStatus="status">
		  <c:if test="${obj.hall_materials_show == 0}">
	        <div id="div${obj.hall_materials_id}" 
	             ${((obj.sacrifice_user_id == user_id && obj.hall_materials_limit != 3) || (is_user_ancestral_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
                 data-name="img${status.index+1}" 
                 data-id="url${status.index+1}" 
                 style="position:absolute;z-index:${obj.hall_materials_z_index};left:${obj.hall_materials_x}px;top:${obj.hall_materials_y}px;width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;"
                 name="1"
                 width_info="${obj.hall_materials_width}" 
                 height_info="${obj.hall_materials_height}" 
	             materials_info="${obj.hall_materials_id}" 
	             imgwidth="${obj.hall_materials_width}" 
	             imgweight="${obj.hall_materials_height}"
	             onclick="onShowSDiv(this);"
	             materials_id_info = "${obj.materials_id}">
              <div class="cao_zuo">
                <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
              </div>
	            
	          <img title="${obj.materials_name}-${obj.materials_class_id}" 
		           style="width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;${obj.sacrifice_user_id == user_id && obj.hall_materials_limit == 0 ? 'border:1px dotted black;' : '' }"
		           src="<%=path%>/goods/${obj.materials_big_img}" />
		    </div>
	      </c:if>
  		</c:forEach>
      </div>
      <div id="zc_goods">
        <img src="<%=path%>/resources/images/zong_ci_home.png" id="zc_house" />
      </div>
      <div class="zhuan_content">
        <c:choose>
      	  <c:when test="${not empty ancestralHall.gold_brick}">
      		<c:forEach items="${ ancestralHall.gold_brick}" var="golds" varStatus="vs">
      		  <c:if test="${golds.gold_brick_type==1 }">
             	<img src="<%=path%>/resources/images/img_zhuan/odd_${golds.gold_brick_number }.png" id="zhuan${golds.gold_brick_number }" />
      		  </c:if>
      		  <c:if test="${golds.gold_brick_type==2 }">
             	<img src="<%=path%>/resources/images/img_zhuan/new_${golds.gold_brick_number }.png" id="zhuan${golds.gold_brick_number }" />
      		  </c:if>
      		</c:forEach>
      	  </c:when>
        </c:choose>
      </div>
    </div>
<!-- </div> -->
</body>
</html>