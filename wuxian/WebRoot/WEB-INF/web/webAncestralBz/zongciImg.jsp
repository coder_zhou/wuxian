<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网站-祠堂</title>
    <link href="<%=path%>/resources/css/ci_tang.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/zongci/zhuan.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/zongci/zong_ci.js"></script>
    <script src="<%=path%>/resources/js/zongci/ci_tang.js"></script>
    <script src="<%=path%>/resources/js/zongci/zhuan.js"></script>
        <c:if test="${ not empty  ancestralHall.ancestral_hall_music_url }">
<!--    <audio src="${ancestralHall.ancestral_hall_music_url }" autoplay="true" loop="true">
 您的浏览器不支持 audio 标签。
    </audio>
     -->
    </c:if>
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
	<script type="text/javascript">
	</script>

</head>
<body>
    <div class="wdzc_wrap">
      <div class="ci_tang_bg">
        <!-- 祠堂 顶部  堂沿-->
        <div class="ct_header">
          <div class="ct_pai">
            <div class="ct_pai_wen_zi"></div>
          </div>
        </div>
        <!-- 背景  墙壁画-->
        <div class="bg_img"></div>
        <!-- 排位下的红色区域-->
        <div class="ct_red_bg"></div>
        <div class="ct_red_pai_wei" >
          <div style="  width: 534px; height: 260px; position: absolute; z-index:3;" id="father">
            ${ht}
		  </div>
        </div>

        <div class="heng_zhe_1"></div>
        <div class="heng_zhe_2"></div>
        <div class="heng_zhe_3"></div>
        <div class="heng_zhe_4"></div>

        <!-- 堂内部的墙-->
        <div class="nei_qiang_left"></div>
        <div class="nei_qiang_right"></div>

        <!-- 堂内 堂联-->
        <div class="ci_lian_left"></div>
        <div class="ci_lian_right"></div>

        <div class="chuang_lian_left_1"></div>
        <div class="chuang_lian_left_2"></div>
        <div class="chuang_lian_left_3"></div>

        <div class="chuang_lian_right_1"></div>
        <div class="chuang_lian_right_2"></div>
        <div class="chuang_lian_right_3"></div>

        <!-- 地-->
        <div class="di"></div>

        <!-- 墙-->
        <div class="qiang_left"></div>
        <div class="qiang_right"></div>

        <!-- 柱子-->
        <div class="zhu_left"></div>
        <div class="zhu_right"></div>

        <div class="gn_btn">
          <img src="/resources/images/zongci/gn_top2.png" id="to" class="top_btn"/>
          <img src="/resources/images/zongci/gn_bottom2.png" id="bu" class="bottom_btn" />
          <img src="/resources/images/zongci/gn_left2.png" id="le" class="left_btn" />
          <img src="/resources/images/zongci/gn_right2.png" id="ri" class="right_btn"/>
        </div>

        <div class="srmy_main" id="srmy_main">
          <c:forEach var="obj" items="${sclist}" varStatus="status">
		    <c:if test="${obj.hall_materials_show == 0}">
	          <div id="div${obj.hall_materials_id}" 
	               ${((obj.sacrifice_user_id == user_id && obj.hall_materials_limit != 3) || (is_user_ancestral_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
	               data-name="img${status.index+1}" 
	               data-id="url${status.index+1}" 
	               style="position:absolute;z-index:${obj.hall_materials_z_index};left:${obj.hall_materials_x}px;top:${obj.hall_materials_y}px;width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;"
	               name="1"
	               width_info="${obj.hall_materials_width}" 
	               height_info="${obj.hall_materials_height}" 
		           materials_info="${obj.hall_materials_id}" 
		           imgwidth="${obj.hall_materials_width}" 
		           imgweight="${obj.hall_materials_height}"
		           onclick="onShowSDiv(this);"
		           materials_id_info = "${obj.materials_id}">
                <div class="cao_zuo">
                  <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                  <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                  <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                  <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                </div>
	            <img title="${obj.materials_name}-${obj.materials_class_id}" 
		             style="width:${obj.hall_materials_width}px;height:${obj.hall_materials_height}px;${obj.sacrifice_user_id == user_id && obj.hall_materials_limit == 0 ? 'border:1px dotted black;' : '' }"
		             src="<%=path%>/goods/${obj.materials_big_img}" />
		      </div>
	        </c:if>
  		  </c:forEach>
        </div>
      </div>
	</div>
</body>
</html>