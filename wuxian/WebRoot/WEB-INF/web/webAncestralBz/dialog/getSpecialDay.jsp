<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'AncestralLog.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
        <link href="<%=path%>/resources/css/zongci/zong_ci.css" rel="stylesheet" />
         	<script src="/resources/js/jquery-1.9.1.min.js"></script>

   	<script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
  </head>
  
  <body>
              <!-- 特殊纪日-->
            <div class="tsjn_content">
                <div class="tsjn_window_content">
                    <c:if test="${is_manage==1}">
                 
                    <div class="tsjn_title">
                        <div class="title_add_btn">
                            <img src="/resources/images/srmy_add.png">
                            <div> 添&nbsp;&nbsp;加</div>
                        </div>
                        <div class="title_message">开启通知特殊祭日，提醒关联账户族人</div>
                    </div>
                       </c:if>
                 <c:choose>
                 <c:when test="${not empty as }">
                 <c:forEach items="${as.special_day_info }" var="var" varStatus="vs">
                          <ul>
                        <li class="tsjn_list_1"><img src="/resources/images/srmy_tsjn_date.png"></li>
                        <li class="tsjn_list_2">${var.special_month }月${var.special_day }日</li>
                        <li class="tsjn_list_3">${var.special_day_remarks }</li>
                        <li class="tsjn_list_4">      
                     <c:if test="${is_manage==1}">    <input type="checkbox" class="ace-switch ace-switch-3" id="sd${vs.index+1 }"   <c:if test="${var.special_day_type==0 }"> checked="checked" </c:if>      onclick="updateSpecialDay('${pd.ancestral_hall_obid}','${var.special_day_id}','${vs.index+1 }')">
                     </c:if>
                     </li>
                    </ul>
                 </c:forEach>
                 </c:when>
                 </c:choose>
                 
           
                 
                </div>
            </div>
<form action="/webAncestralBz/AddSpecialDay" method="post" id="form">
<input type="hidden" name="ancestral_hall_obid" value="${pd.ancestral_hall_obid}">
            <!-- 添加纪念日-->
            <div class="add_jnr_bg bg"></div>
            <div class="add_jnr_content">
                <div class="window_title">添 加 纪 念 日</div>
                <img src="/resources/images/close_window.png" class="add_jnr_window_close  window_close">
                <div class="add_jnr_window_content">
                    <div class="add_date">
                        <div class="add_date_title">特殊日期</div>
                         <input class="span10 date-picker" name="special_time"
									 type="text" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'MM月dd日'})"
									style="width: 88px;" placeholder="纪念日" />
                    </div>
                    <div class="add_message">
                        <div class="add_mess_title">开启提示</div>
                        <div class="add_mess_input">
                                   <input type="radio" name="special_day_type" id="yes" checked="checked"    value="0"><label for="yes">是</label>
                            <input type="radio" name="special_day_type" id="no" value="1"><label for="no">否</label>
                        </div>
                    </div>
                    <div class="add_intro">
                        <div class="add_intro_title">纪念说明</div>
                         <textarea name="special_day_remarks"></textarea>
                    </div>
                    <div class="add_btn"  onclick="sub();">确定</div>
                </div>
            </div>
            </form>
  
  <script type="text/javascript">
  function  updateSpecialDay(Id,Sd,SdIndex){
		var wqx = $("#sd"+SdIndex).attr("checked");
		var vs;
			if(wqx == 'checked'){
				vs="unchecked";
			}else{
				vs="checked";
			}
	  var url = "/webAncestralBz/updateSpecialDay?id="+Id+"&special_day_id="+Sd+"&sd="+vs;
	    $.get(url,function(data){
	   
	    });
}
  
  function sub(){
	  $("#form").submit();
  }
  $(function (){

	    //添加纪念日
	    $(".title_add_btn").click(function(){

	        $(".zc_infor_bg,.zc_infor_content  ,  .jzls_bj_bg,.jzls_bj_content  ,  .jz_history_bg,.jz_history_content  ,  .history_photo_bg,.history_photo_content  ,  .buy_mb_bg,.buy_mb_content  ,  .change_sty_bg,.change_sty_content  , .add_jnr_bg,.add_jnr_content  ,  .guan_lian_num_bg,.guan_lian_num_content  ,   .search_my_num_bg,.search_my_num_content  ,  .bg_music_bg,.bg_music_content  ,  .jisi_manage_bg,.jisi_manage_content  ,  .limit_set_bg,.limit_set_content  ,  .add_manage_bg,.add_manage_content  , .jsrz_bg,.jsrz_content ").css({display:"none"});

	        $(".add_jnr_bg,.add_jnr_content").css({display:"block"});
	    });
	  
  });
  
  </script>
  </body>
</html>
