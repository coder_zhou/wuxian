<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'history.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="<%=path%>/resources/css/zongci/zong_ci.css" rel="stylesheet" />
    <script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
            <!-- 家族历史-->
            <div class="jz_history_content">
                <div class="jzls_window_content">
                    <div class="jzls_title">
                        <div class="jzls_xsqy  jzls_active">姓氏起源</div>
                        <div class="jzls_cgjx">祠规家训</div>
                        <div class="jzls_dsj">大事记</div>
                    </div>
            <c:if test="${is_manage==1}">
              <div class="jzls_bian_ji">编辑</div>
                         </c:if>  
                    <div class="jzls_content">
                        <div>${ah.surname_origin }</div>
                        <div class="jzls_hidden_cont">${ah.family_instructions }</div>
                        <div class="jzls_hidden_cont">${ah.big_event }</div>

                    </div>
                </div>
            </div>
<form action="/webAncestralBz/editHistory" method="post" id="form">
<input type="hidden" name="ancestral_hall_obid" value="${pd.ancestral_hall_obid }">
            <!--    家族历史  编辑   -->
            <div class="jzls_bj_bg   bg"></div>
            <div class="jzls_bj_content">
                <div class="window_title">家 族 历 史</div>
                <img src="/resources/images/close_window.png" class="jzls_bj_window_close  window_close">
                <div class="jzls_bj_window_content">
                    <div class="jzls_bj_title">
                        <div class="bian_ji_model">编 辑 模 块</div>
                        <select  id="se"  name="type"   onchange="chose();">
                            <option  value="1"  selected="selected">&nbsp;&nbsp;姓 氏 起 源</option>
                            <option value="2">&nbsp;&nbsp;祠 规 家 训</option>
                            <option value="3">&nbsp;&nbsp;大 事 记</option>
                        </select>
                    </div>
                    <textarea class="jzls_bj_cont"  name="info" id="info">${ah.surname_origin }</textarea>
                    <div class="jzls_bj_save"  onclick="sub();">保  存</div>
                </div>
            </div>
            </form>
            <script type="text/javascript">
            $(function (){
                //家族历史
                $(".family_history").click(function(){

                    $("  .zc_infor_bg,.zc_infor_content  ,  .history_photo_bg,.history_photo_content  ,  .jzls_bj_bg,.jzls_bj_content  ,  .tsjn_bg,.tsjn_content  ,  .add_jnr_bg,.add_jnr_content  ,  .bg_music_bg,.bg_music_content  ,  .guan_lian_my_bg,.guan_lian_my_content  ,  .search_my_num_bg,.search_my_num_content  ,  .bjpw_bg,.bjpw_content  ,  .bian_ji_kuang_bg,.bian_ji_kuang_content  ").css({display:"none"});

                    $(".jz_history_bg,.jz_history_content").css({display:"block"});
                });
                $(".jzls_window_close").click(function(){
                    $(".jz_history_bg,.jz_history_content").css({display:"none"});
                });
                //tab切换菜单
                $(".jzls_title div").click(function(){
                    $(this).addClass("jzls_active").siblings().removeClass("jzls_active");
                    var menu_index=$(this).index();
                    $(".jzls_content div").eq(menu_index).removeClass("jzls_hidden_cont").siblings().addClass("jzls_hidden_cont");
                });
                //点击编辑按钮
                $(".jzls_bian_ji").click(function(){
                    $(".jzls_bj_bg,.jzls_bj_content").css({display:"block"});

                    $("  .zc_infor_bg,.zc_infor_content  ,  .history_photo_bg,.history_photo_content  ,  .jz_history_bg,.jz_history_content  ,  .tsjn_bg,.tsjn_content  ,  .add_jnr_bg,.add_jnr_content  ,  .bg_music_bg,.bg_music_content  ,  .guan_lian_my_bg,.guan_lian_my_content  ,  .search_my_num_bg,.search_my_num_content  ,  .bjpw_bg,.bjpw_content  ,  .bian_ji_kuang_bg,.bian_ji_kuang_content  ").css({display:"none"});

                });
                $(".jzls_bj_window_close").click(function(){
                    $(".jzls_bj_bg,.jzls_bj_content").css({display:"none"});
                });

            	
            });
            
            function chose(){
            var  ss= $("#se").val();
    
            if(ss==1){
            	$("#info").val('${ah.surname_origin }');
            }
            if(ss==2){
            	$("#info").val('${ah.family_instructions }');
            }
            if(ss==3){
            	$("#info").val('${ah.big_event }');
            }   	
            }
            
            function sub(){
            	$("#form").submit();
            }
            </script>
            
            
  </body>
</html>
