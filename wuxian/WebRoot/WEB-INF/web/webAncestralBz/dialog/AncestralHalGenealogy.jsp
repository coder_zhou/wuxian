<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'AncestralHalGenealogy.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
        <link href="<%=path%>/resources/css/zongci/zong_ci.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
    <!-- 关联账号-->
            <div class="guan_lian_num_content">
                <div class="glzh_window_content">
      <c:choose>
      <c:when test="${not empty ag }">
      <c:forEach items="${ag }" var="var">
                          <ul class="glzh_cont_ul">
                        <li class="glzh_cont_first"><img src="${var.genealogy_img }"></li>
                        <li class="glzh_cont_second">${var.genealogy_name }</li>
                        <li class="glzh_cont_third">字${var.genealogy_word }</li>
                        <li class="glzh_cont_fourth">号${var.genealogy_known_as }</li>
                        <li class="glzh_cont_sixth">
                     <c:if test="${var.be_account_state ==0 }"><div class="glzh_create_btn" onclick="guanlian('${var._id}');">关联账号</div></c:if> 
                            <c:if test="${var.be_account_state==1 }">
                            <div class="glzh_shen_he">申请中</div>
                            </c:if>
                               <c:if test="${var.be_account_state==2 }">
                            <div class="glzh_shen_he">已关联</div>
                            </c:if>
                        </li>
                    </ul>
      </c:forEach>
      </c:when>
      </c:choose>
      


      

                </div>
            </div>

            <!-- 关联墓园   搜索墓园框-->
            <div class="search_my_num_bg"></div>
            <div class="search_my_num_content">
                <div class="glmy_infor_title">
                    <div class="glmy_infor__name">祈福账号</div>
                    <input type="text" placeholder="  请输入"  name="user_id"  id="user">
                    <div class="search_btn"    onclick="search();">搜索</div>
                </div>
                <div class="yst_title">绑定后可加入宗祠议事堂</div>

                <div class="glmy_center_cont">
                    <!-- 缩略图-->
                    <div class="cont_left"><img src=""  id="user_img"></div>
                    <div class="cont_center">
                        <div class="center_hang_1" id="u_name"></div>
                        <div class="center_hang_2" id="u_jianjie">
                       
                        </div>
                    </div>
                </div>
                <input name="us_id" id="us_id" type="hidden">
                <input name="oid" id="oid" type="hidden">

                <div class="my_foot_btn">
                    <div class="my_foot_qx">取消</div>
                    <div class="my_foot_qd"  onclick="queding()">确定</div>
                </div>

            </div>
  
  <script type="text/javascript">
  $(function (){


	    //关联账号上的关闭按钮
	    $(".glzh_window_close,.my_foot_qx").click(function(){
	        $(".search_my_num_bg,.search_my_num_content").css({display:"none"});
	    });

	  
  });
  //关联账号按钮点击，弹出“关联账号”窗口
  function guanlian(id){

    $(".zc_infor_bg,.zc_infor_content  ,  .jzls_bj_bg,.jzls_bj_content  ,  .jz_history_bg,.jz_history_content  ,  .history_photo_bg,.history_photo_content  ,  .buy_mb_bg,.buy_mb_content  ,  .change_sty_bg,.change_sty_content  , .add_jnr_bg,.add_jnr_content  ,  .tsjn_bg,.tsjn_content  ,   .search_my_num_bg,.search_my_num_content  ,  .bg_music_bg,.bg_music_content  ,  .jisi_manage_bg,.jisi_manage_content  ,  .limit_set_bg,.limit_set_content  ,  .add_manage_bg,.add_manage_content  , .jsrz_bg,.jsrz_content ").css({display:"none"});

    $(".search_my_num_bg,.search_my_num_content").css({display:"block"});
    $("#oid").val(id);
    
    
}
  function search(){
	  var user_name=$("#user").val();
	  var url = "/webAncestralBz/getUser?user_id="+user_name+"&";
	    $.get(url,function(data){
	      //document.location.reload();
	      $("#user_img").attr("src",data.user_head_photo);
	      $("#u_name").html(data.user_account);
	      $("#u_jianjie").html(data.user_underwrite);
	      $("#us_id").val(data.user_id);
	    },"json");
  }
  function queding(){
	  var user_name=$("#us_id").val();
	  var oid=$("#oid").val();
	  var url = "/webAncestralBz/toguanlian?user_id="+user_name+"&ancestral_hall_genealogy="+oid+"&ancestral_hall_obid=${pd.ancestral_hall_obid}";
	    $.get(url,function(data){
//          alert(data);
	     document.location.reload();
	    });
  }
  </script>
  </body>
</html>
