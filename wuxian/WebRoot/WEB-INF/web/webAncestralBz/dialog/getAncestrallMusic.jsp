<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'getCemeteryMusic.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
  </head>
  
  <body>
    <!-- 背景音乐-->
            <div class="bg_music_content">
                <div class="bjyy_window_content">
                <c:choose>
                <c:when test="${not empty list }">
                <c:forEach items="${list }" var="var" >
                    <ul>
                        <li class="list_1"><img src="/resources/images/srmy_music_img.png"></li>
                        <li class="list_2">${var.musicName}</li>
                        <li class="list_3"><a href="${var.musicUrl }"><img src="/resources/images/bg_music_try_listen.png"></a></li>
                        <li class="list_4">
                        <c:if test="${pd.url eq var.musicUrl }">
                            <img src="/resources/images/srmy_choose_pre.png" class="srmy_music_choose">
                        </c:if>
                               <c:if test="${pd.url != var.musicUrl }">
                        <a href="/webAncestralBz/setMusic?ancestral_hall_obid=${pd.ancestral_hall_obid }&music=${var.musicUrl}">       <img src="/resources/images/srmy_choose_n.png" class="srmy_music_choose" ></a>
                        </c:if>
                      
                        </li>
                    </ul>
                </c:forEach>
                </c:when>
                </c:choose>
             </div>
            </div>
  
  
  </body>
</html>
