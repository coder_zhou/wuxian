
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'juanzeng.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  
  <link href="/resources/css/zongci/zong_ci.css" rel="stylesheet" type="text/css"></link>
  <script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
  		        <!-- 捐赠-->
            <div class="jzfb_content">
                <div class="jzfb_window_content">
                    <div class="jsfb_title">
                        <div class="jz_money_title">宗祠捐赠总额：</div>
                        <div class="jzfb_money">${am.ancestral_hall_fb_combined }福币</div>
                        <div class="jz_btn">
                            <div class="jz_btn_img"><img src="/resources/images/zc_jz_btn.png"></div>
                            <div class="jz_btn_wz">我要捐赠</div>
                        </div>
                    </div>

                    <ul class="jz_title">
                        <li class="jz_per_photo">头像</li>
                        <li class="jz_per_name">用户昵称</li>
                        <li class="jz_time">时间</li>
                        <li class="jz_money">捐赠福币</li>
                    </ul>
		<c:choose>
				<c:when test="${ not empty list}">
				<c:forEach items="${ list.donation_info}" var="var" varStatus="vs">
				      <ul class="jz_content">
                        <li class="jz_per_photo"><a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.donation_user_myid }" target="_blank"><img src="${var.donation_nick_photo }"></a></li>
                        <li class="jz_per_name"><a  href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.donation_user_myid }" target="_blank">${var.donation_nick_name }</a></li>
                        <li class="jz_time">
                        <c:set target="${myDate}" property="time" value="${var.donation_time_int*1000}"/>
                        <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
                        </li>
                        <li class="jz_money">${var.donation_fb }福币</li>
                    </ul>
				</c:forEach>
				</c:when>
				</c:choose>
                </div>
            </div>


            <!-- 捐赠金额数  -->
            <div class="jzje_bg  bg"></div>
            <div class="jzje_content">
                <div class="j_z_money">捐 赠 金 额</div>
                <div class="jz_input"><input type="text"   name="fb" id="fb"  placeholder="&nbsp;请输入" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"></div>
                <div class="jz_an_niu" onclick="sub();">捐  赠</div>
                <div class="jz_message">捐赠金额单位为福币</div>
            </div>
            <script>
            $(function (){
//            	捐赠金额
            	$(".juan_zeng_jin_e").click(function(){
                    $(".jzfb_bg,.jzfb_content").css({display:"block"});
                });
                $(".jz_btn_wz").click(function(){
                    $(".jzje_bg,.jzje_content").css({display:"block"});
                });

            });	
            function sub(){
            	     var ff=$("#fb").val();
            	     var url = "/Ancestral/juanzeng?fb="+ff+"&ancestral_hall_obid=${pd.ancestral_hall_obid}";
            	     location.href =url;
            }
            </script>
            
  </body>
</html>
