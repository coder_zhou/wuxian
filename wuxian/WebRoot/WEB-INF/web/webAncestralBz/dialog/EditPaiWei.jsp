<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'EditPaiWei.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
        <link href="/resources/css/ci_tang.css" rel="stylesheet" />
            <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
        
  </head>
  
  <body>
    <!-- 编辑排位-->
        <div class="bjpw_content">
            <div class="bjpw_window_content">
            <c:choose>
            <c:when test="${not empty list }">
            <c:forEach items="${list }" var="var">
                        <ul class="bjpw_cont_ul">
                    <li class="cont_ul_first"><img src="${var.genealogy_img }"></li>
                    <li class="cont_ul_second">${var.genealogy_name }</li>
                    <li class="cont_ul_third">字${var.genealogy_word }</li>
                    <li class="cont_ul_fourth">号${var.genealogy_known_as }</li>
                    <li class="cont_ul_fifth">[
                   <c:if test="${empty var.tablet_name }">
                   ${var.genealogy_name }之位
                   </c:if> 
                      <c:if test="${not empty var.tablet_name }">
                   ${var.tablet_name }之位
                   </c:if> 
                    ]</li>
                    <li class="cont_ul_sixth"><div class="bjpw_btn" onclick="toUpdate('${var._id}','${var.tablet_style }');">编辑排位</div></li>
                </ul>
            
            </c:forEach>
            </c:when>
            </c:choose>
            </div>
        </div>
        <!-- 编辑排位  编辑框-->
        <form action="/webAncestralBz/UpdatePaiWei"  method="post" id="form">
        <input name="_id"  type="hidden"  id="oid">
         <input type="hidden" name="tablet_style"  id="tablet_style"/>
         <input type="hidden" name="ancestral_hall_obid" value="${pd.ancestral_hall_obid}" />
        <div class="bian_ji_kuang_bg  bg"></div>
        <div class="bian_ji_kuang_content">
            <div class="pw_title">
                <div class="pw_name">牌 位 名 称</div>
                <input type="text"  name="tablet_name"  placeholder="  请输入">
            </div>
            <div class="please_choose_title">请选择排位样式</div>

            <div class="bjk_center_cont">
                <div class="pw_kind_1">
            <img src="/resources/images/zongci/pai_wei_choose_pre.png"  id="img1"  class="pw_kind_choose"  onclick="check1();"> 
                    <div class="pw_kind_infor">
                        <div class="pw_img"><img src="/resources/images/zongci/card1.png"></div>
                        <div class="pw_cai_zhi">木质牌位</div>
                        <div class="pw_price">( 0福币 )</div>
                    </div>
                </div>

                <div class="pw_kind_1">
                 <img src="/resources/images/zongci/pai_wei_choose_n.png"  id="img2"  class="pw_kind_choose" onclick="check2();"> 
                    <div class="pw_kind_infor">
                        <div class="pw_img"><img src="/resources/images/zongci/card2.png"></div>
                        <div class="pw_cai_zhi">金质牌位</div>
                        <div class="pw_price">( 500福币 )</div>
                    </div>
                </div>
            </div>

            <div class="pw_foot_btn">
                <div class="pw_foot_qd"  onclick="sub();">确定</div>
                <div class="pw_foot_qx">取消</div>

            </div>

        </div>
        </form>
  <script type="text/javascript">
  $(function (){
	  //点击编辑排位，弹出编辑层
	  $(".bjpw_btn").click(function(){
	
	  });
	  //编辑层上的取消
	  $(".pw_foot_qx").click(function(){
	      $(".bian_ji_kuang_bg,.bian_ji_kuang_content").css({display:"none"});
	  });
	  //编辑层上的确定
	  $(".pw_foot_qd").click(function(){
	      $(".bjpw_bg,.bjpw_content,.bian_ji_kuang_bg,.bian_ji_kuang_content").css({display:"none"});
	  });

	  //关闭窗
	  $(".bjpw_window_close").click(function(){
	      $(".bjpw_bg,.bjpw_content").css({display:"none"});
	  });  
	  
  });
function toUpdate(Id,St){
	$("#oid").val(Id);
	$("#tablet_style").val(St);
    $(".bian_ji_kuang_bg,.bian_ji_kuang_content").css({display:"block"});
}
 function sub(){
	 $("#form").submit();
 } 
function check1(){
	$("#img1").attr("src","/resources/images/zongci/pai_wei_choose_pre.png");
	$("#img2").attr("src","/resources/images/zongci/pai_wei_choose_n.png");
	$("#tablet_style").val(1);
}
function check2(){
	$("#img1").attr("src","/resources/images/zongci/pai_wei_choose_n.png");
	$("#img2").attr("src","/resources/images/zongci/pai_wei_choose_pre.png");
	$("#tablet_style").val(2);
}
  </script>
  
  </body>
</html>
