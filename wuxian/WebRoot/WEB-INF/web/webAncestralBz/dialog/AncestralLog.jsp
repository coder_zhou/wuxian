<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'AncestralLog.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
        <link href="<%=path%>/resources/css/zongci/zong_ci.css" rel="stylesheet" />
  </head>
  
  <body>

            <!-- 祭祀管理-->
            <div class="jisi_manage_content">
                <div class="jsgl_window_content">
                
                <c:choose>
                <c:when test="${not empty lm }">
                <c:forEach items="${lm }" var="var">
                       <ul>
                        <li class="jsgl_list_1"><img src="/resources/images/create_tree_goods.png"></li>
                        <li class="jsgl_list_2">${var.sacrifice_materials }</li>
                        <li class="jsgl_list_3">${var.sacrifice_nick_name }</li>
                        <li class="jsgl_list_4">${var.sacrifice_time_str }</li>
                        <li class="jsgl_list_5">
                        <c:if test="${is_manage==1}">
                 
                        <a href="/webAncestralBz/delAncestrallLog?ancestral_hall_obid=${pd.ancestral_hall_obid }&sacrificeId=${var._id}"><img src="/resources/images/fo_my_wish_delete.png"></a>
                               </c:if>
                        </li>
                    </ul> 
                </c:forEach>
                </c:when>
                </c:choose>
                
            
   
   
                </div>
            </div>

  </body>
</html>
