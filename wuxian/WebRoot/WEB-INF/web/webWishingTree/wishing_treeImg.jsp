<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网->个人->许愿树</title>

    <link href="<%=path%>/resources/css/wishingtree/wishing_tree.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/wishingtree/wishing_tree.js"></script>

	<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript">
	
    </script>
</head>
<body>
        <!-- 许愿树部分-->
        <div class="wdmy_wrap" style="width:960px;left:0px;border:none;">
            <!-- 许愿树放置物品-->
            <div class="srmy_main" id="srmy_main" style="position:absolute;left:0;bottom:0;width:100%;height:100%;">
		<c:forEach var="obj" items="${sclist}" varStatus="status">
	        <c:choose>
	          <c:when test="${obj.materials_class_id eq '37'}"><%--  背景 素材分分类id ==37    --%>
	            <img id="div${obj.wish_tree_materials_id}" materials_info="${obj.wish_tree_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:${obj.wish_tree_materials_x}px;top:${obj.wish_tree_materials_y}px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	          </c:when>
	          <c:otherwise>
	            <img id="div${obj.wish_tree_materials_id}"  width_info="${obj.wish_tree_materials_width}" height_info="${obj.wish_tree_materials_height}" 
		            materials_info="${obj.wish_tree_materials_id}" 
		            ${((obj.wish_user_id == user_id && obj.materials_limit != 3) || (is_user_tree_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
		            data-name="img${status.index+1}" 
		            data-id="url${status.index+1}" 
		            title="${obj.materials_name}-${obj.materials_class_id}" 
		            style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:${obj.wish_tree_materials_x}px;top:${obj.wish_tree_materials_y}px;width:${obj.wish_tree_materials_width}px;height:${obj.wish_tree_materials_height}px;${obj.wish_user_id == user_id && obj.materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
		            src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	          </c:otherwise>
	        </c:choose>
  		</c:forEach>
            </div></div></body>
</html>