<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网->个人->许愿树</title> 

    <link href="<%=path%>/resources/css/wishingtree/wishing_tree.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/wishingtree/wishing_tree.js"></script>

	<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
	    <script src="/resources/index/js/html5shiv.min.js"></script>
	    <script src="/resources/index/js/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
	
		//弹出二级素材
    	function onShowMaterialsClass2(materials_class_id){
    		var src = "/webWishingTree/queryMaterialsList?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);
    		$("#showMaterialsList").attr("style","display: block; ");
    	}
	    
	    //切换墓园祭祀、墓园布置
    	function onChooseJisiOrBuzhi(flag, cemetery_id,chooseflag, is_user_tree_flag){
    		
			if(flag == 1){
    			$("#div_jisi").attr("class","");
    			$(".jisi").css({backgroundColor:"#fd844e"});
    			
    			if(is_user_tree_flag > 0 ){
    				$(".buzhi").css({backgroundColor:"#069dd5"});
    			}
   				$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
			if(flag == 2){
				$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_tree_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#fd844e"});
	    		}
    			$("#div_buzhi").attr("class","");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
    		if(flag == 3){
    			$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_tree_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#069dd5"});
	    		}
	    		$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","");
    			$(".choose_ceng").css({backgroundColor:"#fd844e"});
    		}
    		
    		$("#tabflag").val(flag);
    	}
    	
    	//弹出信物弹出窗
    	function onShowXinWu(materials_small_img, materials_measured, materials_price, materials_id, materials_name){
    		onClearXinwu();
    		//时效
	        if(materials_measured == 1){
	        	$("#make_wish_content_time_time").text("天");
	        }else if(materials_measured == 1){
	        	$("#make_wish_content_time_time").text("月");
	        }else if(materials_measured == 1){
	        	$("#make_wish_content_time_time").text("年");
	        }else{
	        	$("#make_wish_content_time_time").text("-");
	        }
	        //价格
	        $("#make_wish_content_price_price").text(materials_price + "福币");
	        //名称
	        $("#make_wish_content_time_wish_img_name").text(materials_name);
	        //总计
	        $("#make_wish_content_tree_sum_amount").text(materials_price + "福币");
	        $("#make_wish_content_tree_sum_amount_vip").text( Math.floor(materials_price * $('#vipcount').val()) + "福币");
    		
    		//图片路径
    		$("#make_wish_content_wish_img").attr("src","<%=path%>/goods"+materials_small_img);
    		
    		$("#xinwu_materials_id").val(materials_id);
    		$(".make_wish_bg,.make_wish_content").css({display:"block"});
    	}
    	
    	//重置信物弹出窗
    	function onClearXinwu(){
    		
    		$("#xinwu_wish_title").val('');
    		$("#xinwu_wish_password").val('');
    		$("#xinwu_wish_info").val('');
    	}
    	
    	//保存信物
    	function onSaveXinwu(){

			var xinwu_wish_info = $("#xinwu_wish_info").val();
			var wish_title = $("#xinwu_wish_title").val();//愿望标题
			
			if(wish_title == null || wish_title == ''){
				alert("请填写愿望标题！");
			}else if(wish_title.length > 30){
		   		alert('愿望标题不得超过30字！');
		   	}else if(xinwu_wish_info != null && xinwu_wish_info.length > 200){
		   		alert('愿望限输入 200 个文字！');
		   	}else{
				$("#xinwuForm").ajaxSubmit(function(result){
	    			if(result == 2){//未登陆
	    				alert("请先登录！");
		    			location.href="<%=path%>/";
	    			}else if(result == 1){
	    				alert("跳转充值页面");
	    			}else{
						location.href="<%=path%>/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${private_wish_tree_id}&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
	    			}
				});
			}
    		
    	}
    	
    	//保存
    	function save(){
    		var wish_tree_id = $("#input_private_wish_tree_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="wish_tree_id" type="hidden" value="' + wish_tree_id + '" />';
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webWishingTree/saveWishTreeMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
				location.href="<%=path%>/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${private_wish_tree_id}&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
			
			});
    	}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    		var vipcount = $('#vipcount').val();
    		
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<div class="confirm_container" >';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price_sum") * 1;
    			}
    			
				if(i % 2 == 0){
					str = str + '<div class="module_first">';
				}else{
					str = str + '<div class="module_second">';
				}
				
				if($(arrTab[i]).attr("ifCheck") == 1){
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" checked="checked" >';
				}else{
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" >';
				}
                str = str + '<div class="module_content">';
                str = str + '<div class="module_title">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<img src="' + $(arrTab[i]).attr("img_url") + '" class="module_goods">';
                str = str + '<div class="module_goods_infor">';
                if($(arrTab[i]).attr("parent_id") != 3){
                	str = str + '<div class="aging">';
	                str = str + '<div class="aging_first">时效</div>';
	                str = str + '<div class="reduce" onclick="onAddDate(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >-</div>';
	                str = str + '<div class="number_sum" id="div_date_count_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</div>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<div>天 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<div>月 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<div>年</div>';
	                }else{
	                	str = str + '<div> - </div>';
	                }
	                str = str + '<div class="add" onclick="onAddDate(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >+</div>';
	                str = str + '</div>';
                }
                str = str + '<div class="number">';
                str = str + '<div class="number_first">数量</div>';
                str = str + '<div class="reduce" onclick="onAddCount(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</div>';
                str = str + '<div class="number_sum" id="div_number_sum_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</div>';
                str = str + '<div>个</div>';
                
                if($(arrTab[i]).attr("materials_class_id") == 37){//背景,不能增加数量
                	str = str + '<div class="add" onclick="alert(\'背景只能买一个！\');">+</div>';
                }else{
                	str = str + '<div class="add" onclick="onAddCount(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</div>';
                }
                
                str = str + '</div>';
                str = str + '<div class="sum">';
                str = str + '<div class="sum_first">小计</div>';
                str = str + '<div class="sum_price" id="div_sum_price_' + $(arrTab[i]).val() + '"><span>' + $(arrTab[i]).attr("materials_price_sum") + '</span>福币</div>';
                str = str + '</div>';
                str = str + '</div>';
                
//                 str = str + '<div class="line"></div>';
//                 str = str + '<div class="kind_price">';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/kinship_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_family") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_wealth") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>200</span>';
//                 str = str + '</div>';
//                 str = str + '</div>';
                str = str + '</div>';
                str = str + '</div>';
            }
            
            fb_sum = fb_sum * 1
            str = str + '</div>';
            str = str + '<div class="pay">';
            str = str + '<div class="pay_sum">总计：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + fb_sum + '</span>福币</div>';
            
            
            str = str + '<div class="pay_sum">会员价：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + Math.floor(fb_sum * vipcount) + '</span>福币</div>';
            
            
            str = str + '</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="balance">';
	            str = str + '<div class="pay_sum">余额不足：</div>';
	            str = str + '<div class="pay_price"><span>' + (user_fb - fb_sum) + '</span>福币</div>';
	            str = str + '</div>';
            }
            str = str + '<div class="pay_btn">';
            if(user_fb < fb_sum){
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >补充福币</div>';
            }else{
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >确定</div>';
            }
			
            str = str + '<div class="pay_cancel" onclick="onClear();">取消</div>';
            str = str + '</div>';
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
    		$("#show").attr("style","display: block; ");
			$(".bg_confirm,.show_confirm").attr("style","display: block; ");
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少时效
    	function onAddDate(flag, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//关闭结算页面
    	function onHideDivJs(){
    		$("#show").attr("style","display: none; ");
			$(".bg_confirm,.show_confirm").attr("style","display: none; ");
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="private_wish_tree_id" value="' + $("#input_private_wish_tree_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#input_type_flag").val() + '"/><input name="wish_type" type="hidden" value="2" />';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webWishingTree/saveWishTreeMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
					location.href="<%=path%>/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${private_wish_tree_id}&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	//编辑信息
        function editWishTreeInfo(){
	        //查询许愿树的基本信息
	        var private_wish_tree_id = $("#input_private_wish_tree_id").val();
	        var url = "<%=path%>/webWishingTree/queryprivateWishTreeById";
	        $.post(url,{private_wish_tree_id:private_wish_tree_id},function(result){
	            $("#infor_color_name").html(result.private_wish_tree_name);
	            $("#infor_color_pwd").html(result.private_wish_tree_password);
	            $("#infor_color_pwd_info").html(result.private_wish_tree_info);
	            $("#infor_color_name_input").val(result.private_wish_tree_name);
	            $("#infor_color_pwd_input").val(result.private_wish_tree_password);
	            $("#infor_color_pwd_info_input").val(result.private_wish_tree_info);
	            
	            $(".infor_bg,.infor_content,.write_content").css({display:"block"});
        		$(".write_content_update,.wish_bg,.wish_content,.music_bg,.music_content,.limit_bg,.limit_content").css({display:"none"});
	        },'json');
        }
        //保存  编辑信息
         function saveEditWishTreeInfo(){
            var   private_wish_tree_id= $("#infor_color_id_input").val();
            var   private_wish_tree_name =  $("#infor_color_name_input").val();
            if(private_wish_tree_name==""){
               alert("请填写私人许愿树名称!");
               return;
            }
	        document.editWishTreeInfoForm.action = '<%=path%>/webWishingTree/saveEditWishTreeInfo';  
	        $("#editWishTreeInfoForm").ajaxSubmit(function(result){
	             if(result=="0"){
	                 alert("保存成功!");
	                  $(".infor_bg").css({display:"none"});
				      $(".infor_content").css({display:"none"});
				      $(".wish_bg,.wish_content,.music_bg,.music_content,.limit_bg,.limit_content").css({display:"none"});
	             }else{
	                 alert("保存失败!");
	             }
	        });
        }
        //愿望管理
        function toWishManegementIframe(){
             var wish_tree_id = $("#input_private_wish_tree_id").val();
             $(".wish_bg").css({display:"block"});
             $(".wish_content").css({display:"block"});
             $(".infor_bg,.infor_content,.music_bg,.music_content,.limit_bg,.limit_content").css({display:"none"});
             $("#wishManagementIframe").attr("src","<%=path%>/webWishingTree/toWishManegementIframe?wish_type=2&wish_tree_id="+wish_tree_id);
        }
         
		//删除素材
    	function onDelMaterials(wish_tree_materials_id,wish_tree_id){
            if(wish_tree_materials_id != null && wish_tree_materials_id != ''){
	    		$.ajax({
					type: "POST",
					url: '/webWishingTree/delWishTreeMaterialsByWishTreeMaterialsId',
			    	data: {wish_tree_materials_id : wish_tree_materials_id},
					dataType:'json',
					cache: false,
					success: function(data){
						 if(data == 0){
						 	//$(".srmy_FixedAdorn").remove();
							var namelist = $("img[name='1']");
							for(var i=0; i<namelist.length; i++){
								
							 	if($(namelist[i]).attr("materials_info") == wish_tree_materials_id){
							 		namelist[i].remove();
							 	}
							}
						}else{
							alert('操作失败，请联系管理员！');
						}
					}
				});
			}else{
    			alert("请选择图层！");
    		}
    	}
    	
    	//清楚勾选记录
    	function onClear(){
//     		if(confirm('关闭将清空勾选记录，是否确认关闭？')){
    			$('#Form', parent.document).empty();
    			$(".bg_confirm, .show_confirm").attr("style","display: none; ");
    			$("#showMaterialsList", parent.document).attr("style","display: none; ");
//     		}
    	}
         
    </script>
</head>
<body>
<input type="hidden" id="vipcount" value="${vipcount == null ? 1 : vipcount}" />
<input type="hidden" id="z_index" value="${z_index}"/>
<input type="hidden" id="choose_img_cemetery_materials_id" value=""/>
<form action="/qjqwmain/cemetery/list" method="post" name="saveForm" id="saveForm">
</form>
<form action="/qjqwmain/cemetery/list" method="post" name="ff" id="ff">
</form>
<input type="hidden" id="input_private_wish_tree_id" value="${private_wish_tree_id}"/>
<input type="hidden" id="tabflag" value="${flag}"/>
<input type="hidden" id="input_type_flag" value="2"/>
<form action="/qjqwmain/cemetery/list" method="post" name="Form" id="Form">
  
</form>
<div class="main">
  <div class="header">
    <div class="srmy_wzgg">公告：纪念512汶川地震7周年</div>
    <img src="<%=path%>/resources/images/logo.png" class="srmy_logo" />
    <!-- 菜单-->
    <ul class="srmy_menu">
      <li class="active"><a href="#">许 愿 树</a></li>
      <li><a href="<%=path%>/">网站首页</a></li>
      <li><a href="<%=path%>/userManage">个人后台</a></li>
    </ul>
    <div class="tree_infor_bg"></div>
    <div class="tree_infor">
      <div class="tree_name">${privateWishTree.private_wish_tree_name}</div>
      <div class="tree_create_user">创&nbsp;建&nbsp;人&nbsp;<span>${privateWishTree.user_nick_name}</span></div>
      <div class="tree_create_time">创建时间<span>${privateWishTree.private_wish_tree_add_time}</span></div>
      <div class="tree_infor_look">
        <div class="srmy_infor_ico_01">
          <div>浏览</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_01.png" />
          <div>${privateWishTree.private_wish_tree_clicks}</div>
        </div>
        <div class="srmy_infor_ico_02">
          <div>许愿</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_02.png" />
          <div>${privateWishTree.private_wish_count}</div>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <!-- 许愿树部分-->
    <div class="wdmy_wrap">
      <!-- 许愿树放置物品-->
      <div class="srmy_main" id="srmy_main">
		<c:forEach var="obj" items="${sclist}" varStatus="status">
     	  <c:choose>
	        <c:when test="${obj.materials_class_id eq '37'}"><%--  背景 素材分分类id ==37    --%>
	          [${obj.wish_tree_materials_id}]<img id="div${obj.wish_tree_materials_id}" materials_info="${obj.wish_tree_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:${obj.wish_tree_materials_x}px;top:${obj.wish_tree_materials_y}px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	        </c:when>
       	    <c:otherwise>
       		  <div id="div${obj.wish_tree_materials_id}" 
            	   ${((obj.wish_user_id == user_id && obj.materials_limit != 3) || (is_user_tree_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
            	   data-name="img${status.index+1}" 
         		   data-id="url${status.index+1}" 
            	   style="position:absolute;z-index:${obj.wish_tree_materials_z_index};left:${obj.wish_tree_materials_x}px;top:${obj.wish_tree_materials_y}px;width:${obj.wish_tree_materials_width}px;height:${obj.wish_tree_materials_height}px;" 
            	   width_info="${obj.wish_tree_materials_width}" 
            	   height_info="${obj.wish_tree_materials_height}" 
         		   materials_info="${obj.wish_tree_materials_id}" 
            	   name="1"
            	   imgwidth="${obj.wish_tree_materials_width}" 
         		   imgweight="${obj.wish_tree_materials_height}" >
                <div class="cao_zuo">
                  <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                  <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                  <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                  <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
              	</div>
         	    <img title="${obj.materials_name}-${obj.materials_class_id}" 
		             style="width:${obj.wish_tree_materials_width}px;height:${obj.wish_tree_materials_height}px;${obj.wish_user_id == user_id && obj.materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
		             src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
          	  </div>
       	    </c:otherwise>
     	  </c:choose>
  	    </c:forEach>
      </div>
            
      <div class="foot_cao_zuo"></div>
      <div class="cao_zuo_btn">
        <div class="foot_big">
          <div><img src="<%=path%>/resources/images/srmy_materisl_big.png" /></div>
          <div>放大</div>
        </div>
        <div class="foot_small">
          <div><img src="<%=path%>/resources/images/srmy_materisl_reduce.png" /></div>
          <div>缩小</div>
        </div>
        <div class="buzhi_before">
          <div><img src="<%=path%>/resources/images/srmy_materisl_before.png" /></div>
          <div>置前</div>
        </div>
        <div class="buzhi_after">
          <div><img src="<%=path%>/resources/images/srmy_materisl_after.png" /></div>
          <div>置后</div>
        </div>
        <div class="foot_delete">
          <div><img src="<%=path%>/resources/images/srmy_materisl_delete.png" /></div>
          <div>删除</div>
        </div>
      </div>

      <!-- 编辑信息-->
      <div class="infor_bg"></div>
      <div class="infor_content">
        <div class="kind">编 辑 信 息</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <!-- 背景-->
        <div class="write_content">
          <!-- 修改按钮-->
          <div class="infor_write_title">
            <div class="infor_write_btn" >
              <img src="<%=path%>/resources/images/user_article_write_btn.png" />
              <div>修改</div>
            </div>
          </div>

          <!-- 信息的详细内容-->
          <div class="infor_container" id="write_content_update">
            <div>许愿树名称：<span class="infor_color" id="infor_color_name">许愿树名称</span></div>
            <div>访 问 密 码：<span class="infor_color" id="infor_color_pwd">123456</span></div>
            <div>空 间 留 言：<span id="infor_color_pwd_info">许愿树的简介许愿树的简介许愿树的简介</span></div>
          </div>

        </div>
        <form id="editWishTreeInfoForm" name="editWishTreeInfoForm" method="post">
          <input type="hidden" id="infor_color_id_input" name="private_wish_tree_id" value="${private_wish_tree_id}"/>
          <div class="write_content write_content_update" id="write_content_update">
            <!-- 信息的详细内容-->
            <div class="infor_container infor_container_write">
              <div>许愿树名称
                <input type="text" value="许愿树名称"  id="infor_color_name_input" name="private_wish_tree_name"/>
                <img src="<%=path%>/resources/images/write_infor_not_null.png" class="write_not_null" />
              </div>
              <div>访 问 密 码
                <input type="password"  value="123456" id="infor_color_pwd_input" name="private_wish_tree_password"/>
                <span class="wish_choose_write" >(选填，必须是数字)</span>
              </div>
              <div class="space_message">空 间 留 言<textarea  id="infor_color_pwd_info_input" name="private_wish_tree_info"></textarea></div>
            </div>
            <input type="button" value="确定" class="write_content_btn" onclick="javascript:saveEditWishTreeInfo();"/>
          </div>
        </form>
      </div>

      <!-- 愿望管理-->
      <div class="wish_bg"></div>
      <div class="wish_content">
        <div class="kind">愿 望 管 理</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />
		<iframe id="wishManagementIframe" name="wishManagementIframe"  style="width:100%;height:440px;border:none;" scrolling="no" iframeborder="no" ></iframe>            
	  </div>

      <!-- 修改愿望-->
      <div class="wish_update_bg"></div>
      <div class="wish_update_content">
        <div class="kind">修 改 愿 望</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="wish_update_container">
          <!-- 信息的详细内容-->
          <div class="infor_container wish_infor">
            <div>愿&nbsp;望&nbsp;标&nbsp;题&nbsp;&nbsp;
              <input type="text" value="" />
              <img src="<%=path%>/resources/images/write_infor_not_null.png" class="write_not_null" />
            </div>
            <div>查&nbsp;看&nbsp;密&nbsp;码&nbsp;&nbsp;
              <input type="text" value="" />
              <span class="wish_choose_write">(选填，必须是数字)</span>
            </div>
            <div class="space_message">愿&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;望<textarea ></textarea></div>
          </div>
          <input type="button" value="确定" class="write_content_btn" />
        </div>
      </div>

      <!-- 愿望查看密码-->
      <div class="wish_look_pwd_bg"></div>
      <div class="wish_look_pwd_content">
        <div class="kind">查 看 密 码</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="wish_look_container">
          <div class="container_content">
            <span class="please_write_title">请输入查看密码：</span>
            <input type="text" class="write_pwd" />
          </div>

          <input type="button" value="确定" class="write_pwd_btn" />
        </div>
      </div>

      <!-- 愿望详情-->
      <div class="wish_infor_bg"></div>
      <div class="wish_infor_content">
        <div class="kind">愿 望</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="wish_infor_container">
          <div class="container_model">
            <div class="container_model_tit">愿 望 标 题&nbsp;&nbsp;:</div>
            <div class="container_model_cont">愿望标题文字</div>
          </div>
          <div class="container_model">
            <div class="container_model_tit">许&nbsp;&nbsp;&nbsp;愿&nbsp;&nbsp;&nbsp;者&nbsp;&nbsp;:</div>
            <div class="container_model_cont">许愿者昵称</div>
          </div>
          <div class="container_model">
            <div class="container_model_tit">填 写 时 间&nbsp;&nbsp;:</div>
            <div class="container_model_cont">2015-07-22</div>
          </div>
          <div class="container_model">
            <div class="container_model_tit">浏 览 次 数&nbsp;&nbsp;:</div>
            <div class="container_model_cont">22</div>
          </div>
          <div class="container_model">
            <div class="container_model_tit">愿&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;望&nbsp;&nbsp;:</div>
            <div class="container_model_wish">我的愿望我的愿望我的愿望我的愿望我的愿望我的愿望我的愿望我的愿望</div>
          </div>
          <div class="container_model_wish_img">
            <img src="<%=path%>/resources/images/make_wish_img01.png" />
          </div>
        </div>
      </div>

      <!--背景音乐-->
      <div class="music_bg"></div>
      <div class="music_content">
        <div class="kind">背 景 音 乐</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="music_container">
          <ul class="container_list_odd">
            <li class="music_name">无</li>
            <li class="music_name_addr">无背景音乐</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">我爱的人和爱我的人</li>
            <li class="music_name_addr">我爱的人和爱我的人</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">三百六十五个祝福</li>
            <li class="music_name_addr">三百六十五个祝福</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">祈祷</li>
            <li class="music_name_addr">祈祷</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">铃儿响叮当</li>
            <li class="music_name_addr">铃儿响叮当</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">广岛之恋</li>
            <li class="music_name_addr">广岛之恋</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">虫儿飞</li>
            <li class="music_name_addr">虫儿飞</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">哀乐</li>
            <li class="music_name_addr">哀乐</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
          <ul class="container_list_odd">
            <li class="music_name">故乡的原风景</li>
            <li class="music_name_addr">故乡的原风景</li>
            <li class="music_try">试听</li>
            <li class="music_choose">选择</li>
          </ul>
        </div>
      </div>

      <!-- 权限设置-->
      <div class="limit_bg"></div>
      <div class="limit_content">
        <div class="kind">权 限 设 置</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />
      </div>


      <!-- 空间布置-->
      <div class="space_fix_bg"></div>
      <div class="space_fix_content">
        <div class="kind">空 间 布 置</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="space_fix_container">
          <img src="<%=path%>/resources/images/space_fix_img01.png" />
          <div class="buy_tree_infor">
            <div class="wish_tree_name">许 愿 树</div>
            <div class="wish_tree_time">
              <div>时&nbsp;&nbsp;效</div>
              <div class="time_infor">永久免费</div>
            </div>
            <div class="wish_tree_price">
              <div>价&nbsp;&nbsp;格</div>
              <div class="price_infor">100福币</div>
            </div>
          </div>

          <div class="space_fix_line"></div>
          <div class="tree_sum">
            <div class="tree_sum_price">100福币</div>
            <div class="tree_sum_amount">总&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计&nbsp;&nbsp;:</div>
          </div>

          <!-- 余额不足时显示-->
          <div class="money_lack">
            <div class="user_balance_price">-50福币</div>
            <div class="user_balance_title">余额不足&nbsp;&nbsp;:</div>
          </div>

          <!-- 补充福币-->
          <div class="wish_tree_btn">
            <input type="button" value="补充福币" class="space_replenish_money" />
            <input type="button" value="取消" class="space_replenish_cancel" />
          </div>

        </div>
      </div>



      <!--提示层-->
      <div class="bg_prompt"></div>
      <div class="prompt_infor">
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />
        <div class="prompt_title">提 示</div>
        <div class="prompt_content">
          <img src="<%=path%>/resources/images/prompt_wait.png" />
          <div class="font">福币补充完成前，请不要关闭窗口。</div>
          <div class="behind">福币补充完成后，请根据情况点击一下按钮。</div>
          <div class="prompt_btn">
            <div class="issue">遇到问题</div>
            <div class="accomplish">完成</div>
          </div>
        </div>
      </div>

      <!-- 许愿-->
      <div class="make_wish_bg"></div>
      <div class="make_wish_content">
        <div class="kind">许 愿</div>
        <img src="<%=path%>/resources/images/close_window.png" class="close_window" />

        <div class="make_wish_container">
          <!-- 左侧信息-->
          <div class="wish_img_infor">
            <div class="wish_img">
              <img src="<%=path%>/resources/images/make_wish_img01.png" id="make_wish_content_wish_img" />
             </div>
             <div class="wish_img_name" id="make_wish_content_time_wish_img_name">真爱</div>
             <div class="wish_img_time">
               <div class="time_time" id="make_wish_content_time_time">30天</div>
               <div class="time_title">时效</div>
             </div>
             <div class="wish_img_price">
               <div class="price_price" id="make_wish_content_price_price">100福币</div>
               <div class="price_title">价格</div>
             </div>
           </div>

		  <form action="/webWishingTree/saveXinwu" method="post" name="xinwuForm" id="xinwuForm">
			<input type="hidden" name="materials_id" id="xinwu_materials_id" />
			<input type="hidden" name="wish_tree_id" id="xinwu_wish_tree_id" value="${private_wish_tree_id}" />
			<input type="hidden" name="wish_type" id="xinwu_wish_type" value="2" />   <!--  1表示公共许愿树的素材或愿望 2表示私人的许愿树和愿望   -->
              <!-- 右侧信息-->
              <div class="right_wish_write">
                <div class="wish_write_title">愿望标题
                  <input type="text" value="" name="wish_title" id="xinwu_wish_title" />
                  <img src="<%=path%>/resources/images/write_infor_not_null.png" class="write_not_null" />
                </div>
                <div>查看密码
                  <input type="password" value="" name="wish_password" id="xinwu_wish_password" onkeyup="value=value.replace(/[^\d]/g,'') " />
                  <span class="wish_choose_write">(选填，必须是数字)</span>
                </div>
                <div class="space_message">愿&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;望&nbsp;
                <textarea value="许愿树的简介许愿树的简介许愿树的简介" name="wish_info" id="xinwu_wish_info"></textarea></div>
                <div class="wish_number_limit"> 限输入 <span id="count1">200</span> 个文字 </div>
              </div>
			</form>
            <!-- 横线-->
            <div class="wish_img_line"></div>
            <div class="tree_sum">
              <div class="tree_sum_price" id="make_wish_content_tree_sum_amount_vip">100福币</div>
              <div class="tree_sum_amount" >会员价&nbsp;&nbsp;:</div>
            </div>
            <div class="tree_sum">
              <div class="tree_sum_price" id="make_wish_content_tree_sum_amount">100福币</div>
              <div class="tree_sum_amount" >总&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计&nbsp;&nbsp;:</div>
            </div>
            
            <!-- 按钮-->
            <div class="wish_tree_btn">
              <input type="button" value="重置" onclick="onClearXinwu();" />
              <input type="button" value="确认" onclick="onSaveXinwu();" />
            </div>
          </div>
        </div>
  	  </div>
    </div>

    <!-- 素材部分-->
    <div class="srmy_sc">
      <div class="srmy_suocai">
        <ul class="tab_menu">
          <li class="jisi" onclick="onChooseJisiOrBuzhi('1', '${private_wish_tree_id}', ${chooseflag}, '${is_user_tree_flag }');" ${flag == 1 ? 'style="background-color:#fd844e"' : ''}>许愿</li>
          <c:choose>
            <c:when test="${is_user_tree_flag > 0}"><!-- 是建造者，显示墓园布置列表 -->
              <li class="buzhi" onclick="onChooseJisiOrBuzhi('2','${private_wish_tree_id}',${chooseflag}, '${is_user_tree_flag }');" ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>空间布置</li>
            </c:when>
            <c:otherwise>
              <li class="buzhi" style='background-color: #bbb;' ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>空间布置</li>
            </c:otherwise>
          </c:choose>
          <li class="choose_ceng" onclick="onChooseJisiOrBuzhi('3','${private_wish_tree_id}',${chooseflag}, '${is_user_tree_flag }');" ${flag == 3 ? 'style="background-color:#fd844e"' : ''}>选择图层</li>
        </ul>
        <div class="srmy_tab_content">
          <!--许愿--> 
          <div id="div_jisi" class="sacrifice ${flag == 1 ? '' : 'content_hide'}">
            <ul>
              <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
            	<li onclick="onShowXinWu('${var.materials_small_img}','${var.materials_measured}','${var.materials_price}','${var.materials_id}','${var.materials_name }');">
              	  <img src="<%=path%>/goods${var.materials_small_img}" style="height: 29px;width: 29px;" />
              	  <div>${var.materials_name }</div>
            	</li>
          	  </c:forEach>
            </ul>
          </div>
          <!-- 空间布置-->
          <div class="sacrifice ${flag == 2 ? '' : 'content_hide'}" id="div_buzhi">
            <ul>
              <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
            	<li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
              	  <img src="<%=path%>/goods/${var.materials_class_img}" style="height: 29px;width: 29px;" />
              	  <div>${var.materials_class_name}</div>
            	</li>
          	  </c:forEach>
            </ul>
          </div>

          <!-- 选择图层-->
          <div class="${flag == 3 ? '' : 'content_hide'} srmy_fix_up" id="div_tuceng">
            <div class="srmy_bg">
              <c:forEach var="obj" items="${bglist}" varStatus="status">
                <c:if test="${status.index == (fn:length(bglist) - 1) }">
                  <li id="list${status.index+1}" class="cc" draggable="true">${obj.materials_name}
    			    <span class="buy_user_name">${obj.user_nick_name}祭祀</span>
                    <span class="buy_user"> </span>
                  </li>
                </c:if>
              </c:forEach>
            </div>
            <ul id="SortContaint" class="house fix_up">
              <c:forEach var="obj" items="${movelist}" varStatus="status">
                <c:if test="${obj.materials_class_id != 37}">
   				  <li id="list${obj.wish_tree_materials_id}" class="cc" draggable="true">
   					${fn:length(obj.materials_name) > 10 ? fn:substring(obj.materials_name, 0, 10) : obj.materials_name}${fn:length(obj.materials_name) > 10 ? '...' : ''}
   					<span class="buy_user_name">${obj.user_nick_name}祭祀</span>
               		<span class="buy_user">
                   	  <c:choose>
                     	<c:when test="${obj.materials_class_id == 8888 }"><!-- 文字信息   -->
                     	  <a href="#" onclick="onChangeWz('${obj.materials_id}','${private_wish_tree_id}', '${obj.wish_tree_materials_id}');">
					  		<img src="<%=path%>/resources/images/editwordblue.png" class="srmy_sucai_big" />
						  </a>
                     	</c:when>
                     	<c:otherwise>
                     	  <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="srmy_sucai_big" />
                   		  <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="srmy_sucai_reduce" />
                     	</c:otherwise>
                   	  </c:choose>
                   	  <img src="<%=path%>/resources/images/srmy_materisl_delete.png" class="srmy_sucai_delete" onclick="onDelMaterials(${obj.wish_tree_materials_id}, ${private_wish_tree_id});" />
                 	</span>
               	  </li>
                </c:if>
  			  </c:forEach>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!--按钮部分-->
    <div class="footer">
      <input type="button" value="编辑信息" class="write_infor"  onclick="javascript:editWishTreeInfo();"/>
      <input type="button" value="愿望管理" class="wish_manage" onclick="javascript:toWishManegementIframe()"/>
      <input type="button" value="背景音乐" class="bg_music" />
      <input type="button" value="权限设置" class="limit_set" />

      <!-- 保存按钮-->
      <input type="button" value="保存" id="sava_id" style="background-color:#bbbbbb" />
    </div>
    
	<!--商品类别详细信息-->
    <div id="bg"></div>
      
    <div id="showMaterialsList">
      <iframe src="" scrolling="no" iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
    </div>
    <div id="show"></div>
    
  	<!--结算购物车-->
    <div class="bg_confirm"></div>
    <div class="show_confirm">
      <div class="confirm_content" id="div_confirm_container_js">
      </div>
    </div>

</div>
</body>
</html>