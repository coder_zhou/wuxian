<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
   <link href="<%=path%>/resources/css/wishingtree/wishing_tree.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/wishingtree/wishing_tree.js"></script>

	<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
      function deleteWish(wish_tree_materials_id){
          var url = "<%=path%>/webWishingTree/deleteWishById";
          $.post(url,{wish_tree_materials_id:wish_tree_materials_id},function(result){
			 if(result=="0"){
			    alert("删除成功!");
				var namelist = $("img[name='1']",window.parent.document);
				for(var i=0; i<namelist.length; i++){
				 	if($(namelist[i],window.parent.document).attr("materials_info") == wish_tree_materials_id){
				 		$(namelist[i],window.parent.document).remove();
				 	}
				}
			    document.ff.action="<%=path%>/webWishingTree/toWishManegementIframe";
			    document.ff.submit();
			 }else{
			    alert("删除失败!");
			 }
          },'json');
      }
    
    </script>
</head>
<body>		
       <div class="wish_container" id="wish_container">
       <form id="gg" name="gg" method="post">
         <input type="hidden" id="dd_wish_tree_materials_id" name="wish_tree_materials_id"/>
                  <c:forEach items="${list}" var="obj" varStatus="j">
                    <ul class="container_list_odd">
                        <li class="wish_goods"><img src="<%=path%>/resources/images/create_tree_goods.png"></li>
                        <li class="wish_title">${obj.wish_title}
                          <c:if test="${obj.wish_password!=null && obj.wish_password !='' }">  
                            <img src="<%=path%>/resources/images/my_wish_open_no.png">    <!-- 锁 -->
                          </c:if>
                          <c:if test="${obj.wish_password==null || obj.wish_password =='' }">  
                             <img src="<%=path%>/resources/images/my_wish_open.png">  
                          </c:if>
                        </li>
                        <li class="wish_user">${obj.user_nick_name}</li>
                        <li class="wish_time">${obj.wish_tree_materials_add_time}</li>
                        <li class="wish_operate">
                            <c:if test="${sessionScope.USER_ID eq obj.wish_user_id}">
	                            <img src="<%=path%>/resources/images/wish_manage_n.png">
                            </c:if>
                            <c:if test="${sessionScope.USER_ID ne obj.wish_user_id}">
	                            <img src="<%=path%>/resources/images/wish_manage_pre.png">
                            </c:if>
<!--                             <img src="<%=path%>/resources/images/user_article_write.png" class="wish_write_img" > -->
                            <c:if test="${obj.wish_user_id eq user_id  || isAdmin eq '1'}">
                            	<img src="<%=path%>/resources/images/srmy_materisl_delete.png"  onclick="javascript:deleteWish('${obj.wish_tree_materials_id}');">
 							</c:if>                       
						</li>
                    </ul>
                    </c:forEach>
         </form>
              <c:if test="${list != null && fn:length(list) > 0}"> 
                <div class="pagination" style="float: right;padding-top: 0px;margin-top: 0px;height:30px;">
			  	  <ff:page mhFrom="ff" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="webWishingTree/toWishManegementIframe"  />
				</div>
                
			  	  <form id="ff" name="ff" method="post">
			  	     <input type="hidden" name="wish_tree_id" value="${wish_tree_id}"/>
			  	     <input type="hidden" name="wish_type" value="${wish_type}"/>
			  	     <input type="hidden" name="pageNo" value="${page.pageNo}" />
			  	     <input type="hidden" name="pageSize" value="${page.pageSize}" />
			  	     <input type="hidden" name="pageCount" value="${page.pageCount}" />
			  	  </form>
              </c:if>
         
         </div>
</body>
</html>

