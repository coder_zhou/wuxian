 <%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>全球祭祀祈福网-私人墓园</title>
    <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet">
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/cemetery/srmy.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    
      <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
     <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

	   <script type="text/javascript">
		$(document).ready(
	        function (){
		        editor = new UE.ui.Editor({initialFrameWidth:651, toolbars: [[
		            'bold', 'italic', 'underline', 'fontborder', 'forecolor',
		             'fontfamily', 'fontsize'
		        ]],elementPathEnabled : false,minFrameHeight:120,initialFrameHeight:120, autoHeightEnabled:false });  
		        editor.render("wz_info");  
	        }
    
        );
        
        //添加文字
        function setWz(materials_big_img){
        	editor.execCommand('insertHtml', materials_big_img);
        }
        
        //确定文字
        function onQdWz(){
        	parent.onQdWz(editor.getContent());
        }

    </script>
 
</head>
<body>

	   <!--编辑内容文本框-->
         <div class="miss_article_content">
           <textarea class="write_miss_content" name="wz_info" id="wz_info"></textarea>
           <div class="miss_article_share"></div>
         </div>
         <div class="queding_btn" onclick="onQdWz();">确定</div>

</body>
</html>