<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
<meta content="height=device-height,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no" name="viewport" />
    <title>全球祭祀祈福网-私人墓园</title>
    <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet">
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/cemetery/srmy.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript">
	
    	$(document).ready(function(){
    		//设置默认选中
			var arrTab=$('.class_materials', parent.document);
			for(var i=0;i<arrTab.length;i++){
				var id = $(arrTab[i]).val();
				$("#div_pitch_on_"+id).show();
            }
		});
		
    	//选中素材
    	function onChooseMaterials(materials_id, materials_name, img_url, materials_family, materials_wealth, materials_measured, materials_price, materials_class_id, parent_id){
    		var arrTab=$('.class_materials', parent.document);
    		
            for(var i=0;i<arrTab.length;i++){
				if($(arrTab[i]).val() == materials_id){
					 $(arrTab[i]).remove();
					 $("#div_pitch_on_"+materials_id).hide();
					 return false;
				}
            }
            
            //背景单选
    		if(materials_class_id == 10){
    			for(var i=0;i<arrTab.length;i++){
					if($(arrTab[i]).attr("materials_class_id") == 10 ){
						$(arrTab[i]).remove();
						$("#div_pitch_on_"+$(arrTab[i]).val()).hide();
					}
	            }
    		}
    		
            $("#div_pitch_on_"+materials_id).show();
            $("#Form", parent.document).append('<input type="hidden" name="materials_id" class="class_materials" id="input_materials_id_' + materials_id + '" value="'+materials_id+'" materials_name="'+materials_name+'" img_url="'+img_url+'" materials_family="' + materials_family + '" materials_wealth="' + materials_wealth + '" materials_measured="' + materials_measured + '" materials_measured_value="1" materials_count="1" materials_price_sum="' + materials_price + '" materials_price="' + materials_price + '" ifCheck="1" materials_class_id="' + materials_class_id + '" parent_id = "' + parent_id + '" />' );
    	}
    	
    	//清楚勾选记录
    	function onClear(){
//     		if(confirm('关闭将清空勾选记录，是否确认关闭？')){
    			$('#Form', parent.document).empty();
    			$("#showMaterialsList", parent.document).attr("style","display: none; ");
//     		}
    	}
    	
    	//弹出结算素材弹出窗
    	function onShowJs(){
    		$.ajax({
				type: "POST",
				url: '/webCemetery/queryUserFb',
				dataType:'json',
				cache: false,
				success: function(data){
					window.parent.onShowJsDiv(data);
				}
			});
    	}
    	
    	//显示鼠标滑动效果
//     	function onShowDiv(id){
//     		$("#div_goods_imgtext_"+id).show();
//     	}
    	
    	//隐藏鼠标滑动效果
//     	function onHideDiv(id){
//     		$("#div_goods_imgtext_"+id).hide();
//     	}
    
    </script>
</head>
<body>
<div class="kind" >${materials_class_name}</div>
  <img src="<%=path%>/resources/images/close_window.png" class="close_window" onclick="onClear();" />

  <!-- 背景-->
  <div class="kind_content"></div>

  <!-- 菜单选项-->
  <ul class="kind_title" id="kind_title">
    <c:choose>
      <c:when test="${flag == 0}"><!-- 没有三级素材类别列表，直接查询素材列表 -->
        <li class="active">${materials_class_name}</li>
      </c:when>
      <c:otherwise><!-- 有三级素材类别列表，先查询三级素材类别列表，在查询默认第一个素材三级类别下的素材列表 -->
        <c:forEach items="${materialsClassList}" var="var" varStatus="i">
          <c:choose>
      	    <c:when test="${i.index == 0}">
      	      <li class="class_tab active" onclick="onChangeMaterialsClassTab('${var.materials_class_id}',this);">${var.materials_class_name}</li>
      	    </c:when>
      	    <c:otherwise>
      	      <li class="class_tab" onclick="onChangeMaterialsClassTab('${var.materials_class_id}',this);">${var.materials_class_name}</li>
      	    </c:otherwise>
      	  </c:choose>
        </c:forEach>
      </c:otherwise>
    </c:choose>
  </ul>

  <div class="tab_content" id="div_kind_list">
    <c:choose>
      <c:when test="${flag == 0}"><!-- 没有三级素材类别列表，直接查询素材列表 -->
        <div class="content_first">
        <c:forEach items="${materialsList}" var="var" varStatus="i">
          <c:choose>
            <c:when test="${i.index % 6 == 0}">
              <div class="content_heng_first">
              <div class="goods_first">
            </c:when>
       	    <c:when test="${i.index % 6 == 1}">
       	      <div class="goods_second">
       	    </c:when>
       	    <c:when test="${i.index % 6 == 2}">
       	      <div class="goods_third">
       	    </c:when>
       	    <c:when test="${i.index % 6 == 3}">
       	      <div class="goods_four">
       	    </c:when>
       	    <c:when test="${i.index % 6 == 4}">
       	      <div class="goods_five">
       	    </c:when>
       	    <c:when test="${i.index % 6 == 5}">
       	      <div class="goods_six">
       	    </c:when>
          </c:choose>
          <div class="content_img">
            <img src="<%=path%>/resources/images/pitch_on.png" class="pitch_on" id="div_pitch_on_${var.materials_id}" />
        	<img src="<%=path%>/goods${var.materials_small_img}" 
        	  	 onclick="onChooseMaterials('${var.materials_id}','${var.materials_name}','<%=path%>/goods${var.materials_small_img}','${var.materials_family}','${var.materials_wealth}','${var.materials_measured}', '${var.materials_price}', '${var.materials_class_id}', '${var.parent_id}');" />
        	<div class="goods_text">
            <div class="goods_imgtext" style="display: none;" id="div_goods_imgtext_${var.materials_id}" >
			<div><img src="<%=path%>/resources/images/kinship_price.png" /><span>亲情</span><span>${var.materials_family}</span></div>	
			<div><img src="<%=path%>/resources/images/wealth_price.png" /><span>财富</span><span>${var.materials_wealth}</span></div>
           </div>
         </div>
         </div>
	     <div class="content_title">${fn:length(var.materials_name) > 5 ? fn:substring(var.materials_name, 0, 5) : var1.materials_name}${fn:length(var.materials_name) > 5 ? '...' : ''}</div>
	     <div class="content_price">${var.materials_price}福币/
           <c:choose>
             <c:when test="${var.materials_measured == 1}">天</c:when>
             <c:when test="${var.materials_measured == 2}">月</c:when>
             <c:when test="${var.materials_measured == 3}">年</c:when>
           </c:choose>
	     </div>
	     </div>
		 <c:if test="${i.index % 6 == 5}">
		   </div>
		 </c:if>
	     <c:if test="${fn:length(materialsList) % 6 != 0 && i.index == (fn:length(materialsList) - 1)}">
	       </div>
	     </c:if>
   	   </c:forEach>
	   </div>
	  </c:when>
            <c:otherwise><!-- 有三级素材类别列表，先查询三级素材类别列表，在查询默认第一个素材三级类别下的素材列表 -->
              <c:forEach items="${materialsLists}" var="var" varStatus="j">
                <c:choose>
                  <c:when test="${j.index == 0}">
                  
                    <c:forEach items="${materialsClassList}" var="var2" varStatus="k">
                      <c:if test="${j.index == k.index}">
                        <div class="content_first" id="div_content_${var2.materials_class_id}">
                      </c:if>
                    </c:forEach>
                  </c:when>
                  <c:otherwise>
                    <c:forEach items="${materialsClassList}" var="var2" varStatus="k">
                      <c:if test="${j.index == k.index}">
                        <div class="content_first content_hide" id="div_content_${var2.materials_class_id}">
                      </c:if>
                    </c:forEach>
                  </c:otherwise>
                </c:choose>
                <c:forEach items="${var}" var="var1" varStatus="i">
                  <c:choose>
            	    <c:when test="${i.index % 6 == 0}">
            	      <div class="content_heng_first">
						<div class="goods_first">
            	    </c:when>
            	    <c:when test="${i.index % 6 == 1}">
            	      <div class="goods_second">
            	    </c:when>
            	    <c:when test="${i.index % 6 == 2}">
            	      <div class="goods_third">
            	    </c:when>
            	    <c:when test="${i.index % 6 == 3}">
            	      <div class="goods_four">
            	    </c:when>
            	    <c:when test="${i.index % 6 == 4}">
            	      <div class="goods_five">
            	    </c:when>
            	    <c:when test="${i.index % 6 == 5}">
            	      <div class="goods_six">
            	    </c:when>
            	  </c:choose>
                  <div class="content_img">
                  <img src="<%=path%>/resources/images/pitch_on.png" class="pitch_on" id="div_pitch_on_${var1.materials_id}" />
                  <img src="<%=path%>/goods${var1.materials_small_img}" onmousemove="onShowDiv('${var1.materials_id}');" onmouseout="onHideDiv('${var1.materials_id}');" 
                  	   onclick="onChooseMaterials('${var1.materials_id}','${var1.materials_name}','<%=path%>/goods${var1.materials_small_img}','${var1.materials_family}','${var1.materials_wealth}','${var1.materials_measured}', '${var1.materials_price}', '${var1.materials_class_id}', '${var1.parent_id}');" />
                  <div class="goods_text">
                  <div class="goods_imgtext" style="display: none;" id="div_goods_imgtext_${var1.materials_id}">
                  <div><img src="<%=path%>/resources/images/kinship_price.png"><span>亲情</span><span>${var1.materials_family}</span></div>
                  <div><img src="<%=path%>/resources/images/wealth_price.png"><span>财富</span><span>${var1.materials_wealth}</span></div>
                  </div>
                  </div>
                  </div>
                  <div class="content_title">${fn:length(var1.materials_name) > 5 ? fn:substring(var1.materials_name, 0, 5) : var1.materials_name}${fn:length(var.materials_name) > 5 ? '...' : ''}</div>
                  <div class="content_price">${var1.materials_price}福币/
                    <c:choose>
	                  <c:when test="${var1.materials_measured == 1}">天</c:when>
	                  <c:when test="${var1.materials_measured == 2}">月</c:when>
	                  <c:when test="${var1.materials_measured == 3}">年</c:when>
	                </c:choose>
                  </div>
             	  </div>
             	  <c:if test="${i.index % 6 == 5}">
		            </div>
		          </c:if>
		          <c:if test="${fn:length(var) % 6 != 0 && i.index == (fn:length(var) - 1)}">
		            </div>
		          </c:if>
						 		
                </c:forEach>
                </div>
              </c:forEach>
					
            </c:otherwise>
          </c:choose>
		  
        </div>
        <div class="tab_btn" onclick = "onShowJs()">确定</div>



</body>
</html>