<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'photoManage.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
   	          	<link rel="stylesheet" href="/resources/js/zyupload/skins/zyupload-1.0.0.min.css " type="text/css"> 
		<script type="text/javascript" src="/resources/js/zyupload/zyupload.basic-1.0.0.min.js"></script>  
   <script src="/resources/js/fancyBox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
       <link rel="stylesheet" href="/resources/js/fancyBox/jquery.fancybox.css?v=2.1.5" type="text/css"/>

		  <script type="text/javascript">
			$(function(){
				// 初始化插件
				$("#zyupload").zyUpload({
					width            :   "600px",                 // 宽度
					height           :   "115px",                 // 高度
					itemWidth        :   "140px",                 // 文件项的宽度
					itemHeight       :   "115px",                 // 文件项的高度
					url              :   "/picture/addPhoto?type=cemetery&id=${pd.id}",  // 上传文件的路径
					fileType         :   ["jpg","png"],// 上传文件的类型
					fileSize         :   1024*1024*4,                // 上传文件的大小
					multiple         :   true,                    // 是否可以多个文件上传
					dragDrop         :   false,                   // 是否可以拖动上传文件
					tailor           :   false,                   // 是否可以裁剪图片
					del              :   true,                    // 是否可以删除文件
					finishDel        :   false,  				  // 是否在上传文件完成后删除预览
					/* 外部获得的回调接口 */
					onSelect: function(selectFiles, allFiles){    // 选择文件的回调方法  selectFile:当前选中的文件  allFiles:还没上传的全部文件
						console.info("当前选择了以下文件：");
						console.info(selectFiles);
					},
					onDelete: function(file, files){              // 删除一个文件的回调方法 file:当前删除的文件  files:删除之后的文件
						console.info("当前删除了此文件：");
						console.info(file.name);
					},
					onSuccess: function(file, response){          // 文件上传成功的回调方法
						console.info("此文件上传成功：");
						console.info(file.name);
						console.info("此文件上传到服务器地址：");
						console.info(response);
						$("#uploadInf").append("<p>上传成功，文件地址是：" + response + "</p>");
						
					},
					onFailure: function(file, response){          // 文件上传失败的回调方法
						console.info("此文件上传失败：");
						console.info(file.name);
					},
					onComplete: function(response){           	  // 上传完成的回调方法
						console.info("文件上传完成");
						console.info(response);
						location.reload();
					}
				});
			});
		function shw(){
			$("#zyupload").show();
		}
		
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();
			$(".manage_photo").toggle(function(){
				$(this).css({display:"block"});
				$(".red_delete").css({display:"block"});
			},function(){
				$(this).css({display:"block"});
				$(".red_delete").css({display:"none"});
			});
			

		});
		
		
		/* function showGuanli(){
			$(".red_delete").toggle(function(){
				   
			});
			
		} */

		</script> 
		
  </head>
  
  <body>

 <!-- 历史相册-->
            <div class="history_photo_content">
                <div class="jsgl_window_content">
                    <c:if test="${is_manage==1}">
                 
                    <div class="jsgl_btn">
                        <div class="add_photo" onclick="shw();">
                            <img src="/resources/images/lsxc_add_photo_n.png" class="add_img">
                            <div >添&nbsp;&nbsp;加</div>
                        </div>
                        <div class="manage_photo">
                            <img src="/resources/images/lsxc_manage_n.png" class="manage_img">
                            <div>管&nbsp;&nbsp;理</div>
                        </div>
                    </div>
                       </c:if>
                    <div class="lsxc_photo">
                    <c:choose>
                    <c:when test="${not empty list }">
                    <c:forEach items="${list.photo_info }" var="var" >
                        <div>
                         <a class="fancybox"   href="${var.photo_url }?imageMogr2/thumbnail/500x500"  data-fancybox-group="gallery" ><img src="${var.photo_url }?imageMogr2/thumbnail/100x100"   /></a>
                        <c:if test="${is_manage==1}"> <div class="red_delete" style='' onclick="del('${list._id}','${var.photo_id }');"><img src='/resources/images/zongci/po_biao_shi.png'></div></c:if>
                      </div> 
                    </c:forEach>
                    </c:when>
                    </c:choose>
                   

                    </div>
                </div>
            </div>
   <div id="zyupload" class="zyupload" style="display: none;"></div>  
<script type="text/javascript">
$(function () {


//点击管理
$(".manage_photo").click(function(){
    $(this).css({backgroundColor:"#5abbf4",color:"white"});
    $(".manage_img").attr("src","/resources/images/lsxc_manage_pre.png");

    $(".add_photo").css({backgroundColor:"white",color:"#5abbf4"});
    $(".add_img").attr("src","/resources/images/lsxc_add_photo_n.png");
    $(".red_delete").css({display:"block"});
    //var glRed="<div style='position: absolute;top: -5px;right: -5px'><img src='images/po_biao_shi.png'></div>";
    //$(".lsxc_photo div").append(glRed);
});

});

function del(oid,pid){
	var url = "/cemetery/delPhoto?oid="+oid+"&pid="+pid;
    $.get(url,function(data){
     if(data=="success"){
    document.location.reload();
     }
    });
	
}

</script>

  </body>
</html>
