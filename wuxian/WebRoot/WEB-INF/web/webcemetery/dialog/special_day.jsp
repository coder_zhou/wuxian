<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'special_day.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>

   	<script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
  </head>
  
  <body>
  
    <!-- 特殊纪念-->
            <div class="tsjn_content">
                <div class="tsjn_window_content">
                    <div class="tsjn_title">
                        <c:if test="${is_manage==1}">
                    
                        <div class="title_add_btn">
                            <img src="/resources/images/srmy_add.png">
                            <div> 添&nbsp;&nbsp;加</div>
                        </div>
                        
                        <div class="title_message">开启通知特殊纪念日进行提醒</div>
                            </c:if>
                    </div>
                    
                    <c:choose>
                    <c:when test="${not empty sd }">
                    <c:forEach items="${sd.special_day_info }" var="var" varStatus="vs">
                               <ul>
                        <li class="tsjn_list_1"><img src="/resources/images/srmy_tsjn_date.png"></li>
                        <li class="tsjn_list_2">${var.special_month }月${var.special_day }日</li>
                        <li class="tsjn_list_3">${var.special_day_remarks }</li>
                        <li class="tsjn_list_4">
                          <c:if test="${is_manage==1}">
                        <input type="checkbox" class="ace-switch ace-switch-3" id="sd${vs.index+1 }"   <c:if test="${var.special_day_type==0 }"> checked="checked" </c:if>      onclick="updateSpecialDay('${pd.id}','${var.special_day_info_id}','${vs.index+1 }')">
                        </c:if>
                        </li>
                    </ul>
                    </c:forEach>
                    </c:when>
                    </c:choose>
                    
         
       
                </div>
            </div>
<form action="/cemetery/AddSpecialDay" method="post" id="form">
<input type="hidden" name="cemetery_id" value="${pd.id}">
            <!-- 添加纪念日-->
            <div class="add_jnr_bg bg"></div>
            <div class="add_jnr_content">
                <div class="window_title">添 加 纪 念 日</div>
                <img src="/resources/images/close_window.png" class="add_jnr_window_close  window_close">
                <div class="add_jnr_window_content">
                    <div class="add_date">
                        <div class="add_date_title">特殊日期</div>
                      
                        <input class="span10 date-picker" name="special_time"
									 type="text" onfocus="WdatePicker({skin:'whyGreen',dateFmt:'MM月dd日'})"
									style="width: 88px;" placeholder="纪念日" />
                    </div>
                    <div class="add_message">
                        <div class="add_mess_title">开启提示</div>
                        <div class="add_mess_input">
                            <input type="radio" name="special_day_type" id="yes" checked="checked"    value="0"><label for="yes">是</label>
                            <input type="radio" name="special_day_type" id="no" value="1"><label for="no">否</label>
                        </div>
                    </div>
                    <div class="add_intro">
                        <div class="add_intro_title">纪念说明</div>
                        <textarea name="special_day_remarks"></textarea>
                    </div>
                    <div class="add_btn"   onclick="sub();">确定</div>
                </div>
            </div>
            </form>
            
            
  <script type="text/javascript">
  function  updateSpecialDay(Id,Sd,SdIndex){
		var wqx = $("#sd"+SdIndex).attr("checked");
		var vs;
			if(wqx == 'checked'){
				vs="unchecked";
			}else{
				vs="checked";
			}
	  var url = "/cemetery/updateSpecialDay?id="+Id+"&special_day_info_id="+Sd+"&sd="+vs;
	    $.get(url,function(data){
	   
	    });
  }
  function sub(){
	  $("#form").submit();
  }
  $(function () {
  //添加纪念日
  $(".title_add_btn").click(function(){
      $(".add_jnr_bg,.add_jnr_content").css({display:"block"});
  });
  //窗口关闭按钮
  $(".add_jnr_window_close").click(function(){
      $(".add_jnr_bg,.add_jnr_content").css({display:"none"});
  });
  });
  </script>
  </body>
</html>
