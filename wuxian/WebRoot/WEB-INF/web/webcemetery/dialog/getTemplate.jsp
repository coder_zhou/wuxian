<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'getTemplate.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>

            <!-- 更换模板-->
            <div class="change_sty_content box">
                <div class="ghmb_window_content   boxs" >

                    <div id="imgs" class="imgs">
                    
                    
                   <c:choose>
                   <c:when test="${not empty list }">
                   <c:forEach items="${list }" var="var">
                         <div class="mo_kuai">
                            <div class="mo_kuai_img">
                                <img src="/goods/${var.cemetery_template_img }">
                                <div class="text"></div>
                                <div class="text_infor">
                                    <div>${var.cemetery_template_name }</div>
                                </div>
                            </div>
                            <div class="mo_kuai_btn">
                                <div class="bg_price">
                           
                                <c:if test="${var.cemetery_template_price==0 }">
                                免费
                                </c:if>
                                <c:if test="${var.cemetery_template_price!=0 }">
                                ${var.cemetery_template_price}福币
                                </c:if>      
                                </div>
                                <div class="bg_choose"  onclick="chose('/goods/${var.cemetery_template_img }',' ${var.cemetery_template_price}','${var.cemetery_template_id }','${var.cemetery_template_name }');" >选择模板</div>
                            </div>
                        </div>
                   </c:forEach>
                   </c:when>
                   </c:choose> 
                    
                  


                    </div>

                </div>


            </div>

            <!-- 购买模板框-->
            <div class="buy_mb_bg  bg"></div>
            <div class="buy_mb_content"   id="content">
                <div class="gm_left"><img src="/resources/images/select_formwork_02.png"  id="im"></div>
                <div class="gm_right">
                    <div class="right_h1">
                        <div class="price_title">墓园价格</div>
                        <div class="price_num" id="price_num">2000福币</div>
                    </div>
                    <div class="right_h2">
                        <div class="price_title">会员价格</div>
                        <div class="price_num" id="huiyuan">1800福币</div>
                    </div>
                    <div class="right_h2">
                        <div class="ye_title">账户余额</div>
                        <div class="ye_num">${sessionScope.user_info.user_fb}福币</div>
                    </div>
                    <div class="right_h3">原建园物将被清除，祭祀物品不受影响</div>

                </div>
                <div class="gm_foot_line"></div>
                <div class="gm_btn">
                    <div class="gm_que_ding" id="gm_que_ding">福币充值</div>
                    <div class="gm_qu_xiao">取消</div>
                </div>
            </div>
            
      <script type="text/javascript">
      $(function () {
    		  
    		    $(".bg_choose").click(function(){
    		        $(".buy_mb_bg,.buy_mb_content").css({display:"block"});
    		    });
    		    $(".gm_qu_xiao").click(function(){
    		        $(".buy_mb_bg,.buy_mb_content").css({display:"none"});
    		    });	  
      }
      );
      
      function chose(im,price,id,name){
    	  $("#im").attr("src",im);
    	  if(${sessionScope.user_info.user_fb}>price){
    		
    		   var  sds="<span onclick=\"goumai('"+price+"','"+id+"','"+name+"');\">确认更换</span>"
    	
    		  $("#gm_que_ding").html(sds);
    	  }else {
    		  var  gtnk="<a href=\"/order/pay\"  target=\"_blank\">福币充值</a>";
    		  $("#gm_que_ding").html(gtnk);
    	  }
    	  $("#price_num").html(price+"福币");
    	  $("#huiyuan").html(price*${vipcount}+"福币");
    	  $("#content").show();
    	  
      }
      
      function goumai(price,id,name){
    	  var url="/cemetery/choseTemplate?templateId="+id+"&cemetery_number="+${pd.cemetery_number}+"&name="+name+"&price="+price;
    	    $.get(url,function(data){
    	        if(data=="success"){
    	           document.location.reload();
    	        }else{
    	        	alert(data);
    	        }
    	       });
      }
      
      </script>      
            
  </body>
</html>
