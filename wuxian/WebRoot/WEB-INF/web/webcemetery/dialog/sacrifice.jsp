<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'jisi.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
  
  
      <!-- 祭祀日志-->
            <div class="jsrz_content">
                <div class="jsrz_window_content">
                    <ul class="jsrz_title">
                        <li class="jsrz_list_1">类型</li>
                        <li class="jsrz_list_2">详细</li>
                        <li class="jsrz_list_3">操作人员</li>
                        <li class="jsrz_list_4">时间</li>
                        <li class="jsrz_list_5">操作</li>
                    </ul>
<c:choose>
<c:when test="${not empty list }">
<c:forEach items="${list }" var="var">
   <ul class="jsrz_cont">
                        <li class="jsrz_list_1"><img src="/resources/images/create_tree_goods.png"></li>
                        <li class="jsrz_list_2">${var.sacrifice_class}  ${var.sacrifice_materials }</li>
                        <li class="jsrz_list_3">${var.sacrifice_nick_name }</li>
                        <li class="jsrz_list_4">${var.sacrifice_time} </li>
                        <li class="jsrz_list_5"><c:if test="${is_manage==1}"><a href="/cemetery/delSacrifice?id=${pd.id }&sid=${var._id}"> <img src="/resources/images/fo_my_wish_delete.png"></a></c:if></li>
                    </ul>
</c:forEach>
</c:when>

</c:choose>
                  
          
                </div>
            </div>
  </body>
</html>
