<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'ArticleAndCommon.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<!--     -->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   <link href="<%=path%>/resources/css/srmy_sacrifice_corpus.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
            <!--多部分按钮时的文集追思-->
             <div class="sacrifice_miss">
                    <div id="buzhi_corpus_content">

                        <!-- 背景-->
                        <div class="kind_content"></div>

                        <!-- 菜单选项-->
                        <ul class="kind_title miss_kind_title">
                            <li class="active">文集</li>
                            <li>追思</li>
                        </ul>
						<div class="corpus_content_write">
                            <div class="write_corpus"><a href="user_write_corpus.html"><img src="/resources/images/user_article_write_btn.png">写文集</a></div>
                            <div class="write_miss"><a href="user_write_miss.html"><img src="/resources/images/user_article_write_btn.png">写追思</a></div>
                        </div>
                        <div class="tab_content miss_tab_content buzhi_tab_content">


                            <!--    内容 1 文集  -->
                            <div  class="content_first">
                                           <c:choose>
<c:when test="${not empty la }">
<c:forEach items="${la }" var="var">            
                     <c:if test="${var.article_type==1 }">       
                                <ul class="content_even">
                                    <li class="content_list_first"><a href="/cemetery/viewArticle?artId=${var._id}"   target="_blank">${var.article_name }</a></li>
                                    <li class="content_list_second"><a href="/cemetery/viewArticle?artId=${var._id}"  target="_blank">${var.user_name }</a></li>
                                    <li class="content_list_third"><c:set target="${myDate}" property="time" value="${var.article_add_time*1000}"/> 
                                               <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> </li>
                                    <li class="content_list_four">(3/9)</li>
                                    <li class="content_list_five">
                <!--                          <img src="/resources/images/user_article_wenzhang.png">
                                        <img src="/resources/images/user_article_write.png">
                                        <img src="/resources/images/user_article_delete.png"> -->
                                    </li>
                                </ul>
            </c:if>
</c:forEach>
</c:when>

</c:choose>    
                            
                            </div>


                            <!--    内容 2 追思 -->
                            <div class="content_first content_hide">
                            
                                              <c:choose>
<c:when test="${not empty la }">
<c:forEach items="${la }" var="var">            
                     <c:if test="${var.article_type==2 }">       
                            
                                <ul class="content_even">
                                    <li class="content_list_first">${var.article_name }</li>
                                    <li class="content_list_second">${var.user_name }</li>
                                    <li class="content_list_third"><c:set target="${myDate}" property="time" value="${var.article_add_time*1000}"/> 
                                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/></li>
                                    <li class="content_list_four">(3/9)</li>
                                    <li class="content_list_five">
        <!--                                 <img src="/resources/images/user_article_wenzhang.png">
                                        <img src="/resources/images/user_article_write.png">
                                        <img src="/resources/images/user_article_delete.png">
                                         -->
                                    </li>
                                </ul>
                                  </c:if>
</c:forEach>
</c:when>

</c:choose>       
                            </div>



                        </div>

                    </div>
                </div>
<script type="text/javascript">
$(function(){
$(".kind_title li").click(function(){
    $(this).css({backgroundColor:"#ffffff",color:"#2a95d4"}).siblings().css({backgroundColor:"#c0e7ff",color:"#777777"});
    var tab_i=$(this).index();
    $(".tab_content .content_first").eq(tab_i).removeClass("content_hide").siblings().addClass("content_hide");
});
});
</script>


  </body>
</html>
