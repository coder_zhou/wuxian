<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'editCemetery.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
	      <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
	                <link href="/resources/css/cropper.min.css" rel="stylesheet">
        <link href="/resources/css/docs.css" rel="stylesheet">
          <script src="/resources/js/cropper.min.js"></script>
  <script src="/resources/js/docs.js"></script>
      <script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
	      <style type="text/css">
	      
.change_img_bg{  
    display: none; 
    position: absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: black;
    z-index:999999999;
    -moz-opacity: 0.6;
    opacity:.60;
    filter: alpha(opacity=60);
}
.change_img_content{
    display: none;
    position: absolute;
    top:50%;
    left:50%;
    width:750px;
    margin-left: -375px;
    margin-top: -215px;
    z-index:9999999999;
    overflow: auto;
    border-radius: 3px;
}
.change_img_window_close{
    position: absolute;
    top: 15px;
    right: 15px;
}
.ggtx_window_content{
	width:720px;
	height:400px;
	position: relative;
	 background-color:white;
	 padding: 30px 0 0 30px;
}
	      
	      </style>
    <script type="text/javascript">
    $(function () {
    //编辑信息
    $(".bian_ji_infor").click(function(){
        $(".bjxx_bg,.bjxx_content").css({display:"block"});
    });
    $(".bjxx_window_close").click(function(){
        $(".bjxx_bg,.bjxx_content").css({display:"none"});
    });
  //点击相片下的编辑
    $(".bjxx_bj_wz").click(function(){
        $(".bjmy_infor_bg,.bjmy_infor_content").css({display:"block"});
    });
    
    //点击关闭窗口和保存按钮
    $(".bjmy_window_close,.bjmy_save").click(function(){
        $(".bjmy_infor_bg,.bjmy_infor_content").css({display:"none"});
    });

    //逝者 1 信息
    $(".bjsz1_window_close").click(function(){
        $(".bjsz1_infor_bg,.bjsz1_infor_content,.bjsz2_infor_bg,.bjsz2_infor_content").css({display:"none"});
    });
    //点击 “逝者1” 的编辑按钮后弹出
    $(".only_bj_btn,.two_bj_1").click(function(){
        $(".bjsz1_infor_bg,.bjsz1_infor_content").css({display:"block"});
    });

    //点击 “逝者2” 的编辑按钮后弹出
    $(".two_bj_2").click(function(){
        $(".bjsz2_infor_bg,.bjsz2_infor_content").css({display:"block"});
    });
    $(".change_img_window_close  , #getDataURL2").click(function(){
    	$(".change_img_bg,.change_img_content").css({display:"none"});
    });
    });
    </script>
  </head>
  
  <body>
              <!-- 编辑信息-->
            <div class="bjxx_content">
                <div class="bjxx_window_content">
                    <div class="cont_left">
                        <img src="${cm.cemetery_death_img }" class="myyz_photo">
                        <div class="cont_left_title">墓园遗照</div>
                        <div class="bjxx_bj">
                            <img src="/resources/images/user_article_write_btn.png">
                            <div class="bjxx_bj_wz">编&nbsp;&nbsp;辑</div>
                        </div>
                    </div>
                    <div class="cont_right">
                        <div class="cont_right_hang1">
                            <div>墓园号码：<span>${cm.cemetery_number }</span></div>
                            <div>墓园名称：<span>${cm.cemetery_name }</span></div>
                        </div>
                        <div class="cont_right_hang1">
                            <div>墓园类型：<span>${cm.cemetery_type==1?"双人墓":"单人墓" }</span></div>
                            <div>公开设置：<span>
                            <c:if test="${cm.cemetery_look==0 }">完全公开</c:if>
                            <c:if test="${cm.cemetery_look==1 }">登录用户可见</c:if>
                            <c:if test="${cm.cemetery_look==2 }">仅自己可见</c:if>
                            
                            </span></div>
                        </div>
                        <div class="cont_right_hang1">
                            <div>墓园地址：<span>${cm.cemetery_province} ${cm.cemetery_city}</span></div>
                        </div>
                        <div class="cont_right_hang4">
                            <div>园主留言：
                            <div>
                            ${cm.cemetery_message}
                            </div></div>
                        </div>

                    </div>
                    <div class="cont_line"></div>



                    <div class="shi_zhe_infor">
         

                   <c:choose>
<c:when test="${not empty cm.cemetery_deceased }">
<c:if test="${fn:length(cm.cemetery_deceased)>1 }">
<c:forEach items="${cm.cemetery_deceased }" var="dec" varStatus="vs">
  <div class="only_two">
                            <div class="two_left">
                                <div class="szxx_title">逝者 ${vs.index+1 } 信息</div>
                                <div class="szxx_bj_btn  two_bj_${vs.index+1 }">编辑</div>
                                <div class="szxx_cont_first">
                                    <div class="cont_one_hang1">
                                        <div>姓名：<span>${dec.deceased_name }</span></div>
                                        <div>性别：<span>${dec.deceased_sex }</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>民族：<span>${dec.deceased_nation }</span></div>
                                        <div>信仰：<span>${dec.deceased_faith }</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>生辰：<span>${dec.deceased_birthday }</span></div>
                                        <div>忌辰：<span>${dec.deceased_die_time }</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div class="sz_guan_xi">
                                        	<div class="guan_xi_container_title">您与逝者关系：</div>
                                        	<div class="guan_xi_container">${dec.deceased_relationship }</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

</c:forEach>
   
</c:if>
<c:if test="${fn:length(cm.cemetery_deceased)==1 }">
<c:forEach items="${cm.cemetery_deceased }" var="dec" varStatus="vs">

               <div class="only_one">
                            <div class="szxx_title">逝者 1 信息</div>
                            <div class="szxx_bj_btn only_bj_btn">编辑</div>
                            <div class="szxx_cont_one">
                                <div class="cont_one_hang1">
                                    <div>姓名：<span>${dec.deceased_name }</span></div>
                                    <div>性别：<span>${dec.deceased_sex }</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    <div>民族：<span>${dec.deceased_nation }</span></div>
                                    <div>信仰：<span>${dec.deceased_faith }</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    <div>生辰：<span>${dec.deceased_birthday }</span></div>
                                    <div>忌辰：<span>${dec.deceased_die_time }</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    
                                    <div class="sz_guan_xi">
                                    		<div class="guan_xi_container_title">您与逝者关系：</div>
                                        	<div class="guan_xi_container">${dec.deceased_relationship }</div>
                                 </div>
                                </div>
                            </div>

                        </div>
</c:forEach>
</c:if>
</c:when>
</c:choose>





                        </div>
                    </div>

                </div>
            </div>
      <!-- 点击图片下的按钮   编辑墓园信息-->
            <div class="bjmy_infor_bg  bg"></div>
            <form action="/cemetery/editCemetery" method="post"  id="editCemetery">
            <input type="hidden" name="cemetery_number" value="${cm.cemetery_number }">
                        <input type="hidden" name="_id" value="${cm._id }">
                                    <input type="hidden" name="cemetery_province" id="province_name">
                <input type="hidden" name="cemetery_city" id="city_name">
            
            <div class="bjmy_infor_content">
                <div class="window_title">墓 园 信 息</div>
                <img src="/resources/images/close_window.png" class="bjmy_window_close  window_close">
                <div class="bjmy_window_content">
                    <div class="bjmy_left">
                        <input type="hidden"  name="cemetery_death_img" id="img_url" value="">
				<img  src="${cm.cemetery_death_img }" onerror="javascript:this.src='/resources/images/zongci/add_peo_photo.png';"     id="ii" class="bjmy_left_img"  onclick="showPic();">
                        <div class="bjmy_title" onclick="showPic();">更换头像</div>
                    </div>
                    <div class="bjmy_right">
                        <div class="bjmy_hang">
                            <div class="bjmy_hang_left">墓园号码</div>
                            <div><input type="text" readOnly="true"  value="${cm.cemetery_number }"></div>
                            <div class="bjmy_hang_right">墓园名称</div>
                            <div><input type="text"   name="cemetery_name" id="cemetery_name"  value="${cm.cemetery_name }"></div>
                        </div>

                        <div class="bjmy_hang">
                            <div class="bjmy_hang_left">墓园类型</div>
                            <div>
                                <select name="cemetery_type">
                                    <option value="0"   <c:if test="${cm.cemetery_type=='0' }"> selected="selected"   </c:if> >单人墓</option>
                                    <option value="1" <c:if test="${cm.cemetery_type=='1' }"> selected="selected"   </c:if> >双人墓</option>
                                </select>
                            </div>

                            <div class="bjmy_hang_right">公开设置</div>
                            <div>
                                <select name="cemetery_look">
                                    <option value="0"   <c:if test="${cm.cemetery_look=='0' }"> selected="selected"   </c:if> >完全公开</option>
                                    <option value="1" <c:if test="${cm.cemetery_look=='1' }"> selected="selected"   </c:if>  >部分公开</option>
                                    <option value="2"  <c:if test="${cm.cemetery_look=='2' }"> selected="selected"   </c:if> >不公开</option>
                                </select>
                            </div>
                        </div>

                        <div class="bjmy_hang">
                            <div class="bjmy_hang_left">所在省</div>
                            <div class="szs_select">
                         
                                	 <select class="chzn-select" name="cemetery_province_code" id="province_code" data-placeholder="请选择" style="vertical-align: top;width: 191px; " onchange="Change(1);">
				                   <option selected="selected" >请选择</option>
									<c:choose>
									<c:when test="${not empty sessionScope.proList }">
									<c:forEach items="${sessionScope.proList}" var="area" varStatus="vs">
									<option value="${area.areaId}"   <c:if test="${cm.cemetery_province_code==area.areaId }"> selected="selected"   </c:if>  >${area.areaName}</option>
									
									</c:forEach>
									
									</c:when>
									</c:choose>
										
								</select>
                                
                            </div>
                            <div class="bjmy_hang_right">所在城市</div>
                            <div>
                     
                                <select name="cemetery_city_code" id="city_code" style="vertical-align: top; width: 191px;  "  onchange="set();" >
						<option value="${cm.cemetery_city_code}" selected="selected">${cm.cemetery_city==null?"选择地区":cm.cemetery_city}</option>
								
								</select>
                                
                            </div>
                        </div>

                        <div class="bjmy_hang">
                            <div class="bjmy_hang_left">墓园地址</div>
                            <div><input type="text" name="cemetery_address" value="${cm.cemetery_address }"></div>
                        </div>

                        <div class="bjmy_hang_mzly">
                            <div class="bjmy_hang_left">园主留言</div>
                            <div>
                                <textarea placeholder="请输入" name="cemetery_message">${cm.cemetery_message }</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="bjmy_foot"></div>
                    <div class="bjmy_save" onclick="editCemetery();">保存</div>
                </div>
            </div>
</form>
<form action="/cemetery/editDec" method="post"  id="editDec">
    <input type="hidden" name="cemetery_number" value="${cm.cemetery_number }">
                        <input type="hidden" name="_id" value="${cm._id }">
                   
                   <c:choose>
<c:when test="${not empty cm.cemetery_deceased }">
<c:forEach items="${cm.cemetery_deceased }" var="dec" varStatus="vs">
        <!--点击逝者信息 1 下的编辑 -->
               <input type="hidden" name="deceased_id_${vs.index+1 }" value="${dec.deceased_id }">
            <div class="bjsz${vs.index+1 }_infor_bg  bg"></div>
            <div class="bjsz${vs.index+1 }_infor_content">
                <div class="window_title">逝 者 ${vs.index+1 } 信 息</div>
                <img src="/resources/images/close_window.png" class="bjsz1_window_close  window_close">
                <div class="bjsz1_window_content">
                    <div class="bjsz_hang">
                        <div class="bjsz_hang_left">姓&nbsp;&nbsp;&nbsp;&nbsp;名</div>
                        <div><input type="text" name="deceased_name_${vs.index+1 }"    value="${dec.deceased_name }"></div>
                        <div class="bjsz_hang_right">性&nbsp;&nbsp;&nbsp;&nbsp;别</div>
                        <div>
                            <select   name="deceased_sex_${vs.index+1 }">
                                <option value="男"    <c:if test="${dec.deceased_sex=='男' }"> selected="selected"   </c:if>    >男</option>
                                <option value="女" <c:if test="${dec.deceased_sex=='女' }"> selected="selected"   </c:if>  >女</option>
                            </select>
                        </div>
                    </div>

                    <div class="bjsz_hang">
                        <div class="bjsz_hang_left">民&nbsp;&nbsp;&nbsp;&nbsp;族</div>
                        <div>
                            <input type="text" name="deceased_nation_${vs.index+1 }" value="${dec.deceased_nation }">
                        </div>

                        <div class="bjsz_hang_right">信&nbsp;&nbsp;&nbsp;&nbsp;仰</div>
                        <div>
                             <input type="text" name="deceased_faith_${vs.index+1 }" value="${dec.deceased_faith }">
                        </div>
                    </div>

                    <div class="bjsz_hang">
                        <div class="bjsz_hang_left">生&nbsp;&nbsp;&nbsp;&nbsp;辰</div>
                        <div>
<input class="span10 date-picker" name="deceased_birthday_${vs.index+1 }" value="${dec.deceased_birthday }"
									id="deceased_birthday_${vs.index+1 }" type="text" onfocus="WdatePicker({skin:'whyGreen',maxDate:'%y-%M-%d'})"
									style="width: 190px;" placeholder="生辰" />
                            
                        </div>
                        <div class="bjsz_hang_right">忌&nbsp;&nbsp;&nbsp;&nbsp;辰</div>
                        <div>
                               <input class="span10 date-picker" name="deceased_die_time_${vs.index+1 }"  value="${dec.deceased_die_time }"
									id="deceased_die_time_${vs.index+1 }"  type="text"
									onfocus="WdatePicker({minDate:$('#deceased_birthday_${vs.index+1 }').val(),maxDate:'%y-%M-%d'})"
									style="width: 190px;" placeholder="祭日" />
                        </div>
                    </div>

                    <div class="bjsz_hang  bjsz_hang_gx">
                        <div class="bjsz_hang_left">您与逝者关系</div>
                        <textarea name="deceased_relationship_${vs.index+1 }" id="">${dec.deceased_relationship} </textarea>
                        
                    </div>

                    <div class="bjsz_foot"></div>
                    <div class="bjsz_save"   onclick="editDec();">保存</div>
                </div>
            </div>
</c:forEach>
</c:when>
</c:choose>
</form>

    


<div class="change_img_bg"  id="change_img_bg"></div>
<div class="change_img_content" id="change_img_content">
		<div class="window_title">选 择 上 传 头 像</div>
        <img src="/resources/images/close_window.png" class="change_img_window_close">
        <div class="ggtx_window_content">

	    	<div class="main_content" id="content">

          <div class="img-container"><img src="" style="display:none"></div>
          <div class="img-preview img-preview-sm"></div>
          <div class="btn-group">
            <div class="btn-primary" data-method="zoom" data-option="0.1"  title="放大">
				<label><img src="/resources/img/big.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="zoom" data-option="-0.1"  title="缩小">
				 <label><img src="/resources/img/small.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="-90"  title="左转90度">
				 <label><img src="/resources/img/left.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="90"  title="右转90度">
				<label><img src="/resources/img/right.png" width="40" height="25"/></label>
            </div>
         
           
          </div>
          <div class="foot_btn"> <input type="file" style="width: 100px;" id="inputImage"   accept="image/*" placeholder="上传头像" title="上传头像">  <button  id="getDataURL2" data-toggle="tooltip" type="button" title="$().cropper('getDataURL', 'image/jpeg')"  >确定</button></div>

         
  </div>
 </div>
  <input type="hidden" id="imgStr"/>
</div>
<script type="text/javascript">
function showPic(){
		$("#content").show();
		$("#change_img_bg").show();
		$("#change_img_content").show();
}
function test(){
	  var imgStr = $("#imgStr").val();
	  if(imgStr!="" || imgStr!=undefined){
	     console.log(imgStr);
	 	$.ajax({
			type: "POST",
			url: '/picture/savePic',
	    	data: {'imgStr':imgStr},
			dataType:'json',
			//beforeSend: validateData,
			cache: false,
			success: function(data){
				$("#ii").attr("src",data.imgUrl);
				$("#img_url").val(data.imgUrl);
				$("#content").hide();
			
			}
		});
	     
		
      }

  }
  
  function editCemetery(){
	  
		if($("#cemetery_name").val()==""){
			$("#cemetery_name").tips({
				side:2,
	            msg:'请墓园名称不可为空',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#cemetery_name").focus();
			return false;
		}
		if($("#province_code").val()==""){
			$("#province_code").tips({
				side:2,
	            msg:'请选择所在省份',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#province_code").focus();
			return false;
		}
		if($("#city_code").val()==""){
			$("#city_code").tips({
				side:2,
	            msg:'请选择所在城市',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#city_code").focus();
			return false;
		}
		else{
			  $("#editCemetery").submit();
	  }
	  
	
  }
  function editDec(){
	  $("#editDec").submit();
  }
  
	function set(){
		$("#province_name").val($("#province_code").find("option:selected").text());
		$("#city_name").val($("#city_code").find("option:selected").text());
	}
	function Change(aa){
		var obj;
		 
		 if(aa==1){
			 var p= $("#province_code").val();
			  obj=$("#city_code");
			 obj.empty();
		//	 obj.css({ display: "block" });
		 }
		
		
			var url = "/area/getSon?proId="+p+"&tm="+new Date().getTime();
			$.get(url,function(data){
				if(data.length>0){
					var html = "";
					 obj.append("<option value=''>请选择</option>");
					$.each(data,function(i){
	                obj.append("<option value='"+this.areaId+"'>"+this.areaName+"</option>");
						 
	                    
					});
					
				}
			},"json");
	}
  
  
</script>
  
  </body>
</html>
