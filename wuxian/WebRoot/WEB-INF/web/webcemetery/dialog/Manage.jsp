<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Manage.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
   <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
   	<script src="/resources/js/jquery-1.9.1.min.js"></script>
  </head>
  
  <body>
    <!-- 权限设置-->
            <div class="limit_set_content">
                <div class="qxsz_window_content">
                    <div class="qxsz_title">管理员有布置墓园的权限</div>
                    <div class="qxsz_create_person">
                        <div><img src="/resources/images/limit_set_cp.png"></div>
                        <div>建园者：<span>${um.user_account}</span></div>
                    </div>
                    <div class="qxsz_btn">
                        <div class="qxsz_add">
                            <img src="/resources/images/lsxc_add_photo_n.png" class="add_img">
                            <div>添&nbsp;&nbsp;加</div>
                        </div>
                        <div class="qxsz_manage">
                            <img src="/resources/images/lsxc_manage_n.png" class="manage_img">
                            <div>管&nbsp;&nbsp;理</div>
                        </div>
                    </div>
                    <div class="admin_list">
                    <c:choose>
                    <c:when test="${not empty lu }">
                    <c:forEach items="${lu }" var="var" varStatus="vs">
                              <div class="list_cont">
                            <div class="list_title">管理员${vs.index+1 }</div>
                            <img src="${var.user_head_photo }">
                            <div class="list_name">${var.user_account }</div>
                            <div class="list_red_delete"  onclick="addManage('${var._id }','${pd.cemetery_number}')" "><img src='/resources/images/zongci/po_biao_shi.png'></div>
                        </div>
                    </c:forEach>
                    </c:when>
                    </c:choose>
                    
              


                    </div>

                </div>
            </div>
            <!-- 添加管理员-->
            <div class="add_manage_bg  bg"></div>
            <div class="add_manage_content">
                <div class="window_title">添 加 管 理 员</div>
                <img src="/resources/images/close_window.png" class="add_window_close  window_close">
                <div class="add_window_content">
                <c:choose>
                <c:when test="${not empty lm }">
                <c:forEach items="${lm }" var="ss">
                 <div class="add_list">
                        <img src="${ss. user_head_photo}" class="manage_peo_img">
                        <div class="manage_name">${var.user_name }</div>
                        <div class="manage_add_btn"   onclick="addManage('${ss._id }','${pd.cemetery_number}')"  >添加</div>
                    </div>
                </c:forEach>
                </c:when>
                </c:choose>
                
                
                   

                </div>
            </div>

  <script type="text/javascript">
  $(function (){
	    //权限设置
	    $(".limit_set").click(function(){
	        $(".limit_set_bg,.limit_set_content").css({display:"block"});
	    });
	    $(".qxsz_window_close").click(function(){
	        $(".limit_set_bg,.limit_set_content").css({display:"none"});
	    });
	    $(".qxsz_add").click(function(){
	        $(this).css({backgroundColor:"#5abbf4",color:"white"});
	        $(".add_img").attr("src","/resources/images/lsxc_add_photo_pre.png");
	        $(".qxsz_manage").css({backgroundColor:"white",color:"#5abbf4"});
	        $(".manage_img").attr("src","/resources/images/lsxc_manage_n.png");
	        $(".list_red_delete").css({display:"none"});
	        $(".add_manage_bg,.add_manage_content").css({display:"block"});
	    });
	    $(".add_window_close").click(function(){
	        $(".add_manage_bg,.add_manage_content").css({display:"none"});
	    });

	    //点击添加
	    //点击"添加"按钮
	    $(".manage_add_btn").click(function(){
	        $(this).css({backgroundColor:"white",color:"#777",cursor:"text"});
	        $(this).text("已添加");
	    });

	    //点击管理
	    $(".qxsz_manage").click(function(){
	        $(this).css({backgroundColor:"#5abbf4",color:"white"});
	        $(".manage_img").attr("src","/resources/images/lsxc_manage_pre.png");
	        $(".qxsz_add").css({backgroundColor:"white",color:"#5abbf4"});
	        $(".add_img").attr("src","/resources/images/lsxc_add_photo_n.png");
	        $(".list_red_delete").css({display:"block"});
	    });
	  
	  
  });
  
  function addManage(uid,cemetery_number){
	  var url = "/cemetery/addManage?uid="+uid+"&cemetery_number="+cemetery_number;
	    $.get(url,function(data){
	     if(data=="success"){
	     document.location.reload();
	     }
	    });
	  
  }
  function delManage(uid,cemetery_number){
	  var url = "/cemetery/delManage?uid="+uid+"&cemetery_number="+cemetery_number;
	    $.get(url,function(data){
	     if(data=="success"){
	     document.location.reload();
	     }
	    });
	  
  }
  </script>
  
  </body>
</html>
