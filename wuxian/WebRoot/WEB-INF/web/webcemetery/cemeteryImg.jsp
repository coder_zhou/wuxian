﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <title>全球祭祀祈福网-私人墓园</title>
    <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
     <link href="<%=path%>/resources/css/srmy_sacrifice_corpus.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/cemetery/srmy.js"></script>
    <script src="<%=path%>/resources/js/srmy_sacrifice_corpus.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    <c:if test="${ not empty  sessionScope.cemetery.cemetery_music_url }">
<!--    <audio src="${sessionScope.cemetery.cemetery_music_url  }" autoplay="true" loop="true">
 您的浏览器不支持 audio 标签。
    </audio>
     -->
    </c:if>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
</head>
<body>
    <!-- 墓园部分-->
    <div class="wdmy_wrap" style="left:0px;">
    
      <!-- 墓园放置物品-->
      <div class="srmy_main" id="srmy_main">
    
		<c:forEach var="obj" items="${sclist}" varStatus="status">
		  <c:if test="${obj.cemetery_materials_show == 0 && obj.cemetery_materials_id != 210452}">
	        <c:choose>
	          <c:when test="${obj.materials_class_id eq '10'}"><%--  背景 素材分分类id ==10    --%>
	            <img id="div${obj.cemetery_materials_id}" materials_info="${obj.cemetery_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	          </c:when>
	          <c:when test="${obj.materials_class_id eq '8888'}"><!-- 是文字信息   -->
	            <div id="div${obj.cemetery_materials_id}" 
	            	 width_info="${obj.cemetery_materials_width}" 
	            	 height_info="${obj.cemetery_materials_height}" 
		             materials_info="${obj.cemetery_materials_id}"
		             ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
		             data-name="img${obj.cemetery_materials_id}" 
		             data-id="url${obj.cemetery_materials_id}" 
		             style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:auto;height:auto;"
		             onclick="onShowSDivForWz(this);"
		             materials_id_info = "${obj.materials_id}"
		             name="1">
		            
		            ${obj.materials_big_img}
		            
		          <div class="cao_zuo">
                    <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                    <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                  </div>
	            </div>
	          </c:when>
	          <c:otherwise>
	            <div id="div${obj.cemetery_materials_id}" 
	                 ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
	                 data-name="img${status.index+1}" 
	                 data-id="url${status.index+1}" 
	                 style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;"
	                 name="1"
	                 width_info="${obj.cemetery_materials_width}" 
	                 height_info="${obj.cemetery_materials_height}" 
		             materials_info="${obj.cemetery_materials_id}" 
		             imgwidth="${obj.cemetery_materials_width}" 
		             imgweight="${obj.cemetery_materials_height}"
		             onclick="onShowSDiv(this);"
		             materials_id_info = "${obj.materials_id}">
                  <div class="cao_zuo">
                    <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                    <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                    <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                    <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                  </div>
	              <img  title="${obj.materials_name}-${obj.materials_class_id}" 
		            	style="width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;"
		            	src="<%=path%>/goods/${obj.materials_big_img}" />
		        </div>
	          </c:otherwise>
	        </c:choose>
	      </c:if>
  		</c:forEach>
      </div>
  </div>

</body>
</html>