﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />


<!--     <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" /> -->
    <title>全球祭祀祈福网-私人墓园</title>
    <link href="<%=path%>/resources/css/cemetery/srmy.css" rel="stylesheet" />
    <link href="<%=path%>/resources/css/srmy_sacrifice_corpus.css" rel="stylesheet" />
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/cemetery/srmy.js"></script>
    <script src="<%=path%>/resources/js/srmy_sacrifice_corpus.js"></script>
    <script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    <c:if test="${ not empty  sessionScope.cemetery.cemetery_music_url }">
<!--    <audio src="${sessionScope.cemetery.cemetery_music_url  }" autoplay="true" loop="true">
 您的浏览器不支持 audio 标签。
    </audio>
     -->
    </c:if>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
    <!--[if lt IE 9]>
	    <script src="/resources/index/js/html5shiv.min.js"></script>
	    <script src="/resources/index/js/respond.min.js"></script>
    <![endif]-->
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
	<script type="text/javascript">
	
    	//切换墓园祭祀、墓园布置
    	function onChooseJisiOrBuzhi(flag, cemetery_id,chooseflag, is_user_cemetery_flag){
    		
			if(flag == 1){
    			$("#div_jisi").attr("class","");
    			$(".jisi").css({backgroundColor:"#fd844e"});
    			
    			if(is_user_cemetery_flag > 0 ){
    				$(".buzhi").css({backgroundColor:"#069dd5"});
    			}
   				$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
			if(flag == 2){
				$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#fd844e"});
	    		}
    			$("#div_buzhi").attr("class","");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
    		if(flag == 3){
    			$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#069dd5"});
	    		}
	    		$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","");
    			$(".choose_ceng").css({backgroundColor:"#fd844e"});
    		}
    		
    		$("#tabflag").val(flag);
    	}
    	
    	//弹出二级素材
    	function onShowMaterialsClass2(materials_class_id){
    		var src = "/webCemetery/queryMaterialsList?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);
    		$("#showMaterialsList").attr("style","display: block; ");
    	}
    	
    	//切换选项卡
    	function onChangeMaterialsClassTab(materials_class_id,bk){
    		var arrTab=$('.class_tab');
            for(var i=0;i<arrTab.length;i++){
				$(arrTab[i]).attr("class", "class_tab");
            }
            $(bk).attr("class", "class_tab active");
             
            var arrTab1=$('.content_first');
            for(var i=0;i<arrTab1.length;i++){
				$(arrTab1[i]).attr("class", "content_first content_hide");
            }
            $("#div_content_"+materials_class_id).attr("class", "content_first");
    	}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    		var vipcount = $('#vipcount').val();
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<div class="confirm_container" >';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price_sum") * 1;
    			}
    			
				if(i % 2 == 0){
					str = str + '<div class="module_first">';
				}else{
					str = str + '<div class="module_second">';
				}
				
				if($(arrTab[i]).attr("ifCheck") == 1){
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" checked="checked" >';
				}else{
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" >';
				}
                str = str + '<div class="module_content">';
                str = str + '<div class="module_title">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<img src="' + $(arrTab[i]).attr("img_url") + '" class="module_goods">';
                str = str + '<div class="module_goods_infor">';
                if($(arrTab[i]).attr("parent_id") != 1){
                	str = str + '<div class="aging">';
	                str = str + '<div class="aging_first">时效</div>';
	                str = str + '<div class="reduce" onclick="onAddDate(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >-</div>';
	                str = str + '<div class="number_sum" id="div_date_count_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</div>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<div>天 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<div>月 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<div>年</div>';
	                }else{
	                	str = str + '<div> - </div>';
	                }
	                str = str + '<div class="add" onclick="onAddDate(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >+</div>';
	                str = str + '</div>';
                }
                
                str = str + '<div class="number">';
                str = str + '<div class="number_first">数量</div>';
                str = str + '<div class="reduce" onclick="onAddCount(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</div>';
                str = str + '<div class="number_sum" id="div_number_sum_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</div>';
                str = str + '<div>个</div>';
                
                if($(arrTab[i]).attr("materials_class_id") == 10){//背景,不能增加数量
                	str = str + '<div class="add" onclick="alert(\'背景只能买一个！\');">+</div>';
                }else{
                	str = str + '<div class="add" onclick="onAddCount(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</div>';
                }
                
                str = str + '</div>';
                str = str + '<div class="sum">';
                str = str + '<div class="sum_first">小计</div>';
                str = str + '<div class="sum_price" id="div_sum_price_' + $(arrTab[i]).val() + '"><span>' + $(arrTab[i]).attr("materials_price_sum") + '</span>福币</div>';
                str = str + '</div>';
                str = str + '</div>';
                
//                 str = str + '<div class="line"></div>';
//                 str = str + '<div class="kind_price">';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/kinship_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_family") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_wealth") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>200</span>';
//                 str = str + '</div>';
//                 str = str + '</div>';
                str = str + '</div>';
                str = str + '</div>';
            }
            
            fb_sum = fb_sum * 1
            str = str + '</div>';
            str = str + '<div class="pay">';
            str = str + '<div class="pay_sum">总计：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + fb_sum + '</span>福币</div>';
            
            
            str = str + '<div class="pay_sum">会员价：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + Math.floor(fb_sum * vipcount) + '</span>福币</div>';
            
            
            str = str + '</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="balance">';
	            str = str + '<div class="pay_sum">余额不足：</div>';
	            str = str + '<div class="pay_price"><span>' + (user_fb - fb_sum) + '</span>福币</div>';
	            str = str + '</div>';
            }
            str = str + '<div class="pay_btn">';
            if(user_fb < fb_sum && ($('#userr_user_id').val() == null || $('#userr_user_id').val() == '')){
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >补充福币</div>';
            }else{
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >确定</div>';
            }
			
            str = str + '<div class="pay_cancel" onclick="onClear();">取消</div>';
            str = str + '</div>';
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
    		$("#show").attr("style","display: block; ");
			$(".bg_confirm,.show_confirm").attr("style","display: block; ");
    	}
    
    	//关闭结算页面
    	function onHideDivJs(){
    		$("#show").attr("style","display: none; ");
			$(".bg_confirm,.show_confirm").attr("style","display: none; ");
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少时效
    	function onAddDate(flag, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	function onChooseJsCheckbox(user_fb, materials_id){
    		var ifcheck = $("#checkbox_" + materials_id).attr("checked");
    		if(ifcheck == "checked"){
    			$("#input_materials_id_" + materials_id).attr("ifCheck", 1);
    		}else{
    			$("#input_materials_id_" + materials_id).attr("ifCheck", 0);
    		}
    		onShowJsDiv(user_fb);
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="cemetery_id" value="' + $("#input_cemetery_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#input_type_flag").val() + '"/>';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webCemetery/saveCemeteryMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
					location.href="<%=path%>/webCemetery/queryCemeteryInfoForBz?cemetery_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();;
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	//保存
    	function save(){
    		var cemetery_id = $("#input_cemetery_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="cemetery_id" type="hidden" value="' + cemetery_id + '" />';
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webCemetery/saveCemeteryMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
				location.href="<%=path%>/webCemetery/queryCemeteryInfoForBz?cemetery_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
			});
    	}
    	
    	//控制素材显示隐藏
    	function onUpdMaterialsShow(cemetery_materials_id, cemetery_id,bg){
    		//显示隐藏更新刷新tab页面标志位
    		$("#tabflag").val(3);
    		var cemetery_materials_show = '';
    		if($(bg).attr("flag_info") == 1){//隐藏图标
    			cemetery_materials_show = 0;
    			$(bg).attr("src", "<%=path%>/resources/images/srmy_materisl_show.png");
    		}else{//显示图标
    			cemetery_materials_show = 1;
    			$(bg).attr("src", "<%=path%>/resources/images/srmy_materisl_hidden.png");
    		}
    		$.ajax({
				type: "POST",
				url: '/webCemetery/updateCemeteryMaterialsForCemeteryMaterialsShow',
		    	data: {cemetery_materials_id : cemetery_materials_id, cemetery_id : cemetery_id, cemetery_materials_show : cemetery_materials_show},
				dataType:'json',
				cache: false,
				success: function(data){
					if(data == 0){
						if(cemetery_materials_show == 0 ){//需要显示，的刷新页面
							location.href="<%=path%>/webCemetery/queryCemeteryInfoForBz?cemetery_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();;
						}else{
							var namelist = $("div[name='1']");
							for(var i=0; i<namelist.length; i++){
							 	if($(namelist[i]).attr("materials_info") == cemetery_materials_id){
							 		namelist[i].remove();
							 	}
							}
						}
						$(bg).attr("flag_info", cemetery_materials_show);
						
					}else{
						alert('操作失败，请联系管理员！');
					}
				}
			});
    	}
    	
    	//删除素材
    	function onDelMaterials(cemetery_materials_id, cemetery_id){
    		$.ajax({
				type: "POST",
				url: '/webCemetery/delCemeteryMaterialsByCemeteryMaterialsId',
		    	data: {cemetery_materials_id : cemetery_materials_id, cemetery_id : cemetery_id},
				dataType:'json',
				cache: false,
				success: function(data){
					 if(data == 0){
						var namelist = $("div[name='1']");
						for(var i=0; i<namelist.length; i++){
						 	if($(namelist[i]).attr("materials_info") == cemetery_materials_id){
						 		namelist[i].remove();
						 	}
						}
					}else{
						alert('操作失败，请联系管理员！');
					}
				}
			});
    	}
    	
    	//显示弹出窗
    	function onShowWZ(){
    		$("#show").attr("style","display: block; ");
			$(".bg_wz,.show_wz").attr("style","display: block; ");
    	}
    	
    	//隐藏弹出窗
    	function onCloseWz(){
    		$(".bg_wz,.show_wz").attr("style","display: none; ");
    	}
    	
    	//保存文字信息
    	function onQdWz(wz){
			$("#id_wz").val(wz);
			$("#id_cemetery_id").val("${cemetery_id}");
			document.fff.action = "<%=path%>/webCemetery/saveCemeteryMaterialsForWz";
			$("#fff").ajaxSubmit(function(result){
				location.href="<%=path%>/webCemetery/queryCemeteryInfoForBz?cemetery_id="+$("#input_cemetery_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
			});
    	}
    	
    	//修改文字信息
    	function onChangeWz(materials_id, cemetery_id, cemetery_materials_id){
			$("#id_cemetery_id").val(cemetery_id);
			$("#id_materials_id").val(materials_id);
			$("#id_cemetery_materials_id").val(cemetery_materials_id);
			document.fff.action = "<%=path%>/webCemetery/queryWzByCemeteryMaterialsId";
			$("#fff").ajaxSubmit(function(result){
	    		$("#show").attr("style","display: block; ");
				$(".bg_wz,.show_wz").attr("style","display: block; ");
				$(window.parent.document).contents().find("#iframe_wz_id")[0].contentWindow.setWz(result.materials_big_img); 
				$("#id_materials_id").val(materials_id);
				
			});
    	}
    	
    	//修改文字信息
    	function onChangeWz1(){
    		var cemetery_id = $("#input_cemetery_id").val();
    		var cemetery_materials_id = $("#choose_img_cemetery_materials_id").val();
    		var materials_id = $("#choose_img_materials_id").val();
    		
			$("#id_cemetery_id").val(cemetery_id);
			$("#id_materials_id").val(materials_id);
			$("#id_cemetery_materials_id").val(cemetery_materials_id);
			document.fff.action = "<%=path%>/webCemetery/queryWzByCemeteryMaterialsId";
			$("#fff").ajaxSubmit(function(result){
	    		$("#show").attr("style","display: block; ");
				$(".bg_wz,.show_wz").attr("style","display: block; ");
				$(window.parent.document).contents().find("#iframe_wz_id")[0].contentWindow.setWz(result.materials_big_img); 
				$("#id_materials_id").val(materials_id);
				
			});
    	}
    	
    	//显示放大缩小提前之后div(素材)
    	function onShowSDiv(bg){
    		$(".foot_cao_zuo,.cao_zuo_btn").css({display:"block",border:"1px solid #5abbf4"});
    		$("#div_foot_big").show();
    		$("#div_foot_small").show();
    		$("#div_foot_edit").hide();
    		$(".cao_zuo").css({width:"100px"});
    		$(".foot_cao_zuo,.cao_zuo_btn").css({width:"300px"});
    		$(bg).children('div').css({display:"block"});
    	}
    	
    	//显示放大缩小提前之后div（文字）
    	function onShowSDivForWz(bg){
    		$(".foot_cao_zuo,.cao_zuo_btn").css({display:"block",border:"1px solid #5abbf4"});
    		$("#div_foot_big").hide();
    		$("#div_foot_small").hide();
    		$("#div_foot_edit").show();
    		$(".cao_zuo").css({width:"50px"});
    		$(".foot_cao_zuo,.cao_zuo_btn").css({width:"250px"});
    		$(bg).children('div').css({display:"block"});
    	}
    	
    	//扫墓
    	function toCleanCemetery(){
    		var sex = $("input[name='sex']:checked").val();
    		
    		document.cleanform.action = "<%=path%>/webCemetery/updateCemeteryForCleanCemetery";
			$("#cleanform").ajaxSubmit(function(result){
				if(result == 1){
					if(sex == 1){//男
						$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: 0px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_boy.gif' width='149' height='186' /></DIV>");
					}else{//女
						$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: 0px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_gril.gif' width='149' height='186' /></DIV>");
					}
					//关闭扫墓弹出窗
    				onCloseCleanCemetery()
    				//移动扫墓小人
					moveCleanPeople(sex);
					//清扫落叶
					$(".clean").remove();
				}else{
					alert('扫墓失败，请联系管理员！');
				}
			});
    	}
    	//移动扫墓小人
    	function moveCleanPeople(sex){
    		
    		left = 17;	
			setTimeout(moveCleanPeopleleft(left, sex), 600);
    	}
    	function moveCleanPeopleleft(left, sex){
    		
    		$("#cleanPeople").remove();
    		if(sex == 1){//男
    		    $("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: " + left + "px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_boy.gif' width='149' height='186' /></DIV>");
    		}else{//女
    			$("#srmy_main").append("<DIV style='Z-INDEX: 9999999999; POSITION: absolute; WIDTH: 149px; HEIGHT: 186px; TOP: 309px; LEFT: " + left + "px' id='cleanPeople'><IMG src='<%=path%>/resources/images/saomu_right_gril.gif' width='149' height='186' /></DIV>");
    		}
    		if(left > 940){
    			left=-100;
    		}
    		left = left + 17;
    		setTimeout("moveCleanPeopleleft("+left+","+sex+")", 600);
    	}
    	
    	//显示扫墓弹出窗
    	function onShowCleanCemetery(){
    		$(".sao_mu_content, .sao_mu_person").css({display: "block"});
    	}
    	
    	//关闭扫墓弹出窗
    	function onCloseCleanCemetery(){
    		$(".sao_mu_content, .sao_mu_person").css({display: "none"});
    	}
    	
    	//然送
    	function onRanSong(cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		
    		$("#ransongform_cemetery_materials_id").val(cemetery_materials_id);
    		document.ransongform.action = "<%=path%>/webCemetery/updateCemeteryMaterialsForRansong";
			$("#ransongform").ajaxSubmit(function(result){
				$("#srmy_main").append("<img id='hire_"+cemetery_materials_id+"' style='position:absolute;z-index:"+(cemetery_materials_z_index+1)+";left:360px;top:330px;width:100px;height:180px;' src='/resources/images/hire02.gif' />");
	    		$("#div_ransong_"+cemetery_materials_id).remove();
	    		var count = 0.8;
	    		setTimeout("onChangeRansong1('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
			});
    	}
    	function onChangeRansong1(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.6;
    		setTimeout("onChangeRansong2('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong2(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.4;
    		setTimeout("onChangeRansong3('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong3(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		count = 0.2;
    		setTimeout("onChangeRansong4('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong4(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    	    $("#srmy_main").append("<img id='ransong_"+cemetery_materials_id+"' style='opacity:"+count+";position:absolute;z-index:"+cemetery_materials_z_index+";left:360px;top:400px;width:"+cemetery_materials_width+"}px;height:"+cemetery_materials_height+"px;' src='/goods/"+materials_big_img+"' />");
    		setTimeout("onChangeRansong5('"+count+"','"+cemetery_materials_id+"','"+cemetery_materials_z_index+"','"+cemetery_materials_width+"','"+cemetery_materials_height+"','"+materials_big_img+"')", 300);
    	}
    	function onChangeRansong5(count,cemetery_materials_id, cemetery_materials_z_index, cemetery_materials_width, cemetery_materials_height, materials_big_img){
    		$("#ransong_"+cemetery_materials_id).remove();
    		$("#hire_"+cemetery_materials_id).remove();
    	}
    	
    	//清楚勾选记录
    	function onClear(){
//     		if(confirm('关闭将清空勾选记录，是否确认关闭？')){
    			$('#Form', parent.document).empty();
    			$(".bg_confirm, .show_confirm").attr("style","display: none; ");
    			$("#showMaterialsList", parent.document).attr("style","display: none; ");
//     		}
    	}
    	
    </script>
 
</head>
<body>
<input type="hidden" id="vipcount" value="${vipcount}" />
<form action="" method="post" name="ransongform" id="ransongform">
  <input type="hidden" name="cemetery_id" value="${cemetery_id}"/>
  <input type="hidden" name="cemetery_materials_id" id="ransongform_cemetery_materials_id" value=""/>
</form>
<input type="hidden" id="choose_img_cemetery_materials_id" value=""/>
<input type="hidden" id="choose_img_materials_id" value=""/>
<input type="hidden" id="userr_user_id" value="${userr_user_id}"/>
<form action="/qjqwmain/cemetery/list" method="post" name="saveForm" id="saveForm">
</form>
<input type="hidden" id="input_cemetery_id" value="${cemetery_id}"/>
<input type="hidden" id="z_index" value="${z_index}"/>
<input type="hidden" id="input_type_flag" value="2"/>
<input type="hidden" id="tabflag" value="${flag}"/>
<input type="hidden" id="id_chooseflag" value="${chooseflag}"/>
<form action="/qjqwmain/cemetery/list" method="post" name="ff" id="ff">
</form>
<form method="post" name="fff" id="fff">
  <input type="hidden" name="wz" id="id_wz" value=""/>
  <input type="hidden" name="materials_id" id="id_materials_id" value=""/>
  <input type="hidden" name="cemetery_id" id="id_cemetery_id" value=""/>
  <input type="hidden" name="cemetery_materials_id" id="id_cemetery_materials_id" value=""/>
</form>
<form action="/qjqwmain/cemetery/list" method="post" name="Form" id="Form">
  
</form>
<div class="main">
  <div class="header">
    <div class="srmy_wzgg">公告：纪念512汶川地震7周年</div>
    <img src="<%=path%>/resources/images/logo.png" class="srmy_logo" />
    <!-- 菜单-->
    <ul class="srmy_menu">
      <li class="active"><a href="#">墓    园</a></li>
      <li><a href="<%=path%>/">网站首页</a></li>
      <li><a href="<%=path%>/userManage">个人后台</a></li>
    </ul>
    <div class="srmy_infor_bg"></div>    
    <div class="srmy_infor">
      <c:choose>
        <c:when test="${cemetery.cemetery_death_img == null || cemetery.cemetery_death_img == ''}"><!-- 默认图片 -->
          <img src="<%=path%>/resources/images/srmy_infor_photo.png" class="infor_photo" />
        </c:when>
        <c:otherwise><!-- 有图片 -->
          <c:choose>
            <c:when test="${cemetery.cemetery_death_img_flag == 1}"><!-- 存本地 -->
              <img src="<%=path%>${cemetery.cemetery_death_img}" class="infor_photo" />
            </c:when>
            <c:otherwise><!-- 存qiniu -->
              <img src="${cemetery.cemetery_death_img}" class="infor_photo" />
            </c:otherwise>
          </c:choose>
        </c:otherwise>
      </c:choose>
     
       <div class="top_infor_right">
	     	<ul>
	        	<li>${cemetery.cemetery_name}</li>
	        	<li><img src="<%=path%>/resources/images/srmy_infor_my.png" /></li>
	      	</ul>
	      	<ul class="top_my_num">
	      	 	<li>墓 园 号<span>${cemetery.cemetery_number}</span></li>
	        	<li>建 园 者<span>${Manage.user_nick_name}</span></li>
	      	</ul>
     	</div>
      <div class="srmy_infor_addr">墓园地址<span>${cemetery.cemetery_country} ${cemetery.cemetery_province} ${cemetery.cemetery_city}</span></div>
      <div class="srmy_infor_jnr">纪 念 日
        <span>
          <c:forEach var="obj" items="${specialTimeList}" > ${obj} </c:forEach>
        </span>
      </div>
      <div class="srmy_infor_look">
        <div class="srmy_infor_ico_01">
          <div>浏览</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_01.png" />
          <div>${cemetery.cemetery_clicks}</div>
        </div>
        <div class="srmy_infor_ico_02">
          <div>祭祀</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_02.png" />
          <div>${cemetery.cemetery_sacrifice}</div>
        </div>
        <div class="srmy_infor_ico_03">
          <div>亲情</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_03.png" />
          <div>${cemetery.cemetery_family}</div>
        </div>
        <div class="srmy_infor_ico_04">
          <div>财富</div>
          <img src="<%=path%>/resources/images/srmy_infor_ico_04.png" />
          <div>${cemetery.cemetery_wealth}</div>
        </div>
      </div>
    </div>
  </div>
 
  <div class="content">
    <!-- 墓园部分-->
    <div class="wdmy_wrap">
      <!--<iframe src="srmy_my.html" scrolling="no" iframeborder="no" class="iframe_wdmy"></iframe>-->

		


      <!-- 墓园放置物品-->
      <div class="srmy_main" id="srmy_main">
      <img src="/resources/images/ttsongxin.gif"  style="position:absolute;z-index:999999"/>
    
		<c:forEach var="obj" items="${sclist}" varStatus="status">
		  <c:if test="${obj.cemetery_materials_show == 0 && obj.cemetery_materials_id != 210452}">
	        <c:choose>
	          <c:when test="${obj.materials_class_id eq '10'}"><%--  背景 素材分分类id ==10    --%>
	            <img id="div${obj.cemetery_materials_id}" materials_info="${obj.cemetery_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:0;left:0px;top:0px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
	          </c:when>
	          <c:when test="${obj.materials_class_id eq '8888'}"><!-- 是文字信息   -->
	            <div id="div${obj.cemetery_materials_id}" 
	            	 width_info="${obj.cemetery_materials_width}" 
	            	 height_info="${obj.cemetery_materials_height}" 
		             materials_info="${obj.cemetery_materials_id}"
		             ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
		             data-name="img${obj.cemetery_materials_id}" 
		             data-id="url${obj.cemetery_materials_id}" 
		             style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:auto;height:auto;${obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit == 0 ? 'border:1px dotted black;' : '' }"
		             onclick="onShowSDivForWz(this);"
		             materials_id_info = "${obj.materials_id}"
		             name="1">
		            
		            ${obj.materials_big_img}
		            
		          <div class="cao_zuo">
                    <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                    <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                  </div>
	            </div>
	          </c:when>
	          <c:when test="${obj.materials_class_id eq '74'}">
	            <img  title="${obj.materials_name}-${obj.materials_class_id}"  id="ransong_${obj.cemetery_materials_id}"
		            	style="opacity:1;position:absolute;z-index:${obj.cemetery_materials_z_index};left:360px;top:400px;width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;"
		            	src="<%=path%>/goods/${obj.materials_big_img}" />
	            <div id="div_ransong_${obj.cemetery_materials_id}" style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:380px;top:430px;" onclick="onRanSong('${obj.cemetery_materials_id}','${obj.cemetery_materials_z_index}','${obj.cemetery_materials_width}','${obj.cemetery_materials_height}','${obj.materials_big_img}');">燃送</div>
	          </c:when>
	          <c:otherwise>
	            <div id="div${obj.cemetery_materials_id}" 
	                 ${((obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit != 3) || (is_user_cemetery_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''} 
	                 data-name="img${status.index+1}" 
	                 data-id="url${status.index+1}" 
	                 style="position:absolute;z-index:${obj.cemetery_materials_z_index};left:${obj.cemetery_materials_x}px;top:${obj.cemetery_materials_y}px;width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;"
	                 name="1"
	                 width_info="${obj.cemetery_materials_width}" 
	                 height_info="${obj.cemetery_materials_height}" 
		             materials_info="${obj.cemetery_materials_id}" 
		             imgwidth="${obj.cemetery_materials_width}" 
		             imgweight="${obj.cemetery_materials_height}"
		             onclick="onShowSDiv(this);"
		             materials_id_info = "${obj.materials_id}">
                  <div class="cao_zuo">
                    <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                    <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                    <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                    <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                  </div>
	              <img  title="${obj.materials_name}-${obj.materials_class_id}" 
		            	style="width:${obj.cemetery_materials_width}px;height:${obj.cemetery_materials_height}px;${obj.sacrifice_user_id == user_id && obj.cemetery_materials_limit == 0 ? 'border:1px dotted black;' : '' }"
		            	src="<%=path%>/goods/${obj.materials_big_img}" />
		        </div>
	          </c:otherwise>
	        </c:choose>
	      </c:if>
  		</c:forEach>
  		
  		
  		<c:forEach var="obj" items="${leafsArray}" varStatus="status">
  			${obj }
  		</c:forEach>
      </div>
      
      <div class="foot_cao_zuo"></div>
      <div class="cao_zuo_btn">
        <div id="div_foot_big" class="foot_big">
          <div><img src="<%=path%>/resources/images/srmy_materisl_big.png" class="foot_big" /></div>
          <div>放大</div>
        </div>
        <div id="div_foot_small" class="foot_small">
          <div><img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="foot_small" /></div>
          <div>缩小</div>
        </div>
        <div id="div_foot_before" class="buzhi_before">
          <div><img src="<%=path%>/resources/images/srmy_materisl_before.png" class="buzhi_before" /></div>
          <div>置前</div>
        </div>
        <div id="div_foot_after" class="buzhi_after">
          <div><img src="<%=path%>/resources/images/srmy_materisl_after.png" class="buzhi_after" /></div>
          <div>置后</div>
        </div>
        <div id="div_foot_edit">
          <div><img src="<%=path%>/resources/images/editwordblue.png" class="" onclick="onChangeWz1();" /></div>
          <div>编辑</div>
        </div>
        <div id="div_foot_delete" class="foot_delete">
          <div><img src="<%=path%>/resources/images/srmy_materisl_delete.png" class="foot_delete" /></div>
          <div>删除</div>
        </div>
      </div>
     
      <!--商品类别详细信息-->
      <div id="bg"></div>
      
      <div id="showMaterialsList">
      	<iframe src="" scrolling="no" iframeborder="no" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
      </div>
      <div id="show"></div>
      
      <!--结算购物车-->
      <div class="bg_confirm"></div>
      <div class="show_confirm">
        <div class="confirm_content" id="div_confirm_container_js">
        </div>
      </div>
      
      <div class="bg_wz"></div>
      <div class="show_wz" id="showWz">
        <img src="<%=path%>/resources/images/close_window.png" class="prompt_close" onclick="onCloseWz();" />
        <div class="prompt_title">提 示</div>
        <div class="wz_content">
       	  <iframe src="<%=path%>/webCemetery/queryWz" scrolling="no" iframeborder="no" name="iframe_wz_name" id="iframe_wz_id"></iframe>
        </div>
      </div>
    
            
    <!--提示层-->
    <div class="bg_prompt"></div>
    <div class="prompt_infor">
      <img src="/resources/images/close_window.png" class="prompt_close" />
      <div class="prompt_title">提 示</div>
      <div class="prompt_content">
        <img src="/resources/images/prompt_wait.png" />
        <div class="font">福币补充完成前，请不要关闭窗口。</div>
        <div class="behind">福币补充完成后，请根据情况点击一下按钮。</div>
        <div class="prompt_btn">
          <div class="issue">遇到问题</div>
          <div class="accomplish">完成</div>
        </div>
      </div>
    </div>
    
    
    <!--文集追思部分-->

          
          

            <!-- 墓园档案-->
            <div class="myda_bg bg"></div>
            <div class="myda_content">
                <div class="window_title">墓 园 档 案</div>
                <img src="/resources/images/close_window.png" class="myda_window_close  window_close">
                <div class="bjxx_window_content">
                    <div class="cont_left">
                        <img src="/resources/images/add_peo_photo.png" class="myyz_photo">
                        <div class="cont_left_title">墓园遗照</div>
                    </div>
                    <div class="cont_right">
                        <div class="cont_right_hang1">
                            <div>墓园号码：<span>12345678</span></div>
                            <div>墓园名称：<span>张三纪念园</span></div>
                        </div>
                        <div class="cont_right_hang1">
                            <div>墓园类型：<span>单人墓</span></div>
                            <div>公开设置：<span>完全公开</span></div>
                        </div>
                        <div class="cont_right_hang1">
                            <div>墓园地址：<span>哈尔滨市宾县公墓</span></div>
                        </div>
                        <div class="cont_right_hang4">
                            <div>园主留言：<div>个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介个人简介</div></div>
                        </div>

                    </div>
                    <div class="cont_line"></div>

                    <div class="shi_zhe_infor">
                        <div class="only_one">
                            <div class="szxx_title">逝者 1 信息</div>
                            <div class="szxx_cont_one">
                                <div class="cont_one_hang1">
                                    <div>姓名：<span>张三</span></div>
                                    <div>性别：<span>男</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    <div>民族：<span>汉族</span></div>
                                    <div>信仰：<span>天主教</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    <div>生辰：<span>1973-02-04</span></div>
                                    <div>忌辰：<span>1999-05-20</span></div>
                                </div>
                                <div class="cont_one_hang1">
                                    <div class="sz_guan_xi">您与逝者关系：<span></span></div>
                                </div>
                            </div>

                        </div>

                        <div class="only_two">
                            <div class="two_left">
                                <div class="szxx_title">逝者 1 信息</div>
                                <div class="szxx_cont_first">
                                    <div class="cont_one_hang1">
                                        <div>姓名：<span>张三</span></div>
                                        <div>性别：<span>男</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>民族：<span>汉族</span></div>
                                        <div>信仰：<span>天主教</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>生辰：<span>1973-02-04</span></div>
                                        <div>忌辰：<span>1999-05-20</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div class="sz_guan_xi">您与逝者关系：<span></span></div>
                                    </div>
                                </div>
                            </div>

                            <div class="two_right">
                                <div class="szxx_title">逝者 2 信息</div>
                                <div class="szxx_cont_first">
                                    <div class="cont_one_hang1">
                                        <div>姓名：<span>张三</span></div>
                                        <div>性别：<span>男</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>民族：<span>汉族</span></div>
                                        <div>信仰：<span>天主教</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div>生辰：<span>1973-02-04</span></div>
                                        <div>忌辰：<span>1999-05-20</span></div>
                                    </div>
                                    <div class="cont_one_hang1">
                                        <div class="sz_guan_xi">您与逝者关系：<span></span></div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>

                </div>
            </div>




        
    
    
  	</div>

    <!-- 素材部分-->
    <div class="srmy_sc">
      <!--<iframe src="srmy_sucai.html" scrolling="no" iframeborder="no" class="srmy_sucai_iframe"></iframe>-->
      <div class="srmy_suocai">
        <ul class="tab_menu">
          <li class="jisi" onclick="onChooseJisiOrBuzhi('1', '${cemetery_id}', ${chooseflag}, '${is_user_cemetery_flag }');" ${flag == 1 ? 'style="background-color:#fd844e"' : ''} >祭祀物品</li>
          <c:choose>
            <c:when test="${is_user_cemetery_flag > 0}"><!-- 是墓园建造者，显示墓园布置列表 -->
              <li class="buzhi" onclick="onChooseJisiOrBuzhi('2','${cemetery_id}',${chooseflag}, '${is_user_cemetery_flag }');" ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>建园物品</li>
            </c:when>
            <c:otherwise>
              <li class="buzhi" style='background-color: #bbb;' ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>建园物品</li>
            </c:otherwise>
          </c:choose>
          <li class="choose_ceng" onclick="onChooseJisiOrBuzhi('3','${cemetery_id}',${chooseflag}, '${is_user_cemetery_flag }');" ${flag == 3 ? 'style="background-color:#fd844e"' : ''} >移动布置</li>
        </ul>
        <div class="srmy_tab_content">
          <!-- 墓园祭祀-->
            <div id="div_jisi" class="sacrifice ${flag == 1 ? '' : 'content_hide'}">
              <ul>
                <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
                  <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
                    <img src="<%=path%>/goods/${var.materials_class_img }" style="height: 29px;width: 29px;" />
                    <div>${var.materials_class_name }</div>
                  </li>
                </c:forEach>
              </ul>
		    </div>
            <!-- 墓园布置-->
            <div class="sacrifice ${flag == 2 ? '' : 'content_hide'}" id="div_buzhi">
              <ul>
                <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
                  <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
                    <img src="<%=path%>/goods/${var.materials_class_img}" style="height: 29px;width: 29px;" />
                    <div>${var.materials_class_name}</div>
                  </li>
                </c:forEach>
                <li onclick = "onShowWZ();">
                    <img src="<%=path%>/resources/images/text.jpg" style="height: 29px;width: 29px;" />
                    <div>文字</div>
                  </li>
              </ul>
            </div>
          <!-- 选择图层-->
          <div ${flag == 3 ? '' : 'class="content_hide"'} id="div_tuceng">
             <div class="srmy_bg">
             
               <c:forEach var="obj" items="${bglist}" varStatus="status">
                 <c:if test="${status.index == (fn:length(bglist) - 1) }">
                   <li id="list${status.index+1}" class="cc" draggable="true">${obj.materials_name}
    			     <span class="buy_user_name">${obj.user_nick_name}祭祀</span>
                     <span class="buy_user"> </span>
                   </li>
                 </c:if>
                 
              </c:forEach>
             </div>
              <ul id="SortContaint" class="house fix_up">
                <c:forEach var="obj" items="${movelist}" varStatus="status">
                  <c:if test="${obj.materials_class_id != 10}">
	    			<li id="list${obj.cemetery_materials_id}" class="cc" draggable="true">
	    			${fn:length(obj.materials_name) > 10 ? fn:substring(obj.materials_name, 0, 10) : obj.materials_name}${fn:length(obj.materials_name) > 10 ? '...' : ''}
	    			<span class="buy_user_name">${obj.user_nick_name}祭祀</span>
	                <span class="buy_user">
	                    <c:choose>
	                      <c:when test="${obj.materials_class_id == 8888 }"><!-- 文字信息   -->
	                      	<a href="#" onclick="onChangeWz('${obj.materials_id}','${cemetery_id}', '${obj.cemetery_materials_id}');">
							  <img src="<%=path%>/resources/images/editwordblue.png" class="srmy_sucai_big" />
							</a>
	                      </c:when>
	                      <c:otherwise>
	                      	<img src="<%=path%>/resources/images/srmy_materisl_big.png" class="srmy_sucai_big" />
	                    	<img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="srmy_sucai_reduce" />
	                      </c:otherwise>
	                    </c:choose>
	                    
	                    <img name="eye_${obj.cemetery_materials_id}" src="<%=path%>/resources/images/srmy_materisl_${obj.cemetery_materials_show == 0 ? 'show' : 'hidden'}.png" class="srmy_sucai_hidden" onclick="onUpdMaterialsShow(${obj.cemetery_materials_id}, ${obj.cemetery_id},this);" flag_info = '${obj.cemetery_materials_show}' />
	                    <img src="<%=path%>/resources/images/srmy_materisl_delete.png" class="srmy_sucai_delete" onclick="onDelMaterials(${obj.cemetery_materials_id}, ${obj.cemetery_id});" />
	                  </span>
	                </li>
                  </c:if>
  			    </c:forEach>
              </ul>
            </div>
          </div>
        </div>
	  </div>
	
      <!--按钮部分-->
      <div class="footer">
	      <c:if test="${is_user_cemetery_flag==1}">    <div class="bian_ji_infor" onclick="toEditCemetery('${cemetery.cemetery_number}');">编辑信息</div> </c:if>
	        <div class="bian_ji_infor" onclick="onShowCleanCemetery();">扫墓</div>
	   <c:if test="${is_user_cemetery_flag==1}">      <div class="bei_jin_music"  onclick="getCemeteryMusic('${cemetery.cemetery_music_url}');" >背景音乐</div> </c:if>
	        <div class="te_shu_date" onclick="getSpecial_day('${cemetery._id}');">特殊纪念</div>
	        <div class="history_photo"    onclick="photoManage('${cemetery._id}');">历史相册</div>
	     <c:if test="${is_user_cemetery_flag==1}">
	      <div class="change_formwork" onclick="getTemplate('${cemetery.cemetery_number}');">更换模板</div>
	         </c:if> 
	        <div class="buzhi_corpus_miss"  onclick="ArticleAndCommon('${cemetery.cemetery_number}');">文集追思</div>
	        <div class="ji_si_manage"   onclick="sacrifice('${cemetery._id}');">祭祀日志</div>
	 <c:if test="${is_user_cemetery_flag==1}">        <div class="limit_set"  onclick="getManage('${cemetery.cemetery_number}');">权限设置</div> </c:if>
    
        <!-- 保存按钮-->
              <c:choose>
          <c:when test="${moveFlag > 0 }">
            <!--<div class="mu_yuan_dang_an"  style="background-color:#bbbbbb" />墓园档案</div>-->
            <!--<div class="ji_si_ri_zhi"  style="background-color:#bbbbbb" />祭祀日志</div>-->
            <!--<div class="buzhi_corpus_miss"  style="background-color:#bbbbbb" />文集追思</div>-->
            <!--<div class="add_friends"  style="background-color:#bbbbbb" />加好友</div>-->
            <!--&lt;!&ndash; 保存按钮&ndash;&gt;-->

            <!-- 保存按钮-->
            <c:if test="${moveFlag > 0}">
              <input type="button" value="保存"  id="sava_id" style="background-color:#bbbbbb" />
            </c:if>
          </c:when>
          <c:otherwise>
            
            <c:if test="${is_user_cemetery_flag == 1}">
              <input type="button" value="保存"  id="sava_id" style="background-color:#bbbbbb" />
            </c:if>
          </c:otherwise>
        </c:choose>
       
      </div>


  
  </div>




 <!-- 聘用扫墓人-->
 <form action="" method="post" name="cleanform" id="cleanform">
 <input type="hidden" name="cemetery_id" value="${cemetery.cemetery_id}" />
            <div class="bg  sao_mu_person"></div>
            <div class="sao_mu_content">
                <div class="window_title">请选择扫墓人的性别</div>
                <img src="<%=path%>/resources/images/close_window.png" class="jsrz_window_close  window_close" onclick="onCloseCleanCemetery();" />
                <div class="smr_window_content">
                    <div class="smr_sex">
                        <div class="choose_title">请选择扫墓人性别：</div>
                        <label for="man"><input type="radio" id="man" name="sex" value="1" checked="checked" />男</label>
                        <label for="woman"><input type="radio" id="woman" name="sex" value="0"/>女</label>
                    </div>
                    <div class="smr_message">提示:每扫墓一次需收取<span>50福币</span>，您确定要扫墓吗?</div>
                    <div class="smr_btn">
                        <div class="smr_qd" onclick="toCleanCemetery();">确定</div>
                        <div class="smr_qx" onclick="onCloseCleanCemetery();">取消</div>
                    </div>
                </div>
            </div>
         </form>   
            
 </div>
 <script type="text/javascript">
	function toEditCemetery(Id){
		 var diag = new Dialog();
		 diag.Drag=true;
		 diag.Title ="编辑信息";
		 diag.URL = '/cemetery/toEditCemetery?number='+Id;
		 diag.Width = 710;
		 diag.Height = 550;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	} 
	function getCemeteryMusic(Id){
		 var diag = new Dialog();
		 diag.Drag=true;
		 diag.Title ="背景音乐";
		 diag.URL = '/cemetery/getMusic?url='+Id;
		 diag.Width = 720;
		 diag.Height = 550;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	} 
		function getSpecial_day(Id){
		 var diag = new Dialog();
		 diag.Drag=true;
		 diag.Title ="特殊纪念";
		 diag.URL = '/cemetery/getSpecial_day?id='+Id;
		 diag.Width = 720;
		 diag.Height = 550;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	}
		function photoManage(Id){
			 var diag = new Dialog();
			 diag.Drag=true;
			 diag.Title ="历史相册";
			 diag.URL = '/cemetery/photoManage?id='+Id;
			 diag.Width = 700;
			 diag.Height = 550;
			 diag.CancelEvent = function(){ //关闭事件
				diag.close();
			 };
			 diag.show();
		}
		
		function sacrifice(Id){
			 var diag = new Dialog();
			 diag.Drag=true;
			 diag.Title ="祭祀日志";
			 diag.URL = '/cemetery/sacrifice?id='+Id;
			 diag.Width = 720;
			 diag.Height = 550;
			 diag.CancelEvent = function(){ //关闭事件
				diag.close();
			 };
			 diag.show();
		}
		
		function ArticleAndCommon(Id){
			 var diag = new Dialog();
			 diag.Drag=true;
			 diag.Title ="文集追思";
			 diag.URL = '/cemetery/ArticleAndCommon?cemetery_number='+Id;
			 diag.Width = 720;
			 diag.Height = 420;
			 diag.CancelEvent = function(){ //关闭事件
				diag.close();
			 };
			 diag.show();
		}
		function getTemplate(Id){
			 var diag = new Dialog();
			 diag.Drag=true;
			 diag.Title ="更改模板";
			 diag.URL = '/cemetery/getTemplate?cemetery_number='+Id;
			 diag.Width = 727;
			 diag.Height = 550;
			 diag.CancelEvent = function(){ //关闭事件
				diag.close();
			 };
			 diag.show();
		}
		function getManage(Id){
			 var diag = new Dialog();
			 diag.Drag=true;
			 diag.Title ="权限设置";
			 diag.URL = '/cemetery/getManage?cemetery_number='+Id;
			 diag.Width = 700;
			 diag.Height = 550;
			 diag.CancelEvent = function(){ //关闭事件
				diag.close();
			 };
			 diag.show();
		}
 </script>
 
 
</body>
</html>