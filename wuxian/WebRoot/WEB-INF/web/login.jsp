<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
<link href="/goods/css/denglu.css" rel="stylesheet">
<script src="/resources/js/jquery-1.9.1.min.js"></script>
<script src="/goods/js/denglu.js"></script>
<script type="text/javascript">
		function denglu(){
			
			
			if(check()){
			$("#loginForm").submit();
			}
		}
		function quxiao(){
			$("#loginname").val('');
			$("#password").val('');
		}
	</script>
<c:if test="${error != null && error != '' }">
	<script type="text/javascript">
		alert("${error}");
	</script>
</c:if>
</head>
<body>
	<div class="main">
		<!--菜单部分(头部)-->
		<div class="header">
			<ul>
				 <li id="shou_ye"><a href="/index">首 页</a></li>
                <li id="sec_srmy"><a href="/webCemetery/index">私人墓园</a></li>
                <li id="sec_gjpt"><a href="/gongjipingtai">公祭平台</a></li>
                <li id="sec_zcyq"><a href="/webAncestralBz/index">宗祠园区</a></li>
                <li id="sec_lfyq"><a href="/webLifo/fozhu">礼佛园区</a></li>
                <li id="sec_wish_tree"><a href="/webWishingTree/querySecondWishTree?public_wish_tree_id=1">心愿树</a></li>
                <li id="sec_wzxx"><a href="/textForIndex">文字信息</a></li>
                <li id="sec_use_help"><a href="#">使用帮助</a></li>
			</ul>
		</div>

		<!--蓝色logo标部分-->
		<div class="banner">
			<img src="/goods/images/denglu_logo_big.png" class="logo">
		</div>

		<!--中间白色部分-->
		<div class="kongbai"></div>

		<!--渐变背景部分-->
		<div class="jianbian"></div>

		<!--底部-->
		<div class="footer">
			<div>哈尔滨祈福科技有限公司 Copyright 2007-2015 All rights reserved&nbsp;黑ICP备07002328号</div>
			<div>全球祭祀祈福网&nbsp;客服电话：400-0240-535&nbsp;客服邮箱：5027721600qq.com</div>
		</div>

		<!--登录/注册部分-->
		<div class="denglu_center">
			<ul class="denglu_tab_menu">
				<li class="denglu_yhdl">用户登录</li>
				<li class="denglu_mfzc">免费注册</li>
			</ul>

			<div class="denglu_tab_content">
				<!--登录内容-->

				<div class="denglu_denglu">
					<form id="loginForm" action="tologin" method="post">
						<div class="denglu_user">
							<img src="/goods/images/denglu_user.png"> <input type="text" name="userAccount" id="userAccount" value="" placeholder="请输入用户名" maxlength="20" onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\@\.]/g,'')">
						</div>
						<div class="denglu_pwd">
							<img src="/goods/images/denglu_pwd.png"> <input type="password" name="userPwd" id="userPwd" value="" placeholder="请输入密码"  onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\@\.]/g,'')">
						</div>
						<div class="denglu_btn">
							<input type="button"  id="dl"  value="登录" onclick="denglu();">
						</div>

						<!--使用福卡和忘记密码部分-->
						<div class="denglu_forget">
						<!--  	<input type="checkbox"><span>使用福卡</span> --> 
							<span class="forget_pwd"><a href="/findPassWord">忘记密码？</a></span>
						</div>
						<input type="hidden" name="token" value="${token}" />
					</form>
				</div>


				<div class="denglu_content_hide denglu_zhuce">
					<!--注册内容-->
					<form action="register" method="post" onsubmit="return checkR();">
						<div class="denglu_user">
							<img src="/goods/images/denglu_user.png"> <input type="text" value="" name="userAccount" id="em" placeholder="请输入手机号" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" onblur="chau();">
						</div>
						<div class="denglu_pwd">
							<img src="/goods/images/denglu_pwd.png"> <input type="password" id="pwd1" value="" name="userPwd" placeholder="设置密码" maxlength="50">
						</div>
						<div class="denglu_yzm">
							<img src="/goods/images/denglu_pwd.png"> <input type="password" id="pwd2" value="" placeholder="确认密码" maxlength="50">
						</div>
						<!--  <div class="denglu_yzm denglu_yzm_01"><img src="/goods/images/denglu_yzm.png">
                <input type="text" name="code" id="code"  value="" placeholder="请输入验证码">
                    <span>
                    <img style="height:22px; float: left;margin-left: -10px;" id="codeImg" alt="点击更换" title="点击更换" src="" />
                    </span>
                </div> -->
						<div class="yan_zhenmg_ma">
							<div class="inp_tel_num">
								<input type="text" name="code" id="code">
							</div>
							<input id="huo_qu_ma" type="button" value="免费获取验证码"  disabled="true">
						</div>

						<div class="denglu_btn">
							<input type="submit" value="注册">
						</div>

						<!--使用福卡和忘记密码部分-->
						<div class="denglu_forget">
							<input type="checkbox"><span>使用福卡</span> <span class="forget_pwd"><a href="#">忘记密码？</a></span>
						</div>
						<input type="hidden" name="token" value="${token}" />
					</form>
				</div>

			</div>

			<!--第三方登录-->
			<div class="denglu_three">
				<div class="denglu_three_title">第三方登录</div>
				<div class="denglu_three_btn">
					<img src="/goods/images/denglu_three_qq.png"><span>QQ账号登录</span>
				</div>
				<div class="denglu_three_btn">
					<img src="/goods/images/denglu_three_sine.png"><span>新浪微博登录</span>
				</div>
				<div class="denglu_three_btn">
					<img src="/goods/images/denglu_three_dou.png"><span>豆瓣账号登录</span>
				</div>
				<div class="denglu_three_btn">
					<img src="/goods/images/denglu_three_rr.png"><span>人人账号登录</span>
				</div>
			</div>
		</div>


	</div>

	<script src="/resources/js/jquery-1.8.2.js"></script>

	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
	<script type="text/javascript">
		var errInfo = "${errInfo}";
		$(document).ready(function(){
			if(errInfo!=""){
				if(errInfo.indexOf("验证码")>-1){
					
					$("#code").tips({
						side:1,
			            msg:errInfo,
			            bg:'#FF5080',
			            time:5
			        });
					
					$("#code").focus();
				}else{
					$("#userAccount").tips({
						side:1,
			            msg:errInfo,
			            bg:'#FF5080',
			            time:5
			        });
				}
			}
		
			$("#userAccount").focus();
		

		});
		
		$(document).keyup(function(event){
			  if(event.keyCode ==13){
			//    $("#dl").trigger("click");
				  denglu();
			  }
			});
	
		function genTimestamp(){
			var time = new Date();
			return time.getTime();
		}
	
		
		
		function check(){
			
			if($("#userAccount").val()==""){

				$("#userAccount").tips({
					side:2,
		            msg:'用户名不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#userAccount").focus();
				return false;
			}else
			if($("#userPwd").val()==""){

				$("#userPwd").tips({
					side:2,
		            msg:'密码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#userPwd").focus();
				return false;
			}else{
			
			$("#loginbox").tips({
				side:1,
	            msg:'正在登录 , 请稍后 ...',
	            bg:'#68B500',
	            time:1000
	        });
			return true;
			}
		}
		function checkR(){
		
		    var pattern = /^([\.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;  
		/*    if (!pattern.test($("#em").val())) {  
		    	$("#em").tips({
					side:2,
		            msg:'請輸入正確的郵箱地址',
		            bg:'#AE81FF',
		            time:3
		        });
		        $("#em").focus(); 
		        return false;  
		    }*/  
			chau();
			if($("#em").val()==""){

				$("#em").tips({
					side:2,
		            msg:'用户名不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#em").focus();
				return false;
			}else{
				
				$("#em").val(jQuery.trim($('#em').val()));
			}
			
			if($("#pwd1").val()==""){

				$("#pwd1").tips({
					side:2,
		            msg:'密码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd1").focus();
				return false;
			}else if($("#pwd1").val().length<6){
				$("#pwd1").tips({
					side:2,
		            msg:'密碼太短了',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd1").focus();
				return false;
			}
			if($("#pwd2").val()==""){

				$("#pwd2").tips({
					side:2,
		            msg:'请输入确认密码',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd2").focus();
				return false;
			}
			if($("#pwd2").val()!=$("#pwd1").val()){

				$("#pwd2").tips({
					side:2,
		            msg:'两次密码不一致',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#pwd2").focus();
				return false;
			}
			if($("#code").val()==""){

				$("#code").tips({
					side:1,
		            msg:'验证码不得为空',
		            bg:'#AE81FF',
		            time:3
		        });

				$("#code").focus();
				return false;
			}
			
			$("#loginbox").tips({
				side:1,
	            msg:'正在登录 , 请稍后 ...',
	            bg:'#68B500',
	            time:1000
	        });
			
			return true;
		}
		/*
		function savePaw(){
			if(!$("#saveid").attr("checked")){
				$.cookie('userAccount', '', { expires: -1 });
				$.cookie('userPwd', '', { expires: -1 });
				$("#userAccount").val('');
				$("#userPwd").val('');
			}
		}
		*/
		/*
		jQuery(function(){
			var userAccount = $.cookie('userAccount');
			var userPwd = $.cookie('userPwd');
			if(typeof(userAccount) != "undefined" && typeof(userPwd) != "undefined"){
				$("#userAccount").val(userAccount);
				$("#userPwd").val(userPwd);
				$("#saveid").attr("checked",true);
				$("#code").focus();
			}
		});
		*/
		function chau(){
			if(checkMobile()){
				$.ajax({
					type: "POST",
					url: '/getUser?username='+$("#em").val(),
					//beforeSend: validateData,
					success: function(data){
						
						if(data=="此帐号已存在")
							{
							$("#em").tips({
								side:1,
					            msg:data,
					            bg:'#AE81FF',
					            time:3
					        });
							return false;
							}else{
								$("#em").tips({
									side:1,
						            msg:"此号码可以注册",
						            bg:'#AE81FF',
						            time:3
						        });
							    document.getElementById("huo_qu_ma").removeAttribute("disabled");
							}
					}
				});
			}else{
				$("#em").tips({
					side:2,
		            msg:'请输入正确的手机号码',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#em").focus();
				return false;
			}
			
		}
		</script>

	<script type="text/javascript">
		    var wait=60;
		    function time(o) {
		        if (wait == 0) {
		            o.removeAttribute("disabled");
		            o.value="免费获取验证码";
		            o.style.backgroundColor="#4bacf1";
		            wait = 60;
		        } else {
		           $("#huo_qu_ma").attr("disabled", "true");
		   //         o.value="重发验证码(" + wait + ")";
		            $("#huo_qu_ma").val("重发验证码(" + wait + ")");
		            $("#huo_qu_ma").css("background-color","#999");
		                       
		   //         o.style.backgroundColor="#999";
		            wait--;
		            setTimeout(function() {
		                        time(o)
		                    },
		                    1000)
		        }
		       
		    }
		    document.getElementById("huo_qu_ma").onclick=function(){

				if($("#em").val()==""){

					$("#em").tips({
						side:2,
			            msg:'请输入手机号',
			            bg:'#AE81FF',
			            time:3
			        });
					
					$("#em").focus();
					return false;
				}else if(!checkMobile()){       
					$("#em").tips({
						side:2,
			            msg:'请输入正确的手机号码',
			            bg:'#AE81FF',
			            time:3
			        });
					
					$("#em").focus();
					return false;
				}
		    	 else{
		    	var  a=$("#em").val();
			var url = "/sendSms?phone="+a;
		    $.get(url,function(data){
		     if(data=="success"){
		    	 alert("已发送验证码");
		    	 time(this);
		     }
		    }
		    );}
		    }
		    //验证电话  
		    function checkMobile() {  
		        var s=$("#em").val();
		        if (trim(s) != "") {  
		            var regu = /^[1][3-9][0-9]{9}$/;  
		            var re = new RegExp(regu);  
		            if (re.test(s)) {  
		                return true;  
		            } else {  
		                return false;  
		            }  
		        }  
		    } 
		    
		    function trim(str) { //删除左右两端的空格  
		        return str.replace(/(^\s*)|(\s*$)/g, "");  
		    }  
		    
</script>

</body>
</html>
