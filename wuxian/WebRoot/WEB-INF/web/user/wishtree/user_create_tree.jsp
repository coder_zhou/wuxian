<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title> ${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <link href="/resources/index/css/user_create_tree.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>
    <script src="/resources/index/js/user_create_tree.js"></script>
    
</head>
<body>

<!-- 礼佛管理-内容-->
<div>
    <div class="content_btn">

        <!-- 心愿树管理按钮-->
        <a href="/wish/toAddWish" target="mainFrame">
            <div class="user_new_pro">
                <img src="/resources/index/images/user_add.png">
                <div>创建许愿树</div>
            </div>
        </a>


        <div class="user_muyuan_table">
            <img src="/resources/index/images/user_content_tog.png">
            <div>我的许愿树</div>
        </div>
    </div>

    <div class="tree_content">
        <!-- 我的许愿树-->
        <div class="my_tree">
            <!-- 许愿树 1-->
            <c:choose>
            <c:when test="${not empty varList }">
            <c:forEach items="${varList}" var="var" varStatus="vs">
            
                 <div class="each_tree">
                <div class="each_tree_img">
                  <img src="/goods/${var.private_wish_tree_img }">
                    <div class="text"></div>
                    <div class="text_infor">
                        <div>
                            <img src="/resources/index/images/create_tree_see.png">
                            <div class="text_infor_num">浏览  ${var.private_wish_tree_clicks}</div>
                        </div>
                        <div>
                            <img src="/resources/index/images/create_tree_love.png">
                            <div class="text_infor_num">许愿  ${var.private_wish_count}</div>
                        </div>
                    </div>
                </div>
                <div class="tree_name">${var.private_wish_tree_name }</div>
                <div class="tree_create_user"></div>
                <div class="tree_create_time">
                  <c:set target="${myDate}" property="time" value="${var.private_wish_tree_add_time*1000}"/> 
                    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
                </div>
                <div class="tree_create_button">
                    <div class="tree_create_btn"><a target="_blank" href="/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id=${var.private_wish_tree_id}">管理</a></div>
                    <div class="tree_create_btn"><a href="/wish/del?id=${var.private_wish_tree_id}">删除</a></div>
                </div>


            </div>
            </c:forEach>
            
            </c:when>
            </c:choose>
        </div>
    </div>

    <div class="my_wishing">
        <div class="my_wishing_list">
            <img src="/resources/index/images/user_content_tog.png">
            <div>我的愿望</div>
        </div>

        <div class="my_wishing_list_content">
            <ul class="list_content_first">
                <li class="list_first">信物</li>
                <li class="list_second">愿望标题</li>
                <li class="list_third">创建人</li>
                <li class="list_four">许愿树名称</li>
                <li class="list_five">时间</li>
                <li class="list_six">操作</li>
            </ul>
            <c:choose>
            <c:when test="${not empty tm }">
            <c:forEach items="${tm}" var="tm" varStatus="vs">
               <!-- 内容第一行-->
            <ul class="list_content">
                <li class="list_first"><img src="/goods/${tm.materials_small_img}" width="30" height="30"></li>
                <li class="list_second">${tm.wish_title }<span>${tm.wish_type==1?"[公]":"[私]" }</span></li>
                <li class="list_third">${sessionScope.USER_NAME}</li>
                <li class="list_four"><c:if test="${tm.wish_type==2}">${tm.private_wish_tree_name}</c:if>
                <c:if test="${tm.wish_type==1}">公共许愿树</c:if>                                    
                </li>
                <li class="list_five"> <c:set target="${myDate}" property="time" value="${tm.wish_tree_materials_add_time*1000}"/> 
                <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> </li>
                <li class="list_six">
                    <img src="/resources/index/images/my_wish_open.png" onclick="edit('${tm.wish_tree_materials_id}');">
                    <img src="/resources/index/images/my_wish_delete.png" onclick="del('${tm.wish_tree_materials_id}');">
                </li>
            </ul>
            </c:forEach>
            </c:when>
            </c:choose>
            
         
        
            <form id="Form" name="Form"></form>
	<ff:page mhFrom="Form" showReSize="flase" counts="false"  field="page" onlyOneShow="false" showListNo="false" action="wish/getUserWish"/>

        </div>

    </div>

</div>
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	
	
	});
	
//删除
function del(Id){
	if(confirm("确认要删除吗？")){
		var url = "/wish/delWish?id="+Id+"&tm="+new Date().getTime();
		$.get(url,function(data){
				window.location.reload();
		});
	}
	
	
}	


//修改
function edit(Id){
	 var diag = new top.Dialog();
	 diag.Drag=true;
	 diag.Title ="编辑";
	 diag.URL = '/wish/goEditPWD?id='+Id;
	 diag.Width = 450;
	 diag.Height = 355;
	 diag.CancelEvent = function(){ //关闭事件
		diag.close();
	 };
	 diag.show();
}
$(function(){
	
	var ll='${pd.toWish}';
	
	if(ll!=''){
		window.open('${pd.toWish}');
	}
	
});
</script>
</body>
</html>

