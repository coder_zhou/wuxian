<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="/resources/index/css/create_wish_tree.css" rel="stylesheet">
        <script src="/resources/index/js/jquery-1.8.2.js"></script>
        	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
</head>
<body>
<!--创建许愿树-->
<div>
<form action="/wish/addWish" method="post" id="Form">
    <!-- 流程部分-->
    <div class="shortcut_create">
        <div class="shortcut_logo"></div>
        <div>
            <a href="/wish/getUserWish" target="mainFrame">许愿树</a> >
            <a href="#">创建许愿树</a>
        </div>
    </div>

    <!-- 创建-->
    <div class="create">
        <img src="/resources/index/images/write_cemetery_infor.png" class="select_formwork_logo">
        <div class="select_formwork_title">创建许愿树</div>
    </div>

    <div class="create_content">
        <div class="space_name">
            <div class="space_name_title">空间名称</div>
            <input type="text" name="privateWishTreeName" id="privateWishTreeName" maxlength="4">
            <img src="/resources/index/images/corpus_must_write.png">
        </div>
        <div class="space_pwd">
            <div class="space_pwd_title">查看密码</div>
            <input type="text" name="privateWishTreePassword" maxlength="10" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
                    onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}">
            <span>（选填）</span>
        </div>
        <div class="space_content">
            <div class="space_content_title">空间留言</div>
            <textarea name="privateWishTreeInfo" onkeyup="checkLen(this);"></textarea>
        </div>
            <div class="spac_prompt">您还可以输入 <span id="count">200</span> 个文字</div> 
    </div>
    <div class="space_btn">
        <div class="affirm" onclick="sub();">确认创建</div>
        <div class="reset" onclick="res();">重置</div>
    </div>
    </form>
</div>
</body>
</html>
<script type="text/javascript">

function sub(){
	if($("#privateWishTreeName").val()==""){
		$("#privateWishTreeName").tips({
			side:2,
            msg:'请输入名称',
            bg:'#AE81FF',
            time:3
	    });
		$("#privateWishTreeName").focus();
		return false;
	}
	$("#Form").submit();
}


$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
function checkLen(obj) {  

	var maxChars = 200;//最多字符数  

	if (obj.value.length > maxChars)  obj.value = obj.value.substring(0,maxChars);  

	var curr = maxChars - obj.value.length;  

	document.getElementById("count").innerHTML = curr.toString(); 

	} 
	function res(){
		

		document.getElementById("Form").reset(); 
	}
</script>