 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
<link href="/resources/css/user_message.css" rel="stylesheet">
<script src="/resources/index/js/jquery-1.8.2.js"></script>
<script src="/resources/js/user_message.js"></script>
</head>
<body>
 <div>
			<div class="shortcut_create">
		        <div class="shortcut_logo"></div>
		        <div><a href="user_set.html">个人设置</a></div>
		    </div>
		    <div class="message_container">
		        <div class="mess_title">
		            <div><img src="<%=path%>/resources/images/message/write_cemetery_infor.png"></div>
		            <div>消息列表</div>
		        </div>
		        <ul class="mess_list_title">
		            <li class="mess_list_1">标题</li>
		            <li class="mess_list_2">内容</li>
		            <li class="mess_list_3">已读/未读</li>
		            <li class="mess_list_4">时间</li>
		        </ul>
		
		      <c:if test="${not empty list}">
				<c:forEach items="${list}" var="map" varStatus="vs">
 		       <ul class="friend_ask">
<!--             //消息类型  0追思留言    1.好友申请消息  2.关联账号消息提醒  3.关联墓园消息提醒  4.管理权限消息提醒   5.网站升级改版进行中  6.特殊祭日消息提醒   7.还愿消息提醒  9.取消关联账号消息提醒    10.取消关联墓园消息提醒-->
                    <li class="mess_list_1">
                        <c:if test="${map.msg_type eq '1'}"><div><img src="<%=path%>/resources/images/message/user_message_sq.png" /></div><span>好友申请消息</span></c:if>
                        <c:if test="${map.msg_type eq '2'}"><div><img src="<%=path%>/resources/images/message/user_message_glzh.png" /></div><span>关联账号消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '3'}"><div><img src="<%=path%>/resources/images/message/user_message_glmy.png" /></div><span>关联墓园消息提醒 </span></c:if>
                        <c:if test="${map.msg_type eq '4'}"><div><img src="<%=path%>/resources/images/message/user_message_manage.png" /></div><span>管理权限消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '5'}"><div><img src="<%=path%>/resources/images/message/user_message_infor.png" /></div><span>系统消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '6'}"><div><img src="<%=path%>/resources/images/message/user_message_tsjr.png" /></div><span>特殊祭日消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '7'}"><div><img src="<%=path%>/resources/images/message/user_message_hytx.png" /></div><span>还愿消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '9'}"><div><img src="<%=path%>/resources/images/message/user_message_glzh.png" /></div><span>加入宗祠消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '9'}"><div><img src="<%=path%>/resources/images/message/user_message_glzh.png" /></div><span>取消关联账号消息提醒</span></c:if>
                        <c:if test="${map.msg_type eq '10'}"><div><img src="<%=path%>/resources/images/message/user_message_glmy.png" /></div><span>取消关联墓园消息提醒 </span></c:if>
                        <c:if test="${map.msg_type eq '11'}"><div><img src="<%=path%>/resources/images/message/user_message_glmy.png" /></div><span>取消关联宗祠消息提醒 </span></c:if>
                    </li>
                    <li class="mess_list_2"  >
                        <a href="javascript:void(0);" onclick="showWindow(this,'${map.msg_url}','${map.msg_id}','${map.content_id}','${map.msg_type}');">${map.msg_title}</a>
                    </li>
                    <li class="mess_list_3"><c:if test="${map.msg_statue eq '0'}">未读</c:if><c:if test="${map.msg_statue ne '0'}">已读</c:if></li>
                    <li class="mess_list_4">
                        <c:set target="${myDate}" property="time" value="${map.addtime*1000}"/> 
                        <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> 
                    </li>
                </ul>

</c:forEach>
</c:if>
</div>
		<div class="number_page_bottomx">
              <form name="ff" id="ff" method="post" ></form>
            <ff:page mhFrom="ff" showReSize="false" counts="false"  field="page" onlyOneShow="false" showListNo="false" action="webmsg/unreadMsgList" />
        </div>		    
		    
	<form id="dd" name="dd" method="post">
	    <input type="hidden" id="msg_url" name="msg_url" value=''/>
	    <input type="hidden" id="msg_id" name="msg_id" value='' />
	</form>	    
		    
		    <!-- 消息详细内容-->
    <div class="message_bg  bg"></div>
    <div class="message_content">
        <div class="window_title">消 息</div>
        <img src="<%=path%>/resources/images/message/close_window.png" class="message_window_close  window_close">
        <div class="message_window_content">
            <div class="message_kind">
                <div class="kind_img"><img src="<%=path%>/resources/images/message/close_window.png" id="kind_img_iframe"></div>
                <div class="kind_title"> </div>
            </div>
            <div class="message_time" id="message_time"> </div>
            <div class="message_infor">
                <div style="word-break:break-all;" class="infor_cont"  id="infor_cont"> </div>
                <div class="infor_btn" id="infor_btn">
                    <div class="agree" id="agree_div" onclick="submit();">同&nbsp;&nbsp;意</div>
                    <div class="cancel"  onclick="cancel();">取&nbsp;&nbsp;消</div>
                </div>
            </div>
        </div>
    </div>
		    
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
function showWindow(obj,msg_url,msg_id,content_id,msg_type){
     $(obj).parents("ul").find("li:eq(2)").html("已读");
     if(content_id=="1"){
         $("#infor_btn").css("display","none");  
         $("#msg_url").val('');
         $("#msg_id").val('');
     }else{
	       if(msg_type=="1" || msg_type=="2" || msg_type=="3" ){  //判断是否有确认按钮
	            $("#infor_btn").css("display","");  
	            $("#msg_url").val(msg_url);
	            $("#msg_id").val(msg_id);
	       }else{
	            $("#infor_btn").css("display","none");  
	            $("#msg_url").val('');
	            $("#msg_id").val('');
	       }
     }
     $(obj).parents("ul").css({backgroundColor:"#f1f8fe"}).siblings('ul').css({backgroundColor:"#fff"});
     var src = $(obj).parents("ul").find("img").attr("src") ;
     $("#kind_img_iframe").attr("src",src)
     $(".kind_title").html($(obj).parents("ul").find("span").html());
     var time = $(obj).parents("ul").find("li:eq(3)").html(); 
     $("#message_time").html($.trim(time));
     $("#infor_cont").html($(obj).parents("ul").find("a").html());
     $(".message_bg,.message_content").css({display:"block"});
     var url1 = "<%=path%>/appPushMessage/updateMsgStatue";
     $.post(url1,{msg_id:msg_id},function(){},'json');
}
//确认
function submit(){
   var url = $("#msg_url").val();
   var msg_id = $("#msg_id").val();
   $.post(url,{msg_id:msg_id},function(result){
   	    var url1 = "<%=path%>/appPushMessage/updateMsgContentId";
	    $.post(url1,{msg_id:msg_id},function(){},'json');
        if(result.flag==1){
	        alert(result.msg);
	        $(".message_bg,.message_content").css({display:"none"});
        }else{
            alert(result.msg);
        }
   },'json');
}
//取消
function cancel(){
    var url = "<%=path%>/appPushMessage/updateMsgContentId";
    $.post(url,function(){
	    $(".message_bg,.message_content").css({display:"none"});
    },'json');
}
</script>
          
</div>
</body>
<script type="text/javascript">
function createCemetery(){
	$("#chuangjian").hide();
	$("#changgui").show();
	$("#kuaijie").show();
}
function del(){
	if(!confirm("确认要删除？")){
	window.event.returnValue = false;
	}
	} 
</script>
</html>
