<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="/resources/js/sockjs.js"></script>
<script type="text/javascript" src="/resources/js/stomp.js"></script>
        <script>
            var websocket;
            if ('WebSocket' in window) {
                websocket = new WebSocket("ws://localhost//webSocketServer");
            } else if ('MozWebSocket' in window) {
                websocket = new MozWebSocket("ws://localhost//webSocketServer");
            } 
            //    else {
            //    websocket = new SockJS("http://localhost/sockjs/webSocketServer");
           // }
            websocket.onopen = function (evnt) {
            };
            websocket.onmessage = function (evnt) {
                  $(".user_infor_message").attr("src","/resources/index/images/user_infor_message_choose.png");
                  $("#information_warn").css("display","block");
//              alert(evnt+"--"+evnt.data);
            };
            websocket.onerror = function (evnt) {
            };
            websocket.onclose = function (evnt) {
            }
            function changeImg(){
            	$(".user_infor_message").attr("src","/resources/index/images/user_infor_message.png");
            	$("#information_warn").css("display","none");
            }
        </script>
        
        
       
        
        
    <div class="user_infor">
          <c:choose>
<c:when test="${not empty   user.userInfo.userHeadPhoto}">
 <img alt="${user.userInfo.userName}" src="${user.userInfo.userHeadPhoto}">
</c:when>
<c:otherwise>
 <img src="/resources/index/images/user_bg_head_photo.png">
</c:otherwise>
</c:choose>
        <div class="user_infor_content">
            <div class="user_infor_name">
                <span>${user.userAccount}</span>
                <!-- 用户名旁的消息框-->
                <a target="mainFrame" href="/webmsg/unreadMsgList" onclick="changeImg();" class="message_img">
               
                	<img src="/resources/index/images/user_infor_message.png" class="user_infor_message" >
                	<!-- 消息提醒的红色点-->
                	<img src="/resources/images/user_bg_infor_warn.png" class="information_warn" id="information_warn">
                </a>
                
               
                <a href="/webusersettiongs/toUserSet" target="mainFrame"><img src="/resources/images/user_bg_set.png"  class="user_infor_write"></a>

                <a href="/logout"><img src="/resources/index/images/user_infor_switch.png" class="user_infor_switch"></a>
            </div>

            <!-- 福币积分值-->
            <div class="user_infor_back">
                <ul>
                    <li>福&nbsp;&nbsp;币</li>
                    <li class="number">${um.user_fb }</li>
                    <li class="btn"><a href="/order/pay" target="_blank">充值</a></li>
                </ul>
                <ul>
                    <li>积&nbsp;&nbsp;分</li>
                    <li class="number">${user.userWealth.userIntegral }</li>
                    <li class="btn"><a href="#">兑换</a></li>
                </ul>
                <ul class="user_infor_hyjb">
                    <li>会员级别</li>
                    <li><img src="/resources/index/images/user_infor_dengji.png">VIP${user.userWealth.userVipLevel }</li>
                    <li></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- 选项分类-->
    <div class="user_menu" id="user_menu">
        <ul class="tab_menu tab_menu_img">
            <li><a href="/cemetery/cemeteryList" target="mainFrame"><img src="/resources/index/images/srmy_muyuan_pre.png" class="img_active"></a></li>
            <li><a href="/UserAncestral/list" target="mainFrame"><img src="/resources/index/images/srmy_temple_n.png" class="img_active"></a></li>
            <li><a href="/fo/MyFo" target="mainFrame"><img src="/resources/index/images/srmy_buddha_n.png" class="img_active"></a></li>
            <li><a href="wish/getUserWish" target="mainFrame"><img src="/resources/index/images/srmy_tree_n.png" class="img_active"></a></li>
            <li><a href="/cemetery/getAtricle" target="mainFrame"><img src="/resources/index/images/srmy_article_n.png" class="img_active"></a></li>
            <li><a href="/fbManage" target="mainFrame"><img src="/resources/index/images/srmy_currency_n.png" class="img_active"></a></li>
            <li><a href="/webmsg/unreadMsgList" target="mainFrame"><img src="/resources/index/images/srmy_infor_n.png" class="img_active"></a></li>
            <li><a href="user_log_manage.html" target="iframe_content"><img src="/resources/index/images/srmy_log_n.png" class="img_active"></a></li>
            <li><a href="/myfriend" target="mainFrame"><img src="/resources/index/images/srmy_friends_n.png" class="img_active"></a></li>
        </ul>
        <ul class="tab_menu tab_title">
            <li><a href="/cemetery/cemeteryList" target="mainFrame">墓园管理</a></li>
            <li><a href="/UserAncestral/list" target="mainFrame">宗祠管理</a></li>
            <li><a href="/fo/MyFo" target="mainFrame">礼佛管理</a></li>
            <li><a href="wish/getUserWish" target="mainFrame">心 愿 树</a></li>
            <li><a href="/cemetery/getAtricle" target="mainFrame">文章管理</a></li>
            <li><a href="/fbManage" target="mainFrame">福币管理</a></li>
            <li><a href="/webmsg/unreadMsgList" target="mainFrame">信息管理</a></li>
            <li><a href="#" target="mainFrame">日志管理</a></li>
            <li><a href="/myfriend" target="mainFrame">我的好友</a></li>
        </ul>
    </div>