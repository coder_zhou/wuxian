<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>全球祭祀祈福网-个人后台页-我的好友</title>
    <link href="/resources/index/css/user_friends_manage.css" rel="stylesheet">
    <script src="/resources/js/jquery-1.9.1.min.js"></script>

</head>
<body>

<!--墓园管理-->
<div>
    <div class="content_btn">

        <!-- 内容标题-->
        <div class="user_fb_table">
            <img src="/resources/images/write_cemetery_infor.png">
            <div>好友列表</div>
        </div>
    </div>

    <div class="container">

         		<c:choose>
         				<c:when test="${ not empty uf}">
         				<c:forEach items="${uf }" var="var" varStatus="vs">
         				        <div>
            <a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }" class="friends_photo"  target="_blank"><img src="${var.user_head_photo }"></a>
            <a href="/webusersettiongs/queryToUserSettiongs?user_myid=${var.user_id }"  class="friends_name" target="_blank">${var.user_nick_name }</a>
        </div>
         				</c:forEach>
         				</c:when>
         				</c:choose>
    </div>


</div>
<script>
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
</script>
</body>
</html>