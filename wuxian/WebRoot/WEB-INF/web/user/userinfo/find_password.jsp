<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>全球祭祀祈福网-登录页-无验证码</title>
    <link href="/resources/css/userinfo/find_password.css" rel="stylesheet">
    <script src="/resources/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
    <!--&lt;!&ndash;[if lt IE 9]>-->
        <!--<script src="/resources/js/html5shiv.min.js"></script>-->
        <!--<script src="/resources/js/respond.min.js"></script>-->
    <!--<![endif]&ndash;&gt;-->

    <script type="text/javascript">
        if( !('placeholder' in document.createElement('input')) ){

            $('input[placeholder],textarea[placeholder]').each(function(){
                var that = $(this),
                        text= that.attr('placeholder');
                if(that.val()===""){
                    that.val(text).addClass('placeholder');
                }
                that.focus(function(){
                    if(that.val()===text){
                        that.val("").removeClass('placeholder');
                    }
                })
                        .blur(function(){
                            if(that.val()===""){
                                that.val(text).addClass('placeholder');
                            }
                        })
                        .closest('form').submit(function(){
                            if(that.val() === text){
                                that.val('');
                            }
                        });
            });
        }
        $(function(){
//          登录注册模块的Tab页
          $(".denglu_tab_menu .denglu_yhdl").click(function(){
              $(this).css({borderRight:"1px solid #ccc",borderTop:"1px solid #ccc",borderBottom:"1px solid white",backgroundColor:"white",color:"#555555"})
                  .siblings().css({border:"none",backgroundColor:"#7bb7c4",color:"white"});
          });
          $(".denglu_tab_menu .denglu_mfzc").click(function(){
              $(this).css({marginTop:"0",borderLeft:"1px solid #ccc",borderRight:"1px solid #ccc",borderBottom:"1px solid white",backgroundColor:"white",color:"#555555"})
                  .siblings().css({border:"none",borderBottom:"1px solid #ccc",backgroundColor:"#7bb7c4",color:"white"});
          });
          $(".denglu_tab_menu>li").click(function(){
              var v2=$(this).index();
              $(".denglu_tab_content>div").eq(v2).removeClass("denglu_content_hide").siblings().addClass("denglu_content_hide");
          });

//          去除input点击时的边框
          $("input").focus(function(){
              $(this).css({outline:"none"});
          });






      });
        
    </script>

</head>
<body>
<div class="main">
    <!--菜单部分(头部)-->
    <div class="header">
        <div class="return_btn"><a href="/login"> < 返回登录</a></div>
        <ul>
            <li><a href="#">首页</a></li>
            <li><a href="#">墓园</a></li>
            <li><a href="#">宗祠</a></li>
            <li><a href="#">礼佛</a></li>
            <li><a href="#">心愿</a></li>
            <li class="help"><a href="#">帮助</a></li>
        </ul>
    </div>

    <!--蓝色logo标部分-->
    <div class="banner"><img src="/resources/images/denglu_logo_big.png" class="logo"></div>
    <div class="mian_bg"><img src="/resources/images/main_bg.png"></div>

    <div class="input_tel_content">
        <div class="input_tel"><input type="text"  name="phone" id="em"  placeholder="请输入与账号绑定的手机号"></div>
        <div class="input_tel_next"   onclick="check();">下 一 步</div>
    </div>

    <div class="input_password">
        <div class="input_odd_pdw"><input type="password"  name="password"  id="password"   placeholder="请输入新密码"></div>
        <div class="input_new_pdw"><input type="password"  name="password2" id="password2"  placeholder="确认密码"></div>
        <div class="send_message">验证码已发送至手机<span id="pnb"></span></div>
        <div class="input_yzm">
            <div><input type="text"   id="code" name="code"  placeholder="请输入验证码"></div>
            <div class="send_again_btn">重发验证码(18)</div>
        </div>
        <div class="finish"  onclick="sub();">完成</div>
    </div>

    <div class="finish_bg"></div>
    <div class="finish_content">
        <div class="finish_img"><img src="/resources/images/finish_img.png"></div>
        <div class="finish_wn_zi">恭喜您，重置密码成功</div>
        <div class="finish_btn"><a href="/login">确定</a></div>
    </div>


    <!--底部-->
    <div class="footer">
        <div>如果没有绑定手机或因其他原因无法找回密码</div>
        <div>请联系客服：<span>400-0240-535</span></div>
    </div>




</div>
<script>

function  check(){
	
	if($("#em").val()==""){

		$("#em").tips({
			side:2,
            msg:'请输入手机号',
            bg:'#AE81FF',
            time:3
        });
		
		$("#em").focus();
		return false;
	}else if(!checkMobile()){       
		$("#em").tips({
			side:2,
            msg:'请输入正确的手机号码',
            bg:'#AE81FF',
            time:3
        });
		
		$("#em").focus();
		return false;
	}
	 else{
	var  a=$("#em").val();
var url = "/getPhone?phone="+a;
$.get(url,function(data){
 if(data=="success"){
     $(".input_tel_content").css({display:"none"});
     $(".input_password").css({display:"block"});
	 alert("已发送验证码");
	 $("#pnb").html(a);
 }else{
	 alert("该手机号未绑定任何账号");
 }
}
);}
}

//验证电话  
function checkMobile() {  
    var s=$("#em").val();
    if (trim(s) != "") {  
        var regu = /^[1][3-9][0-9]{9}$/;  
        var re = new RegExp(regu);  
        if (re.test(s)) {  
            return true;  
        } else {  
            return false;  
        }  
    }  
} 

function trim(str) { //删除左右两端的空格  
    return str.replace(/(^\s*)|(\s*$)/g, "");  
}  

function sub(){
	if($("#password").val()==""){

		$("#password").tips({
			side:2,
            msg:'密码不得为空',
            bg:'#AE81FF',
            time:3
        });
		
		$("#password").focus();
		return false;
	}else if($("#password").val().length<6){
		$("#password").tips({
			side:2,
            msg:'密碼太短了',
            bg:'#AE81FF',
            time:3
        });
		
		$("#password").focus();
		return false;
	}else 
	if($("#password2").val()==""){

		$("#password2").tips({
			side:2,
            msg:'请输入确认密码',
            bg:'#AE81FF',
            time:3
        });
		
		$("#password2").focus();
		return false;
	}else 
	if($("#password2").val()!=$("#password2").val()){

		$("#password2").tips({
			side:2,
            msg:'两次密码不一致',
            bg:'#AE81FF',
            time:3
        });
		
		$("#password2").focus();
		return false;
	}else 
	if($("#code").val()==""){

		$("#code").tips({
			side:1,
            msg:'验证码不得为空',
            bg:'#AE81FF',
            time:3
        });

		$("#code").focus();
		return false;
	}else{
		     var phone=$("#em").val();
		     var password=$("#password2").val();
		     var code =$("#code").val();
		     var url = "/resetPassword?phone="+phone+"&password="+password+"&code="+code;
		    $.get(url,function(data){
		     if(data=="success"){
		      //document.location.reload();
	              $(".finish_bg,.finish_content").css({display:"block"});
	              
		     }else{
		    		$("#code").tips({
		    			side:1,
		                msg:'验证码错误',
		                bg:'#AE81FF',
		                time:3
		            });
		     }
		    });
		
	}
	
}

</script>
</body>
</html>