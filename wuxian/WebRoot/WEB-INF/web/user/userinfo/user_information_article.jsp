 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<title>全球祭祀祈福网站-用户详细信息</title>
<link href="/resources/css/userinfo/user_information.css" rel="stylesheet">
<link href="/resources/css/userinfo/jsCarousel-2.0.0.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/resources/js/jquery-1.8.2.js"></script>
<script src="/resources/js/userinfo/jsCarousel-2.0.0.js" type="text/javascript"></script>
<script src="/resources/js/userinfo/user_information.js"></script>
<script type="text/javascript">
        $(document).ready(function() {

            $('#carouselh').jsCarousel({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

            $('#carouselh2').jsCarousel2({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

            $('#carouselh3').jsCarousel3({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

        });
        function judgeState(article_state,isAdmin,user_id,article_type){
            if(article_state==2 && )
        }
    </script>
</head>
<body> 
           <c:forEach items="${article_list}" var="obj" varStatus="status">
                <!--  "article_state":int,  //文章状态 1完全公开 2登录用户可见   3完全保密      -->
	            
                            <ul class="article_ul">
				                <li class="article_list_1">
				                   <c:if test="${obj.article_type eq '1'}"><img src="/resources/images/userinfo/user_infor_wz.png"></c:if>
				                   <c:if test="${obj.article_type ne '1'}"><img src="/resources/images/userinfo/user_infor_ly.png"></c:if>
				                </li>
				                <li class="article_list_2">
				                    <c:if test="${obj.article_type eq '1'}" >
				                            <c:choose>
				                               <c:when test="${sessionScope.USER_ID ==null && obj.article_state==2  && isAdmin eq '0'}">
							                    <a href="javascript:void(0);" onclick="alert('该文集详细只有登录用户可见!')">${obj.article_name}</a> 
							                   </c:when>
							                   <c:when test="${sessionScope.USER_ID != user_id && obj.article_state==3 &&  isAdmin eq '0'}" >
							                      <a href="javascript:void(0);" onclick="alert('该文集详细不可见!')">${obj.article_name}</a></li>
							                   </c:when>
							                   <c:otherwise>
							                        <a href="/cemetery/viewArticle?artId=${obj._id}"   target="_blank">${obj.article_name }</a> 
							                   </c:otherwise>
						                     </c:choose>
				                     </c:if>
				                     <c:if test="${obj.article_type ne '1'}">
				                             ${obj.article_name }
				                     </c:if>
				                </li>
				                <li class="article_list_3">
									<c:set target="${myDate}" property="time" value="${obj.article_add_time*1000}"/> 
			                        <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> 
								</li>
				                <li class="article_list_4">(${obj.article_praise}/${obj.article_clicks})</li>
				            </ul>
           </c:forEach>
            <div class="pagination" style="margin: 0px;">
		  	  <form id="ff" name="ff" method="post"></form>
		  	  <ff:page mhFrom="ff" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="webusersettiongs/toArticleIframe?user_id_mongo=${user_id_mongo}"/>
			</div>
</body>
</html>
