 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<title>全球祭祀祈福网站-用户详细信息</title>
<link href="/resources/css/userinfo/user_information.css" rel="stylesheet">
<link href="/resources/css/userinfo/jsCarousel-2.0.0.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/user_message.css" rel="stylesheet">
<script type="text/javascript" src="/resources/js/jquery-1.8.2.js"></script>
<script src="/resources/js/userinfo/jsCarousel-2.0.0.js" type="text/javascript"></script>
<script src="/resources/js/userinfo/user_information.js"></script>
<script type="text/javascript">
        $(document).ready(function() {

            $('#carouselh').jsCarousel({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

            $('#carouselh2').jsCarousel2({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

            $('#carouselh3').jsCarousel3({
                autoscroll: false,
                circular: true,
                masked: false,
                itemstodisplay: 4,
                orientation: 'h'
            });

        });
        //跳转墓园
        function toCemetery(id){
            document.gg.action = "/webCemetery/queryCemeteryInfoForBz?cemetery_id="+id;
            document.gg.target = "_blank";
            document.gg.submit();
        }
        //跳转宗祠
		function toAnscetral(id){
		    document.gg.action = "/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id="+id;
            document.gg.target = "_blank";
            document.gg.submit();
		}
		//跳转礼佛
		function toFo(id){
		   document.gg.action = "/webLifo/queryFoPersonalWorshipForBz?personal_worship_id="+id;
           document.gg.target = "_blank";
           document.gg.submit();
		}
		//跳转许愿
		function toWishTree(){
		    var id = $("#private_wish_tree_id").val();
		    alert(id)
			var wishtree_pwd_input = $("#wishtree_pwd_input").val();
		    var private_wish_tree_password = $("#private_wish_tree_password").val();
		    if(private_wish_tree_password==wishtree_pwd_input){
			    document.gg.action = "/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id="+id;
            	document.gg.target = "_blank";
            	document.gg.submit();
		   }else{
		      $("#wishtree_pwd_input").val('');
		      alert("密码错误!");
		   }
		    
		}
		function showWindow(id,pwd){
		     $("#wishtree_pwd_input").val('');
			 $("#private_wish_tree_password").val('');
		     if(pwd==""){
		        document.gg.action = "/webWishingTree/queryprivateWishTreeForBz?private_wish_tree_id="+id;
            	document.gg.target = "_blank";
            	document.gg.submit();
		     }else{
		         $("#private_wish_tree_id").val(id);
		         $("#private_wish_tree_password").val(pwd);
			     $(".message_bg,.message_content").css({display:"block"});
		     }
		}
		//取消
		function cancel(){
		    $(".message_bg,.message_content").css({display:"none"});
		}
    </script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="infor_main">
     <form id="gg" name="gg" method="post"></form>
     <input type="hidden" id="private_wish_tree_password" value=""/>
     <input type="hidden" id="private_wish_tree_id" value=""/>
    <!-- 头部-->
    <jsp:include page="/jsp/common/head.jsp"></jsp:include>
<!--     <div class="header"> -->
<!--         <div class="logo"><a href="index.html"></a><img src="/resources/images/userinfo/logo.png"></div> -->
<!--         <div class="menu"> -->
<!--             <ul> -->
<!--                 <li class="menu_active"><a href="#">首 页</a></li> -->
<!--                 <li><a href="#">私人墓园</a></li> -->
<!--                 <li><a href="#">宗祠园区</a></li> -->
<!--                 <li><a href="#">礼佛园区</a></li> -->
<!--                 <li><a href="#">心愿树</a></li> -->
<!--                 <li><a href="#">文字信息</a></li> -->
<!--                 <li><a href="#">使用帮助</a></li> -->
<!--             </ul> -->
            <!-- 登录注册按钮-->
<!--             <div class="loading"><a href="#">登录/注册</a></div> -->
<!--         </div> -->
<!--     </div> -->

    <div class="infor_content">
        <!-- 个人信息-->
        <div class="personal_infor">
            <div class="per_photo"><img src="${user.user_head_photo}"></div>
            <div class="per_xx_infor">
                <div class="per_name">${user.user_nick_name}</div>
                <div class="user_id">用&nbsp;&nbsp;户&nbsp;ID：${user.user_id}</div>
                <div class="user_name_addr">
                    <div>真实姓名：${user.user_name}</div>
                    <div>籍&nbsp;&nbsp;贯：${user.user_country}-${user.user_city}</div>
                </div>
            </div>
            <div class="infor_right">
                <div class="right_add_btn">加好友</div>
                <div class="right_er_wei_ma">
                    <img src="/resources/images/userinfo/infor_ewm.png">
                    <img src="${user.user_qrcode}" class="infor_ewm_big">
                </div>
            </div>
            <div class="user_per_jianjie">${user.user_underwrite}</div>
        </div>

        <!-- 私人墓园-->
        <div class="srmy_cont">
            <div class="srmy_title"><div class="srmy_blue"></div>私 人 墓 园</div>
            <div class="srmy_container" id="carouselh">
                <c:forEach items="${cemetery_list}" var="obj" varStatus="status">
                
                  <!-- 墓园是否公开0完全公开1登陆用户可见2仅自己可见     -->
	                   <c:if test="${(sessionScope.USER_ID eq user_id && obj.cemetery_look==2)|| isAdmin eq '1'}">
	                        <div onclick="toCemetery('${obj.cemetery_id}');">
			                    <img src="${obj.cemetery_img}">
			                    <div class="my_img_infor">
			                        <div class="my_name">${obj.cemetery_name} </div>
			                        <div class="my_num">园号:${obj.cemetery_number}</div>
			                    </div>
			                </div>
	                   </c:if>
                       <c:if test="${(sessionScope.USER_ID !=null && obj.cemetery_look==1)|| isAdmin eq '1'}">
                            <div  onclick="toCemetery('${obj.cemetery_id}');">
			                    <img src="${obj.cemetery_img}">
			                    <div class="my_img_infor">
			                        <div class="my_name">${obj.cemetery_name} </div>
			                        <div class="my_num">园号:${obj.cemetery_number}</div>
			                    </div>
			                </div>
                       </c:if>
                       <c:if test="${obj.cemetery_look==0}">
                            <div  onclick="toCemetery('${obj.cemetery_id}');">
			                    <img src="${obj.cemetery_img}">
			                    <div class="my_img_infor">
			                        <div class="my_name">${obj.cemetery_name} </div>
			                        <div class="my_num">园号:${obj.cemetery_number}</div>
			                    </div>
			                </div>
                       </c:if>
                </c:forEach>
            </div>
        </div>

        <!-- 宗祠族谱-->
        <div class="zczp">
            <div class="zczp_title"><div class="srmy_blue"></div>宗 祠 族 谱</div>
			<c:forEach items="${ancestral_list}" var="obj" varStatus="status">
            <div class="zczp_cont" onclick="toAnscetral('${obj.ancestral_myid}');">
                <img src="${obj.ancestral_hall_external_img}" class="zczp_img">
                <div class="zczp_right_cont">
                    <div class="zczp_name">
                        <div>${obj.ancestral_hall_surname}</div>
                        <div><img src="/resources/images/userinfo/zczp_logo.png"></div>
                    </div>
                    <div class="zczp_detail_infor">
                        <div class="zczp_detail_left">
                            <div>宗&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;谱：${obj.ancestral_hall_genealogy}</div>
                            <div>所在地区：${obj.ancestral_hall_country} - ${obj.ancestral_hall_city}</div>
                        </div>

                        <div class="zczp_detail_center">
                            <div>堂&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号：${obj.ancestral_hall_family}</div>
                            <div>创建时间：<c:set target="${myDate}" property="time" value="${obj.ancestral_hall_addtime*1000}"/> 
                                        <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> 
                            </div>
                        </div>

                        <div class="zczp_detail_right">
                            <div>宗&nbsp;祠&nbsp;号：${obj.ancestral_myid }</div>
                            <div>代&nbsp;族&nbsp;长：${obj.ancestral_user_nick_name}</div>
                        </div>
                    </div>
                </div>
            </div>
            </c:forEach>
        </div>

        <!-- 礼佛园区-->
        <div class="li_fo_area">
            <div class="srmy_title"><div class="srmy_blue"></div>礼 佛 园 区</div>
            <div class="lf_container" id="carouselh2">
                <c:forEach items="${worship_list}" var="obj" varStatus="status">
	                <div onclick="toFo('${obj.personal_worship_id}');">
	                    <img src="${obj.fo_material_url}">
	                    <div class="lfyq_img_infor">${obj.personal_worship_fo_name}</div>
	                </div>
                </c:forEach>
             </div>
		</div>
        <!-- 许愿树-->
        <div class="wish_tree">
            <div class="srmy_title"><div class="srmy_blue"></div>许 愿 树</div>
            <div class="wish_tree_container" id="carouselh3">
                <c:forEach items="${wishtree_list}" var="obj" varStatus="status">
	                <div onclick="showWindow('${obj.private_wish_tree_id}','${obj.private_wish_tree_password}');">
	                    <img src="${obj.private_wish_tree_img}">
	                    <div class="lfyq_img_infor">${obj.private_wish_tree_name}</div>
	                </div>
                </c:forEach>
            </div>
        </div>

        <!-- 文集追思   超出时  有分页  -->
        <div class="article_miss">
            <div class="article_title"><div class="srmy_blue"></div>文 集 追 思</div>
            <iframe id="article_iframe" src="/webusersettiongs/toArticleIframe?user_id_mongo=${user_id_mongo}"></iframe>

        </div>
</div>
        <!-- 底部-->
        <footer>
            <div class="footer_left">
                <img src="/resources/images/userinfo/sy_yqlj_service.png" class="service_photo">
                <div class="service_tel">
                    <div>
                        <div><img src="/resources/images/userinfo/sy_yqlj_tel.png"></div>
                        <div>400-02490535</div>
                    </div>
                    <div>
                        <div><img src="/resources/images/userinfo/sy_yqlj_message.png"></div>
                        <div>502772160@qq.com</div>
                    </div>
                </div>
            </div>

            <div class="footer_center">
                <div>
                    <ul>
                        <li><a href="#">关于我们</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">客户服务</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">人才招募</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">免责声明</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">版权声明</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">友情链接</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                        <li><a href="#">网站地图</a></li>
                    </ul>
                </div>
                <div class="comp_infor">
                    哈尔滨祈福科技有限公司&nbsp;&nbsp;Copyright 2007-2015&nbsp;&nbsp;All rights reserved&nbsp;&nbsp;黑ICP备07002329号&nbsp;&nbsp;|&nbsp;&nbsp;网站统计
                </div>
            </div>

            <div class="footer_right"><img src="/resources/images/userinfo/sy_yqlj_ewm.png"></div>
        </footer>
    </div>


<!-- 消息详细内容-->
    <div class="message_bg  bg"></div>
    <div class="message_content">
        <div class="window_title">许愿树密码</div>
        <img src="<%=path%>/resources/images/message/close_window.png" class="message_window_close  window_close">
        <div class="message_window_content">
            <div class="message_kind">
                <div class="kind_img"><img src="<%=path%>/resources/images/message/close_window.png" id="kind_img_iframe"></div>
                <div class="kind_title"> </div>
            </div>
            <div class="message_time" id="message_time"> </div>
            <div class="message_infor">
                <div style="word-break:break-all;" class="infor_cont"  id="infor_cont"> 
                    <input type="text" id="wishtree_pwd_input" value="" onkeyup="value=value.replace(/[^\d]/g,'')" >
                </div>
                <div class="infor_btn" id="infor_btn">
                    <div class="agree" id="agree_div" onclick="toWishTree();">确&nbsp;&nbsp;认</div>
                    <div class="cancel"  onclick="cancel();">取&nbsp;&nbsp;消</div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
