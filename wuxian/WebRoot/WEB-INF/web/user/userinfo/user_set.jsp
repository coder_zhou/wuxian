 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="/resources/css/userinfo/user_set.css" rel="stylesheet">
    <link href="/resources/css/cropper.min.css" rel="stylesheet">
    <link href="/resources/css/docs.css" rel="stylesheet">
    <script type="text/javascript" src="/resources/js/jquery-1.8.2.js"></script>
    <script src="/resources/js/cropper.min.js"></script>
    <script src="/resources/js/docs.js"></script>
    <script type="text/javascript" src="/resources/js/jquery/jquery.form.2.36.js"></script>
    <script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
    <script type="text/javascript" src="/resources/js/userinfo/user_set.js"></script>
    <script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
    <style type="text/css">
	      
.change_img_bg{  
    display: none; 
    position: absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: black;
    z-index:999999999;
    -moz-opacity: 0.6;
    opacity:.60;
    filter: alpha(opacity=60);
}
.change_img_content{
    display: none;
    position: absolute;
    top:50%;
    left:50%;
    width:750px;
    margin-left: -375px;
    margin-top: -215px;
    z-index:9999999999;
    overflow: auto;
    border-radius: 3px;
}
.change_img_window_close{
    position: absolute;
    top: 15px;
    right: 15px;
}
.ggtx_window_content{
	width:720px;
	height:400px;
	position: relative;
	 background-color:white;
	 padding: 30px 0 0 30px;
}
	      
	      </style>
    <script  type="text/javascript">
    $(function () {
        var p  = $("#user_province_code option:selected").val() ;
        if(p!=""){
               var obj=$("#user_city_code");
			   var url = "/area/getSon?proId="+p+"&tm="+new Date().getTime();
				$.get(url,function(data){
					if(data.length>0){
						var html = "";
						obj.append("<option value=''>请选择</option>");
						$.each(data,function(i){
						   if(this.areaName==$("#user_city").val()){
						      obj.append("<option value='"+this.areaId+"'  selected>"+this.areaName+"</option>");
						   }else{
			                   obj.append("<option value='"+this.areaId+"'>"+this.areaName+"</option>");
						   }
						});
					}
				},"json");
		}
    });
       //修改密码
       function updateUserPwd(){
           if($("#user_pwd_old").val()==""){
				$("#user_pwd_old").tips({
					side:2,
		            msg:'请输入原始密码',
		            bg:'#AE81FF',
		            time:3
			    });
				$("#user_pwd_old").focus();
				return false;
			}
			if($("#user_pwd_new").val()==""){
				$("#user_pwd_new").tips({
					side:2,
		            msg:'请输入新密码',
		            bg:'#AE81FF',
		            time:3
			    });
				$("#user_pwd_new").focus();
				return false;
			}
			if($("#user_pwd_new_repeat").val()==""){
				$("#user_pwd_new_repeat").tips({
					side:2,
		            msg:'请输入重复密码',
		            bg:'#AE81FF',
		            time:3
			    });
				$("#user_pwd_new_repeat").focus();
				return false;
			}
			if($("#user_pwd_new_repeat").val()!= $("#user_pwd_new").val()){
				$("#user_pwd_new_repeat").tips({
					side:2,
		            msg:'新密码两次输入不一致,请重新输入',
		            bg:'#AE81FF',
		            time:3
			    });
			    $("#")
				$("#user_pwd_new_repeat").focus();
				return false;
			}
			var user_pwd_old = $("#user_pwd_old").val();
            var user_pwd_new = $("#user_pwd_new").val();
            var url = "/webusersettiongs/updateUserPwd";
            $.post(url,{user_pwd_old:user_pwd_old,user_pwd_new:user_pwd_new},function(rs){
                   if(rs.result=="1"){
                       alert(rs.msg);
                       top.document.location.href = "/login";
                   }else{
                       alert(rs.msg);
                   }
            },'json');
       }
       //修改个人信息
       function updateUserInfo(){
       		if($("#user_nick_name").val()==""){
				$("#user_nick_name").tips({
					side:2,
		            msg:'请输入用户昵称',
		            bg:'#AE81FF',
		            time:3
			    });
			    $("#")
				$("#user_nick_name").focus();
				return false;
			}
			if($("#user_province_code option:selected").val()!=""){
			   var value = $("#user_province_code option:selected").html();
			   $("#user_province").val(value);
			}
			if($("#user_city_code option:selected").val()!=""){
			   var value = $("#user_city_code option:selected").html();
			   $("#user_city").val(value);
			}
			document.ff.action = "/webusersettiongs/updateUserInfo";
            document.ff.submit();
// 			$("#ff").ajaxSubmit(function(result){
			      
// 			});
       }
       function test(){
		  var imgStr = $("#imgStr").val();
		  if(imgStr!="" || imgStr!=undefined){
		     console.log(imgStr);
		 	$.ajax({
				type: "POST",
				url: '/picture/savePic',
		    	data: {'imgStr':imgStr},
				dataType:'json',
				cache: false,
				success: function(data){
					$("#ii").attr("src",data.imgUrl);
					$("#user_head_photo").val(data.imgUrl);
					$("#content").hide();
				}
			});
	      }
  		}
  		function showPic(){
			$("#content").show();
			$("#change_img_bg").show();
			$("#change_img_content").show();
		}
		 $("#change_img_window_close").click(function() {
		     $(".change_img_bg,.ggtx_window_content").css({display:"none"});
   		 });
   		 
   		 
 		function changePro(tag){
 			var obj;
 			obj=$("#user_city_code");
 			obj.empty();
 			var p = $(tag).val();
 			if(p!=""){
 			    var url = "/area/getSon?proId="+p+"&tm="+new Date().getTime();
				$.get(url,function(data){
					if(data.length>0){
						var html = "";
						obj.append("<option value=''>请选择</option>");
						$.each(data,function(i){
		                   obj.append("<option value='"+this.areaId+"'>"+this.areaName+"</option>");
						});
					}
				},"json");
 			}
		}
    </script>
</head>
<body>
<div>
    <div class="shortcut_create">
        <div class="shortcut_logo"></div>
        <div><a href="<%=path%>/webusersettiongs/toUserSet">个人设置</a></div>
    </div>

    <div class="set_content">

        <div class="set_menu">
            <div class="personal_infor menu_active">个人信息</div>
            <div class="change_pwd">修改密码</div>
        </div>
        <div class="menu_foot_line"></div>
            <form id="ff" name="ff" method="post">
        <div class="menu_content">
            <div>
                <div class="user_photo">
                    <input type="hidden" id="user_head_photo" name="user_head_photo" value="${user.user_head_photo}"/>
                    <img src="${user.user_head_photo}" class="user_photo_img"  onclick="showPic();"  onerror="javascript:this.src='/resources/images/zongci/add_peo_photo.png';"     id="ii">
                    <div class="user_photo_intro">点击图片更换头像<br>大小不超过50k,尺寸不超过160px*160px,格式为jpg、png、gif</div>
                </div>
                <div class="user_name">
                    <div class="infor_cont_title">用户名(ID)&nbsp;&nbsp;</div>
                    <div><input type="text" id="user_account" value="${user.user_account}" disabled></div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">昵&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称</div>
                    <div><input type="text" id="user_nick_name" name="user_nick_name" value="${user.user_nick_name}"></div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">真实姓名</div>
                    <div><input type="text" id="user_name" name="user_name" value="${user.user_name}"></div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别</div>
                    <div class="user_sex">  <!-- 性别0男1女2保密    -->
                        <div><input type="radio" name="user_sex" value="0" id="man" ${user.user_sex eq '男' ? 'checked="true"' : '' }><label for="man">男</label></div>
                        <div><input type="radio" name="user_sex" value="1" id="woman" ${user.user_sex eq '女' ? 'checked="true"' : '' }><label for="woman">女</label></div>
                    </div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">出生日期</div>
                    <div class="birth_date"><input class="span10 date-picker" name="user_birthday"
									id="user_birthday" type="text" onfocus="WdatePicker({skin:'whyGreen',maxDate:'%y-%M-%d'})"
									style="width: 88px;" placeholder="出生日期" value="${user.user_birthday}"/></div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业</div>
                    <div><input type="text" id="user_job" name="user_job" value="${user.user_job}"></div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">信&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;仰</div>
                    <div><input type="text" id="user_faith" name="user_faith" value="${user.user_faith}"></div>
                </div>

<!--                 <div class="user_name"> -->
<!--                     <div class="infor_cont_title">兴趣爱好</div> -->
<!--                     <div><input type="text" id="user_faith" name="user_faith" value="${user.user_faith}"></div> -->
<!--                 </div> -->

                <div class="user_name">
                    <div class="infor_cont_title">所在地区</div>
                    <div class="location_addr">
                        <div>
                            <select name="user_country_code">
                                <option value="0">中国</option>
                            </select>&nbsp;国&nbsp;
                        </div>
                        <div>
                            <input type="hidden" id="user_province" name="user_province" value="${user.user_province}"/>
                            <input type="hidden" id="user_city" name="user_city" value="${user.user_city}"/>
                            <select class="chzn-select" name="user_province_code" id="user_province_code"  style="vertical-align: top;min-width:100px;max-width: 150px;" onchange="changePro(this)">
				                   <option value="">请选择</option>
									<c:choose>
									<c:when test="${not empty sessionScope.proList}">
									<c:forEach items="${sessionScope.proList}" var="area" varStatus="vs">
									<option value="${area.areaId}" ${area.areaName eq user.user_province ? 'selected' : '' }>${area.areaName}</option>
									</c:forEach>
									</c:when>
									</c:choose>
							</select>&nbsp;省
                        </div>
                        <div>
                            <select name="user_city_code" id="user_city_code" style="vertical-align: top; min-width:100px;max-width: 150px;" >
									<option value="${pd.cty}" selected="selected">${pd.ctyname==null?"选择地区":pd.ctyname}</option>
								</select>&nbsp; 市 
                        </div>
                    </div>
                </div>

                <div class="user_name">
                    <div class="infor_cont_title">个性签名</div>
                    <div><input type="text" name="user_underwrite" value="${user.user_underwrite}"></div>
                </div>

                <div class="user_infor_finish" onclick="updateUserInfo();">完成</div>
            </div>
            
            <div class="menu_cont_hidden">
                <div class="user_name">
                    <div class="infor_cont_title">原 密 码</div>
                    <div><input name="user_pwd_old" id="user_pwd_old" type="password" placeholder="&nbsp;原密码"></div>
                </div>
                <div class="user_name">
                    <div class="infor_cont_title">新 密 码</div>
                    <div><input name="user_pwd_new" id="user_pwd_new" type="password" placeholder="&nbsp;新密码"></div>
                </div>
                <div class="user_name">
                    <div class="infor_cont_title">重复密码</div>
                    <div><input name="user_pwd_new_repeat" id="user_pwd_new_repeat" type="password" placeholder="&nbsp;再输入一次"></div>
                </div>
                <div class="user_infor_finish" onclick="updateUserPwd();">完成</div>
            </div>
        </div>
            </form>

    </div>
</div>

<div class="change_img_bg"  id="change_img_bg"></div>
<div class="change_img_content" id="change_img_content">
        <div class="ggtx_window_content">
		<div class="window_title">选 择 上 传 头 像</div>
        <img src="/resources/images/close_window.png" class="change_img_window_close" id="change_img_window_close">

	    	<div class="main_content" id="content">

          <div class="img-container"><img src="" style="display:none"></div>
          <div class="img-preview img-preview-sm"></div>
          <div class="btn-group">
            <div class="btn-primary" data-method="zoom" data-option="0.1"  title="放大">
				<label><img src="/resources/img/big.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="zoom" data-option="-0.1"  title="缩小">
				 <label><img src="/resources/img/small.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="-90"  title="左转90度">
				 <label><img src="/resources/img/left.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="90"  title="右转90度">
				<label><img src="/resources/img/right.png" width="40" height="25"/></label>
            </div>
          </div>
          <div class="foot_btn"> <input type="file" style="width: 100px;" id="inputImage"   accept="image/*" placeholder="上传头像" title="上传头像">  <button  id="getDataURL5" data-toggle="tooltip" type="button" title="$().cropper('getDataURL', 'image/jpeg')"  >确定</button></div>
  </div>
 </div>
  <input type="hidden" id="imgStr"/>
</div>
</body>
</html>