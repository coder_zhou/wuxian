<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'viewAncestralManager.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	    <link href="/resources/css/zongci/user_zong_ci_manage.css" rel="stylesheet">
	        <script src="/resources/js/jquery-1.8.2.js"></script>
    <script src="/resources/js/user_zong_ci_manage.js"></script>
    
  </head>
  
  <body>
  <!-- 权限设置-->
            <div class="limit_set_content">
                <div class="qxsz_window_content">
                    <div class="qxsz_title">管理员有布置宗祠的权限</div>
                    <div class="qxsz_create_person">
                        <div><img src="/resources/images/zongci/_limit_set_cp.png"></div>
                        <div>建园者：<span>${Manager.user_nick_name }</span></div>
                    </div>
                    <div class="qxsz_btn">
                        <div class="qxsz_add">
                            <img src="/resources/images/zongci/lsxc_add_photo_n.png" class="add_img">
                            <div>添&nbsp;&nbsp;加</div>
                        </div>
                        <div class="qxsz_manage">
                            <img src="/resources/images/zongci/lsxc_manage_n.png" class="manage_img">
                            <div>管&nbsp;&nbsp;理</div>
                        </div>
                    </div>
                    <div class="admin_list">
                    <c:choose>
                    <c:when test="${not empty UserManager }">
                    <c:forEach items="${UserManager }" var="var" varStatus="vs">
                            <div class="list_cont">
                            <div class="list_title">管理员${vs.index+1}</div>
                            <img src="${var.user_head_photo }">
                            <div class="list_name">${var.user_nick_name }</div>
                            <div class="list_red_delete"   onclick="delManager('${am._id }','${var._id}');" ><img src='/resources/images/zongci/po_biao_shi.png'></div>
                        </div>
                    </c:forEach>
                    </c:when>
                    </c:choose>
                    </div>

                </div>
            </div>

            <!-- 添加管理员-->
            <div class="add_manage_bg  bg"></div>
            <div class="add_manage_content">
                <div class="window_title">添 加 管 理 员</div>
                <img src="/resources/images/close_window.png" class="add_window_close  window_close">
                <div class="add_window_content">
            <c:choose>
            <c:when test="${not empty ul }">
            <c:forEach items="${ul }" var="ul">
                     <div class="add_list">
                        <img src="${ul.user_head_photo}" class="manage_peo_img">
                        <div class="manage_name">${ul.user_nick_name }</div>
                        <div class="manage_add_btn" onclick="addManager('${am._id }','${ul._id}');">添加</div>
                    </div>
            </c:forEach>
            </c:when>
            </c:choose>
                </div>
            </div>
            <script type="text/javascript">
            
        	function delManager(Id,ui){
        		
    	 
    				if(confirm("确定要删除吗?")) {
    					var url = "/UserAncestral/delManager?oid="+Id+"&uid="+ui+"&tm="+new Date().getTime();
    					$.get(url,function(data){
    						if(data=="success"){
    							alert("删除成功");
    						}
    					});
    				}
    		}
          	function addManager(Id,ui){
          		alert("1");
    					var url = "/UserAncestral/addManager?oid="+Id+"&uid="+ui+"&tm="+new Date().getTime();
    					$.get(url,function(data){
    						if(data=="success"){
    							alert("添加成功");
    						}
    					});
    		}
            </script>
            
  </body>
</html>
