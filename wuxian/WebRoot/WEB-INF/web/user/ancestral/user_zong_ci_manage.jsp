<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'create_zong_ci.jsp' starting page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
			<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
	    <link href="/resources/css/zongci/user_zong_ci_manage.css" rel="stylesheet">
    <script src="/resources/js/jquery-1.8.2.js"></script>
    <script src="/resources/js/user_zong_ci_manage.js"></script>
    
    <!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
    
  </head>
  
  <body>
  
  <!-- 礼佛管理-内容-->
        <div>
            <div class="content_btn">

                <!-- 新建宗祠按钮-->
                <a href="/UserAncestral/ToAddAncestral" target="mainFrame">
                    <div class="user_new_pro">
                        <img src="/resources/images/user_add.png">
                        <div>新建宗祠</div>
                    </div>
                </a>

                <!--<a href="" target="iframe_content">-->
                    <!--<div class="user_new_pro">-->
                        <!--<img src="images/user_add.png">-->
                        <!--<div>加入宗祠</div>-->
                    <!--</div>-->
                <!--</a>-->


                <div class="user_muyuan_table">
                    <img src="/resources/images/user_content_tog.png">
                    <div>宗祠管理</div>
                </div>
            </div>

        
              <c:choose>
              <c:when test="${not empty list }">
              <c:forEach items="${list }" var="var" varStatus="vs">
              
                  <div class="zc_infor">
                <div class="each_zc_img">
                    <img src="${var.ancestral_hall_img }">
                    <div class="text"></div>
                    <div class="text_infor">
                        <div>
                            <img src="/resources/images/zongci/zc_ht_xiang_huo.png">
                            <div class="text_infor_num">香火
                            <c:if test="${var.ancestral_hall_succeed>1000}">
                            ${var.ancestral_hall_succeed/1000 }K
                            </c:if>
                            <c:if test="${var.ancestral_hall_succeed<1000}">
                             ${var.ancestral_hall_succeed }
                            </c:if>
                            </div>
                        </div>
                        <div>
                            <img src="/resources/images/zongci/zc_ht_yu_e.png">
                            <div class="text_infor_num">余额
                            <c:if test="${var.ancestral_hall_fb_balance>1000}">
                            ${var.ancestral_hall_fb_balance/1000}K
                            </c:if>
                            <c:if test="${var.ancestral_hall_fb_balance<1000}">
                              ${var.ancestral_hall_fb_balance}
                            </c:if>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="infor_right">
                    <div class="hang_first"><a  href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_myid}&chooseflag=1&flag=1" target="_blank">${var.ancestral_hall_surname }</a>
                        <span>（宗祠号
                            <span class="zong_ci_num">${var.ancestral_myid }</span>
                            <span>秘钥</span><span class="mi_yao_num">${var.ancestral_hall_pwd }</span>）
                        </span>
                    </div>

                    <div class="hang_second">
                        <div class="zong_pu"><a href="/Ancestral/viewAncestral?id=${var.ancestral_myid }" target="_blank">族谱：${var.ancestral_hall_genealogy }</a></div>
                        <div class="tang_hao">堂号：${var.ancestral_hall_family }</div>
                    </div>
                
                    <div class="hang_third">
                        <div class="create_time">创建时间：
                        <c:set target="${myDate}" property="time" value="${var.ancestral_hall_addtime*1000}"/> 
                  <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/> 
                        </div>
                        <div class="zc_addr">所在地区：${var.ancestral_hall_province } ${var. ancestral_hall_city}</div>
                    </div>

                    <div class="hang_four">
                       <c:forEach items="${var.ancestral_hall_admin }" var="ad">
                   <c:if test="${ad.user_type==1 }">
                     <div class="create_people_name">创建：<span>${var.manage.user_nick_name }
                        </span></div>
                   </c:if>
                   
                   </c:forEach>
                      
           
                    </div>
                </div>

                <div class="infor_right_btn">
                    <div class="zhgl"><a  href="/webAncestralBz/queryAncestralBzOutside?ancestral_hall_id=${var.ancestral_myid}&chooseflag=1&flag=1" target="_blank">综合管理</a></div>
                    <div class="yst" onclick="viewAccount('${var. _id}');">议事堂</div>
                         <c:if test="${var.manage.user_id==sessionScope.USER_ID }">
                      <div class="limit_set" onclick="viewAncestralManager('${var. _id}');">权限设置</div>
                    </c:if>
                
                    <div class="jsgl" onclick="viewLoglist('${var. _id}');">祭祀管理</div>
                    <div class="jrct"><a  href="/Ancestral/queryAncestralBzInside?ancestral_hall_id=${var.ancestral_myid}&chooseflag=1&flag=1" target="_blank">进入祠堂</a></div>
                    <c:if test="${var.manage.user_id==sessionScope.USER_ID }">
                    <div class="zcsc"><a href="/UserAncestral/delAncestral?id=${var. ancestral_myid}">宗祠删除</a></div>
                    </c:if>
                    <!--<div class="zcjs">宗祠祭扫</div>-->
              <!--       <div class="family_photo">家族相册</div>  -->
                    <!--<div class="family_history">家族历史</div>-->
                </div>
              </div>
              </c:forEach>
              </c:when>
              </c:choose>




            <!-- 祭祀管理-->
           


            <!-- 议事堂-->


            <!-- 家族相册-->
            <div class="history_photo_bg bg"></div>
            <div class="history_photo_content">
                <div class="window_title">家 族 相 册</div>
                <img src="/resources/images/close_window.png" class="lsxc_window_close  window_close">
                <div class="jsgl_window_content">
                    <div class="jsgl_btn">
                        <div class="add_photo">
                            <img src="/resources/images/zongci/lsxc_add_photo_n.png" class="add_img">
                            <div>添&nbsp;&nbsp;加</div>
                        </div>
                        <div class="manage_photo">
                            <img src="/resources/images/zongci/lsxc_manage_n.png" class="manage_img">
                            <div>管&nbsp;&nbsp;理</div>
                        </div>
                    </div>
                    <div class="lsxc_photo imgs"  id="imgs">
                        <!--<div><img src="/resources/images/zongci/lsxc_photo.png"><div class="red_delete"><img src='/resources/images/zongci/po_biao_shi.png'></div></div>-->
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>

                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>

                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                        <div><img src="/resources/images/zongci/lsxc_photo.png"></div>
                    </div>
                </div>
            </div>

            <!-- 家族历史-->
            <div class="jz_history_bg   bg"></div>
            <div class="jz_history_content">
                <div class="window_title">家 族 历 史</div>
                <img src="/resources/images/close_window.png" class="jzls_window_close  window_close">
                <div class="jzls_window_content">
                    <div class="jzls_title">
                        <div class="jzls_xsqy  jzls_active">姓氏起源</div>
                        <div class="jzls_cgjx">祠规家训</div>
                        <div class="jzls_dsj">大事记</div>
                    </div>
                    <div class="jzls_bian_ji">编辑</div>
                    <div class="jzls_content">
                        <div>上古时代，颛顼的后裔陆终有六子，其中幼子名季连。季连的后裔曾做过周文王的老师，被周成王追封在荆山（今湖北省西部）一带，立国为荆，定都丹阳，后迁都于郢，改国号为楚。春秋时，楚庄王曾孙戌，在楚平王时任沈县（今安徽省临泉县）尹，其后人便以沈为氏。戌后任楚国左司马，他为人正直，疾恶如仇，深得楚人的敬重。楚昭王十八年（公元前498年），在与吴军打仗时英勇战死，楚昭王遂封他的儿子沈诸梁在叶为尹。沈诸梁承其父志曾平定白公胜的叛乱以复惠王，为楚国立下大功，被封到南阳，赐爵为公，世人尊为叶公。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。</div>
                        <div class="jzls_hidden_cont"></div>
                        <div class="jzls_hidden_cont"></div>

                    </div>
                </div>
            </div>

            <!--    家族历史  编辑   -->
            <div class="jzls_bj_bg   bg"></div>
            <div class="jzls_bj_content">
                <div class="window_title">家 族 历 史</div>
                <img src="/resources/images/close_window.png" class="jzls_bj_window_close  window_close">
                <div class="jzls_bj_window_content">
                    <div class="jzls_bj_title">
                        <div class="bian_ji_model">编 辑 模 块</div>
                        <select>
                            <option>&nbsp;&nbsp;姓 氏 起 源</option>
                            <option>&nbsp;&nbsp;祠 规 家 训</option>
                            <option>&nbsp;&nbsp;大 事 记</option>
                        </select>
                    </div>
                    <div class="jzls_bj_cont">上古时代，颛顼的后裔陆终有六子，其中幼子名季连。季连的后裔曾做过周文王的老师，被周成王追封在荆山（今湖北省西部）一带，立国为荆，定都丹阳，后迁都于郢，改国号为楚。春秋时，楚庄王曾孙戌，在楚平王时任沈县（今安徽省临泉县）尹，其后人便以沈为氏。戌后任楚国左司马，他为人正直，疾恶如仇，深得楚人的敬重。楚昭王十八年（公元前498年），在与吴军打仗时英勇战死，楚昭王遂封他的儿子沈诸梁在叶为尹。沈诸梁承其父志曾平定白公胜的叛乱以复惠王，为楚国立下大功，被封到南阳，赐爵为公，世人尊为叶公。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。于是其后人便以邑为氏，沈诸梁则被尊为叶姓始祖，因其字号子高，后人也习惯称其为叶子高。</div>
                    <div class="jzls_bj_save">保  存</div>
                </div>
            </div>


        </div>
<script>

   
   
	//修改
	function viewAccount(Id){
		 var diag = new top.Dialog();
		 diag.Drag=true;
		 diag.Title ="查看议事堂成员";
		 diag.URL = '/UserAncestral/viewAccount?oid='+Id;
		 diag.Width = 700;
		 diag.Height = 400;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	}
	//修改
	function viewAncestralManager(Id){
		 var diag = new top.Dialog();
		 diag.Drag=true;
		 diag.Title ="权限管理";
		 diag.URL = '/UserAncestral/viewAncestralManager?oid='+Id;
		 diag.Width = 700;
		 diag.Height = 400;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	} 
	

	function viewLoglist(Id){
		 var diag = new Dialog();
		 diag.Drag=true;
		 diag.Title ="祭祀日志";
		 diag.URL = '/webAncestralBz/getAncestrallLog?ancestral_hall_obid='+Id;
		 diag.Width = 700;
		 diag.Height = 400;
		 diag.CancelEvent = function(){ //关闭事件
			diag.close();
		 };
		 diag.show();
	} 
</script>
        
        
  
  
  </body>
</html>
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	
	
	});
	
</script>