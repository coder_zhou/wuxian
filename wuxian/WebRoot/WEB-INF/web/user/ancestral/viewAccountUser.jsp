<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'viewAccountUser.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="/resources/css/zongci/user_zong_ci_manage.css" rel="stylesheet">
  </head>
  
  <body>
            <div class="yst_content">
                <div class="yst_window_content">
                <c:choose>
                <c:when test="${not empty ul }">
                <c:forEach items="${ul }" var="var">
                      <div class="yst_list">
                        <img src="${var.user_head_photo }">
                        <div>${var.user_nick_name }</div>
                    </div>
                </c:forEach>
                </c:when>
                </c:choose>
                </div>
            </div>
  
  
  </body>
</html>
