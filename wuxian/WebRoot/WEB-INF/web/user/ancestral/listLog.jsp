<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'listLog.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
            <div class="jisi_manage_content">
                <div class="jsgl_window_content">
                <c:choose>
                <c:when test="${not empty list }">
                <c:forEach items="${list }" var="var">
                       <ul>
                        <li class="jsgl_list_1"><img src="${var.ancestral_hall_img }"></li>
                        <li class="jsgl_list_2">供奉  ${var.sacrifice_materials }</li>
                        <li class="jsgl_list_3">${var.sacrifice_nick_name }</li>
                        <li class="jsgl_list_4">
                         <c:set target="${myDate}" property="time" value="${var.sacrifice_time_int*1000}"/> 
                                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/>
                        </li>
                        <li class="jsgl_list_5"><img src="/resources/images/zongci/fo_my_wish_delete.png"></li>
                    </ul>
                </c:forEach>
                </c:when>
                </c:choose>
                <form action="/UserAncestral/listLog" id="Form"></form>
                <ff:page mhFrom="Form" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="UserAncestral/listLog"/>
                </div>
            </div>
  
  </body>
</html>
