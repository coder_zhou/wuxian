<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'create_zong_ci.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  <link href="/resources/css/zongci/create_zong_ci.css" rel="stylesheet">
      <script src="/resources/js/jquery-1.8.2.js"></script>
  </head>
  
  <body>
  <!--创建宗祠-->
<div>
    <!-- 流程部分-->
    <div class="shortcut_create">
        <div class="shortcut_logo"></div>
        <div>
            <a href="user_zong_ci_manage.html">宗祠管理</a> >
            <a href="create_zong_ci.html">创建宗祠</a>
        </div>
    </div>

    <!-- 创建-->
    <div class="create">
        <img src="/resources/images/write_cemetery_infor.png" class="select_formwork_logo">
        <div class="select_formwork_title">创建宗祠</div>
    </div>
<form action="/UserAncestral/AddAncestral" method="post" id="form">
    <div class="create_content">
        <div class="zc_name">
            <div class="space_name_title">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;氏</div>
            <input type="text" name="ancestral_hall_surname" id="ancestral_hall_surname" maxlength="4">
            <span> *  （4字以内  例如：“赵”）</span>
        </div>
        <div class="space_pwd">
            <div class="space_pwd_title">族谱名称</div>
            <input type="text" name="ancestral_hall_genealogy" id="ancestral_hall_genealogy"  maxlength="10">
        </div>
        <div class="space_content">
            <div class="space_content_title">堂&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号</div>
            <input type="text" name="ancestral_hall_family" id="ancestral_hall_family" maxlength="5">
            <span>（5字以内）</span>
        </div>
        <div class="zong_ci_addr">
            <div class="zc_add_name">所在地址</div>
            <div class="zc_add_cont">
                	<select style="width: 100px;" name="ancestral_hall_country_code" id="ancestral_hall_country_code" >
					<option value="1"> 中国</option>
				</select> 国
				 <select class="chzn-select" name="ancestral_hall_province_code" id="ancestral_hall_province_code" data-placeholder="请选择" style="vertical-align: top;min-width:100px;max-width: 150px; " onchange="Change(1);">
				                   <option selected="selected" >请选择</option>
									<c:choose>
									<c:when test="${not empty sessionScope.proList }">
									<c:forEach items="${sessionScope.proList}" var="area" varStatus="vs">
									<option value="${area.areaId}">${area.areaName}</option>
									
									</c:forEach>
									
									</c:when>
									</c:choose>
										
								</select> 省 
				<select name="ancestral_hall_city_code" id="ancestral_hall_city_code" style="vertical-align: top; min-width:100px;max-width: 150px;  "  onchange="set();" >
						<option value="${pd.cty}" selected="selected">${pd.ctyname==null?"选择地区":pd.ctyname}</option>
								
								</select> 市 
                
                <input type="hidden" name="province_name" id="province_name">
                <input type="hidden" name="city_name" id="city_name">
                <input type="hidden" name="country_name" id="country_name">
            </div>
            <span>*</span>
        </div>
    </div>
    <div class="space_btn">
        <div class="finish" onclick="sub();">完成</div>
    </div>
</div>
</form>
  </body>
</html>
	<!-- 日期框 -->
	<script type="text/javascript" src="/resources/js/bootbox.min.js"></script>
	<!-- 确认窗口 -->
	<!-- 引入 -->
	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
<script type="text/javascript">

function set(){
	$("#country_name").val($("#ancestral_hall_country_code").find("option:selected").text());
	$("#province_name").val($("#ancestral_hall_province_code").find("option:selected").text());
	$("#city_name").val($("#ancestral_hall_city_code").find("option:selected").text());
}

function sub(){
		if($("#ancestral_hall_surname").val()==""){
			$("#ancestral_hall_surname").tips({
				side:2,
	            msg:'请输入姓氏',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#ancestral_hall_surname").focus();
			return false;
		}
		if($("#ancestral_hall_genealogy").val()==""){
			$("#ancestral_hall_genealogy").tips({
				side:2,
	            msg:'请输入族谱名称',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#ancestral_hall_genealogy").focus();
			return false;
		}
		if($("#ancestral_hall_family").val()==""){
			$("#ancestral_hall_family").tips({
				side:2,
	            msg:'请输入堂号',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#ancestral_hall_family").focus();
			return false;
		}
		if($("#ancestral_hall_city_code").val()==""){
			$("#ancestral_hall_city_code").tips({
				side:2,
	            msg:'请选择所在城市',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#ancestral_hall_city_code").focus();
			return false;
		}
	$("#form").submit();
}


function Change(aa){
	var obj;
	 
	 if(aa==1){
		 var p= $("#ancestral_hall_province_code").val();
		  obj=$("#ancestral_hall_city_code");
		 obj.empty();
	//	 obj.css({ display: "block" });
	 }
	
	
		var url = "/area/getSon?proId="+p+"&tm="+new Date().getTime();
		$.get(url,function(data){
			if(data.length>0){
				var html = "";
				 obj.append("<option value=''>请选择</option>");
				$.each(data,function(i){
                obj.append("<option value='"+this.areaId+"'>"+this.areaName+"</option>");
					 
                    
				});
				
			}
		},"json");
}
</script>
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
</script>