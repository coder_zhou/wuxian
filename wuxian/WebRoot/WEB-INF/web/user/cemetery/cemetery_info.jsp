<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
<link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">

	<script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>

	<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
	<script src="/resources/js/bootstrap.min.js"></script>
	<script src="/resources/js/ace-elements.min.js"></script>
	<script src="/resources/js/ace.min.js"></script>
	
	<!-- 过滤非法字符   -->
	<script src="<%=path%>/resources/js/stripscript.js"></script>

	<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script>
	<!-- 下拉框 -->
	<script type="text/javascript"
		src="/resources/js/bootstrap-datepicker.min.js"></script>
	<!-- 日期框 -->
	<script type="text/javascript" src="/resources/js/bootbox.min.js"></script>
	<!-- 确认窗口 -->
	<!-- 引入 -->
	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
		<script src="/resources/index/js/user_bg_muyuan.js"></script>	
	<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
	
</head>
<body>

<form action="/cemetery/choseNum" method="post" id="form">
		<!-- 墓园管理-内容-->
		<div>
			<!-- 流程部分-->
			<div class="shortcut_create">
				<div class="shortcut_logo"></div>
				<div>
					<a href="user_bg_muyuan.html"> 私人墓园</a> > <a
						href="create_cemetery.html">创建墓园</a>
				</div>
				<div class="convention_generate compile_cemetery_infor">
					<div>
						<div class="course course_01 course_color"></div>
						<div class="course_create course_num_1 course_border">选择模板</div>
					</div>
					<div>
						<div class="course course_02 course_color"></div>
						<div class="course_create course_num_2 course_bg">墓园信息</div>
					</div>
					<div>
						<div class="course course_03"></div>
						<div class="course_create course_num_3">设置园号</div>
						<div class="course course_04"></div>
					</div>
				</div>
			</div>

			<!-- 填写墓园信息-->
			<div class="select_formwork">
				<img src="/resources/index/images/write_cemetery_infor.png"
					class="select_formwork_logo">
				<div class="select_formwork_title">填写墓园信息</div>
			</div>
			            <input type="hidden" name="province_name" id="province_name">
                <input type="hidden" name="city_name" id="city_name">
			<div class="write_infor">
				墓园名称&nbsp;<input type="text" name="cemeteryName" id="cemeteryName" maxlength="20" onkeyup="this.value = stripscript(this.value)"/>
				<img src="/resources/index/images/write_infor_not_null.png"><br>
				<br> 墓园所在&nbsp;
				<select style="width: 100px;">
					<option>中国</option>
				</select> 国
				 <select class="chzn-select" name="deceasedProvince" id="deceasedProvince" data-placeholder="请选择" style="vertical-align: top;min-width:100px;max-width: 150px; " onchange="Change(1);">
				                   <option selected="selected" >请选择</option>
									<c:choose>
									<c:when test="${not empty sessionScope.proList }">
									<c:forEach items="${sessionScope.proList}" var="area" varStatus="vs">
									<option value="${area.areaId}">${area.areaName}</option>
									
									</c:forEach>
									
									</c:when>
									</c:choose>
										
								</select> 省 
				<select name="deceasedCity" id="deceasedCity" style="vertical-align: top; min-width:100px;max-width: 150px;  " onchange="set();">
						<option value="${pd.cty}" selected="selected">${pd.ctyname==null?"选择地区":pd.ctyname}</option>
								
								</select> 市 
				<input type="text"> <br>
				<br> 公开设置&nbsp;
				<input type="radio" name="cemeteryLook" checked="checked" value="0">完全公开
				<input type="radio" name="cemeteryLook" value="1">只对登录用户公开 
				<input type="radio" name="cemeteryLook" value="2">完全保密 <br>
				<br> 墓园类型&nbsp;
				<input type="radio" name="cemeteryType" checked="checked" onclick="rad(0);"  value="0">单人墓
				<input type="radio" id="shizhe2" name="cemeteryType" onclick="rad(1);" value="1">双人墓 <br>
				<br>
				<br>
	            <br> 简介&nbsp;
						<textarea rows="5" name="cemeteryMessage" id="cemeteryMessage" onkeyup="checkLen(this);"  ></textarea>
						<div class="text_tishi">您还可以输入 <span id="count">500</span> 个文字</div> 
				<br>
				<br>
				
				<div class="infor_01">
					<div>逝者1信息</div>
					<br>
					<div class="infor_01_infor">
						姓名&nbsp;<input type="text" name="deceasedName" id="deceasedName" maxlength="25" onkeyup="this.value = stripscript(this.value)" /> <br>
						<br> 性别&nbsp;<input type="radio" name="deceasedSex" value="0" checked="checked" >男
						<input type="radio" name="deceasedSex" value="1">女 <br>
						<br> 生辰&nbsp;	<input class="span10 date-picker" name="deceasedBirthday"
									id="deceasedBirthday" type="text" onfocus="WdatePicker({skin:'whyGreen',maxDate:'%y-%M-%d'})"
									style="width: 88px;" placeholder="生辰" /><br>
						<br> 祭日&nbsp; 	<input class="span10 date-picker" name="deceasedDieTime"
									id="deceasedDieTime"  type="text"
									onfocus="WdatePicker({minDate:$('#deceasedBirthday').val(),maxDate:'%y-%M-%d'})"
									style="width: 88px;" placeholder="祭日" /> <br>
						<br> 关系&nbsp;&nbsp;<input type="text" value="" class="relation" name="deceasedRelationship" id="deceasedRelationship" placeholder="逝者是您的..." onkeyup="this.value = stripscript(this.value)" maxlength="10" /> <br>
					
					</div>

				</div>
				
				
				<!-- 逝者2 -->
				
		        <div class="infor_01 infor_02" id="deceased2" style="display: none;">
					<div>逝者2信息</div>
					<br>
					<div class="infor_01_infor">
						姓名&nbsp;<input type="text" name="deceasedName2" id="deceasedName2" maxlength="25" onkeyup="this.value = stripscript(this.value)" /> <br>
						<br> 性别&nbsp;<input type="radio" name="deceasedSex2" value="0" checked="checked" >男
						<input type="radio" name="deceasedSex2" value="1">女 <br>
						<br> 生辰&nbsp;	<input class="span10 date-picker" name="deceasedBirthday2"
									id="deceasedBirthday2" onfocus="WdatePicker({skin:'whyGreen',maxDate:'%y-%M-%d'})"
									style="width: 88px;"type="text" placeholder="生辰" /><br>
						<br> 祭日&nbsp; 	<input class="span10 date-picker" name="deceasedDieTime2"
									id="deceasedDieTime2"  type="text"
								onfocus="WdatePicker({minDate:$('#deceasedBirthday2').val(),maxDate:'%y-%M-%d'})"
									style="width: 88px;" placeholder="祭日" /> <br>
						<br> 关系&nbsp;&nbsp;<input type="text" value="" class="relation" name="deceasedRelationship2" id="deceasedRelationship2" placeholder="逝者是您的..." onkeyup="this.value = stripscript(this.value)" maxlength="10" /> <br>
					</div>
					

				</div>
				
				<div class="infor_btn">
						<div class="infor_next">
							<a onclick="check();" >下一步</a>
						</div>
						<div class="infor_reset" onclick="re();">重置</div>
					</div>
				

			</div>
		</div>

</form>

<script type="text/javascript">
function set(){
	$("#province_name").val($("#deceasedProvince").find("option:selected").text());
	$("#city_name").val($("#deceasedCity").find("option:selected").text());
}



function Change(aa){
	var obj;
	 
	 if(aa==1){
		 var p= $("#deceasedProvince").val();
		  obj=$("#deceasedCity");
		 obj.empty();
	//	 obj.css({ display: "block" });
	 }
	
	
		var url = "/area/getSon?proId="+p+"&tm="+new Date().getTime();
		$.get(url,function(data){
			if(data.length>0){
				var html = "";
				 obj.append("<option value=''>请选择</option>");
				$.each(data,function(i){
                obj.append("<option value='"+this.areaId+"'>"+this.areaName+"</option>");
					 
                    
				});
				
			}
		},"json");
}
function rad(a){
	if(a==0){
		$("#deceased2").hide();
		
	}else if(a==1){
		$("#deceased2").show();
		var main = $(window.parent.document).find("#mainFrame");
		var thisheight = $(document).height()+30;
		main.height(thisheight);
	}
	
}	
	function re(){
		document.getElementById("form").reset();
	}	
	function check(){
 		if($("#cemeteryName").val()==""){
			$("#cemeteryName").tips({
				side:2,
	            msg:'请输入墓园名称',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#cemeteryName").focus();
			return false;
		}
		
		if($("#deceasedProvince").val()==""){
			$("#deceasedProvince").tips({
				side:2,
	            msg:'请选择墓园所在省',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#deceasedProvince").focus();
			return false;
		}
		
		if($("#deceasedCity").val()==""){
			$("#deceasedCity").tips({
				side:2,
	            msg:'请选择墓园所在市',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#deceasedCity").focus();
			return false;
		}
		

		if($("#deceasedName").val()==""){
			$("#deceasedName").tips({
				side:2,
	            msg:'请输入逝者名称',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#deceasedName").focus();
			return false;
		}
		if(getshizhe()==1){
		if($("#deceasedName2").val()==""){
			$("#deceasedName2").tips({
				side:2,
	            msg:'请输入逝者名称',
	            bg:'#AE81FF',
	            time:3
		    });
			$("#deceasedName2").focus();
			return false;
		}
		}

		
	 	$("#form").submit();
	}
 
	
	function getshizhe(){
		var value="";
		var radio=document.getElementsByName("cemeteryType");
		for(var i=0;i<radio.length;i++){
		if(radio[i].checked==true){
		value=radio[i].value;
		break;
		}
		}
		return value;
		} 
	function checkLen(obj) {  

		var maxChars = 500;//最多字符数  

		if (obj.value.length > maxChars)  obj.value = obj.value.substring(0,maxChars);  

		var curr = maxChars - obj.value.length;  

		document.getElementById("count").innerHTML = curr.toString(); 

		} 
 	
</script>


<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>


</body>
</html>
