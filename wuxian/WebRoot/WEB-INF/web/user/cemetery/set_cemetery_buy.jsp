<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>
</head>
<body>

        <div>
            <!-- 流程部分-->
            <div class="shortcut_create">
                <div class="shortcut_logo"></div>
                <div>
                    <a href="new_create_cemetery.html"  target="iframe_content"> 私人墓园</a> >
                    <a href="create_cemetery.html" target="iframe_content">创建墓园</a>
                </div>
                <div class="convention_generate compile_cemetery_infor">
                    <div>
                        <div class="course course_01 course_color"></div>
                        <div class="course_create course_num_1 course_border" >选择模板</div>
                    </div>
                    <div>
                        <div class="course course_02 course_color"></div>
                        <div class="course_create course_num_2 course_border">墓园信息</div>
                    </div>
                    <div>
                        <div class="course course_03 course_color"></div>
                        <div class="course_create course_num_3 course_bg">设置园号</div>
                        <div class="course course_04"></div>
                    </div>
                </div>
            </div>
<form id="ff" action="/cemetery/getSpecialNum" name="ff" method="post">
            <!-- 填写墓园信息-->
            <div class="select_formwork special_number">
                <img src="/resources/index/images/write_cemetery_infor.png" class="select_formwork_logo">
                <div class="select_formwork_title">特殊墓园号</div>
                <div class="select_search">
                   
                    <img src="/resources/index/images/search.png" class="search_photo">
                    <input type="text" name="cemetery_number" id="cemetery_number">
                    <div class="search_char" onclick="search();">搜索</div>
               
                </div>
            </div>
      </form>
            <div class="number_all">
         
            <c:choose>
<c:when test="${not empty numlist}">
<c:forEach items="${numlist}" var="var" varStatus="vs">
    <div class="number_list ${var.special_num_type==1?"number_use":""}">
                    <div class="number">${var.special_num}</div>
                    <div class="price">${var.special_num_price}福币</div>
                    <div class="number_choose">
                    <c:if test="${var.special_num_type!=1}">
                        <div class="choose_btn">
                            <div><a href="/cemetery/generate?cemeteryNumber=${var.special_num}&cemeteryNumberPrice=${var.special_num_price}&type=特殊墓园号"  target="mainFrame">确认选择</a></div>
                        </div>
                        </c:if>
                    </div>
                </div>
</c:forEach>
</c:when>
        </c:choose>   
              
            </div>
             <div class="number_page_bottom">
              
                <ff:page mhFrom="ff" showReSize="flase" counts="false"  field="page" onlyOneShow="false" showListNo="false" action="cemetery/getSpecialNum" />
            </div>
			
        </div>
<script type="text/javascript">
function search(){
	$("#ff").submit();
	
}
</script>
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>
</body>
</html>
