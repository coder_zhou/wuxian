<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>

</head>
<body>

        <div>
            <!-- 流程部分-->
            <div class="shortcut_create">
                <div class="shortcut_logo"></div>
                <div><a href="#"  target="mainFrame"> 私人墓园</a> >
                    <a href="#" target="mainFrame">创建墓园</a>
                </div>
                <div class="convention_generate compile_cemetery_infor">
                    <div>
                        <div class="course course_01 course_color"></div>
                        <div class="course_create course_num_1 course_border">选择模板</div>
                    </div>
                    <div>
                        <div class="course course_02 course_color"></div>
                        <div class="course_create course_num_2 course_border">墓园信息</div>
                    </div>
                    <div>
                        <div class="course course_03 course_color"></div>
                        <div class="course_create course_num_3 course_bg">设置园号</div>
                        <div class="course course_04"></div>
                    </div>
                </div>
            </div>

            <!-- 填写墓园信息-->
            <div class="select_formwork">
                <img src="/resources/index/images/write_cemetery_infor.png" class="select_formwork_logo">
                <div class="select_formwork_title">自定义墓园号</div>
            </div>
            <div class="custom_prompt">提示：购买自定义特殊号码需要支付  1000  福币</div>
            <div class="custom_number">
                <div>请输入墓园号</div>
                <input type="text" name="cemetery_number" id="cemetery_number" onblur="checknum();"  onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
                    onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}" class="custom_number_num">
                <div class="custom_number_btn"><a onclick="submit();">确认</a></div>
                <div class="custom_number_btn"><a  onclick="history.go(-1)" target="mainFrame">返回</a></div>
            </div>
            <div class="custom_number_prompt">说明：请输入8位数字，可以输入您喜欢的数字或亲人的生日组合等</div>


        </div>
    <script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
<script type="text/javascript">
var b=false;
function checknum(){
	if($("#cemetery_number").val()==""){
		$("#cemetery_number").tips({
			side:2,
            msg:'请输入墓园号',
            bg:'#AE81FF',
            time:3
	    });
		$("#cemetery_number").focus();
		b=false;
		return false;
	}else if($("#cemetery_number").val().length!=8){
		$("#cemetery_number").tips({
			side:2,
            msg:'请输入8位墓园号',
            bg:'#AE81FF',
            time:3
	    });
		$("#cemetery_number").focus();
		b=false;
		return false;
	}
	
	   var FirstChar=$("#cemetery_number").val().substr(0,1);
	   if(FirstChar=="0")
	   {
		   $("#cemetery_number").tips({
				side:1,
	           msg:'不能以0开头',
	           bg:'#AE81FF',
	           time:3
	       });
		   b=false;
	      return false;
	   }
	
	
	$.ajax({
		type: "POST",
		url: '/cemetery/selfnum?cemetery_number='+$("#cemetery_number").val(),
		//beforeSend: validateData,
		success: function(data){
			$("#cemetery_number").tips({
				side:1,
	            msg:data,
	            bg:'#AE81FF',
	            time:3
	        });
		if(data=="此号码为特殊号"||data=="墓园号为空"||data=="此号码已被使用" )
		{
			b=false;
			return false;
		}else{
			b=true;
			return true;

		}
		}
	});
}
function submit(){
	if(b){
	window.location.href='/cemetery/generate?cemeteryNumber='+$("#cemetery_number").val()+'&cemeteryNumberPrice='+1000+'&type=自定义墓园号';
	}
}
function fcheck(Val)
{
   var FirstChar=Val.substr(0,1);
   if(FirstChar=="0")
   {
	   $("#cemetery_number").tips({
			side:1,
           msg:'不能以0开头',
           bg:'#AE81FF',
           time:3
       });
      return false;
   }else{
	   return true;
   }
}
</script>
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>
</body>
</html>
