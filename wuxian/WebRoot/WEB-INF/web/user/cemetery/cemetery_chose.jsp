 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>

</head>
<body>

        <div>
            <!-- 流程部分-->
            <div class="shortcut_create">
                <div class="shortcut_logo"></div>
                <div><a href="new_create_cemetery.html"  target="iframe_content"> 私人墓园</a> >
                    <a href="create_cemetery.html"  target="iframe_content">创建墓园</a>
                </div>
                <div class="convention_generate compile_cemetery_infor">
                    <div>
                        <div class="course course_01 course_color"></div>
                        <div class="course_create course_num_1 course_border" >选择模板</div>
                    </div>
                    <div>
                        <div class="course course_02 course_color"></div>
                        <div class="course_create course_num_2 course_border">墓园信息</div>
                    </div>
                    <div>
                        <div class="course course_03 course_color"></div>
                        <div class="course_create course_num_3 course_bg">设置园号</div>
                        <div class="course course_04"></div>
                    </div>
                </div>
            </div>

            <!-- 填写墓园信息-->
            <div class="select_formwork">
                <img src="/resources/index/images/write_cemetery_infor.png" class="select_formwork_logo">
                <div class="select_formwork_title">设置墓园号</div>
            </div>

            <div class="auto_number">
                <div>系统为您免费自动生成的园号为</div>
                <div class="auto_number_num">${pd.cemetery_id}</div>
                <div class="auto_number_btn"><a href="/cemetery/generate?cemeteryNumber=${pd.cemetery_id}"  target="mainFrame">确认园号  下一步</a></div>
            </div>
            <div class="auto_number_prompt">
                <div>如果您对系统为您生成的园号不满意，您也可以通过以下方式来设置墓园号：</div>
                <div class="change_number">
                    <div><a href="/cemetery/getSpecialNum"  target="mainFrame">购买特殊号  下一步</a></div><img src="/resources/index/images/wenhao.png">
                    <div><a href="/cemetery/toselfnum"  target="mainFrame">自定义墓园号  下一步</a></div><img src="/resources/index/images/wenhao.png">
                </div>
            </div>

<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>
        </div>
        </body>
        </html>