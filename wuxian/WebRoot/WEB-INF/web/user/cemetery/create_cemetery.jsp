<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>

</head>
<body>
        <!-- 墓园管理-内容-->
        <div>
            <!-- 流程部分-->
            <div class="shortcut_create">
                <div class="shortcut_logo"></div>
                <div><a href="user_bg_muyuan.html"> 私人墓园</a> > <a href="create_cemetery.html">创建墓园</a></div>
                <div class="convention_generate">
                    <div>
                        <div class="course course_01 course_color"></div>
                        <div class="course_create course_num_1 course_bg" >选择模板</div>
                    </div>
                    <div>
                        <div class="course course_02"></div>
                        <div class="course_create course_num_2">墓园信息</div>
                    </div>
                    <div>
                        <div class="course course_03"></div>
                        <div class="course_create course_num_3">设置园号</div>
                        <div class="course course_04"></div>
                    </div>
                </div>
            </div>

            <!-- 选择模板-->
            <div class="select_formwork">
                <img src="/resources/index/images/select_formwork.png" class="select_formwork_logo">
                <div class="select_formwork_title">选择模板</div>
            </div>
            <div class="model">
            
              <c:choose>
              <c:when test="${not empty list}">
              <c:forEach items="${list}" var="var" varStatus="vs">
              <div class="model_content">
                    <div class="model_photo">
                        <img src="/goods${var.cemetery_template_img }">
                        <div class="text">
                            <div class="imgtext">
                                <div>${var.cemetery_template_content }</div>
                            </div>
                        </div>
                    </div>
                    <div class="model_title">${var.cemetery_template_name }</div>
                    <div class="model_price">${var.cemetery_template_price }福币</div>
                    <div class="model_choose_btn"><a href="<%=basePath%>/cemetery/cemeteryinfo?cemetery_template_id=${var.cemetery_template_id}">选择</a></div>
                </div>

</c:forEach>
</c:when>

</c:choose>
            
             




              
            </div>
        </div>
            <!--<ul class="pagination">-->
                <!--<li class="pagination_first"><a href="#">上一页</a></li>-->
                <!--<li><a href="#">1</a></li>-->
                <!--<li><a href="#">2</a></li>-->
                <!--<li><a href="#">3</a></li>-->
                <!--<li><a href="#">4</a></li>-->
                <!--<li><a href="#">5</a></li>-->
                <!--<li class="pagination_last"><a href="#">下一页</a></li>-->
            <!--</ul>-->
            <!--<div class="pagination_sum">共1页</div>-->
            <!--<div class="pagination_pre">当前第<span>1</span>页</div>-->


<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>
 
</body>
</html>
