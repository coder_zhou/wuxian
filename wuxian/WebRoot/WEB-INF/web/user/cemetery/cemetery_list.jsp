 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>

</head>
<body>
 
  <div>
            <div class="content_btn">
                <a  id="chuangjian" onclick="createCemetery();"> 
                    <div class="user_new_pro">
                        <img src="/resources/index/images/user_add.png">
                      <div >新建墓园</div>
                    </div>
                </a>
                
                      <!-- 常规建园-->
                <a href="/cemetery/createCemetery" target="mainFrame" style="display: none;" id="changgui">
                    <div class="user_new_pro">
                        <img src="/resources/index/images/user_add.png">
                        <div>常规建园</div>
                    </div>
                </a>

                <!-- 快捷建园-->
                <a href="/cemetery/quick" target="mainFrame" style="display: none;" id="kuaijie">
                    <div class="user_new_pro">
                        <img src="/resources/index/images/user_add.png">
                        <div>快捷建园</div>
                    </div>
                </a>
                
                <div class="user_muyuan_table">
                    <img src="/resources/index/images/user_content_tog.png">
                    <div>墓园列表</div>
                </div>


            </div>

            <div class="user_content_all">
                <ul class="table_title">
                    <li class="table_li_01">墓园号</li>
                    <li class="table_li_02">墓园名称</li>
                    <li class="table_li_03">建园时间</li>
                    <li class="table_li_04">点击数(改成墓园布置花费)</li>
                    <li class="table_li_05">操作</li>
                </ul>
<c:choose>
<c:when test="${not empty list}">
<c:forEach items="${list}" var="cemetery" varStatus="vs">
 <ul>
                    <li class="table_li_01">
<!--                     <a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${cemetery.cemetery_number}" target="_blank"> -->
                    ${cemetery.cemetery_number}
<!--                     </a> -->
                    </li>
                    <li class="table_li_02">${cemetery.cemetery_name}</li>
                    <li class="table_li_03">
   <c:set target="${myDate}" property="time" value="${cemetery.cemetery_add_time*1000}"/> 
                  <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
</li>
                    <li class="table_li_04">${cemetery.cemetery_clicks}</li>
                    <li class="table_li_05">
                    <a href="/webCemetery/queryCemeteryInfoForBz?cemetery_id=${cemetery.cemetery_id}&chooseflag=2&flag=2" target="_blank">管理</a>
                    <a href="/cemetery/delCemetery?cemetery_number=${cemetery.cemetery_number}"  onclick="return del()">删除 </a>
                    </li>
                </ul>

</c:forEach>
</c:when>

</c:choose>
               
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
</script>
          
            </div>
        </div>
        </body>
        <script type="text/javascript">
        function createCemetery(){
        	$("#chuangjian").hide();
        	$("#changgui").show();
        	$("#kuaijie").show();
        }
        function del(){
        	if(!confirm("确认要删除？")){
        	window.event.returnValue = false;
        	}
        	} 
        </script>
        </html>
