    <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>

</head>
<body>
   
   <div>
            <!-- 流程部分-->
            <div class="shortcut_create">

                <div class="shortcut_logo"></div>

                <!-- 引导标题-->
                <div>
                    <a target="mainFrame"> 私人墓园</a> >
                    <a  target="mainFrame">创建墓园</a>
                </div>


                <div class="convention_generate compile_cemetery_infor">
                    <div>
                        <div class="course course_01 course_color"></div>
                        <div class="course_create course_num_1 course_border" >选择模板</div>
                    </div>
                    <div>
                        <div class="course course_02 course_color"></div>
                        <div class="course_create course_num_2 course_border">墓园信息</div>
                    </div>
                    <div>
                        <div class="course course_03 course_color"></div>
                        <div class="course_create course_num_3 course_border">设置园号</div>
                        <div class="course course_04 course_color"></div>
                    </div>
                </div>
            </div>


            <!-- 填写墓园信息-->
            <div class="select_formwork">
                <img src="/resources/index/images/write_cemetery_infor.png" class="select_formwork_logo">
                <div class="select_formwork_title">生成墓园</div>
            </div>

            <div class="formwork_list">
            <c:if test="${ not empty cemeterytemplate }">
                <ul class="list_photo">
                    <li><input type="checkbox" checked="checked" id="type" onclick="check();"></li>
                    <li class="list_number_number"><img id="cemetery_template_img" src="/goods/${cemeterytemplate.cemetery_template_img}"></li>
                    <li class="list_message" id="cemetery_template_name">${cemeterytemplate.cemetery_template_name}</li>
                    <li id="cemetery_template_price">${cemeterytemplate.cemetery_template_price }福币</li>
                </ul>
                </c:if>
                <ul class="list_number">
                    <li><input type="checkbox" checked="checked" id="num" onclick="check();"></li>
                    <li class="list_number_number" id="list_number_number">${sessionScope.cemeteryNumber}</li>
                    <li class="list_message" id="list_message">
                    ${sessionScope.Numtype==''?"默认墓园号":sessionScope.Numtype }
                    </li>
                    <li id="cemeteryNumberPrice">
                      ${sessionScope.cemeteryNumberPrice==''?"0": sessionScope.cemeteryNumberPrice}
                    福币</li>
                </ul>

                <div class="pay_message">
                    <div class="pay_infor"><div class="pay_warn">!</div>取消勾选，则默认使用免费模板和系统分配墓园号</div>
                </div>

                <div class="pay_sum">合&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计：<span id="sum"></span> 会员价:<span id="huiyuan"></span>福币</div>

                <!-- 当余额不足的时候，显示余额。如果余额足够，则只显示合计-->
                <div class="pay_balance" id="pay_balance" style="display: none;">余额不足：<span id="yue">-${user.user_fb }福币</span></div>

                <div class="pay_btn">
                    <div class="pay_pre"><a  onclick="history.go(-1)"  target="iframe_content">上一步</a></div>

                    <!-- 当余额不足的时候，显示补充福币。如果余额足够，则显示确认建园-->
                  <div class="pay_btn_affirm" id="c" style="display: none;"><a onclick="toCreate();" target="mainFrame">确认建园</a></div>
                  <div class="pay_btn_affirm" id="f" style="display: none;"><a id="cz" href="/cemetery/create"  target="_blank">补充福币</a></div> 
                </div>
            </div>
            
            <form action="/cemetery/create" method="post" id="Form"> 
            <input type="hidden" name="cemetery_template_id" id="cemetery_template_id" value="${sessionScope.cemetery_template_id }">
            <input type="hidden" name="cemetery_price" id="cemetery_price" value="${cemeterytemplate.cemetery_template_price }">
            <input type="hidden" name="cemetery_number" id="cemetery_number" value="${sessionScope.cemeteryNumber}">
            <input type="hidden" name="cemetery_number_price" id="cemetery_number_price" value="${sessionScope.cemeteryNumberPrice}">
             <input type="hidden" name="num_type" id="num_type" value="${sessionScope.Numtype}">
             <input type="hidden" name="cemetery_tem_name" id="cemetery_tem_name" value="${cemeterytemplate.cemetery_template_name}">
            </form>
            
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
$(function (){
	
	check();
});
	function check(){
		
		var sum=0;
		if($("#type").attr("checked")){
			$("#cemetery_template_img").attr("src","/goods/${cemeterytemplate.cemetery_template_img}");
			$("#cemetery_template_name").html("${cemeterytemplate.cemetery_template_name}");
			$("#cemetery_template_price").html("${cemeterytemplate.cemetery_template_price }福币")
			$("#cemetery_template_id").val("${cemeterytemplate.cemetery_template_id}");
			$("#cemetery_price").val("${cemeterytemplate.cemetery_template_price}");
			sum=sum+${cemeterytemplate.cemetery_template_price==null?"0":cemeterytemplate.cemetery_template_price };
		
			
		}else{
			$("#cemetery_template_img").attr("src","/goods/model_html/2007111945097249.jpg");
			$("#cemetery_template_name").html("简约模板");
			$("#cemetery_template_price").html("0")
			$("#cemetery_template_id").val("4");
			$("#cemetery_price").val("0");
		}
		if($("#num").attr("checked")){
			$("#list_number_number").html("${sessionScope.cemeteryNumber}");
			$("#list_message").html("${sessionScope.Numtype}");
			$("#cemeteryNumberPrice").html("${sessionScope.cemeteryNumberPrice}福币");
			$("#cemetery_number").val("${sessionScope.cemeteryNumber}");
			$("#cemetery_number_price").val("${sessionScope.cemeteryNumberPrice}");
			$("#num_type").val("${sessionScope.Numtype}");
			
			sum=sum+${sessionScope.cemeteryNumberPrice==null?"0":sessionScope.cemeteryNumberPrice};
			
		}else{
			$("#list_number_number").html("${sessionScope.cemetery_id_def}");
			$("#list_message").html("默认墓园号");
			$("#cemeteryNumberPrice").html("0");
			$("#cemetery_number").val("${sessionScope.cemetery_id_def}");
			$("#cemetery_number_price").val("0");
			$("#num_type").val("默认墓园号");
		}	
		$("#sum").html(sum+"福币");
		$("#huiyuan").html(sum*${vipcount});
		if(sum>${user.user_fb}){
			$("#pay_balance").show();
			$("#yue").html((${user.user_fb}-sum)+"福币");
			$("#f").show();
			$("#c").hide();
			$("#cz").attr("href","/order/pay?orderRmb="+(sum-${user.user_fb})/100+"&orderNum="+(sum-${user.user_fb}));
		}else{
			$("#pay_balance").hide();
			$("#f").hide();
			$("#c").show();
		}
	}
	function toCreate(){
		$("#Form").submit();
	}
	
</script>
        </div>
        </body>
        </html>