<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <link href="/resources/index/css/user_corpus_memorial.css" rel="stylesheet">
    <link href="/resources/index/css/user_sacrifice_corpus.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>
</head>
<body>

        <!-- 文章管理-内容-->
        <div>
            <div class="content_btn">

                <!--文章管理-写文集 -->
                <a href="/cemetery/toAddWenji" target="mainFrame">
                    <div class="user_new_pro">
                        <img src="/resources/index/images/user_add.png">
                        <div>写文集</div>
                    </div>
                </a>

                <!--文章管理-写追思 -->
                <a href="/cemetery/toAddZhuisi" target="mainFrame">
                    <div class="user_new_pro">
                        <img src="/resources/index/images/user_add.png">
                        <div>写追思</div>
                    </div>
                </a>


                <div class="user_muyuan_table">
                    <img src="/resources/index/images/user_content_tog.png">
                    <div>文章列表</div>
                </div>
            </div>

            <div class="user_content_all user_wenzhang_title">
                <ul class="table_title">
                    <li class="table_li_01">标题</li>
                    <li class="table_li_02">作者</li>
                    <li class="table_li_03">所属墓园</li>
                    <li class="table_li_04">时间</li>
                    <li class="table_li_05">操作</li>
                </ul>
              <c:choose>
              <c:when test="${not empty varList }">
              <c:forEach items="${varList }" var="var" varStatus="vs">
                <ul>
                    <li class="table_li_01">
                    <c:if test="${var.article_type==1}"><img src="/resources/index/images/liuyan.png"><a href="/cemetery/viewArticle?artId=${var._id}" target="_blank">${var.article_name}</a>  </c:if>
					<c:if test="${var.article_type==2}"><img src="/resources/index/images/wenzhang.png"><span>${var.article_name}</span></c:if>
					
                    </li>
                    <li class="table_li_02">${var.user_name}</li>
                    <li class="table_li_03">${var.cemetery_name}</li>
                    <li class="table_li_04">
                     <c:set target="${myDate}" property="time" value="${var.article_add_time*1000}"/> 
                    <fmt:formatDate pattern="yyyy-MM-dd HH:MM:SS" value="${myDate}" type="both"/> </li>
                    <li class="table_li_05x">
                     <c:if test="${var.article_type==1}"> 
                     <a href="/cemetery/ToUpdateArticle?artID=${var._id}"><img src="/resources/index/images/article_write2.png"></a>  </c:if>
                     <a href=""><img src="/resources/index/images/article_message.png"></a>
                     <a href="/cemetery/delArt?ArtId=${var._id}"><img src="/resources/index/images/article_delete.png"></a>
                    </li>
                </ul>
            
              </c:forEach>
              
              </c:when>
              </c:choose>
             
<script type="text/javascript">
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>
            
            </div>
             <div class="number_page_bottomx">
              <form name="ff" id="ff" method="post"></form>
                <ff:page mhFrom="ff" showReSize="flase" counts="false"  field="page" onlyOneShow="false" showListNo="false" action="cemetery/getAtricle" />
            </div>
        </div>

</body>
</html>

