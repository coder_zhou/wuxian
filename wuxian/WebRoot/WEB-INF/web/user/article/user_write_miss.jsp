<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title>${applicationScope.SYSNAME}</title>
<meta name="description" content="${applicationScope.seodescription}" />
<meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <link href="/resources/index/css/user_corpus_memorial.css" rel="stylesheet">
    <link href="/resources/index/css/user_sacrifice_corpus.css" rel="stylesheet">
    <link href="/resources/index/css/user_write_miss.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>
<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
  </head>
  <body>




  
<form action="/cemetery/addZhuisi" id="form" method="post">



        <!-- 文集追思--中间部分内容-->
        <div class="wenji_content">

            <!-- 内容部分标题-->
            <div class="corpus_content_title">
                <div class="content_title_log"></div>
                <div>文字信息-文集追思-写追思</div>
            </div>

            <div class="wenji_write_content">
                <div class="write_content_title">
                    <img src="/resources/index/images/article_write.png">
                    <div>写追思</div>
                </div>

                <!--墓园公开设置-->
                <div class="miss_set">
                    <div class="miss_set_belong">所属墓园号
                    
                    <input type="text" name="cemetery_number" id="cemetery_number" value="" maxlength="8" onblur="IsTrue();"  onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"  
                    onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}" placeholder="请输入墓园号">
                    </div>
                    <div class="miss_set_kind">公开设置</div>
                    <div class="miss_set_secret">
                        <div><input name="article_state" value="1" type="radio" checked="checked">完全公开</div>
                    <!--     <div><input name="article_state"  value="2" type="radio">亲友可见</div>  -->
                        <div><input name="article_state" value="3" type="radio">完全保密</div>
                    </div>
                </div>
                 <input type="hidden" name="article_type" value="2"/>
                <!--编辑内容文本框-->
                <div class="miss_article_content">
                    <textarea name="article_name" id="article_name" onkeyup="checkLen(this);"></textarea>
               <div>您还可以输入 <span id="count">500</span> 个文字</div> 
                    <div class="miss_article_share">
  
                        <div class="miss_article_relaease" onclick="sub();">发布</div>
                    </div>
                </div>



            </div>

        </div>
</form>

<script type="text/javascript">

function sub(){
	
	if($("#article_name").val()==""){
		alert("请输入追思")
		$("#article_name").focus();
		return false;
	}
	if($("#cemetery_number").val()!=""){
		IsTrue();
	}
	
$("#form").submit();
}



function  IsTrue(){
	if($("#cemetery_number").val()!=""){
	$.ajax({
		type: "POST",
		url: '/cemetery/IsTrue?cemetery_number='+$("#cemetery_number").val(),
		//beforeSend: validateData,
		success: function(data){
			$("#cemetery_number").tips({
				side:1,
	            msg:data,
	            bg:'#AE81FF',
	            time:5
	        });
		if(data=="无效墓园号")
				{
			
			return false;
				}else{
					return true;
				}
		}
	});
	}
}

function checkLen(obj) {  

	var maxChars = 500;//最多字符数  

	if (obj.value.length > maxChars)  obj.value = obj.value.substring(0,maxChars);  

	var curr = maxChars - obj.value.length;  

	document.getElementById("count").innerHTML = curr.toString(); 

	} 
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
</script>

</body>

</html>
