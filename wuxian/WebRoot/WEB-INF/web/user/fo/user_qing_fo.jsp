<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>全球祭祀祈福网->个人管理->礼佛管理</title>
    <link href="/resources/index/css/user_qing_fo.css" rel="stylesheet">
      <script src="/resources/index/js/jquery-1.8.2.js"></script>
   <style>
      .window_close{
    position: absolute;
    top: 15px;
    right: 15px;
}
.window_title{
    height:39px;
    background-color: #5abbf4;
    text-align: center;
    font:bold 18px 'Microsoft YaHei';
    color: white;
    line-height:39px;
    border-bottom: 1px solid #2a95d4;
}
.qs_fo_bg{
    /*display: none;*/
    position: absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: black;
    z-index:999999999999999990;
    -moz-opacity: 0.6;
    opacity:.60;
    filter: alpha(opacity=60);
}
.qs_fo_content{
    /*display: none;*/
    width:450px;
    position: absolute;
    top:50%;
    left:50%;
    margin: -120px 0 0 -250px;
    z-index:999999999999999991;
    overflow: auto;
    border-radius: 3px;
}
.qs_window_content{
    width:450px;
    height:250px;
    position: relative;
    background-color:white;
    padding: 13px 0 ;
    font: 14px 'Microsoft YaHei';
    color: #333;
    overflow: hidden;
}
.fz_infor{
    width: 150px;
    height: 160px;
    background-color: #e5f5ff;
    padding: 10px 0 0 16px;
    margin: 0 auto;
}
/*捐赠对象图片*/
.jz_object{
    width: 120px;
    height: 120px;
    border: 1px solid white;
}
.jz_object_name{
    width: 120px;
    text-align: center;
    font: 14px 'Microsoft YaHei';
    color:#fd844e;
}
.fo_message{
    width: 100%;
    text-align: center;
    margin: 10px 0;
    color: #5abbf4;
}
.fo_message span{
    color:#fd844e;
}
.qs_btn div{
    float: left;
    width: 80px;
    height: 30px;
    background-color: #5abbf4;
    border-radius: 4px;
    margin:10px;
    color: white;
    text-align: center;
    font: 16px 'Microsoft YaHei';
    line-height: 30px;
}
.qs_btn{
    width:200px;
    height:50px;
    margin: 0 auto;
}
      
      </style>
</head>
<body>

<!-- 礼佛管理-内容-->
<div>
<div class="content_btn">

    <!-- 礼佛管理按钮-->
    <a href="/fo/toqingfo" target="mainFrame">
        <div class="user_new_pro">
            <img src="/resources/index/images/user_add.png">
            <div>请佛</div>
        </div>
    </a>


    <div class="user_muyuan_table">
        <img src="/resources/index/images/user_content_tog.png">
        <div>我的供奉</div>
    </div>
</div>

<div class="tree_content">
    <!-- 我的供奉-->
    <div class="my_tree">
    
    <c:choose>
    <c:when test="${not empty fc }">
    <c:forEach items="${fc }" var="fc">
       <div class="each_tree">
            <div class="each_tree_img">
                <img src="/goods/${fc.fo.foMaterialUrl }">
                <div class="text"></div>
                <div class="text_infor">
                    <div>
                        <img src="/resources/index/images/qing_fo_gf.png">
                        <div class="text_infor_num">拜佛  ${fc.personalWorshipNum}</div>
                    </div>
                </div>
            </div>
            <div class="tree_name">${fc.personalWorshipFoName }</div>
            <div class="tree_create_user">
            <c:set target="${myDate}" property="time" value="${fc.personalWorshipAddTime*1000}"/> 
                  <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
                  </div>
            <div class="tree_create_button">
                <div class="tree_create_btn"><a target="_blank" href="/webLifo/queryFoPersonalWorshipForBz?personal_worship_id=${fc.personalWorshipId}">管理</a></div>
                <div class="tree_create_btn" onclick="songfo('/goods/${fc.fo.foMaterialUrl }','${fc.personalWorshipFoName }','${fc.personalWorshipFoId}','${fc.personalWorshipId}');"><a>送佛</a></div>
            </div>


        </div>
    </c:forEach>
    </c:when>
    </c:choose>
    </div>
</div>
<div class="my_wishing">
    <div class="my_wishing_list">
        <img src="/resources/index/images/user_content_tog.png">
        <div>许愿还愿</div>
    </div>

    <div class="my_wishing_list_content">
        <ul class="list_content_first">
            <li class="list_first">贡品</li>
            <li class="list_second">佛祖</li>
            <li class="list_third">许愿</li>
            <li class="list_four">供奉人</li>
            <li class="list_five">时间</li>
            <li class="list_seven">还原状态</li>
            <li class="list_six">操作</li>
        </ul>
        
        <c:choose>
        <c:when test="${not empty fw }">
        <c:forEach items="${fw }" var="fw">
             <ul class="list_content">
            <li class="list_first"><img src="/resources/index/images/create_tree_goods.png"></li>
            <li class="list_second"><span>[供]</span>${fw.fo_name }</li>
            <li class="list_third">${fw.wish_info }</li>
            <li class="list_four">${fw.user_name }</li>
            <li class="list_five"><c:set target="${myDate}" property="time" value="${fw.wish_add_time*1000}"/> 
                  <fmt:formatDate pattern="yyyy-MM-dd" value="${myDate}" type="both"/></li>
            <li class="list_seven no_huan_wish">
            <c:if test="${fw.wish_come_true_type==1 }">不还愿</c:if>
            <c:if test="${fw.wish_come_true_type==2&&fw.wish_type==1 }">未还愿</c:if>                         
            <c:if test="${fw.wish_come_true_type==2&&fw.wish_type==2 }">已还愿</c:if>                                      
            <c:if test="${fw.wish_come_true_type==2 }">
            ${fw.wish_come_true_price }福币
            </c:if>
            </li>
            <li class="list_six">
            <c:if test="${fw.wish_come_true_type==1 }"> <img src="/resources/index/images/no_huan_wish.png"></c:if>
            <c:if test="${fw.wish_come_true_type==2&&fw.wish_type==1 }"><img src="/resources/index/images/huan_wish.png" onclick="huanyuan('${fw._id }');"></c:if>                         
            <c:if test="${fw.wish_come_true_type==2&&fw.wish_type==2 }"> <img src="/resources/index/images/finish_huan_wish.png"></c:if>                                      
                
                <img src="/resources/index/images/fo_my_wish_delete.png" onclick="delYuan('${fw._id }')">
            </li>
        </ul>
        </c:forEach>
        </c:when>
        </c:choose>
        
       

    </div>

</div>


</div>
<div class="qs_fo_bg" style="display: none"></div>
    <div class="qs_fo_content" style="display: none">
        <div class="window_title">送 佛</div>
        <img src="/resources/images/close_window.png" class="tsjn_window_close  window_close">
        <div class="qs_window_content">
            <div class="fz_infor">
                <img src="images/jz_object.png" id="imm" class="jz_object">
                <div class="jz_object_name" id="ffname">观世音菩萨</div>
            </div>
            <div class="fo_message">奉送<span class="fo_name" id="fo_name">观世音菩萨</span>需要<span class="fo_money">500</span>福币会员价<span id="huiyuan"></span>,您是否要奉送？</div>
            <div class="qs_btn">
                <div class="qs_cancel">取消</div>
                <div class="qs_ensure"><a href="" id="ar">确定</a></div>

            </div>
        </div>
    </div>

</body>
</html>
    <script type="text/javascript">
    function huanyuan(obj){
    	  window.location.href="/fo/huanyuan?id="+obj; 
    }
    function delYuan(obj){
    	if(confirm("确定删除么?")){
    		 window.location.href="/fo/delYuan?id="+obj; 
    	}
    }
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
function songfo(imgStr,foname,id,pd){
    $("#ffname").html(foname);
    $("#imm").attr("src",imgStr);
    $("#fo_name").html(foname);
    $(".qs_fo_bg").show();
    $(".qs_fo_content").show();
    $("#ar").attr("href",'/fo/songfo?id='+pd+'&foid='+id);
    $("#huiyuan").html(500*${vipcount});
    $(".window_close,.qs_cancel").click(function (){
		  $(".qs_fo_bg").hide();
	      $(".qs_fo_content").hide();
	});
	}
</script>