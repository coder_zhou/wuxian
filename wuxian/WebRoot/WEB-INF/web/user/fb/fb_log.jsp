
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>全球祭祀祈福网-个人后台页-墓园管理</title>
    <link href="/resources/css/user_fo_bi_manage.css" rel="stylesheet">
    <script src="/resources/js/jquery-1.9.1.min.js"></script>
    	<script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
</head>
<body>

<!--墓园管理-->
<div>
    <div class="content_btn">

        <!-- 内容标题-->
        <div class="user_fb_table">
            <img src="/resources/images/write_cemetery_infor.png">
            <div>交易记录</div>
        </div>
 <form id="Form" action="/fbManage" method="post">
        <div class="fb_search" onclick="sub();">
            <div><img src="/resources/images/fo_bi_manage.png"></div>
            <div >查询</div>
        </div>
        <div class="time_to"><input type="text" class="span10 date-picker"   name="end"  id="end"  onfocus="WdatePicker({minDate:$('#stat').val(),maxDate:'%y-%M-%d'})"    value="${pd.end }" ></div>
        <div class="time_from"><input type="text" class="span10 date-picker"    name="stat"  id="stat"   onfocus="WdatePicker({skin:'whyGreen',maxDate:'%y-%M-%d'})" 	 value="${pd.stat }"></div>
        <div class="fb_infor_title" >查询历史明细</div>
    </div>
</form>
    <div class="container">
        <ul class="cont_title">
            <li class="list_1">&nbsp;&nbsp;交易时间</li>
            <li class="list_2">&nbsp;&nbsp;金额</li>
            <li class="list_3">交易类型</li>
            <li class="list_4">&nbsp;&nbsp;消费说明</li>
            <li class="list_5">&nbsp;&nbsp;状态</li>
        </ul>
  
        		<c:choose>
        				<c:when test="${ not empty list}">
        				<c:forEach items="${list }" var="var" varStatus="vs">
        				      <ul class="cont_content">
            <li class="list_1 cont_list_1">
<c:set target="${myDate}" property="time" value="${var.buy_time*1000}"/>
<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
            </li>
            <li class="list_2 cont_list_2">-<span>${var.consumption_fb }福币</span></li>
            <li class="list_3 cont_list_3">消费</li>
            <li class="list_4 cont_list_4">${var.consumption_info }</li>
            <li class="list_5 cont_list_5">交易成功</li>
        </ul>
        				</c:forEach>
        				</c:when>
        				</c:choose>
        <div class="pagination" style="float: right;padding-top: 0px;margin-top: 0px;">
     
			<ff:page mhFrom="Form" showReSize="false"   field="page" onlyOneShow="true" showListNo="true" counts="false"   action="fbManage"/>
    </div>
    </div>


</div>
<script>
function sub(){
	$("#Form").submit();
	
}
$(window.parent.document).find("#mainFrame").load(function(){
	var main = $(window.parent.document).find("#mainFrame");
	var thisheight = $(document).height()+30;
	main.height(thisheight);
	});
	
</script>
</body>
</html>