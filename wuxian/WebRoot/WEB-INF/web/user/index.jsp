<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head lang="en">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1,EmulateIE9" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <title>${applicationScope.SYSNAME}</title>
	<meta name="description" content="${applicationScope.seodescription}" />
    <meta name="keywords" content="${applicationScope.seokeywords}">
    <link href="/resources/index/css/user_bg_muyuan.css" rel="stylesheet">
    <link href="/resources/index/css/binding.css" rel="stylesheet">
    <script src="/resources/index/js/jquery-1.8.2.js"></script>
    <script src="/resources/index/js/user_bg_muyuan.js"></script>
    	<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
	<!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
		<script>
			$(function(){
				  $(".shut").click(function(){
				    	$(".bang_ding_bg,.binding_container").css({display:"none"});
				    });
				    

				    
				    $(".confirm").click(function(){
				    	$(".bang_ding_bg,.binding_container,.binding_ok_container").css({display:"none"});
				    });
				    
				    $(".shut_all").click(function(){
				    	$(".bang_ding_bg,.binding_container").css({display:"block"});
				    	$(".binding_ok_container").css({display:"none"});
				    });
			});
		</script>
</head>
<body>
<div class="main_wrap">
    <div class="banner_bg_black"></div>
    <div class="banner_bg_blue"></div>
</div>
<div class="main">
<!--title部分-->
    <div class="header">
        <a href="<%=basePath%>index">
        <img src="/resources/index/images/user_bg_logo.png">
        </a>
        <ul>
            <li class="header_souye"><a href="<%=basePath%>index">首页</a></li>
            <li><a href="#">帮助</a></li>
        </ul>
    </div>
    <div class="banner"></div>
    <div class="banner_bg_whitw"></div>

    <!-- 用户详细信息-->
<jsp:include page="top.jsp" flush="true" />

    <div class="tab_content">
        <!-- 墓园管理-内容-->
   <iframe name="mainFrame" id="mainFrame" frameborder="0" src="/cemetery/cemeteryList" style="margin:0 auto;width:100%;height: 600px;; " scrolling="no" ></iframe>  
      

        </div>

 
    </div>
    
    
    <c:if test="${ um.user_mobile_phone ==0}">

		<div class="bang_ding_bg"></div>
		<div class="binding_container">
			<div class="about_content">
		
		         <div class="text1">绑定手机号，账号更安全，另有<span>100福币</span>赠送！</div>
				 <div class="text2">云祈福会对您的手机信息严格保密。</div>
				 <input type="text" name="" value=""  id="em" class="input1" placeholder="请输入手机号"  onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')" maxlength="11"/>
				 <input type="text" name="" value="" id="code" class="input2" placeholder="验证码"/>
				 <a href="#" class="yzm" id="yzm"  onclick="check();"  >发送验证码</a>
				 <div class="instructions">如果收不到验证码<br>请联系客服：<span>400-0240-535</span></div>
				 <div class="binding"  onclick="bindphone();"></div>
				 <div  class="shut"><img src="/resources/images/shut.png"/></div>
			</div>
	</div>
	
	<!-- 绑定成功 -->
	<div class="binding_ok_container">

			<div class="about_content">
		
		         <div class="ok"></div>
				 <div class="text3">恭喜您，绑定手机成功</div>
				 <div class="text4">您绑定的手机为 <span id="pp"></span><br/>可用于找回密码，请妥善保管</div>
				 <div  class="confirm">确&nbsp;&nbsp;&nbsp;&nbsp;认</div>
				 <div   class="shut    shut_all"><img src="/resources/images/shut.png"/></div>
			
			</div>

	</div>
    </c:if>

</div>
<script>
	function  check(){
		if($("#em").val()==""){

			$("#em").tips({
				side:2,
	            msg:'请输入手机号',
	            bg:'#AE81FF',
	            time:3
	        });
			
			$("#em").focus();
			return false;
		}else if(!checkMobile()){       
			$("#em").tips({
				side:2,
	            msg:'请输入正确的手机号码',
	            bg:'#AE81FF',
	            time:3
	        });
			
			$("#em").focus();
			return false;
		}else if(checkMobile()){
				$.ajax({
					type: "POST",
					url: '/getUser?username='+$("#em").val(),
					//beforeSend: validateData,
					success: function(data){
						
						if(data=="此帐号已存在")
							{
							$("#em").tips({
								side:1,
					            msg:data,
					            bg:'#AE81FF',
					            time:3
					        });
							return false;
			
							}else{
							   	var  a=$("#em").val();
								var url = "/sendSms?phone="+a;
							    $.get(url,function(data){
							     if(data=="success"){
							    	 alert("已发送验证码");
							    	 time(this);
							     }
							    }
							    );
							}
					}
				});
			}
 
		
	}
    var wait=60;
    function time(o) {
        if (wait == 0) {
            o.removeAttribute("disabled");
            o.value="再次获取验证码";
            o.style.backgroundColor="#4bacf1";
            wait = 60;
        } else {
           $("#huo_qu_ma").attr("disabled", "true");
   //         o.value="重发验证码(" + wait + ")";
            $("#huo_qu_ma").val("重发验证码(" + wait + ")");
            $("#huo_qu_ma").css("background-color","#999");
                       
   //         o.style.backgroundColor="#999";
            wait--;
            setTimeout(function() {
                        time(o)
                    },
                    1000)
        }
       
    }
    
    //验证电话  
    function checkMobile() {  
        var s=$("#em").val();
        if (trim(s) != "") {  
            var regu = /^[1][3-9][0-9]{9}$/;  
            var re = new RegExp(regu);  
            if (re.test(s)) {  
                return true;  
            } else {  
            	$("#em").tips({
					side:2,
		            msg:'请输入正确的手机号码',
		            bg:'#AE81FF',
		            time:3
		        });
				
				$("#em").focus();
				return false;

            }  
        }  
    } 
    
    function trim(str) { //删除左右两端的空格  
        return str.replace(/(^\s*)|(\s*$)/g, "");  
    }  
    
    function bindphone(){
    	if($("#em").val()==""){

			$("#em").tips({
				side:2,
	            msg:'请输入手机号',
	            bg:'#AE81FF',
	            time:3
	        });
			
			$("#em").focus();
			return false;
		}else   if($("#code").val()==""){

			$("#code").tips({
				side:2,
	            msg:'请输入手机号',
	            bg:'#AE81FF',
	            time:3
	        });
			
			$("#code").focus();
			return false;
		}
		else 
			if(checkMobile()){
    	     var phone=$("#em").val();
    	     var code =$("#code").val();
    	     var url = "/bindPhone?phone="+phone+"&code="+code;
    	    $.get(url,function(data){

    	     $("#pp").html(data.phone);
    	   	$(".bang_ding_bg,.binding_container").css({display:"block"});
	    	$(".binding_ok_container").css({display:"block"});

    	    },"json");
    	}
    	
    }
    
    
</script>
</body>
</html>