<%@ page language="java" import="java.util.*" pageEncoding="utf-8" buffer="none" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <title>全球祭祀祈福网->个人->拜佛礼佛</title>

    <link href="<%=path%>/resources/css/lifo/li_fo.css" rel="stylesheet" charset="UTF-8"/>
    <script src="<%=path%>/resources/js/jquery/jquery-1.8.2.js"></script>
    <script src="<%=path%>/resources/js/jquery/jq-dr.js"></script>
    <script src="<%=path%>/resources/js/lifo/li_fo.js" charset="UTF-8"></script>
	<script src="<%=path%>/resources/js/jquery/jquery.form.2.36.js"  type="text/javascript"></script>
    <script  type="text/javascript" src="/resources/My97DatePicker/WdatePicker.js"></script>
    <link href="/resources/css/jquery.dataTables.min.css"  rel="stylesheet" charset="UTF-8" />
    <!--[if lt IE 9]>
	    <script src="/resources/index/js/html5shiv.min.js"></script>
	    <script src="/resources/index/js/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
	
		$(document).ready(function(){
			toCleanImg();
		});
		
		
		//切换墓园祭祀、墓园布置
    	function onChooseJisiOrBuzhi(flag, worship_id,chooseflag, is_user_cemetery_flag){
			if(flag == 1){
    			$("#div_jisi").attr("class","");
    			$(".jisi").css({backgroundColor:"#fd844e"});
    			
    			if(is_user_cemetery_flag > 0 ){
    				$(".buzhi").css({backgroundColor:"#069dd5"});
    			}
   				$("#div_buzhi").attr("class","content_hide");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
			if(flag == 2){
				$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#fd844e"});
	    		}
    			$("#div_buzhi").attr("class","");
    			
    			$("#div_tuceng").attr("class","content_hide");
    			$(".choose_ceng").css({backgroundColor:"#069dd5"});
			}
    		if(flag == 3){
    			$("#div_jisi").attr("class","content_hide");
    			$(".jisi").css({backgroundColor:"#069dd5"});
    			if(is_user_cemetery_flag > 0 ){
	    			$(".buzhi").css({backgroundColor:"#069dd5"});
	    		}
	    		$("#div_buzhi").attr("class","content_hide");
    			$("#div_tuceng").attr("class","");
    			$(".choose_ceng").css({backgroundColor:"#fd844e"});
    		}
    		
    		$("#tabflag").val(flag);
    	}
    	
    	//弹出二级素材
    	function onShowMaterialsClass2(materials_class_id){
    		var src = "/webLifo/queryMaterialsList?materials_class_id=" + materials_class_id;
			$("#iframe_materials").attr("src",src);
    		$("#showMaterialsList").attr("style","display: block; ");
    	}
    	
    	//保存
    	function save(){
    		var worship_id = $("#input_worship_id").val();
			var arr = "";
			var arrTab=$('.inputs_materials_id');
       		for(var i=0;i<arrTab.length;i++){
       			var imgId = $(arrTab[i]).val();
       			var cemetery_materials_id = $(arrTab[i]).val();
		    	var zindex = $(arrTab[i]).attr("z-index");
		    	var x = $(arrTab[i]).attr("left");
		    	var y = $(arrTab[i]).attr("top");
		    	var width = $(arrTab[i]).attr("width");
		    	var height = $(arrTab[i]).attr("height");
		    	
		    	var all = cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex ;
		    	
		    	arr +=  cemetery_materials_id +"_"+ x +"_"+ y + "_"+width+"_"+height+"_"+zindex +",";
       		}
			var tempstr = '<input name="arr" type="hidden" value="' + arr + '" /><input name="fo_id" type="hidden" value="' + worship_id + '" />';
			$("#ff").append(tempstr);
			document.ff.action = "<%=path%>/webLifo/savePubWorshipMaterialsForUpd";
			$("#ff").ajaxSubmit(function(result){
				location.href="<%=path%>/webLifo/queryFoPubWorshipForBz?fo_id="+$("#input_worship_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
			});
    	}
    	
    	//显示结算弹出窗
    	function onShowJsDiv(user_fb){
    		var vipcount = $('#vipcount').val();
    		var arrTab=$('.class_materials');
    		var str = '';
    		var fb_sum = 0 * 1;
    		str = str + '<div class="confirm_container" >';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				fb_sum = fb_sum + $(arrTab[i]).attr("materials_price_sum") * 1;
    			}
    			
				if(i % 2 == 0){
					str = str + '<div class="module_first">';
				}else{
					str = str + '<div class="module_second">';
				}
				
				if($(arrTab[i]).attr("ifCheck") == 1){
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" checked="checked" >';
				}else{
					str = str + '<input type="checkbox" class="checkbox" onclick="onChooseJsCheckbox(' + user_fb + ',' + $(arrTab[i]).val() + ');" id="checkbox_' + $(arrTab[i]).val() + '" >';
				}
                str = str + '<div class="module_content">';
                str = str + '<div class="module_title">' + $(arrTab[i]).attr("materials_name") + '</div>';
                str = str + '<img src="' + $(arrTab[i]).attr("img_url") + '" class="module_goods">';
                str = str + '<div class="module_goods_infor">';
                if($(arrTab[i]).attr("parent_id") != 4){
                	str = str + '<div class="aging">';
	                str = str + '<div class="aging_first">时效</div>';
	                str = str + '<div class="reduce" onclick="onAddDate(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >-</div>';
	                str = str + '<div class="number_sum" id="div_date_count_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_measured_value") + '</div>';
	                if($(arrTab[i]).attr("materials_measured") == 1){
	                	str = str + '<div>天 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 2){
	                	str = str + '<div>月 </div>';
	                }else if($(arrTab[i]).attr("materials_measured") == 3){
	                	str = str + '<div>年</div>';
	                }else{
	                	str = str + '<div> - </div>';
	                }
	                str = str + '<div class="add" onclick="onAddDate(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')" >+</div>';
	                str = str + '</div>';
                }
                
                str = str + '<div class="number">';
                str = str + '<div class="number_first">数量</div>';
                str = str + '<div class="reduce" onclick="onAddCount(-1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">-</div>';
                str = str + '<div class="number_sum" id="div_number_sum_' + $(arrTab[i]).val() + '">' + $(arrTab[i]).attr("materials_count") + '</div>';
                str = str + '<div>个</div>';
                
                if($(arrTab[i]).attr("materials_class_id") == 10){//背景,不能增加数量
                	str = str + '<div class="add" onclick="alert(\'背景只能买一个！\');">+</div>';
                }else{
                	str = str + '<div class="add" onclick="onAddCount(1, ' + $(arrTab[i]).val() + ', ' + user_fb + ')">+</div>';
                }
                
                str = str + '</div>';
                str = str + '<div class="sum">';
                str = str + '<div class="sum_first">小计</div>';
                str = str + '<div class="sum_price" id="div_sum_price_' + $(arrTab[i]).val() + '"><span>' + $(arrTab[i]).attr("materials_price_sum") + '</span>福币</div>';
                str = str + '</div>';
                str = str + '</div>';
                
//                 str = str + '<div class="line"></div>';
//                 str = str + '<div class="kind_price">';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/kinship_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_family") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>' + $(arrTab[i]).attr("materials_wealth") + '</span>';
//                 str = str + '</div>';
//                 str = str + '<div>';
//                 str = str + '<img src="<%=path%>/resources/images/wealth_price.png">';
//                 str = str + '<span>200</span>';
//                 str = str + '</div>';
//                 str = str + '</div>';
                str = str + '</div>';
                str = str + '</div>';
            }
            
            fb_sum = fb_sum * 1
            str = str + '</div>';
            str = str + '<div class="pay">';
            str = str + '<div class="pay_sum">总计：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + fb_sum + '</span>福币</div>';
            
            
            
            str = str + '<div class="pay_sum">会员价：</div>';
            str = str + '<div class="pay_price" ><span id="span_pay_price">' + Math.floor(fb_sum * vipcount) + '</span>福币</div>';
           
           
            str = str + '</div>';
            if(user_fb < fb_sum){
            	str = str + '<div class="balance">';
	            str = str + '<div class="pay_sum">余额不足：</div>';
	            str = str + '<div class="pay_price"><span>' + (user_fb - fb_sum) + '</span>福币</div>';
	            str = str + '</div>';
            }
            str = str + '<div class="pay_btn">';
            if(user_fb < fb_sum && ($('#userr_user_id').val() == null || $('#userr_user_id').val() == '')){
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >补充福币</div>';
            }else{
            	str = str + '<div class="fill_money" onClick="onSaveMaterials();" >确定</div>';
            }
			
            str = str + '<div class="pay_cancel" onclick="onClear();">取消</div>';
            str = str + '</div>';
            $("#div_confirm_container_js").html(str);
    		//显示弹出窗
    		$("#show").attr("style","display: block; ");
			$(".bg_confirm,.show_confirm").attr("style","display: block; ");
    	}
    	
    	//添加减少数量
    	function onAddCount(flag, materials_id, user_fb){
    		var materials_count = $("#input_materials_id_" + materials_id).attr("materials_count");
    		if(flag == -1 && materials_count == 1){
    			return false;
    		}else{
    			materials_count = materials_count * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_count", materials_count);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//添加减少时效
    	function onAddDate(flag, materials_id, user_fb){
    		var materials_measured_value = $("#input_materials_id_" + materials_id).attr("materials_measured_value");
    		if(flag == -1 && materials_measured_value == 1){
    			return false;
    		}else{
    			materials_measured_value = materials_measured_value * 1 + flag * 1;
    		}
    		$("#input_materials_id_" + materials_id).attr("materials_measured_value", materials_measured_value);
    		$("#input_materials_id_" + materials_id).attr("materials_price_sum", $("#input_materials_id_" + materials_id).attr("materials_count") * $("#input_materials_id_" + materials_id).attr("materials_price") * $("#input_materials_id_" + materials_id).attr("materials_measured_value"));
    		onShowJsDiv(user_fb);
    	}
    	
    	//关闭结算页面
    	function onHideDivJs(){
    		$("#show").attr("style","display: none; ");
			$(".bg_confirm,.show_confirm").attr("style","display: none; ");
    	}
    	
    	//结算
    	function onSaveMaterials(){
    		var arrTab=$('.class_materials');
    		var fb_sum = 0;
    		var str = '<input type="hidden" name="fo_id" value="' + $("#input_worship_id").val() + '"/><input type="hidden" name="type_flag" value="' + $("#input_type_flag").val() + '"/>';
    		for(var i=0;i<arrTab.length;i++){
    			if($(arrTab[i]).attr("ifCheck") == 1){
    				str = str + '<input type="hidden" name="materials_info" value="'+$(arrTab[i]).val()+'_'+$(arrTab[i]).attr("materials_measured_value")+'_'+$(arrTab[i]).attr("materials_count")+'" />';
    			}
    		}
    		$("#ff").html(str);
    		
    		document.ff.action = '<%=path%>/webLifo/saveFoPubWorshipMaterials';
			$("#ff").ajaxSubmit(function(result){
				if(result.flag == 0){
					location.href="<%=path%>/webLifo/queryFoPubWorshipForBz?fo_id="+$("#input_worship_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
				}else{
					alert("余额不足，跳转支付页面！");
				}
			});
    	}
    	
    	/// 去开光
    	function kaiguangle(){
	  		window.location.href=""; 
       	}
       	
    	function checkLen(obj) {  
    		var maxChars = 200;//最多字符数  
    		if (obj.value.length > maxChars)  obj.value = obj.value.substring(0,maxChars);  
    		var curr = maxChars - obj.value.length;  
    		document.getElementById("count").innerHTML = curr.toString(); 
    	} 
    	
    	function checkLen1(obj) {  
    		var maxChars = 200;//最多字符数  
    		if (obj.value.length > maxChars)  obj.value = obj.value.substring(0,maxChars);  
    		var curr = maxChars - obj.value.length;  
    		document.getElementById("count1").innerHTML = curr.toString(); 
    	} 
    		
    		
    	//删除素材
   		function onDelMaterials(worship_materials_id, worship_id){
           if(worship_materials_id != null && worship_materials_id != ''){
    		$.ajax({
				type: "POST",
				url: '/webLifo/delPubWorshipMaterialsByWorshipMaterialsId',
		    	data: {worship_materials_id : worship_materials_id},
				dataType:'json',
				cache: false,
				success: function(data){
					 if(data == 0){
						var namelist = $("li[class='cc']");
						for(var i=0; i<namelist.length; i++){
						 	if($(namelist[i]).attr("materials_info") == wish_tree_materials_id){
						 		namelist[i].remove();
						 	}
						}
					}else{
						alert('操作失败，请联系管理员！');
					}
				}
			});
		}else{
   			alert("请选择图层！");
   		}
		
   	}
   	
   	//控制素材显示隐藏
   	function onUpdMaterialsShow(worship_materials_id, worship_id,bg){
   		//显示隐藏更新刷新tab页面标志位
   		$("#tabflag").val(3);
   		
   		var worship_materials_show = '';
   		if($(bg).attr("flag_info") == 1){//隐藏图标
   			worship_materials_show = 0;
   			$(bg).attr("src", "<%=path%>/resources/images/srmy_materisl_show.png");
   		}else{//显示图标
   			worship_materials_show = 1;
   			$(bg).attr("src", "<%=path%>/resources/images/srmy_materisl_hidden.png");
   		}
   		
   		$.ajax({
			type: "POST",
			url: '/webLifo/updatePubWorshipMaterialsForMaterialsShow',
	    	data: {worship_materials_id : worship_materials_id, worship_id : worship_id, worship_materials_show : worship_materials_show},
			dataType:'json',
			cache: false,
			success: function(data){
				if(data == 0){
					if(worship_materials_show == 0 ){//需要显示，的刷新页面
						location.href="<%=path%>/webLifo/queryFoPubWorshipForBz?fo_id="+$("#input_worship_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();;
					}else{
						var namelist = $("img[name='1']");
						for(var i=0; i<namelist.length; i++){
						 	if($(namelist[i]).attr("materials_info") == worship_materials_id){
						 		namelist[i].remove();
						 	}
						}
					}
					$(bg).attr("flag_info", worship_materials_show);
					
				}else{
					alert('操作失败，请联系管理员！');
				}
			}
		});
   	}
    	
   	//捐赠 弹出窗
   	function onShowJuanzeng(){
   		$(".juan_zeng_bg,.juan_zeng_content").css({display: "block"});
       	$(".gong_de_bg,.gong_de_content,.ri_zhi_bg,.ri_zhi_content,.fo_zu_bg,.fo_zu_content,.qi_yuan_bg,.qi_yuan_content").css({display: "none"});
   	}
   	
   	//保存捐赠
   	function onSaveJuanzeng(){
   		var jzform_donation_info = $("#jzform_donation_info").val();
   		
   		var jzform_donation_price = $("#jzform_donation_price").val();
   		if(jzform_donation_price == null || jzform_donation_price == ''){
   			alert('请填写捐赠金额！');
   		}else if(jzform_donation_info != null && jzform_donation_info.length > 200){
   			alert('回向功德限输入 200 个文字！');
   		}else{
    		$("#jzform").ajaxSubmit(function(result){
    			
    			if(result == 0){
    				alert("请先登录！");
    				location.href="<%=path%>/";
    			}else{
					location.href="<%=path%>/webLifo/queryFoPubWorshipForBz?fo_id="+$("#input_worship_id").val()+"&chooseflag=${chooseflag}&flag="+$("#tabflag").val();
    			}
			});
		}
   	}
    	
   	//功德簿
   	function onShowGongde(){
		$("#lifoIframe").attr("src","<%=path%>/webLifo/gdxList?fo_id=${fo_id}");
    	$(".gong_de_bg,.gong_de_content").css({display: "block"});
        $(".juan_zeng_bg,.juan_zeng_content,.ri_zhi_bg,.ri_zhi_content,.fo_zu_bg,.fo_zu_content,.qi_yuan_bg,.qi_yuan_content").css({display: "none"});
   	}
   	
   	//关闭佛祖简介弹出窗
    function onCloseJianjie(){
    	$(".fo_zu_bg,.fo_zu_content").css({display: "none"});
    }
   	
   	//保存祈愿
   	function onSaveQiyuan(){
   		var xuyuan_wish_info = $("#xuyuan_wish_info").val();
    	if(xuyuan_wish_info == null || xuyuan_wish_info == ' '){
   			alert('请输入祈愿内容！');
	   	}else if(xuyuan_wish_info != null && xuyuan_wish_info.length > 200){
		   	alert('祈愿内容限输入 200 个文字！');
	   	}else{
    		$("#xuyuan").submit();
   		}
   	}
    
    //开光
    var i=0;
    function toCleanImg(){
        $(".srmy_main").append("<img id='cleanPeople' src='/resources/images/light"+ i + ".png' style='position:absolute;left:300px;top:300px;'/>");
        moveCleanPeople();
    }
    function moveCleanPeople(){
        i=0;
        setTimeout(moveCleanPeopleleft(i), 300);
    }
    function moveCleanPeopleleft(i){
        $("#cleanPeople").remove();
        $(".srmy_main").append("<img id='cleanPeople' src='/resources/images/light"+ i + ".png' style='position:absolute;left:390px;top:100px;'/>");
        if(i == 5){
            i=0;
        }
        i++;
        setTimeout("moveCleanPeopleleft("+i+")", 300);
    }
    
    //清楚勾选记录
   	function onClear(){
//    		if(confirm('关闭将清空勾选记录，是否确认关闭？')){
   			$('#Form', parent.document).empty();
   			$(".bg_confirm, .show_confirm").attr("style","display: none; ");
   			$("#showMaterialsList", parent.document).attr("style","display: none; ");
//    		}
   	}
    
</script>
</head>
<body>
<input type="hidden" id="vipcount" value="${vipcount == null ? 1 : vipcount}" />
<input type="hidden" id="z_index" value="${z_index}"/>
<input type="hidden" id="choose_img_cemetery_materials_id" value=""/>
<input type="hidden" id="userr_user_id" value="${userr_user_id}"/>
<form action="/qjqwmain/cemetery/list" method="post" name="saveForm" id="saveForm">
</form>
<form action="/qjqwmain/cemetery/list" method="post" name="ff" id="ff">
</form>
<input type="hidden" id="input_worship_id" value="${fo_id}"/>
<input type="hidden" id="tabflag" value="${flag}"/>
<input type="hidden" id="input_type_flag" value="2"/>
<form action="/qjqwmain/cemetery/list" method="post" name="Form" id="Form">
  
</form>
<div class="main">
  <div class="header">
    <div class="srmy_wzgg">公告：纪念512汶川地震7周年</div>
    <img src="/resources/images/logo.png" class="srmy_logo"></img>
    <!-- 菜单-->
    <ul class="srmy_menu">
      <li class="active"><a href="srmy.html">礼佛园区</a></li>
      <li><a href="<%=path%>/">网站首页</a></li>
      <li><a href="<%=path%>/userManage">个人后台</a></li>
    </ul>
    <div class="tree_infor_bg"></div>
    <div class="tree_infor">
      <div class="tree_name">${foPubWorship.fo_name}</div>

      <img src="/resources/images/srmy_infor_ico_01.png" class="srmy_infor_ico_01"></img>
      <div class="tree_create_user">浏览次数<span>${foPubWorship.fo_clicks}次</span></div>
      <img src="/resources/images/srmy_infor_ico_02.png" class="srmy_infor_ico_02"></img>
      <div class="tree_create_time">拜佛次数<span>${foPubWorship.fo_worship_num}次</span></div>
    </div>

    <div class="lf_infor">
      <div class="tree_name">${foPubWorship.fo_name}</div>
      <div class="lf_infor_first">
        <div>请佛时间</div>
        <div>
          <c:set target="${myDate}" property="time" value=""/> 
          <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
        </div>
      </div>
      <div class="lf_infor_second">
        <div>请&nbsp;佛&nbsp;人&nbsp;</div>
        <div class="lf_qf_name"></div>
      </div>
      <div class="lf_infor_third">
        <div>
          <div>浏览</div>
          <div><img src="/resources/images/srmy_infor_ico_01.png"></img></div>
          <div>${foPubWorship.fo_clicks}</div>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <!-- 许愿树部分-->
    <div class="wdmy_wrap">
      <!-- 许愿树放置物品-->
      <div class="srmy_main" id="srmy_main">
      <img  style="position:absolute;z-index:0;left:0px;top:0px;width:100%;height:100%"  
 			  	    src="<%=path%>/goods/${foPubWorship.fo_img_url}"  />
 			  	    
		<c:forEach var="obj" items="${foSclist}" varStatus="status">
	  	  <c:choose>
         	<c:when test="${obj.isbg eq '1'}"><%--  背景  --%>
<!-- 			  <img  style="position:absolute;z-index:0;left:0px;top:0px;width:100%;height:100%"  -->
<!-- 			  	    src="<%=path%>/goods/${obj.materials_big_img}"  /> -->
			</c:when>
			<c:otherwise>
<!-- 			  <img  style="position:absolute;z-index:1;left:${(960-obj.materials_img_width)/2 }px;top:${(600-obj.materials_img_height)/2 }px;width:${obj.materials_img_width};height:${obj.materials_img_height}"  -->
<!-- 			  		src="<%=path%>/goods/${obj.materials_big_img}"  /> -->
			</c:otherwise>
		  </c:choose>
		</c:forEach>

        <c:forEach var="obj" items="${sclist}" varStatus="status">
          <c:if test="${obj.worship_materials_show == 0 }">
       		<c:choose>
         	  <c:when test="${obj.materials_class_id eq '10'}"><%--  背景 素材分分类id ==37    --%>
           		<img id="div${obj.worship_materials_id}" materials_info="${obj.worship_materials_id}" data-name="img${status.index+1}" data-id="url${status.index+1}" title="${obj.materials_name}-${obj.materials_class_id}" style="position:absolute;z-index:${obj.worship_materials_z_index};left:0px;top:0px;width:100%;height:100%" src="<%=path%>/goods/${obj.materials_big_img}" name="1" />
         	  </c:when>
         	  <c:otherwise>
          		<div id="div${obj.worship_materials_id}" 
	                 ${((user_id == user_id && worship_materials_limit != 3) || (is_user_worship_flag == 1) || (chooseflag == 2)) ? 'class="srmy_FixedAdorn"' : ''}  
	                 data-name="img${status.index+1}" 
	                 data-id="url${status.index+1}" 
	                 style="position:absolute;z-index:${obj.worship_materials_z_index};left:${obj.worship_materials_x}px;top:${obj.worship_materials_y}px;width:${obj.worship_materials_width}px;height:${obj.worship_materials_height}px;" 
	                 name="1"
	                 width_info="${obj.worship_materials_width}"
	                 height_info="${obj.worship_materials_height}"
	                 materials_info="${obj.worship_materials_id}"
	                 imgwidth="${obj.worship_materials_width}" 
            		 imgweight="${obj.worship_materials_height}">
                  <div class="cao_zuo">
                    <img src="<%=path%>/resources/images/srmy_materisl_big.png" class="big" />
                    <img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="small" />
                    <img src="<%=path%>/resources/images/srmy_materisl_before.png" class="before"/>
                    <img src="<%=path%>/resources/images/srmy_materisl_after.png" class="after"/>
                  </div>
         
           		  <img title="${obj.materials_name}-${obj.materials_class_id}" 
             		   style="width:${obj.worship_materials_width}px;height:${obj.worship_materials_height}px;${obj.user_id == user_id && obj.worship_materials_limit == 0 ? 'border:1px dotted black;' : '' }" 
             		   src="<%=path%>/goods/${obj.materials_big_img}" name="1" ></img>
            	</div>
         	  </c:otherwise>
       		</c:choose>
       	  </c:if>
  		</c:forEach>
      </div>
            
      <div class="foot_cao_zuo"></div>
      <div class="cao_zuo_btn">
        <div class="foot_big">
          <div><img src="<%=path%>/resources/images/srmy_materisl_big.png" /></div>
          <div>放大</div>
        </div>
        <div class="foot_small">
          <div><img src="<%=path%>/resources/images/srmy_materisl_reduce.png" /></div>
          <div>缩小</div>
        </div>
        <div class="buzhi_before">
          <div><img src="<%=path%>/resources/images/srmy_materisl_before.png" /></div>
          <div>置前</div>
        </div>
        <div class="buzhi_after">
          <div><img src="<%=path%>/resources/images/srmy_materisl_after.png" /></div>
          <div>置后</div>
        </div>
        <div class="foot_delete">
          <div><img src="<%=path%>/resources/images/srmy_materisl_delete.png" /></div>
          <div>删除</div>
        </div>
	  </div>


      <!-- 捐赠-->
      <div class="juan_zeng_bg"></div>
      <div class="juan_zeng_content">
        <div class="window_title">捐赠</div>
        <img src="/resources/images/close_window.png" class="jz_window_close"/>
        <div class="window_content">
          <!-- 左侧图片相应信息-->
          <div class="jz_left">
            <div class="left_bg">
              <img src="/goods/${foPubWorship.fo_material_url }" class="jz_object"/>
              <div class="jz_object_name">${foPubWorship.fo_name }</div>
              <div class="jz_money">
                <span class="jz_money_title">已有捐赠</span>
                <span class="jz_money_num">${foPubWorship.fo_merits }福币</span>
              </div>
            </div>
          </div>

          <!-- 右侧具体内容-->
          <form action="/webLifo/saveJuanzeng" method="post" name="jzform" id="jzform">
            <input  type="hidden" value="${foPubWorship.fo_id}" name="fo_id"/>
          
            <div class="jz_right">
              <div class="jz_first">
                <div class="first_title">捐赠金额</div>
                <div class="first_input">
                  <input type="text" id="jzform_donation_price" name="donation_price" onkeyup="value=value.replace(/[^\d]/g,'')" /></div>
                <div class="first_message">*&nbsp;(单位:福币)</div>
              </div>

              <div class="jz_first">
                <div class="first_title">回向功德</div>
                <div class="first_textarea">
                  <textarea name="donation_info" id="jzform_donation_info"></textarea>  <!-- onkeyup="checkLen1(this);" -->
                </div>
              </div>

              <div class="jz_message">限输入 <span id="count1">200</span> 个文字  </div>
            </div>
          </form>

          <div class="jz_line"></div>
          <div class="jz_btn_kuang">
          	<div class="jz_btn" onclick="onSaveJuanzeng();">确认</div>
          </div>
        </div>
      </div>

      <!-- 功德簿-->
      <div class="gong_de_bg"></div>
      <div class="gong_de_content">
        <div class="window_title">功德簿</div>
        <img src="/resources/images/close_window.png" class="gd_window_close"/>
        <div class="window_content">

          <!-- 标题栏-->
          <div class="gd_title">
            <div class="gd_title_cont">
              <div class="gd_name">${foPubWorship.fo_name}</div>
              <div class="gd_sum">累计获得捐赠</div>
              <div class="gd_money">${foPubWorship.fo_merits}</div>
              <div class="gd_money_danwei">福币</div>
            </div>
          </div>

          <!-- 详细列表内容-->
          <iframe id="lifoIframe" name="lifoIframe"  style="width:700px;height:400px;border:none;" scrolling="no" iframeborder="no" ></iframe>            
        </div>
      </div>
      
      <script type="text/javascript" src="/resources/js/jquery.dataTables.min.js"></script>
 	  <script type="text/javascript">
        $(document).ready(function() {
            $("#tb").dataTable({  
            	  "bFilter": false,//去掉搜索框
                "bProcessing": true, // 是否显示取数据时的那个等待提示
                "bServerSide": true,//这个用来指明是通过服务端来取数据
                "sAjaxSource": "/webLifo/logList?id="+${foPubWorship.fo_id}+"&tm="+new Date().getTime()+"&type_flag=2",//这个是请求的地址
                "fnServerData": retrieveData, // 获取数据的处理函数
                "bPaginate": true, //翻页功能
                "bLengthChange": false, //改变每页显示数据数量
                "sAjaxDataProp": "aData",
                "bScrollInfinite":false,
                "iDisplayLength":10,
                "aoColumns" : [ {
                    "mDataProp" : "materials_name",
                }, {
                    "mDataProp" : "user_id",
                }, {
                    "mDataProp" : "worship_add_time",
                }, {
                    "mDataProp" : "worship_info",
                } ],
                "oLanguage" : {
                    "sProcessing" : "正在加载中......",
                    "sLengthMenu" : "每页显示 _MENU_ 条记录",
                    "sZeroRecords" : "没有数据！",
                    "sEmptyTable" : "表中无数据存在！",
                    "sInfo" : "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
                    "sInfoEmpty" : "显示0到0条记录",
                    "sInfoFiltered" : "数据表中共为 _MAX_ 条记录",
                    //"sSearch" : "搜索",
                    "oPaginate" : {
                        "sFirst" : "首页",
                        "sPrevious" : "上一页",
                        "sNext" : "下一页",
                        "sLast" : "末页"
                    }}
            });
            
           
        });
         
        // 3个参数的名字可以随便命名,但必须是3个参数,少一个都不行
        function retrieveData( sSource,aoData, fnCallback) {
            $.ajax({
                url : sSource,//这个就是请求地址对应sAjaxSource
                data : {"aoData":JSON.stringify(aoData)},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
                type : 'post',
                dataType : 'json',
                async : false,
                success : function(result) {
                    fnCallback(result);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的
                },
                error : function(msg) {
                }
            });
        }
        
        
    </script>

	  <!-- 日志-->
      <div class="ri_zhi_bg"></div>
      <div class="ri_zhi_content">
        <div class="window_title">日志</div>
        <img src="/resources/images/close_window.png" class="rz_window_close" />
        <div class="window_content">
          <!-- 详细列表内容-->
          <div class="rz_list">
            <table id="tb" cellspacing="0">
              <thead>
	            <tr>
	              <th>类型</th>
	              <th>操作人员</th>
	              <th>时间</th>
	              <th>详细</th>
	            </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <!-- 佛祖简介-->
      <div class="fo_zu_bg"></div>
      <div class="fo_zu_content">
        <div class="window_title">佛 祖 简 介</div>
        <img src="/resources/images/close_window.png" class="fzjj_window_close" />
        <div class="window_content">
          <!-- 左侧图片相应信息-->
          <div class="jz_left">
            <div class="left_bg">
              <img src="/goods/${foPubWorship.fo_material_url }" class="jz_object" />
              <div class="jz_object_name">${foPubWorship.fo_name }</div>
              <div class="jz_num">
                <span class="jz_money_title">供奉次数</span>
                <span class="jz_money_num">${foPubWorship.fo_worship_num }次</span>
              </div>
              <div class="jz_num">
                <span class="jz_money_title">请佛次数</span>
                <span class="jz_money_num">${foPubWorship.fo_qingfo_num }次</span>
              </div>
            </div>
          </div>

          <!-- 右侧具体内容-->
          <div class="fzjj_right">
            <div>&nbsp;&nbsp;&nbsp;&nbsp;
              ${foPubWorship.fo_info }
            </div>
          </div>
          <div class="fzjj_line"></div>
          <button class="fzjj_btn" onclick="onCloseJianjie();">确认</button>
        </div>
      </div>


      <form id="xuyuan" name="xuyuan" action="/webLifo/xuyuan" method="post">
        <!-- 祈愿-->
        <input  type="hidden" value="${foPubWorship.fo_id}" name="fo_id"/>
        <input  type="hidden" value="${foPubWorship.fo_name}" name="fo_name"/>
        <input  type="hidden" value="1" name="fo_type"/>
        <div class="qi_yuan_bg"></div>
        <div class="qi_yuan_content">
          <div class="window_title">祈 愿</div>
          <img src="/resources/images/close_window.png" class="qy_window_close" />
          <div class="window_content">
            <!-- 左侧图片相应信息-->
            <div class="jz_left">
              <div class="left_bg">
                <img src="/goods/${foPubWorship.fo_material_url }" class="jz_object" />
                <div class="jz_object_name">${foPubWorship.fo_name }</div>
              </div>
            </div>

            <!-- 右侧具体内容-->
            <div class="jz_right">
              <div class="qy_first">
                <div class="first_title">祈愿内容</div>
                <div class="first_textarea"><textarea name="wish_info" id="xuyuan_wish_info" > </textarea>
                  <div class="jz_message">限输入 <span id="count">200</span> 个文字 </div> 
                </div>
              </div>
              <div class="choose_hy">
                <div class="choose_hy_title">是否还愿</div>
                <div class="choose_hy_cont">
                  <!-- 是-->
                  <input type="radio" value="2" name="wish_come_true_type" id="choose_yes" />
                  <label for="choose_yes">是</label>

                  <!-- 否-->
                  <input type="radio" value="1" name="wish_come_true_type" id="choose_noe" checked="checked" />
                  <label for="choose_noe">否</label>
                </div>
                <div class="huan_yuan_cont">
                  <div class="cont_first">
                    <div class="huan_yuan_title">愿望达成，我要向佛祖捐赠香火</div>
                    <div class="huan_yuan_text"><input type="text"  name="wish_come_true_price" id="wish_come_true_price" onkeyup="value=value.replace(/[^\d]/g,'')" /></div>
                    <div class="huan_yuan_message">* (单位：福币)</div>
                  </div>

                  <div class="cont_second">
                    <div class="huan_yuan_title">还愿日期&nbsp;</div>
                    <!-- 是-->
                    <input type="radio" value="1" name="choose" id="choose_set" />
                    <label for="choose_yes">设置</label>

                    <!-- 否-->
                    <input type="radio" value="0" name="choose" id="choose_no_set" checked="checked" />
                    <label for="choose_noe">不设置</label>

                    <div class="set_time">
                      <input name="wish_come_true_time"   type="text" onfocus="WdatePicker({minDate:'%y-%M-{%d+1}'})" style="width: 150px;"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="jz_line"></div>
            <div class="jz_btn_kuang">
              <div class="jz_btn" onclick="onSaveQiyuan();">确认</div>
            </div>
          </div>
        </div>
      </form>




      <!--提示层-->
      <div class="bg_prompt"></div>
      <div class="prompt_infor">
        <img src="/resources/images/close_window.png" class="close_window"/>
        <div class="prompt_title">提 示</div>
        <div class="prompt_content">
          <img src="/resources/images/prompt_wait.png"/>
          <div class="font">福币补充完成前，请不要关闭窗口。</div>
          <div class="behind">福币补充完成后，请根据情况点击一下按钮。</div>
          <div class="prompt_btn">
            <div class="issue">遇到问题</div>
            <div class="accomplish">完成</div>
          </div>
        </div>
      </div>

      <!-- 开光-->
      <div class="kai_guan_bg"></div>
      <div class="kai_guan_content">
        <div class="window_title">开 光</div>
        <img src="/resources/images/close_window.png" class="kg_window_close"/>
        <div class="kg_window_content">
          <div class="kg_cont">给此佛开光需要500福币，您确要开光吗？</div>
          <button class="kg_btn"  onclick="kaiguangle();">确定</button>
        </div>
      </div>
    </div>

  </div>

  <!-- 素材部分-->
  <!-- 园主人进入时-->
  <div class="srmy_sc">

    <div class="srmy_suocai">

      <!-- 园主人-->
      <ul class="tab_menu">
        <li class="jisi" onclick="onChooseJisiOrBuzhi('1', '${fo_id}', ${chooseflag}, '${is_user_worship_flag }');" ${flag == 1 ? 'style="background-color:#fd844e"' : ''}>礼佛</li>
        <c:choose>
      	  <c:when test="${is_user_worship_flag > 0}"><!-- 是建造者，显示墓园布置列表 -->
            <li class="buzhi" onclick="onChooseJisiOrBuzhi('2','${fo_id}',${chooseflag}, '${is_user_worship_flag }');" ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>供奉布置</li>
      	  </c:when>
      	  <c:otherwise>
            <li class="buzhi" style='background-color: #bbb;' ${flag == 2 ? 'style="background-color:#fd844e"' : '' }>供奉布置</li>
      	  </c:otherwise>
        </c:choose>
                
        <li class="choose_ceng" onclick="onChooseJisiOrBuzhi('3','${fo_id}',${chooseflag}, '${is_user_worship_flag }');" ${flag == 3 ? 'style="background-color:#fd844e"' : ''}>选择图层</li>
      </ul>

      <div class="srmy_tab_content">
        <!--许愿-->
        <div class="sacrifice ${flag == 1 ? '' : 'content_hide'}" id="div_jisi" >
		  <ul>
            <c:forEach items="${jisiMaterialsClassList}" var="var" varStatus="vs">
	          <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
	            <img src="<%=path%>/goods/${var.materials_class_img}" style="height: 29px;width: 29px;" />
	            <div>${var.materials_class_name}</div>
	          </li>
          	</c:forEach>
          </ul>
        </div>

        <!-- 空间布置-->
        <div class="${flag == 2 ? '' : 'content_hide'} sacrifice" id="div_buzhi">
          <ul>
            <c:forEach items="${buzhiMaterialsClassList}" var="var" varStatus="vs">
              <li onclick = "onShowMaterialsClass2('${var.materials_class_id}');">
                <img src="<%=path%>/goods/${var.materials_class_img}" style="height: 29px;width: 29px;" />
                <div>${var.materials_class_name}</div>
              </li>
            </c:forEach>
          </ul>
        </div>
        <!-- 选择图层-->
        <div class="${flag == 3 ? '' : 'content_hide'} srmy_fix_up" id="div_tuceng">
          <div class="srmy_bg">
            <c:forEach var="obj" items="${bglist}" varStatus="status">
              <c:if test="${status.index == (fn:length(bglist) - 1) }">
                <li id="list${status.index+1}" class="cc" draggable="true">${obj.materials_name}
    			  <span class="buy_user_name">${obj.user_nick_name}祭祀</span>
                  <span class="buy_user"> </span>
                </li>
              </c:if>
            </c:forEach>
          </div>
             
          <ul id="SortContaint" class="fix_up">
			<c:forEach var="obj" items="${movelist}" varStatus="status">
              <c:if test="${obj.materials_class_id != 10}">
    			<li id="list${obj.worship_materials_id}" class="cc" draggable="true">
    			  ${fn:length(obj.materials_name) > 10 ? fn:substring(obj.materials_name, 0, 10) : obj.materials_name}${fn:length(obj.materials_name) > 10 ? '...' : ''}
    			  <span class="buy_user_name">${obj.user_nick_name}祭祀</span>
                  <span class="buy_user">
                    <c:choose>
                      <c:when test="${obj.materials_class_id == 8888 }"><!-- 文字信息   -->
                      	<a href="#" onclick="onChangeWz('${obj.materials_id}','${private_wish_tree_id}', '${obj.worship_materials_id}');">
						  <img src="<%=path%>/resources/images/editwordblue.png" class="srmy_sucai_big"/>
						</a>
                      </c:when>
                      <c:otherwise>
                      	<img src="<%=path%>/resources/images/srmy_materisl_big.png" class="srmy_sucai_big"/>
                    	<img src="<%=path%>/resources/images/srmy_materisl_reduce.png" class="srmy_sucai_reduce"/>
                      </c:otherwise>
                    </c:choose>
                    <img name="eye_${obj.worship_materials_id}" src="<%=path%>/resources/images/srmy_materisl_${obj.worship_materials_show == 0 ? 'show' : 'hidden'}.png" class="srmy_sucai_hidden" onclick="onUpdMaterialsShow(${obj.worship_materials_id}, ${fo_id},this);" flag_info = '${obj.worship_materials_show}' />
                    
                    <img src="<%=path%>/resources/images/srmy_materisl_delete.png" class="srmy_sucai_delete" onclick="onDelMaterials(${obj.worship_materials_id}, ${fo_id});"/>
                  </span>
                </li>
              </c:if>
  			</c:forEach>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!--按钮部分-->
    
  <c:choose>
    <c:when test="${is_user_worship_flag > 0 }"><!-- 自己 -->
  	  <div class="footer jisi_show">
        <input type="button" value="日志" class="ri_zhi"/>
        <input type="button" value="佛祖简介" class="fo_zu"/>
        <input type="button" value="祈愿" class="qi_yuan"/>
        
        <!-- 暂时放一下 -->
       	<input type="button" value="功德簿" class="gong_de"/>

	    <!-- 保存按钮-->
	    <input type="button" value="保存" id="sava_id" style="background-color:#bbbbbb"/>
   	  </div>
    </c:when>
    <c:otherwise>
      <div class="footer">
        <input type="button" value="捐赠" class="juan_zeng" onclick="onShowJuanzeng();"/>  
        <input type="button" value="功德簿" class="gong_de" onclick="onShowGongde();"/>
        <input type="button" value="日志" class="ri_zhi"/>
        <input type="button" value="佛祖简介" class="fo_zu"/>
        <input type="button" value="祈愿" class="qi_yuan"/>

       <!-- 保存按钮-->
       <input type="button" value="保存" id="sava_id" style="background-color:#bbbbbb"/>
     </div>
   </c:otherwise>
 </c:choose>
    


 <!--商品类别详细信息-->
 <div id="bg"></div>
      
 <div id="showMaterialsList">
   <iframe src="" scrolling="no" frameborder="0" class="srmy_sucai_iframe" id="iframe_materials"></iframe>
 </div>
 <div id="show"></div>
   
 <!--结算购物车-->
 <div class="bg_confirm"></div>
 <div class="show_confirm">
   <div class="confirm_content" id="div_confirm_container_js">
   </div>
 </div>

</div>

</body>
</html>