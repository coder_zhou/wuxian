<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8">
    <meta http-equiv=X-UA-Compatible content=IE=EmulateIE8>
    <meta name="viewport" content=" width = device-width ,height = device-height ,initial-scale = 1, minimum-scale = 1, maximum-scale = 1, user-scalable =no," >
    <title></title>

    <style>
        .btn{
            position: fixed;
            bottom: 0;
            left: 44%;
        }
        #q1,#q2{
            width:80px;
            height:30px;
            margin-right: 20px;
            background-color: black;
            color: white;
            font:bolder 16px '微软雅黑';
        }
        .main{
            width:960px;
            height:600px;
            padding: 0 10px 10px 0;
            background: url(<%=path%>/goods/images/bg.png) no-repeat;
            background-size:960px 600px;
            /*让IE兼容background-size*/
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(
                    src='<%=path%>/goods/images/bg.png',sizingMethod='scale')

            -ms-background-size:960px 600px;
            /*让IE6-8兼容CSS3*/
            behavior: url(path/PIE.htc);
        }
    </style>

</head>
<body>

<div class="main">
    <#list list as cm>
    <#if cm.materials_class_id  == "10">

	        <img id="img_${cm.cemetery_materials_id}" data-name="img_${cm_index+1}" title="${cm.materials_name}-${cm.materials_class_id}" style="position:absolute;z-index:${cm.cemetery_materials_z_index};left:${cm.cemetery_materials_x}px;top:${cm.cemetery_materials_y}px;width:100%;height:100%" src="../goods${cm.materials_big_img}" name="1">

</#if>
<#if cm.materials_class_id  != "10">

            <img id="img_${cm.cemetery_materials_id}" data-name="img_${cm_index+1}" title="${cm.materials_name}-${cm.materials_class_id}" style="position:absolute;z-index:${cm.cemetery_materials_z_index};left:${cm.cemetery_materials_x}px;top:${cm.cemetery_materials_y}px;width:${cm.cemetery_materials_width}px;height:${cm.cemetery_materials_height}px" src="../goods${cm.materials_big_img}" name="1">

</#if>
    
	
    </#list>

</div>
<div class="btn">
    <button id="q1">放大</button>
    <button id="q2">缩小</button>
   <button id="saveBtn" >保存</button>
</div>
</body>

</html>