
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `qjqw_Links`
-- ----------------------------
DROP TABLE IF EXISTS `TB_Links`;
CREATE TABLE `qjqw_Links` (
 		`Links_ID` varchar(100) NOT NULL,
		`links_id` int(11) NOT NULL COMMENT 'ID',
		`links_name` varchar(255) DEFAULT NULL COMMENT '友链名称',
		`links_url` varchar(255) DEFAULT NULL COMMENT '友链路径',
		`links_img` varchar(255) DEFAULT NULL COMMENT '友链图片',
		`links_flag` int(11) NOT NULL COMMENT '友链状态',
		`links_addTime` varchar(255) DEFAULT NULL COMMENT '添加时间',
  		PRIMARY KEY (`Links_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
