
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `TB_USERINFO`
-- ----------------------------
DROP TABLE IF EXISTS `TB_USERINFO`;
CREATE TABLE `TB_USERINFO` (
 		`USERINFO_ID` varchar(100) NOT NULL,
		`user_id` int(11) NOT NULL COMMENT '用户ID',
		`phone` varchar(255) DEFAULT NULL COMMENT '手机号',
		`password` varchar(255) DEFAULT NULL COMMENT '密码',
		`user_nick_name` varchar(255) DEFAULT NULL COMMENT '用户昵称',
		`statue` int(11) NOT NULL COMMENT '用户状态',
		`user_head_photo` varchar(255) DEFAULT NULL COMMENT '头像',
		`sex` varchar(255) DEFAULT NULL COMMENT '用户性别',
		`addtime` varchar(255) DEFAULT NULL COMMENT '添加时间',
		`checktime` varchar(255) DEFAULT NULL COMMENT '激活时间',
		`height` int(11) NOT NULL COMMENT '身高',
		`weight` int(11) NOT NULL COMMENT '体重',
		`job` varchar(255) DEFAULT NULL COMMENT '工作',
		`message` varchar(255) DEFAULT NULL COMMENT '个人简介',
		`keshi` varchar(255) DEFAULT NULL COMMENT '到期时间',
		`user_name` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  		PRIMARY KEY (`USERINFO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
