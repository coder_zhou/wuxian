<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<%=basePath%>">
		
		<meta charset="utf-8" />
		<title></title>
		
		<meta name="description" content="overview & stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/resources/css/font-awesome.min.css" />
		<!--[if IE 7]><link rel="stylesheet" href="/resources/css/font-awesome-ie7.min.css" /><![endif]-->
		<!--[if lt IE 9]><link rel="stylesheet" href="/resources/css/ace-ie.min.css" /><![endif]-->
		<!-- 下拉框 -->
		<link rel="stylesheet" href="/resources/css/chosen.css" />
		
		<link rel="stylesheet" href="/resources/css/ace.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-skins.min.css" />
		
		<link rel="stylesheet" href="/resources/css/datepicker.css" /><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/jquery-1.7.2.js"></script>
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
		
<script type="text/javascript">
	
	
	
	//保存
	function save(){
			if($("#class_name").val()==""){
			$("#class_name").tips({
				side:3,
	            msg:'请输入分类名称',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#class_name").focus();
			return false;
		}
		if($("#class_info").val()==""){
			$("#class_info").tips({
				side:3,
	            msg:'请输入分类简介',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#class_info").focus();
			return false;
		}
		if($("#up_id").val()==""){
			$("#up_id").tips({
				side:3,
	            msg:'请输入上级ID',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#up_id").focus();
			return false;
		}
		$("#Form").submit();
		$("#zhongxin").hide();
		$("#zhongxin2").show();
	}
	
</script>
	</head>
<body>
	<form action="/qjqwmain/newsclass/${msg }" name="Form" id="Form" method="post">
		<input type="hidden" name="class_id" id="class_id" value="${newsclass.class_id}"/>
		<div id="zhongxin">
		<table  class="table table-striped table-bordered table-hover">
			<tr>
				<td>分类名称:</td><td><input type="text" name="class_name" id="class_name" value="${newsclass.class_name}" maxlength="32" placeholder="这里输入分类名称" title="分类名称"/></td>
			</tr>
			<tr>
				<td>分类简介:</td><td><input type="text" name="class_info" id="class_info" value="${newsclass.class_info}" maxlength="32" placeholder="这里输入分类简介" title="分类简介"/></td>
			</tr>
			<tr>
				<td>上级ID:</td><td>
				<select name="up_id" id="up_id" onchange="chose();">
				<option value="0" selected="selected">顶级栏目</option>
				<c:choose>
				<c:when test="${not empty list  }">
			
				<c:forEach items="${list }" var="list" varStatus="vs">
					<c:if test="${list.up_id ==0 }">
				<option value="${list.class_id }"   <c:if test="${list.class_id==newsclass.up_id }">selected="selected"  </c:if>>     ${list.class_name}</option>
				<c:if test="${not empty list.son }">
				<c:forEach items="${list.son }" var="son">
				<option value="${son.class_id }" <c:if test="${son.class_id==newsclass.up_id }">selected="selected"  </c:if> > -->${son.class_name }</option>
				</c:forEach>
				</c:if>
				</c:if>
				</c:forEach>
				
				</c:when>
				</c:choose>
				</select>
				
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					<a class="btn btn-mini btn-primary" onclick="save();">保存</a>
					<a class="btn btn-mini btn-danger" onclick="top.Dialog.close();">取消</a>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="zhongxin2" class="center" style="display:none"><br/><br/><br/><br/><br/><img src="/resources/images/jiazai.gif" /><br/><h4 class="lighter block green">提交中...</h4></div>
		
	</form>
	
	
		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		<script type="text/javascript">
		$(window.parent.hangge());
		$(function() {
			
			//单选框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
		});
		
		</script>
</body>
</html>