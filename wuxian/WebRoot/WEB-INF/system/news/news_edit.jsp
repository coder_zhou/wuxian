<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<%=basePath%>">
		
		<meta charset="utf-8" />
		<title></title>
		
		<meta name="description" content="overview & stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/resources/css/font-awesome.min.css" />
		<!--[if IE 7]><link rel="stylesheet" href="/resources/css/font-awesome-ie7.min.css" /><![endif]-->
		<!--[if lt IE 9]><link rel="stylesheet" href="/resources/css/ace-ie.min.css" /><![endif]-->
		<!-- 下拉框 -->
		<link rel="stylesheet" href="/resources/css/chosen.css" />
		
		<link rel="stylesheet" href="/resources/css/ace.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-skins.min.css" />
		
		<link rel="stylesheet" href="/resources/css/datepicker.css" /><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/jquery-1.7.2.js"></script>
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
		<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
        <script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
        <script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
        <link href="/resources/css/cropper.min.css" rel="stylesheet">
        <link href="/resources/css/docs.css" rel="stylesheet">
     
<script type="text/javascript">
	
	
	
	//保存
	function save(){
			if($("#news_name").val()==""){
			$("#news_name").tips({
				side:3,
	            msg:'请输入文章名称',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#news_name").focus();
			return false;
		}
		if($("#class_id").val()==""){
			$("#class_id").tips({
				side:3,
	            msg:'请输入文章分类',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#class_id").focus();
			return false;
		}
		if($("#news_img").val()==""){
			$("#news_img").tips({
				side:3,
	            msg:'请输入文章封面图片',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#news_img").focus();
			return false;
		}
		if($("#news_author").val()==""){
			$("#news_author").tips({
				side:3,
	            msg:'请输入文章作者',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#news_author").focus();
			return false;
		}
		if($("#news_from").val()==""){
			$("#news_from").tips({
				side:3,
	            msg:'请输入文章来源',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#news_from").focus();
			return false;
		}
		$("#Form").submit();
		$("#zhongxin").hide();
		$("#zhongxin2").show();
	}
	$(document).ready(
	        function (){
	        
	        var editor = new UE.ui.Editor({initialFrameHeight:600,initialFrameWidth:651 });  
	        editor.render("news_info");  
	        
	       //     UE.getEditor('article_info_url');
	        }
	    
	        );
</script>
	</head>
<body>
	<form action="/qjqwmain/news/${msg }" name="Form" id="Form" method="post" enctype="multipart/form-data">
		<input type="hidden" name="news_id" id="news_id" value="${news.news_id}"/>
		<div id="zhongxin">
		<table  class="table table-striped table-bordered table-hover">
			<tr>
				<td>文章名称:</td><td><input type="text" name="news_name" id="news_name" value="${news.news_name}" maxlength="100" placeholder="这里输入文章名称" title="文章名称"/></td>
			</tr>
			<tr>
				<td>文章简介:</td><td>
				<textarea rows="5" cols="600" style="width: 400px;" name="news_abstract" id="news_abstract" placeholder="这里输入文章简介">
				${news.news_abstract}
				</textarea>
				</td>
			</tr>
			<tr>
				<td>文章分类:</td><td>
						<select name="class_id" id="class_id" >
			
				<c:choose>
				<c:when test="${not empty list  }">
			
				<c:forEach items="${list }" var="list" varStatus="vs">
					<c:if test="${list.up_id ==0 }">
				<option value="${list.class_id }" <c:if test="${list.class_id==news.class_id }">selected="selected"  </c:if>  >${list.class_name}</option>
				<c:if test="${not empty list.son }">
				<c:forEach items="${list.son }" var="son">
				<option value="${son.class_id }"   <c:if test="${son.class_id==news.class_id }">selected="selected"  </c:if> >-->${son.class_name }</option>
				</c:forEach>
				</c:if>
				</c:if>
				</c:forEach>
				
				</c:when>
				</c:choose>
				</select>
				
				</td>
			</tr>
			<tr>
				<td>文章内容:</td><td>
				<textarea name="news_info" id="news_info">
				${news.news_info}
				</textarea>
				
				</td>
			</tr>
			<tr>
				<td>文章封面图片:</td><td >
				<input type="hidden"  name="news_img" id="img_url" value="${news.news_img }">
				<img src="${news.news_img }?imageMogr2/thumbnail/50x50" onerror="javascript:this.src='/resources/images/noPic.png';" id="ii" width="100" height="100" onclick="show();">
						<div class="main_content" id="content" style="display: none;">

          <div class="img-container"><img src="" style="display:none"></div>
          <div class="img-preview img-preview-sm"></div>
          <div class="btn-group">
            <div class="btn-primary" data-method="zoom" data-option="0.1"  title="放大">
				<label><img src="/resources/img/big.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="zoom" data-option="-0.1"  title="缩小">
				 <label><img src="/resources/img/small.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="-90"  title="左转90度">
				 <label><img src="/resources/img/left.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="90"  title="右转90度">
				<label><img src="/resources/img/right.png" width="40" height="25"/></label>
            </div>
         
           
          </div>
          <div class="foot_btn"> <input type="file" style="width: 100px;" id="inputImage"   accept="image/*" placeholder="这里输入文章封面图片" title="文章封面图片">  <button  id="getDataURL2" data-toggle="tooltip" type="button" title="$().cropper('getDataURL', 'image/jpeg')"  >确定</button></div>

         
  </div>
  <input type="hidden" id="imgStr"/><!-- 这里是上传到服务器的截取后的图片的base64码,因为此参数在docs.js中指定了，所以不要该参数名，如需更改务必保持页面此input的id和docs.js中的一致 -->
				</td>
			</tr>
			<tr>
				<td>文章作者:</td><td><input type="text" name="news_author" id="news_author" value="${news.news_author}" maxlength="32" placeholder="这里输入文章作者" title="文章作者"/></td>
			</tr>
			<tr>
				<td>文章来源:</td><td><input type="text" name="news_from" id="news_from" value="${news.news_from}" maxlength="32" placeholder="这里输入文章来源" title="文章来源"/></td>
			</tr>
			<tr>
				<td style="text-align: center;">
					<a class="btn btn-mini btn-primary" onclick="save();">保存</a>
					<a class="btn btn-mini btn-danger" onclick="top.Dialog.close();">取消</a>
				</td>
			</tr>
		</table>
		</div>

		<div id="zhongxin2" class="center" style="display:none"><br/><br/><br/><br/><br/><img src="/resources/images/jiazai.gif" /><br/><h4 class="lighter block green">提交中...</h4></div>
		
	</form>
	
	 <input type="hidden" id="imgStr"/>
		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		
		<script type="text/javascript">
		$(window.parent.hangge());
		$(function() {
			
			//单选框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
		});
		function show(){
			$("#content").show();
		}
		</script>
  <script src="/resources/js/cropper.min.js"></script>
  <script src="/resources/js/docs.js"></script>
  <script>
      function test(){
		  var imgStr = $("#imgStr").val();
		  if(imgStr!="" || imgStr!=undefined){
		     console.log(imgStr);
		 	$.ajax({
				type: "POST",
				url: '/picture/savePic',
		    	data: {'imgStr':imgStr},
				dataType:'json',
				//beforeSend: validateData,
				cache: false,
				success: function(data){
					$("#ii").attr("src",data.imgUrl);
					$("#img_url").val(data.imgUrl);
					$("#content").hide();
				}
			});
		     
			
	      }

	  }
  
  </script>
</body>
</html>