<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ff" uri="fenYeListNo"%>
<jsp:useBean id="myDate" class="java.util.Date"/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
	<base href="<%=basePath%>"><!-- jsp文件头和头部 -->
	<%@ include file="/WEB-INF/system/admin/top.jsp"%> 
	</head>
<body>
		
<div class="container-fluid" id="main-container">

	<div id="breadcrumbs">
	
	<ul class="breadcrumb">
		<li><i class="icon-home"></i> <a>资讯管理</a><span class="divider"><i class="icon-angle-right"></i></span></li>
		<li class="active">资讯管理</li>
	</ul><!--.breadcrumb-->
	
	<div id="nav-search">
	</div><!--#nav-search-->
	
	</div><!--#breadcrumbs-->

<div id="page-content" class="clearfix">
						
  <div class="row-fluid">

	<div class="row-fluid">
	
			<!-- 检索  -->
			<form action="/qjqwmain/news/list" method="post" name="Form" id="Form">
			<table>
				<tr>
					<td>
						<span class="input-icon">
							<input autocomplete="off" id="nav-search-input" type="text" name="news_name" value="${pd.news_name }" placeholder="文章标题" />
							<i id="nav-search-icon" class="icon-search"></i>
						</span>
					</td>
					<td>
						<span class="input-icon">
							<input autocomplete="off" id="nav-search-input" type="text" name="news_author" value="${pd.news_author }" placeholder="作者" />
							<i id="nav-search-icon" class="icon-search"></i>
						</span>
					</td>
					<td><input class="span10 date-picker" name="lastStart" id="lastLoginStart" value="${pd.lastStart}" type="text" data-date-format="yyyy-mm-dd" readonly="readonly" style="width:88px;" placeholder="开始日期"/></td>
					<td><input class="span10 date-picker" name="lastEnd" id="lastLoginEnd" value="${pd.lastEnd}" type="text" data-date-format="yyyy-mm-dd" readonly="readonly" style="width:88px;" placeholder="结束日期"/></td>
					<td style="vertical-align:top;">
					<select name="class_id" id="class_id"  class="chzn-select" data-placeholder="类别" style="vertical-align:top;">
					<c:if test="${pd.class_id!='' }">
						<option value="${pd.class_id }"></option>
					</c:if>
				<option value=""></option>
				<c:choose>
				<c:when test="${not empty list  }">
			
				<c:forEach items="${list }" var="list" varStatus="vs">
					<c:if test="${list.up_id ==0 }">
				<option value="${list.class_id }" >${list.class_name}</option>
				<c:if test="${not empty list.son }">
				<c:forEach items="${list.son }" var="son">
				<option value="${son.class_id }">-->${son.class_name }</option>
				</c:forEach>
				</c:if>
				</c:if>
				</c:forEach>
				
				</c:when>
				</c:choose>
				</select>
					
					</td>
					<td style="vertical-align:top;"> 
					 	<select class="chzn-select" name="status" data-placeholder="状态" style="vertical-align:top;width: 79px;">
						<option value=""></option>
						<option value="1" <c:if test="${pd.status == '1' }">selected</c:if> >已隐藏</option>
						<option value="0" <c:if test="${pd.status == '0' }">selected</c:if> >正显示</option>
						</select>
					</td>
					<td style="vertical-align:top;"><button class="btn btn-mini btn-light" onclick="search();"  title="检索"><i id="nav-search-icon" class="icon-search"></i></button></td>
				</tr>
			</table>
			<!-- 检索  -->
		</form>
		
			<table id="table_report" class="table table-striped table-bordered table-hover">
				
				<thead>
					<tr>
						<th class="center">
						<label><input type="checkbox" id="zcheckbox" /><span class="lbl"></span></label>
						</th>
						<th>序号</th>
						<th>文章名称</th>
						<th>文章简介</th>
						<th>文章分类</th>
						<th>文章封面图片</th>
						<th>文章作者</th>
						<th>文章来源</th>
						<th>添加时间</th>
						<th>文章状态</th>
					
						<th class="center">操作</th>
					</tr>
				</thead>
										
				<tbody>
					
				<!-- 开始循环 -->	
				<c:choose>
					<c:when test="${not empty varList}">
						<c:if test="${QX.cha == 1 }">
						<c:forEach items="${varList}" var="var" varStatus="vs">
							<tr>
								<td class='center' style="width: 30px;">
									<label><input type='checkbox' name='ids' value="${var.news_id}" /><span class="lbl"></span></label>
								</td>
								<td class='center' style="width: 30px;">${vs.index+1}</td>
										<td>${var.news_name}</td>
										<td>${var.news_abstract}</td>
										<td>${var.class_name}</td>
										<td>
										<img alt="${var.news_name}" src="${var.news_img}?imageMogr2/thumbnail/50x50" width="50" height="50" onerror="javascript:this.src='/resources/images/noPic.png';">
										</td>
										<td>${var.news_author}</td>
										<td>${var.news_from}</td>
										<td>
										 <c:set target="${myDate}" property="time" value="${var.news_addtime*1000}"/> 
                                               <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${myDate}" type="both"/> 
										</td>
										<td>${var.news_statue==0?"显示":"隐藏"}</td>
									
								<td style="width: 30px;" class="center">
									<div class='hidden-phone visible-desktop btn-group'>
									
										<c:if test="${QX.edit != 1 && QX.del != 1 }">
										<span class="label label-large label-grey arrowed-in-right arrowed-in"><i class="icon-lock" title="无权限"></i></span>
										</c:if>
											<c:if test="${QX.edit == 1 }">
											<button class="btn btn-mini btn-info" data-toggle="dropdown" onclick="edit('${var.news_id}');" name="编辑"><i class="icon-edit icon-only"></i></button>
											</c:if>
										<c:if test="${QX.del == 1 }">
											<button class="btn btn-mini btn-info" data-toggle="dropdown" onclick="del('${var.news_id}');" name="删除"><i class="icon-trash icon-only"></i></button>
										</c:if>
									
									</div>
								</td>
							</tr>
						
						</c:forEach>
						</c:if>
						<c:if test="${QX.cha == 0 }">
							<tr>
								<td colspan="100" class="center">您无权查看</td>
							</tr>
						</c:if>
					</c:when>
					<c:otherwise>
						<tr class="main_info">
							<td colspan="100" class="center" >没有相关数据</td>
						</tr>
					</c:otherwise>
				</c:choose>
					
				
				</tbody>
			</table>
			
		<div class="page-header position-relative">
		<table style="width:100%;">
			<tr>
				<td style="vertical-align:top;">
				    <!-- 添加推送区消息   begin    -->
					 <select  name="push_type" id="push_type" data-placeholder="推送类型" style="vertical-align:top;width: 120px;">
					     <option value="">请选择推送类型 </option>
		  			 </select>
					 <a class="btn btn-small btn-success" onclick="pushInfo();" title="添加推送消息">推送消息</a>
					 <!-- 添加推送区消息   end    -->
					<c:if test="${QX.add == 1 }">
					<a class="btn btn-small btn-success" onclick="add();">新增</a>
					</c:if>
					<c:if test="${QX.del == 1 }">
					<a class="btn btn-small btn-danger" onclick="makeAll('确定要删除选中的数据吗?');" title="批量删除" ><i class='icon-trash'></i></a>
					</c:if>
				</td>
				<td style="vertical-align:top;"><div class="pagination" style="float: right;padding-top: 0px;margin-top: 0px;">
		
			<ff:page mhFrom="Form" showReSize="true"  field="page" onlyOneShow="true" showListNo="false" action="qjqwmain/news/list"/>
				
				</div></td>
			</tr>
		</table>
		</div>
		
	</div>
 
 
 
 
	<!-- PAGE CONTENT ENDS HERE -->
  </div><!--/row-->
	
</div><!--/#page-content-->
</div><!--/.fluid-container#main-container-->
		
		<!-- 返回顶部  -->
		<a href="#" id="btn-scroll-up" class="btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only"></i>
		</a>
		
		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/bootbox.min.js"></script><!-- 确认窗口 -->
		<!-- 引入 -->
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script><!--提示框-->
		<script type="text/javascript">
		
		$(window.parent.hangge());
		
		//检索
		function search(){
			window.parent.jzts();
			$("#Form").submit();
		}
		
		//新增
		function add(){
			 window.parent.jzts();
			 var diag = new top.Dialog();
			 diag.Drag=true;
			 diag.Title ="新增";
			 diag.URL = '/qjqwmain/news/goAdd';
			 diag.Width = 1000;
			 diag.Height = 700;
			 diag.CancelEvent = function(){ //关闭事件
				 if(diag.innerFrame.contentWindow.document.getElementById('zhongxin').style.display == 'none'){
					 if('${page.pageNo}' == '0'){
						 window.parent.jzts();
						 setTimeout("self.location.reload()",100);
					 }else{
						 jump(${page.pageNo});
					 }
				}
				diag.close();
			 };
			 diag.show();
		}
		
		//添加推送区  2015.10.13 guohua
		function pushInfo(){
		    var push_name = $("#push_type").val();
		    var push_channel ;
		    if(push_type==null || push_type==""){
		       alert("请选择推送类型");
		       return;
		    }else{
		       push_channel =  $("#push_type").find("option:selected").text();
		    }
		    var ids = "";
			$("input[type='checkbox'][name='ids']:checked").each(function(){ 
			   var push_param_name = $(this).parents("tr").find("td:eq(2)").html();
		  	   ids +=$(this).val()+"|"+push_param_name+",";
			});
			if(ids==""){
			   alert("请选择操作项!");
			   return;
			}
		    if(confirm("您确定要推送选中数据为"+push_channel+"吗?")){
				var url  = "/qjqwmain/pushinfo/addPushInfo";
				$.post(url,{push_name:push_name,ids:ids},function(result){
				    if(result=="0"){
				       alert("操作成功!");
				    }else{
				       alert("操作失败!");
				    }
				},'json');
		    }
		}
		
		//删除
		function del(Id){
			bootbox.confirm("确定要删除吗?", function(result) {
				if(result) {
					var url = "/qjqwmain/news/delete?news_id="+Id+"&tm="+new Date().getTime();
					$.get(url,function(data){
						if(data=="success"){
							jump(${page.pageNo});
						}
					});
				}
			});
		}
		
		//修改
		function edit(Id){
			 window.parent.jzts();
			 var diag = new top.Dialog();
			 diag.Drag=true;
			 diag.Title ="编辑";
			 diag.URL = '/qjqwmain/news/goEdit?news_id='+Id;
			 diag.Width = 1000;
			 diag.Height = 700;
			 diag.CancelEvent = function(){ //关闭事件
				 if(diag.innerFrame.contentWindow.document.getElementById('zhongxin').style.display == 'none'){
					 jump(${page.pageNo});
				}
				diag.close();
			 };
			 diag.show();
		}
		</script>
		
		<script type="text/javascript">
		
		$(function() {
			
			//下拉框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
			//复选框
			$('table th input:checkbox').on('click' , function(){
				var that = this;
				$(this).closest('table').find('tr > td:first-child input:checkbox')
				.each(function(){
					this.checked = that.checked;
					$(this).closest('tr').toggleClass('selected');
				});
					
			});
			
			//增加推送区消息类型 2015.10.13 guohua
			var url = "/qjqwmain/pusharea/queryAll?ids=4";
			$.post(url,function(result){
			     var list = result.list ; 
			     if(list!=null){
				     var option = "";
				      for(var i =0;i<list.length;i++){
				           option+="<option value='"+list[i]['push_name']+"'> "+list[i]['push_channel']+"</option>";
				      }
				      $("#push_type").append(option);
			     }
			},'json');
		});
		
		
		//批量操作
		function makeAll(msg){
			bootbox.confirm(msg, function(result) {
				if(result) {
					var str = '';
					for(var i=0;i < document.getElementsByName('ids').length;i++)
					{
						  if(document.getElementsByName('ids')[i].checked){
						  	if(str=='') str += document.getElementsByName('ids')[i].value;
						  	else str += ',' + document.getElementsByName('ids')[i].value;
						  }
					}
					if(str==''){
						bootbox.dialog("您没有选择任何内容!", 
							[
							  {
								"label" : "关闭",
								"class" : "btn-small btn-success",
								"callback": function() {
									//Example.show("great success");
									}
								}
							 ]
						);
						
						$("#zcheckbox").tips({
							side:3,
				            msg:'点这里全选',
				            bg:'#AE81FF',
				            time:8
				        });
						
						return;
					}else{
						if(msg == '确定要删除选中的数据吗?'){
							$.ajax({
								type: "POST",
								url: '/qjqwmain/news/deleteAll?tm='+new Date().getTime(),
						    	data: {DATA_IDS:str},
								dataType:'json',
								//beforeSend: validateData,
								cache: false,
								success: function(data){
									 $.each(data.list, function(i, list){
											jump(${page.pageNo});
									 });
								}
							});
						}
					}
				}
			});
		}
		
		</script>
		<script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>
		
	</body>
</html>

