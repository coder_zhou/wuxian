<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
	<head>
	<base href="<%=basePath%>"><!-- jsp文件头和头部 -->
	<%@ include file="/WEB-INF/system/admin/top.jsp"%> 
	</head>
<body>

<div class="col-sm-6 widget-container-col ui-sortable">
										<div class="widget-box ui-sortable-handle">
											<div class="widget-header widget-header-small">
												<h5 class="widget-title smaller">网站管理</h5>

												<!-- #section:custom/widget-box.tabbed -->
												<div class="widget-toolbar no-border">
													<ul class="nav nav-tabs" id="myTab">
														<li class="active">
															<a aria-expanded="true" data-toggle="tab" href="#home">参数设置</a>
														</li>

														<li class="">
															<a aria-expanded="false" data-toggle="tab" href="#profile">短信及邮件</a>
														</li>

														<li class="">
															<a aria-expanded="false" data-toggle="tab" href="#info">其他</a>
														</li>
													</ul>
												</div>

												<!-- /section:custom/widget-box.tabbed -->
											</div>

											<div class="widget-body">
												<div class="widget-main padding-6">
													<div class="tab-content">
														<div id="home" class="tab-pane active">
															<!-- 内容区 -->
															<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 网站名称</label>

										<div class="col-sm-9">
											<input  placeholder="网站名称" class="col-xs-10 col-sm-5" value="${applicationScope.SYSNAME}" type="text" name="SYSNAME" id="SYSNAME">
											<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('SYSNAME');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> SEO DESCRIPTION</label>

										<div class="col-sm-9">
											<input   placeholder="seo description" class="col-xs-10 col-sm-5" type="text" value="${applicationScope.SEODESCRIPTION}" name="SEODESCRIPTION" id="SEODESCRIPTION">
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('SEODESCRIPTION');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> SEO KEYWORDS </label>

										<div class="col-sm-9">
											<input   placeholder="seo keywords" class="col-xs-10 col-sm-5" type="text" value="${applicationScope.SEOKEYWORDS}" name="SEOKEYWORDS" id="SEOKEYWORDS">
																	<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('SEOKEYWORDS');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ICP </label>

										<div class="col-sm-9">
											<input   placeholder="icp" class="col-xs-10 col-sm-5" value="${applicationScope.ICP}"  type="text" name="ICP"  id="ICP">
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('ICP');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> COPYRIGHTS</label>

										<div class="col-sm-9">
											<input   placeholder="版权" value="${applicationScope.COPYRIGHT}" class="col-xs-10 col-sm-5" type="text" name="COPYRIGHT" id="COPYRIGHT" >
											
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('COPYRIGHT');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
										<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 公司名称</label>

										<div class="col-sm-9">
											<input   placeholder="公司名称" class="col-xs-10 col-sm-5" type="text" name="COMPANY" id="COMPANY" value="${applicationScope.COMPANY}">
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('COMPANY');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
										<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> 电话号码 </label>

										<div class="col-sm-9">
											<input   placeholder="TEL" class="col-xs-10 col-sm-5" type="text" name="TEL" id="TEL"  value="${applicationScope.TEL}">
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('TEL');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
										<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1">联系邮箱</label>

										<div class="col-sm-9">
											<input   placeholder="emial" class="col-xs-10 col-sm-5" type="text" name="EMAIL" id="EMAIL"  value="${applicationScope.EMAIL}">
												<span class="input-group-btn">
																	<button class="btn btn-sm btn-default" type="button" onclick="upparam('EMAIL');">
																		<i class="ace-icon fa fa-calendar bigger-110"></i>
																		修改
																	</button>
																</span>
										</div>
									</div>
															<!-- 内容区end -->
														</div>

														<div id="profile" class="tab-pane">
															<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.</p>
														</div>

														<div id="info" class="tab-pane">
															<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>




		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/bootbox.min.js"></script><!-- 确认窗口 -->
		<!-- 引入 -->
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script><!--提示框-->
		<script type="text/javascript">
		
	//	$(window.parent.hangge());
		
		//检索
		function search(){
			window.parent.jzts();
			$("#Form").submit();
		}
		
		//新增
		function add(){
			 window.parent.jzts();
			 var diag = new top.Dialog();
			 diag.Drag=true;
			 diag.Title ="新增";
			 diag.URL = '<%=basePath%>/qjqwmain/param/goAdd';
			 diag.Width = 450;
			 diag.Height = 355;
			 diag.CancelEvent = function(){ //关闭事件
				 if(diag.innerFrame.contentWindow.document.getElementById('zhongxin').style.display == 'none'){
					 if('${page.currentPage}' == '0'){
						 window.parent.jzts();
						 setTimeout("self.location.reload()",100);
					 }else{
						 jump(${page.currentPage});
					 }
				}
				diag.close();
			 };
			 diag.show();
		}
		
		//删除
		function del(Id){
			bootbox.confirm("确定要删除吗?", function(result) {
				if(result) {
					var url = "<%=basePath%>/qjqwmain/param/delete?PARAM_ID="+Id+"&tm="+new Date().getTime();
					$.get(url,function(data){
						if(data=="success"){
							jump(${page.currentPage});
						}
					});
				}
			});
		}
		
		//修改属性
		function upparam(obj){
			  var key=document.getElementById(obj);
			bootbox.confirm("确定要修改吗?", function(result) {
				if(result) {
                  
					var url = "<%=basePath%>/qjqwmain/param/upparam?key="+obj+"&values="+encodeURI(encodeURI(key.value))+"&tm="+new Date().getTime();
					$.get(url,
							function(data){
						if(data=="success"){
							jump(${page.currentPage});
						}
					});
				}
			});
		}
		
		
		//修改
		function edit(Id){
			 window.parent.jzts();
			 var diag = new top.Dialog();
			 diag.Drag=true;
			 diag.Title ="编辑";
			 diag.URL = '<%=basePath%>/qjqwmain/param/goEdit?PARAM_ID='+Id;
			 diag.Width = 450;
			 diag.Height = 355;
			 diag.CancelEvent = function(){ //关闭事件
				 if(diag.innerFrame.contentWindow.document.getElementById('zhongxin').style.display == 'none'){
					 jump(${page.currentPage});
				}
				diag.close();
			 };
			 diag.show();
		}
		</script>
		
		<script type="text/javascript">
		
		$(function() {
			
			//下拉框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
			//复选框
			$('table th input:checkbox').on('click' , function(){
				var that = this;
				$(this).closest('table').find('tr > td:first-child input:checkbox')
				.each(function(){
					this.checked = that.checked;
					$(this).closest('tr').toggleClass('selected');
				});
					
			});
			
		});
		
		

		</script>
		<script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>
		
	</body>
</html>
