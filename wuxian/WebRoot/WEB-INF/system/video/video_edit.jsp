﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<%=basePath%>">
		
		<meta charset="utf-8" />
		<title></title>
		
		<meta name="description" content="overview & stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/resources/css/font-awesome.min.css" />
		<!--[if IE 7]><link rel="stylesheet" href="/resources/css/font-awesome-ie7.min.css" /><![endif]-->
		<!--[if lt IE 9]><link rel="stylesheet" href="/resources/css/ace-ie.min.css" /><![endif]-->
		<!-- 下拉框 -->
		<link rel="stylesheet" href="/resources/css/chosen.css" />
		
		<link rel="stylesheet" href="/resources/css/ace.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-skins.min.css" />
		
		<link rel="stylesheet" href="/resources/css/datepicker.css" /><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/jquery-1.7.2.js"></script>
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
		
<script type="text/javascript">
	
	
	
	//保存
	function save(){
			if($("#video_photo").val()==""){
			$("#video_photo").tips({
				side:3,
	            msg:'请输入视频封面',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#video_photo").focus();
			return false;
		}
		if($("#video_url").val()==""){
			$("#video_url").tips({
				side:3,
	            msg:'请输入视频路径',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#video_url").focus();
			return false;
		}
		if($("#video_name").val()==""){
			$("#video_name").tips({
				side:3,
	            msg:'请输入视频名称',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#video_name").focus();
			return false;
		}
		$("#Form").submit();
		$("#zhongxin").hide();
		$("#zhongxin2").show();
	}
	
</script>
    <link href="/resources/css/cropper.min.css" rel="stylesheet">
        <link href="/resources/css/docs.css" rel="stylesheet">
	</head>
<body>
	<form action="/qjqwmain/video/${msg }" name="Form" id="Form" method="post">
		<input type="hidden" name="video_id" id="video_id" value="${video.video_id}"/>
		<div id="zhongxin">
		<table  class="table table-striped table-bordered table-hover">
			<tr>
				<td>视频封面:</td><td>
				<input type="hidden"  name="video_photo" id="img_url" value="${video.video_photo}">
				<img src="${video.video_photo}?imageMogr2/thumbnail/50x50" onerror="javascript:this.src='/resources/images/noPic.png';" id="ii" width="100" height="100" onclick="show();">
						<div class="main_content" id="content" style="display: none;">

          <div class="img-container"><img src="" style="display:none"></div>
          <div class="img-preview img-preview-sm"></div>
          <div class="btn-group">
            <div class="btn-primary" data-method="zoom" data-option="0.1"  title="放大">
				<label><img src="/resources/img/big.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="zoom" data-option="-0.1"  title="缩小">
				 <label><img src="/resources/img/small.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="-90"  title="左转90度">
				 <label><img src="/resources/img/left.png" width="40" height="25"/></label>
            </div>
            <div class="btn-primary" data-method="rotate" data-option="90"  title="右转90度">
				<label><img src="/resources/img/right.png" width="40" height="25"/></label>
            </div>
         
           
          </div>
          <div class="foot_btn"> <input type="file" style="width: 100px;" id="inputImage"   accept="image/*" placeholder="这里输入文章封面图片" title="文章封面图片">  <button  id="getDataURL2" data-toggle="tooltip" type="button" title="$().cropper('getDataURL', 'image/jpeg')"  >确定</button></div>

         
  </div>
  <input type="hidden" id="imgStr"/><!-- 这里是上传到服务器的截取后的图片的base64码,因为此参数在docs.js中指定了，所以不要该参数名，如需更改务必保持页面此input的id和docs.js中的一致 -->
				
				</td>
			</tr>
			<tr>
				<td>视频路径:</td><td><input type="text" name="video_url" id="video_url" value="${video.video_url}"  placeholder="这里输入视频路径" title="视频路径"/></td>
			</tr>
			<tr>
				<td>视频名称:</td><td><input type="text" name="video_name" id="video_name" value="${video.video_name}" maxlength="32" placeholder="这里输入视频名称" title="视频名称"/></td>
			</tr>
			<tr>
				<td style="text-align: center;">
					<a class="btn btn-mini btn-primary" onclick="save();">保存</a>
					<a class="btn btn-mini btn-danger" onclick="top.Dialog.close();">取消</a>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="zhongxin2" class="center" style="display:none"><br/><br/><br/><br/><br/><img src="/resources/images/jiazai.gif" /><br/><h4 class="lighter block green">提交中...</h4></div>
		
	</form>
	
	
		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		<script type="text/javascript">
		$(window.parent.hangge());
		$(function() {
			
			//单选框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
		});
		function show(){
			$("#content").show();
		}
		</script>
		 <script src="/resources/js/cropper.min.js"></script>
  <script src="/resources/js/docs.js"></script>
  <script>
      function test(){
		  var imgStr = $("#imgStr").val();
		  if(imgStr!="" || imgStr!=undefined){
		     console.log(imgStr);
		 	$.ajax({
				type: "POST",
				url: '/picture/savePic',
		    	data: {'imgStr':imgStr},
				dataType:'json',
				//beforeSend: validateData,
				cache: false,
				success: function(data){
					$("#ii").attr("src",data.imgUrl);
					$("#img_url").val(data.imgUrl);
					$("#content").hide();
				}
			});
		     
			
	      }

	  }
  
  </script>
		
</body>
</html>