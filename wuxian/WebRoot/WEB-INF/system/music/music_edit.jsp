<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<%=basePath%>">
		
		<meta charset="utf-8" />
		<title></title>
		
		<meta name="description" content="overview & stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/resources/css/font-awesome.min.css" />
		<!--[if IE 7]><link rel="stylesheet" href="/resources/css/font-awesome-ie7.min.css" /><![endif]-->
		<!--[if lt IE 9]><link rel="stylesheet" href="/resources/css/ace-ie.min.css" /><![endif]-->
		<!-- 下拉框 -->
		<link rel="stylesheet" href="/resources/css/chosen.css" />
		
		<link rel="stylesheet" href="/resources/css/ace.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-skins.min.css" />
		
		<link rel="stylesheet" href="/resources/css/datepicker.css" /><!-- 日期框 -->
		<script type="text/javascript" src="/resources/js/jquery-1.7.2.js"></script>
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>
		
<script type="text/javascript">
	
	
	
	//保存
	function save(){
			if($("#musicName").val()==""){
			$("#musicName").tips({
				side:3,
	            msg:'请输入音乐名称',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#musicName").focus();
			return false;
		}
		if($("#musicUrl").val()==""){
			$("#musicUrl").tips({
				side:3,
	            msg:'请输入音乐路径',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#musicUrl").focus();
			return false;
		}
		if($("#musicType").val()==""){
			$("#musicType").tips({
				side:3,
	            msg:'请输入音乐类型',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#musicType").focus();
			return false;
		}
		if($("#musicInfo").val()==""){
			$("#musicInfo").tips({
				side:3,
	            msg:'请输入音乐说明',
	            bg:'#AE81FF',
	            time:2
	        });
			$("#musicInfo").focus();
			return false;
		}
		$("#Form").submit();
		$("#zhongxin").hide();
		$("#zhongxin2").show();
	}
	
</script>
	</head>
<body>
	<form action="/qjqwmain/music/${msg }" name="Form" id="Form" method="post">
		<input type="hidden" name="musicId" id="musicId" value="${music.musicId}"/>
		<div id="zhongxin">
		<table>
			<tr>
				<td>音乐名称:</td><td><input type="text" name="musicName" id="musicName" value="${music.musicName}" maxlength="32" placeholder="这里输入音乐名称" title="音乐名称"/></td>
			</tr>
			<tr>
				<td>音乐路径:</td><td><input type="text" name="musicUrl" id="musicUrl" value="${music.musicUrl}" maxlength="32" placeholder="这里输入音乐路径" title="音乐路径"/></td>
			</tr>
			<tr>
				<td>音乐类型:</td><td><input type="number" name="musicType" id="musicType" value="${music.musicType}" maxlength="32" placeholder="这里输入音乐类型" title="音乐类型"/></td>
			</tr>
			<tr>
				<td>音乐介绍:</td><td><input type="text" name="musicInfo" id="musicInfo" value="${music.musicInfo}" maxlength="32" placeholder="这里输入音乐说明" title="音乐说明"/></td>
			</tr>
			<tr>
				<td style="text-align: center;">
					<a class="btn btn-mini btn-primary" onclick="save();">保存</a>
					<a class="btn btn-mini btn-danger" onclick="top.Dialog.close();">取消</a>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="zhongxin2" class="center" style="display:none"><br/><br/><br/><br/><br/><img src="/resources/images/jiazai.gif" /><br/><h4 class="lighter block green">提交中...</h4></div>
		
	</form>
	
	
		<!-- 引入 -->
		<script type="text/javascript">window.jQuery || document.write("<script src='/resources/js/jquery-1.9.1.min.js'>\x3C/script>");</script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/ace-elements.min.js"></script>
		<script src="/resources/js/ace.min.js"></script>
		<script type="text/javascript" src="/resources/js/chosen.jquery.min.js"></script><!-- 下拉框 -->
		<script type="text/javascript" src="/resources/js/bootstrap-datepicker.min.js"></script><!-- 日期框 -->
		<script type="text/javascript">
		$(window.parent.hangge());
		$(function() {
			
			//单选框
			$(".chzn-select").chosen(); 
			$(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
			
			//日期框
			$('.date-picker').datepicker();
			
		});
		
		</script>
</body>
</html>