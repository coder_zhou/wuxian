<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="ctx" value="/resources" />
<html lang="en"
	class="app js no-touch no-android chrome no-firefox no-iemobile no-ie no-ie10 no-ie11 no-ios no-ios7 ipad">
<head>
<link rel="stylesheet" href="/resources/notebook/notebook_files/font.css" type="text/css">
<link rel="stylesheet" href="/resources/notebook/notebook_files/app.v1.css" type="text/css">
<link rel="stylesheet" href="/resources/css/lanyuan.css" type="text/css">
	<!-- base start 重要部分不可删改-->
	<script type="text/javascript" src="/resources/notebook/notebook_files/app.v1.js"></script>
<script type="text/javascript" src="/resources/notebook/notebook_files/app.plugin.js"></script>
<script type="text/javascript" src="/resources/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="/resources/js/jquery/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="/resources/js/jquery/jquery-validation/messages_cn.js"></script>
<script type="text/javascript" src="/resources/js/layer-v1.9.2/layer/layer.js"></script>
<script type="text/javascript" src="/resources/js/common.js"></script>
<script type="text/javascript" src="/resources/js/lyGrid.js"></script>
<!--[if lt IE 9]> <script src="/resources/js/jquery/ie/html5shiv.js"></script> <script src="/resources/js/jquery/ie/respond.min.js"></script><![endif]-->
	<%@ include file="/WEB-INF/system/admin/top.jsp"%> 
<style type="text/css">
.l_err{
    background: none repeat scroll 0 0 #FFFCC7;
    border: 1px solid #FFC340;
    font-size: 12px;
    padding: 4px 8px;
    width: 200px;
    display: none;
}
.error{
  border: 3px solid #FFCCCC;
}
.form-group{
  padding-left: 15px
}
.left{
	text-align: left;
	padding-left: 10px;
}
.right{
	text-align: right;
}
</style>
<script type="text/javascript">
var rootPath = "";
function onloadurl(){
	$("[data-url]").each(function () {
		var tb = $(this);
		tb.html(CommnUtil.loadingImg());
		//tb.load(rootPath,tb.attr("data-url"));
    });
}
layer.config({
    extend: ['skin/style.css'], //加载新皮肤
    fix : false,// 用于设定层是否不随滚动条而滚动，固定在可视区域。
    skin: 'layer-ext-myskin' //一旦设定，所有弹层风格都采用此主题。
});
</script>
<script src="/resources/echarts/esl/esl.js"
	type="text/javascript"></script>
<script src="/resources/echarts/echarts-all.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="/resources/js/system/monitor/systemInfo.js"></script>
	<script type="text/javascript">
	function modifySer(key){
		$.ajax({
	        async: false,
	        url: "/qjqwmain/monitor/modifySer",
	        data:{"key":key,"value":$("#"+key).val()},
	        dataType: "json",
	        success: function (data) {
	    	    if(data.flag){
	    	    	alert("更新成功！");
	    	    }else{
	    	    	alert("更新失败！");
	    	    }
	        }
		});
	}
	$(window.parent.hangge());
	</script>
</head>
<body class="" style="" >
	<section class="vbox">
		<div class="row"
			style="padding-right: 8px; padding-left: 8px; padding-top: 8ps; padding-bottom: 30px;">
			<div class="col-md-12 portlet ui-sortable"
				style="padding-bottom: 15px">
				<section class="panel panel-success portlet-item">
					<header class="panel-heading">
						<i class="fa fa-briefcase"></i> 警告设置
					</header>
					<table class="table table-striped table-bordered table-hover"
						width="100%" style="vertical-align: middle;">
						<thead>
							<tr style="background-color: #faebcc; text-align: center;">
								<td width="100">名称</td>
								<td width="100">参数</td>
								<td width="205">预警设置</td>
								<td width="375">邮箱设置</td>
							</tr>
						</thead>
						<tbody id="tbody">
							<tr>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>CPU</td>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>当前使用率：<span
									id="td_cpuUsage" style="color: red;">50</span> %
								</td>
								<td align="center">
									<table>
										<tr>
											<td>使用率超出</td>
											<td><input class='inputclass' name='cpu' id='cpu'
												type='text' value='${cpu}' /> %,</td>
											<td>发送邮箱提示 <a class='btn btn-info'
												href='javascript:void(0)' onclick='modifySer("cpu");'>
													修改 </a></td>
										</tr>
									</table>
								</td>
								<td rowspan='3' align="center" style="vertical-align: middle;"><input
									class='inputclass' style='width: 250px; height: 32px;'
									name='toEmail' id='toEmail' type='text'
									value='${toEmail}' /><a class='btn btn-info'
									href='javascript:void(0)' onclick='modifySer("toEmail");'>
										修改 </a></td>
							</tr>
							<tr>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>服务器内存</td>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>当前使用率：<span
									id="td_serverUsage" style="color: blue;">50</span> %
								</td>
								<td align="center">
									<table>
										<tr>
											<td>使用率超出</td>
											<td><input class='inputclass' name='ram' id='ram'
												type='text' value='${ram}' /> %,</td>
											<td>发送邮箱提示 <a class='btn btn-info'
												href='javascript:void(0)' onclick='modifySer("ram");'>
													修改 </a></td>
										</tr>
									</table>

								</td>
							</tr>
							<tr>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>JVM内存</td>
								<td style='padding-left: 10px; text-align: left;vertical-align: middle;'>当前使用率：<span
									id="td_jvmUsage" style="color: green;">50</span> %
								</td>
								<td align="center">
									<table>
										<tr>
											<td>使用率超出</td>
											<td><input class='inputclass' name='jvm' id='jvm'
												type='text' value='${jvm}' /> %,</td>
											<td>发送邮箱提示 <a class='btn btn-info'
												href='javascript:void(0)' onclick='modifySer("jvm");'>
													修改 </a></td>
										</tr>
									</table>
							</tr>
						</tbody>
					</table>
				</section>
			</div>
			<div class="col-md-6">
				<section class="panel panel-info portlet-item">
					<header class="panel-heading">
						<i class="fa fa-th-list"></i> 服务器信息
					</header>
					<div class="panel-body" style="padding: 0px"
						>
		<table class="table table-striped table-bordered table-hover" style="margin: 0px;">
	<tbody>
		<tr>
			<td class="left">ip地址</td>
			<td id="hostIp" class="left">${systemInfo.hostIp}</td>
		</tr>
		<tr>
			<td class="left">主机名</td>

			<td class="left" id="hostName">${systemInfo.hostName}</td>
		</tr>
		<tr>
			<td class="left">操作系统的名称</td>

			<td class="left" id="osName">${systemInfo.osName}</td>
		</tr>
		<tr>
			<td class="left">操作系统的构架</td>

			<td class="left" id="arch">${systemInfo.arch}</td>
		</tr>
		<tr>
			<td class="left">操作系统的版本</td>

			<td class="left" id="osVersion">${systemInfo.osVersion}</td>
		</tr>
		<tr>
			<td class="left">处理器个数</td>

			<td class="left" id="processors">${systemInfo.processors}</td>
		</tr>
		<tr>
			<td class="left">Java的运行环境版本</td>

			<td class="left" id="javaVersion">${systemInfo.javaVersion}</td>
		</tr>
		<tr>
			<td class="left">Java供应商的URL</td>

			<td class="left" id="javaUrl">${systemInfo.javaUrl}</td>
		</tr>
		<tr>
			<td class="left">Java的安装路径</td>

			<td class="left" id="javaHome">${systemInfo.javaHome}</td>
		</tr>
		<tr>
			<td class="left">临时文件路径</td>

			<td class="left" id="tmpdir">${systemInfo.tmpdir}</td>
		</tr>
	</tbody>
</table>
						</div>
				</section>
			</div>
			<div class="col-md-6">
				<section class="panel panel-danger portlet-item">
					<header class="panel-heading">
						<i class="fa fa-fire"></i> 实时监控
					</header>

					<div class="panel-body">
						<div id="main" style="height: 370px;"></div>
					</div>
				</section>
			</div>
			<!-- /.span -->
			<div class="col-md-12" style="margin-top: 10px; height: 330px">
				<section class="panel panel-primary portlet-item">
					<header class="panel-heading">
						<i class="fa fa-rss-square"></i> 实时监控
					</header>

					<div class="panel-body">
						<table style="width: 100%;">
							<tr>
								<td width="33.3%"><div id="main_one" style="height: 240px;"></div></td>
								<td width="33.3%"><div id="main_two" style="height: 240px;"></div></td>
								<td width="33.3%"><div id="main_three"
										style="height: 240px;"></div></td>
							</tr>
						</table>
					</div>
				</section>
			</div>
			<!-- /.span -->
		</div>
	</section>
</body>
</html>
