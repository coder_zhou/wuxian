﻿
		
		<meta charset="utf-8" />
		<title>${applicationScope.SYSNAME}</title>
		<meta name="description" content="${applicationScope.seodescription}" />
        <meta name="keywords" content="${applicationScope.seokeywords}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="/resources/css/bootstrap.min.css" rel="stylesheet" />
		<link href="/resources/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/resources/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="/resources/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		
		<!-- 下拉框-->
		<link rel="stylesheet" href="/resources/css/chosen.css" />
		
		<!-- ace styles -->
		<link rel="stylesheet" href="/resources/css/ace.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="/resources/css/ace-skins.min.css" />
		<!--[if lt IE 9]>
		  <link rel="stylesheet" href="/resources/css/ace-ie.min.css" />
		<![endif]-->
		
		<script type="text/javascript" src="/resources/js/jquery-1.7.2.js"></script>
		
		<link rel="stylesheet" href="/resources/css/datepicker.css" /><!-- 日期框 -->
		
		<!--引入弹窗组件start-->
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDrag.js"></script>
		<script type="text/javascript" src="/resources/js/attention/zDialog/zDialog.js"></script>
		<!--引入弹窗组件end-->
		
		<script type="text/javascript" src="/resources/js/jquery.tips.js"></script>