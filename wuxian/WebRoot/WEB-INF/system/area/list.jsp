<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'list.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <ul>
<c:choose>
<c:when test="${not empty  areaList}">
<c:forEach items="${areaList}" var="area" varStatus="vs">
<li>
ID:${area.areaId}
NAME:${area.areaName}
<a href="/area/listCity?proId=${area.areaId}" >获取下级</a>
<a href="/area/getParent?proId=${area.parentid}">获取上级</a>
<a href="/area/toaddSon?proId=${area.areaId}">添加下级 </a>
<a href="/area/delCity?id=${area.areaId}"> 删除自己</a>
<a href="/area/toedit?id=${area.areaId}">修改自己</a>
</li>

</c:forEach>
</c:when>

</c:choose>
</ul>
  </body>
</html>
