
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `TB_PEILIAN`
-- ----------------------------
DROP TABLE IF EXISTS `TB_PEILIAN`;
CREATE TABLE `TB_PEILIAN` (
 		`PEILIAN_ID` varchar(100) NOT NULL,
		`pl_id` int(11) NOT NULL COMMENT '陪练ID',
		`pl_name` varchar(255) DEFAULT NULL COMMENT '陪练名称',
		`pl_age` int(11) NOT NULL COMMENT '陪练年龄',
		`pl_sex` varchar(255) DEFAULT NULL COMMENT '陪练性别',
		`pl_job` varchar(255) DEFAULT NULL COMMENT '陪练工作',
		`pl_jiesao` varchar(255) DEFAULT NULL COMMENT '陪练介绍',
		`pl_photo` varchar(255) DEFAULT NULL COMMENT '陪练头像',
  		PRIMARY KEY (`PEILIAN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
