package com.qf.system.peilian.service;


import com.qf.system.peilian.model.Peilian;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface PeilianService {


     int save(Peilian peilian);
     int delete(String peilian_id);
     int edit(Peilian peilian);
     Map<String,Object> list(Page page, PageData pd);
     Peilian findById(String peilian_id);
     void deleteAll(List<Integer> list);

}
