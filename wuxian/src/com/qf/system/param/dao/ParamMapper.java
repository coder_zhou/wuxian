package com.qf.system.param.dao;

import java.util.List;

import com.qf.system.param.model.Param;

public interface ParamMapper {
    int deleteByPrimaryKey(Integer paramId);

    int insert(Param record);

    int insertSelective(Param record);

    Param selectByPrimaryKey(Integer paramId);

    int updateByPrimaryKeySelective(Param record);

    int updateByPrimaryKey(Param record);

	List<Param> list();

	Param getSysName();

	int findByName(String param_name);
}