package com.qf.system.param.service;

import java.util.List;

import com.qf.system.param.model.Param;


public interface ParamService {


	int save(Param param);

	int delete(String Param_ID);

	int edit(Param param);

	List<Param> list();

	Param findById(String Param_ID);

	void deleteAll(List<Integer> list);

   	List<Param> listAll();

	Param getSysName();

	int findByName(String string);

}
