package com.qf.system.param.service;



import java.util.List;










import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qf.system.param.dao.ParamMapper;
import com.qf.system.param.model.Param;



@Service("paramService")
public class ParamServiceImpl implements ParamService {
	
	private ParamMapper dao;

	public ParamMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(ParamMapper dao) {
		this.dao = dao;
	}
	@Override
	public int save(Param param) {
		// TODO Auto-generated method stub
		return dao.insert(param);
	}
	@Override
	public int delete(String Param_ID) {
		// TODO Auto-generated method stub
		return dao.deleteByPrimaryKey(Integer.parseInt(Param_ID));
	}
	@Override
	public int edit(Param param) {
		// TODO Auto-generated method stub
		return dao.updateByPrimaryKeySelective(param);
	}
	@Override
	public List<Param> list() {
		// TODO Auto-generated method stub
		return dao.list();
	}
	@Override
	public Param findById(String Param_ID) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(Integer.parseInt(Param_ID));
	}
	@Override
	public void deleteAll(List<Integer> list) {
		// TODO Auto-generated method stub
//		ParamExample example=new ParamExample();
//		example.createCriteria().andParamIdIn(list);
//		dao.deleteByExample(example);
	}
		@Override
	public List<Param> listAll() {
		// TODO Auto-generated method stub
		return dao.list();
	}
		@Override
		public Param getSysName() {
			// TODO Auto-generated method stub
			return dao.getSysName();
		}
		
		@Override
		public int findByName(String string) {
			// TODO Auto-generated method stub
			return dao.findByName(string);
		}
}
