package com.qf.system.serverInfo.dao;

import java.util.List;

import com.qf.system.serverInfo.model.ServerInfo;

public interface ServerInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ServerInfo record);

    int insertSelective(ServerInfo record);

    ServerInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ServerInfo record);

    int updateByPrimaryKey(ServerInfo record);

	List<ServerInfo> list();
}