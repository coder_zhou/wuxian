package com.qf.system.serverInfo.controller;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;











import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.hyperic.sigar.Sigar;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.qf.system.menu.model.Menu;
import com.qf.system.serverInfo.model.ServerInfo;
import com.qf.system.serverInfo.service.ServerInfoService;
import com.qf.util.PageData;
import com.qf.util.PropertyReader;
import com.qf.util.common.Const;
import com.qf.util.controller.BaseController;
import com.qf.util.page.Page;


@Controller
@RequestMapping("/qjqwmain/monitor/")
public class ServerInfoController extends BaseController {
	@Resource(name="serverInfoService")
	private ServerInfoService serverInfoService;
	@RequestMapping("list")
	public String listUI() throws Exception {
		return  "/WEB-INF/system/monitor/list";
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("findByPage")
	public ModelAndView findByPage(Page page){
		logBefore(logger, "userController");
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			this.getHC(); //调用权限
			//////////////////////////////
			Map<String, Object> map = serverInfoService.list(page);
			page = (Page)map.get("page");
			List<ServerInfo> list = (List<ServerInfo>)map.get("list");
			mv.addObject("varList", list);
			mv.addObject("pd", pd);
			mv.addObject("page", page);
			mv.setViewName("/WEB-INF/system/appuser/appuser_list");
			
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		return mv;
	}
	
	
	@RequestMapping("info")
	public ModelAndView info(Model model) throws Exception {
		ModelAndView mv=new ModelAndView();
		model.addAttribute("cpu", 	PropertyReader.readValue(Const.CONF, "cpu"));
		model.addAttribute("jvm", PropertyReader.readValue(Const.CONF, "jvm"));
		model.addAttribute("ram",PropertyReader.readValue(Const.CONF, "ram"));
		model.addAttribute("toEmail", PropertyReader.readValue(Const.CONF, "toEmail"));
		model.addAttribute("systemInfo", ServerInfo.SystemProperty());
		mv.setViewName("/WEB-INF/system/monitor/info");
		return  mv;
	}
	
	@RequestMapping("monitor")
	public ModelAndView monitor() throws Exception {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("/WEB-INF/system/monitor/monitor");
		return mv;
	}
	
	@RequestMapping("systemInfo")
	public ModelAndView systemInfo(Model model) throws Exception {
		ModelAndView mv=new ModelAndView();
		mv.addObject("systemInfo",ServerInfo.SystemProperty());
		mv.setViewName("/WEB-INF/system/monitor/systemInfo");
		return mv;
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping("usage")
	public Map<String, Object> usage(Model model) throws Exception {
		return ServerInfo.usage(new Sigar());
	}
	/**
	 * 修改配置　
	 * @param request
	 * @param nodeId
	 * @return
	 * @throws Exception
	 */
    @ResponseBody
	@RequestMapping("/modifySer")
    public Map<String, Object> modifySer(String key,String value) throws Exception{
    	Map<String, Object> dataMap = new HashMap<String,Object>();
    	try {
		// 从输入流中读取属性列表（键和元素对）
    		PropertyReader.writeData(key, value, Const.CONF);
		} catch (Exception e) {
			dataMap.put("flag", false);
		}
    	dataMap.put("flag", true);
		return dataMap;
    }
    
    
	/* ===============================权限================================== */
	@SuppressWarnings("unchecked")
	public void getHC(){
		ModelAndView mv = this.getModelAndView();
		HttpSession session = this.getRequest().getSession();
		Map<String, String> map = (Map<String, String>)session.getAttribute(Const.SESSION_QX);
		mv.addObject(Const.SESSION_QX,map);	//按钮权限
		List<Menu> menuList = (List<Menu>)session.getAttribute(Const.SESSION_menuList);
		mv.addObject(Const.SESSION_menuList, menuList);//菜单权限
	}
	/* ===============================权限================================== */
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(format,true));
	}
}