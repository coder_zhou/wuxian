package com.qf.system.serverInfo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qf.system.serverInfo.dao.ServerInfoMapper;
import com.qf.util.interceptor.PageHelper;
import com.qf.util.page.Page;

@Service("serverInfoService")
public class ServerInfoServiceImpl implements ServerInfoService {

	private ServerInfoMapper dao;
	
	public ServerInfoMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(ServerInfoMapper dao) {
		this.dao = dao;
	}

	@Override
	public Map<String,Object> list(Page page) {
		Map<String,Object> map = new HashMap<String,Object>();
		
		PageHelper.startPage(page); //起始位置，长度
	    dao.list();
		Page p  = PageHelper.endPage();
		page.setRowCount(p.getRowCount());
		page.setPageCount(p.getPageCount());
		map.put("page", page);
		map.put("list", p.getResult());
		return map;
	}

}
