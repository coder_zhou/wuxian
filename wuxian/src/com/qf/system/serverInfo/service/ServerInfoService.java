package com.qf.system.serverInfo.service;

import java.util.Map;

import com.qf.util.page.Page;

public interface ServerInfoService {

	Map<String,Object> list(Page page);

}
