package com.qf.system.serverInfo.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.Swap;

import com.qf.util.PageData;
import com.qf.util.PublicUtil;

public class ServerInfo {
    private Integer id;

    private String cpuusage;

    private String setcpuusage;

    private String jvmusage;

    private String setjvmusage;

    private String ramusage;

    private String setramusage;

    private String email;

    private Date opertime;

    private String mark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpuusage() {
        return cpuusage;
    }

    public void setCpuusage(String cpuusage) {
        this.cpuusage = cpuusage == null ? null : cpuusage.trim();
    }

    public String getSetcpuusage() {
        return setcpuusage;
    }

    public void setSetcpuusage(String setcpuusage) {
        this.setcpuusage = setcpuusage == null ? null : setcpuusage.trim();
    }

    public String getJvmusage() {
        return jvmusage;
    }

    public void setJvmusage(String jvmusage) {
        this.jvmusage = jvmusage == null ? null : jvmusage.trim();
    }

    public String getSetjvmusage() {
        return setjvmusage;
    }

    public void setSetjvmusage(String setjvmusage) {
        this.setjvmusage = setjvmusage == null ? null : setjvmusage.trim();
    }

    public String getRamusage() {
        return ramusage;
    }

    public void setRamusage(String ramusage) {
        this.ramusage = ramusage == null ? null : ramusage.trim();
    }

    public String getSetramusage() {
        return setramusage;
    }

    public void setSetramusage(String setramusage) {
        this.setramusage = setramusage == null ? null : setramusage.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getOpertime() {
        return opertime;
    }

    public void setOpertime(Date opertime) {
        this.opertime = opertime;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
    }
	public static PageData SystemProperty() {
		 PageData pd=new PageData();
		Runtime r = Runtime.getRuntime();
		Properties props = System.getProperties();
		InetAddress addr = null;
		String ip = "";
		String hostName = "";
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			ip = "无法获取主机IP";
			hostName = "无法获取主机名";
		}
		if (null != addr) {
			try {
				ip = addr.getHostAddress();
			} catch (Exception e) {
				ip = "无法获取主机IP";
			}
			try {
				hostName = addr.getHostName();
			} catch (Exception e) {
				hostName = "无法获取主机名";
			}
		}
		pd.put("hostIp", ip);// 本地ip地址
		pd.put("hostName", hostName);// 本地主机名
		pd.put("osName", props.getProperty("os.name"));// 操作系统的名称
		pd.put("arch", props.getProperty("os.arch"));// 操作系统的构架
		pd.put("osVersion", props.getProperty("os.version"));// 操作系统的版本
		pd.put("processors", r.availableProcessors());// JVM可以使用的处理器个数
		pd.put("javaVersion", props.getProperty("java.version"));// Java的运行环境版本
		pd.put("vendor", props.getProperty("java.vendor"));// Java的运行环境供应商
		pd.put("javaUrl", props.getProperty("java.vendor.url"));// Java供应商的URL
		pd.put("javaHome", props.getProperty("java.home"));// Java的安装路径
		pd.put("tmpdir", props.getProperty("java.io.tmpdir"));// 默认的临时文件路径
		return pd;
	}

	public static PageData memory(Sigar sigar) {
		 PageData pd=new PageData();
		try {
			Runtime r = Runtime.getRuntime();
			pd.put("jvmTotal", PublicUtil.div(r.totalMemory(), (1024 * 1024), 2) + "M");// java总内存
			pd.put("jvmUse", PublicUtil.div(r.totalMemory() - r.freeMemory(), (1024 * 1024), 2) + "M");// JVM使用内存
			pd.put("jvmFree", PublicUtil.div(r.freeMemory(), (1024 * 1024), 2) + "M");// JVM剩余内存
			pd.put("jvmUsage", PublicUtil.div(r.totalMemory() - r.freeMemory(), r.totalMemory(), 2));// JVM使用率

			Mem mem = sigar.getMem();
			// 内存总量
			pd.put("ramTotal", PublicUtil.div(mem.getTotal(), (1024 * 1024 * 1024), 2) + "G");// 内存总量
			pd.put("ramUse", PublicUtil.div(mem.getUsed(), (1024 * 1024 * 1024), 2) + "G");// 当前内存使用量
			pd.put("ramFree", PublicUtil.div(mem.getFree(), (1024 * 1024 * 1024), 2) + "G");// 当前内存剩余量
			pd.put("ramUsage", PublicUtil.div(mem.getUsed(), mem.getTotal(), 2));// 内存使用率

			Swap swap = sigar.getSwap();
			// 交换区总量
			pd.put("swapTotal", PublicUtil.div(swap.getTotal(), (1024 * 1024 * 1024), 2) + "G");
			// 当前交换区使用量
			pd.put("swapUse", PublicUtil.div(swap.getUsed(), (1024 * 1024 * 1024), 2) + "G");
			// 当前交换区剩余量
			pd.put("swapFree", PublicUtil.div(swap.getFree(), (1024 * 1024 * 1024), 2) + "G");
			pd.put("swapUsage", PublicUtil.div(swap.getUsed(), swap.getTotal(), 2));//

		} catch (Exception e) {
		}
		return pd;
	}
	public static PageData usage(Sigar sigar) {
		 PageData pd=new PageData();
		try {
			Runtime r = Runtime.getRuntime();
			pd.put("jvmUsage", Math.round(PublicUtil.div(r.totalMemory()-r.freeMemory(), r.totalMemory(), 2)*100));// JVM使用率

			Mem mem = sigar.getMem();
			// 内存总量
			pd.put("ramUsage", Math.round(PublicUtil.div(mem.getUsed(), mem.getTotal(), 2)*100));// 内存使用率

 			List<PageData> cpu = cpuInfos(sigar);
			double b = 0.0;
			for (PageData m : cpu) {
				b += Double.valueOf(m.get("cpuTotal")+"");
			}
			pd.put("cpuUsage", Math.round(b/cpu.size()));// cpu使用率
		} catch (Exception e) {
		}
		return pd;
	}

	public static List<PageData> cpuInfos(Sigar sigar) {
		List<PageData> pds = new ArrayList<PageData>();
		try {
			CpuPerc cpuList[] = sigar.getCpuPercList();
			for (CpuPerc cpuPerc : cpuList) {
				PageData pd = new PageData();
				pd.put("cpuUserUse", Math.round(cpuPerc.getUser()*100));// 用户使用率
				pd.put("cpuSysUse", Math.round(cpuPerc.getSys()*100));// 系统使用率
				pd.put("cpuWait", Math.round(cpuPerc.getWait()*100));// 当前等待率
				pd.put("cpuFree", Math.round(cpuPerc.getIdle()*100));// 当前空闲率
				pd.put("cpuTotal",Math.round(cpuPerc.getCombined()*100));// 总的使用率
				pds.add(pd);
			}
		} catch (Exception e) {
		}
		return pds;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> diskInfos(Sigar sigar) throws Exception {
		List<Map<String, Object>> pds = new ArrayList<Map<String, Object>>();
		FileSystem fslist[] = sigar.getFileSystemList();
		for (int i = 0; i < fslist.length; i++) {
			Map<String,Object> pd = new PageData();
			FileSystem fs = fslist[i];
			// 文件系统类型名，比如本地硬盘、光驱、网络文件系统等
			FileSystemUsage usage = null;
			usage = sigar.getFileSystemUsage(fs.getDirName());
			switch (fs.getType()) {
			case 0: // TYPE_UNKNOWN ：未知
				break;
			case 1: // TYPE_NONE
				break;
			case 2: // TYPE_LOCAL_DISK : 本地硬盘

				pd.put("diskName", fs.getDevName());// 系统盘名称
				pd.put("diskType", fs.getSysTypeName());// 盘类型
				// 文件系统总大小
				pd.put("diskTotal", fs.getSysTypeName());
				// 文件系统剩余大小
				pd.put("diskFree", usage.getFree());
				// 文件系统已经使用量
				pd.put("diskUse", usage.getUsed());
				double usePercent = usage.getUsePercent() * 100D;
				// 文件系统资源的利用率
				pd.put("diskUsage", usePercent);// 内存使用率
				pds.add(pd);
				break;
			case 3:// TYPE_NETWORK ：网络
				break;
			case 4:// TYPE_RAM_DISK ：闪存
				break;
			case 5:// TYPE_CDROM ：光驱
				break;
			case 6:// TYPE_SWAP ：页面交换
				break;
			}
		}
		return pds;
	}
}