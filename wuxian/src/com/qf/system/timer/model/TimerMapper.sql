
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `TB_TIMER`
-- ----------------------------
DROP TABLE IF EXISTS `TB_TIMER`;
CREATE TABLE `TB_TIMER` (
 		`TIMER_ID` varchar(100) NOT NULL,
		`time_id` int(11) NOT NULL COMMENT '时段ID',
		`stattime` varchar(255) DEFAULT NULL COMMENT '开始时间',
		`endtime` varchar(255) DEFAULT NULL COMMENT '结束时间',
		`timstr` varchar(255) DEFAULT NULL COMMENT '时间段文字',
		`timeimg` varchar(255) DEFAULT NULL COMMENT '时段图片',
		`xueyuan` int(11) NOT NULL COMMENT '学员数',
		`xueyuan_laile` int(11) NOT NULL COMMENT '已预约人数',
  		PRIMARY KEY (`TIMER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
