package com.qf.system.timer.model;

import java.util.List;

import org.omg.PortableInterceptor.USER_EXCEPTION;

import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;

public class Timer3{

	
	private int time_id;  //时段ID
	
	private String stattime;  //开始时间
	
	private String endtime;  //结束时间
	
	private String timstr;  //时间段文字
	
	private String timeimg;  //时段图片
	
	private int xueyuan;  //学员数
	
	private int xueyuan_laile=0;  //已预约人数
    private String timename="";
	public String getTimename() {
		return timename;
	}

	public void setTimename(String timename) {
		this.timename = timename;
	}

	private List<Peilian2> peilian;
	public List<Peilian2> getPeilian() {
		return peilian;
	}

	public void setPeilian(List<Peilian2> peilian) {
		this.peilian = peilian;
	}

	private List<UserInfo2> userInfo;
	
	



	public List<UserInfo2> getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(List<UserInfo2> userInfo) {
		this.userInfo = userInfo;
	}

	public int getTime_id() {
		return time_id;
	}

	public void setTime_id(int time_id) {
		this.time_id = time_id;
	}

	public String getStattime() {
		return stattime;
	}

	public void setStattime(String stattime) {
		this.stattime = stattime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public String getTimstr() {
		return timstr;
	}

	public void setTimstr(String timstr) {
		this.timstr = timstr;
	}

	public String getTimeimg() {
		return timeimg;
	}

	public void setTimeimg(String timeimg) {
		this.timeimg = timeimg;
	}

	public int getXueyuan() {
		return xueyuan;
	}

	public void setXueyuan(int xueyuan) {
		this.xueyuan = xueyuan;
	}

	public int getXueyuan_laile() {
		return xueyuan_laile;
	}

	public void setXueyuan_laile(int xueyuan_laile) {
		this.xueyuan_laile = xueyuan_laile;
	}

}