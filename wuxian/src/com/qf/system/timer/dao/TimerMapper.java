package com.qf.system.timer.dao;

import java.util.List;
import java.util.Map;

import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.system.timer.model.Timer;
import com.qf.system.timer.model.Timer2;
import com.qf.system.timer.model.Timer3;
import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.util.PageData;

public interface TimerMapper {
    int deleteByPrimaryKey(Integer timeId);

    int insert(Timer record);

    int insertSelective(Timer record);

    Timer selectByPrimaryKey(Integer timeId);

    int updateByPrimaryKeySelective(Timer record);

    int updateByPrimaryKey(Timer record);

	List<Map> list(PageData pd);

	void deleteAll(List<Integer> list);

	List<Peilian2> getPeilianByTimerId(int time_id);

	List<UserInfo> getXueyuanByTimerId(int time_id);

	List<Timer> getTimerList(PageData pd);

	void yuyue(Map<String, Object> valueMap);

	void updateXueyuanshu(Map<String, Object> valueMap);

	List<Timer2> getTimerListByPeilian(Map map);

	List<Timer3> getTimerByUserId(int user_id);

	List<UserInfo2> getXueyuanByTimerId2(int time_id);

	List<Map> getPeilianByTimerIdForMap(int time_id);

	List<Map> getXueyuanByTimerIdForMap(int time_id);

	List<Peilian2> getPeilianByTimerId2(Map map);

	Timer getTimerByUserForToday(Map map);

	void updateUserKeshi_num(Map<String, Object> valueMap);

}