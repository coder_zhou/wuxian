package com.qf.system.timer.service;


import com.qf.system.timer.model.Timer;
import com.qf.system.timer.model.Timer2;
import com.qf.system.timer.model.Timer3;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface TimerService {


     int save(Timer timer);
     int delete(String timer_id);
     int edit(Timer timer);
     Map<String,Object> list(Page page, PageData pd);
     Timer findById(String timer_id);
     void deleteAll(List<Integer> list);
     List<Timer> getList();
	Timer getTimerById(String id);
	void yuyue(Map<String, Object> valueMap);
	List<Timer3> getTimerByUserId(int user_id);
	List<Timer2> getTimerByPeilianId(String string);
	Timer getTimerByUserForToday(String string);

}
