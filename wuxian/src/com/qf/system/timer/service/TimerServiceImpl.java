package com.qf.system.timer.service;






import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import com.qf.system.peilian.dao.PeilianMapper;
import com.qf.system.peilian.model.Peilian;
import com.qf.system.timer.dao.TimerMapper;
import com.qf.system.timer.model.Timer;
import com.qf.system.timer.model.Timer2;
import com.qf.system.timer.model.Timer3;
import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.util.interceptor.PageHelper;
import com.qf.util.page.Page;
import com.qf.util.PageData;
import com.qf.util.common.DateUtil;


@Service("timerService")
public class TimerServiceImpl implements TimerService {
	
	private PeilianMapper pdao;
	
	public PeilianMapper getPdao() {
		return pdao;
	}
	@Autowired
	public void setPdao(PeilianMapper pdao) {
		this.pdao = pdao;
	}

	private TimerMapper dao;

	public TimerMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(TimerMapper dao) {
		this.dao = dao;
	}
    /*
	* 新增
	*/
	@Override
	public int save(Timer timer){
			return dao.insert(timer);
	}
	
	/*
	* 删除
	*/
	@Override
	public int delete(String timer_id){
		return dao.deleteByPrimaryKey(Integer.parseInt(timer_id));
	}
	
	/*
	* 修改
	*/
	@Override
	public int edit(Timer timer){
		return dao.updateByPrimaryKeySelective(timer);
	}
	
	/*
	*列表
	*/
	@Override
	public  Map<String,Object> list(Page page,PageData pd){
			Map<String,Object> map = new HashMap<String,Object>();
			PageHelper.startPage(page); //起始位置，长度
			dao.list(pd);
			Page p  = PageHelper.endPage();
			page.setRowCount(p.getRowCount());
			page.setPageCount(p.getPageCount());
			map.put("page", page);
			map.put("list", p.getResult());
			return map;
	}
	
	
	/*
	* 通过id获取数据
	*/
	@Override
	public Timer findById(String timer_id){
		return dao.selectByPrimaryKey(Integer.parseInt(timer_id));
	}
	
	/*
	* 批量删除
	*/
	@Override
	public void deleteAll(List<Integer> list){
		dao.deleteAll(list);
	}
	@Override
	public List<Timer> getList() {
		// TODO Auto-generated method stub
		PageData pd=new PageData();
		pd.put("today",DateUtil.getDay());
		List<Timer> list= dao.getTimerList(pd);
		if(list!=null && list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setPeilian(dao.getPeilianByTimerId(list.get(i).getTime_id()));
				list.get(i).setUserInfo(dao.getXueyuanByTimerId(list.get(i).getTime_id()));
			}
		}
		
		return list;
	}
     @Override
	public Timer getTimerById(String id) {
		// TODO Auto-generated method stub
    	 Timer timer=dao.selectByPrimaryKey(Integer.parseInt(id));
    	 timer.setPeilian(dao.getPeilianByTimerId(timer.getTime_id()));
    	 List<UserInfo> lu=dao.getXueyuanByTimerId(timer.getTime_id());
    	 if(lu!=null&& lu.size()>0){
    		 for (int i = 0; i < lu.size(); i++) {
    			 Map map =new HashMap();
    			 map.put("time_id", timer.getTime_id());
    			 map.put("user_id", lu.get(i).getUser_id());
    			 Peilian peilian=pdao.getPeilianByTimeAndUser(map);
				 lu.get(i).setPeilian(peilian);
			}
    	 }
    	 timer.setUserInfo(lu);
		return timer;
	}
     
    @Override
    public void yuyue(Map<String, Object> valueMap) {
    	// TODO Auto-generated method stub
    	dao.updateUserKeshi_num(valueMap);
      dao.yuyue(valueMap);	
      dao.updateXueyuanshu(valueMap);//更新已预约学员数
    } 
     @Override
    public List<Timer3> getTimerByUserId(int user_id) {
    	// TODO Auto-generated method stub
    	 List<Timer3> timer=dao.getTimerByUserId(user_id);
    	 if(timer!=null && timer.size()>0){
    		 for(int i=0;i<timer.size();i++){
    			 Map map =new HashMap();
    			 map.put("time_id", timer.get(i).getTime_id());
    			 map.put("user_id", user_id);
    	 timer.get(i).setPeilian(dao.getPeilianByTimerId2(map));
    	 timer.get(i).setUserInfo(dao.getXueyuanByTimerId2(timer.get(i).getTime_id()));
    		 }
    	 }
    	return timer;
    }
    @Override
    public List<Timer2> getTimerByPeilianId(String string) {
    	// TODO Auto-generated method stub
    	Map map=new HashMap();
    	map.put("pl_id", string);
    	map.put("today",DateUtil.getDay());
    	return dao.getTimerListByPeilian(map);
    } 
    @Override
    public Timer getTimerByUserForToday(String string) {
    	// TODO Auto-generated method stub
    	Map map=new HashMap();
    	map.put("user_id", string);
    	map.put("today",DateUtil.getDay());
    	return dao.getTimerByUserForToday(map);
    }
     
}
