package com.qf.system.timer.controller;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.qf.system.menu.model.Menu;
import com.qf.system.timer.model.Timer;
import com.qf.system.timer.service.TimerService;
import com.qf.util.AppUtil;
import com.qf.util.ObjectExcelView;
import com.qf.util.PageData;
import com.qf.util.common.Const;
import com.qf.util.common.DateUtil;
import com.qf.util.common.Tools;
import com.qf.util.controller.BaseController;
import com.qf.util.page.Page;
/** 
 * 类名称：TimerController
 * 创建人：周广文
 * 创建时间：2015-11-16
 */
@Controller
@RequestMapping(value="/qjqwmain/timer")
public class TimerController extends BaseController{
	@Resource(name="timerService")
	private TimerService timerService;
	/**
	 * 新增
	 */
	@RequestMapping(value="/save")
	public ModelAndView save() throws Exception{
		logBefore(logger, "新增Timer");
		ModelAndView mv = this.getModelAndView();
		PageData pd=this.getPageData();
		
		Timer timer =new Timer();
		timer.setStattime(pd.getString("stattime"));
		timer.setEndtime(pd.getString("endtime"));
		timer.setTimeimg(pd.getString("timeimg"));
		
		timer.setXueyuan(Integer.parseInt(pd.getString("xueyuan")));
		if(Tools.notEmpty(pd.getString("xueyuan_laile")))
		timer.setXueyuan_laile(Integer.parseInt(pd.getString("xueyuan_laile")));
		else
			timer.setXueyuan_laile(0);	
		timer.setTimename(pd.getString("timername"));
			timer.setTimstr(pd.getString("timstr"));
			int i=timerService.save(timer);
			if(i>0){
				mv.addObject("msg","success");
				mv.setViewName("save_result");
				return mv;
			}else{
				mv.addObject("msg","error");
				mv.setViewName("save_result");
				return mv;
			}
	}
	/**
	 * 删除
	 */
	@RequestMapping(value="/delete")
	public void delete(@NotNull String timer_id, PrintWriter out){
		logBefore(logger, "删除Timer");
		try{
		   int i=timerService.delete(timer_id);
		   if(i>0){
			out.write("success");
			out.close();
		   }else{
			   out.write("error");
				out.close();
		   }
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		
	}
	
	/**
	 * 修改
	 */
	@RequestMapping(value="/edit")
	public ModelAndView edit(@Validated @ModelAttribute("timer") Timer timer ,Errors errors) throws Exception{
		logBefore(logger, "修改Timer");
		ModelAndView mv = this.getModelAndView();
		if(errors.hasErrors()){
			 mv.setViewName("/WEB-INF/system/error/error_all");
	            return mv;  
		}else{
		   int i= timerService.edit(timer);
		   if(i>0){
		   mv.addObject("msg","success");
		   mv.setViewName("save_result");
		   return mv;
		   }else{
			   mv.addObject("msg","error");
			   mv.setViewName("save_result");
			   return mv;
		   }
		}
	}
	
	/**
	 * 列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/list")
	public ModelAndView list(Page page){
		logBefore(logger, "列表Timer");
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			
		   this.getHC(); //调用权限
			//////////////////////////////
		   if(Tools.isEmpty(pd.getString("today"))){
			   pd.put("today", DateUtil.getDay());
		   }
		   
			Map<String,Object> map = timerService.list(page,pd);
			page = (Page)map.get("page");
			List<Timer> list = (List<Timer>)map.get("list");
			mv.addObject("varList", list);
			mv.addObject("pd", pd);
			mv.addObject("page", page);
			mv.setViewName("/WEB-INF/system/timer/timer_list");
			
			
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		return mv;
	}
	/**
	 * 去新增页面
	 */
	@RequestMapping(value="/goAdd")
	public ModelAndView goAdd(){
		logBefore(logger, "去新增Timer页面");
		ModelAndView mv = this.getModelAndView();
		try {
			mv.setViewName("/WEB-INF/system/timer/timer_edit");
			mv.addObject("msg", "save");
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}						
		return mv;
	}	
	@RequestMapping(value="/getyuyue")
	public ModelAndView getyuyue(){
		ModelAndView mv = this.getModelAndView();
		PageData pd= new PageData();
				pd=this.getPageData();
		try {
			Timer timer=timerService.getTimerById(pd.getString("time_id"));
			mv.addObject("timer",timer);
			mv.setViewName("/WEB-INF/system/timer/getyuyue");
			mv.addObject("msg", "save");
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}						
		return mv;
	}	
	/**
	 * 去修改页面
	 */
	@RequestMapping(value="/goEdit")
	public ModelAndView goEdit(@NotNull String timer_id){
		logBefore(logger, "去修改Timer页面");
		ModelAndView mv = this.getModelAndView();
		try {
			Timer timer =timerService.findById(timer_id);	//根据ID读取
			mv.setViewName("/WEB-INF/system/timer/timer_edit");
			mv.addObject("msg", "edit");
			mv.addObject("timer", timer);
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}						
		return mv;
	}	
	/**
	 * 批量删除
	 */
	@RequestMapping(value="/deleteAll")
	@ResponseBody
	public Object deleteAll() {
		logBefore(logger, "批量删除Timer");
		PageData pd = new PageData();		
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			pd = this.getPageData();
			List<PageData> pdList = new ArrayList<PageData>();
			String DATA_IDS = pd.getString("DATA_IDS");
			if(null != DATA_IDS && !"".equals(DATA_IDS)){
				String ArrayDATA_IDS[] = DATA_IDS.split(",");
				List<Integer> list=new ArrayList<Integer>();
				for(int i=0;i<ArrayDATA_IDS.length;i++){
					list.add(Integer.parseInt(ArrayDATA_IDS[i]));
							
				}
				timerService.deleteAll(list);
				pd.put("msg", "ok");
			}else{
				pd.put("msg", "no");
			}
			pdList.add(pd);
			map.put("list", pdList);
		} catch (Exception e) {
			logger.error(e.toString(), e);
		} finally {
			logAfter(logger);
		}
		return AppUtil.returnObject(pd, map);
	}
	
	
	/* ===============================权限================================== */
	@SuppressWarnings("unchecked")
	public void getHC(){
		ModelAndView mv = this.getModelAndView();
		HttpSession session = this.getRequest().getSession();
		Map<String, String> map = (Map<String, String>)session.getAttribute(Const.SESSION_QX);
		mv.addObject(Const.SESSION_QX,map);	//按钮权限
		List<Menu> menuList = (List<Menu>)session.getAttribute(Const.SESSION_menuList);
		mv.addObject(Const.SESSION_menuList, menuList);//菜单权限
	}
	/* ===============================权限================================== */
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(format,true));
	}
	
	
	
}
