package com.qf.system.peilian.model;

import java.util.List;

import com.qf.system.timer.model.Timer;
import com.qf.system.timer.model.Timer2;

public class Peilian2{

	
	private int pl_id;  //陪练ID
	
	private String pl_name;  //陪练名称
	
	private int pl_age;  //陪练年龄
	
	private String pl_sex;  //陪练性别
	
	private String pl_job;  //陪练工作
	
	private String pl_jiesao;  //陪练介绍
	
	private String pl_photo;  //陪练头像
    

	public int getPl_id() {
		return pl_id;
	}

	public void setPl_id(int pl_id) {
		this.pl_id = pl_id;
	}

	public String getPl_name() {
		return pl_name;
	}

	public void setPl_name(String pl_name) {
		this.pl_name = pl_name;
	}

	public int getPl_age() {
		return pl_age;
	}

	public void setPl_age(int pl_age) {
		this.pl_age = pl_age;
	}

	public String getPl_sex() {
		return pl_sex;
	}

	public void setPl_sex(String pl_sex) {
		this.pl_sex = pl_sex;
	}

	public String getPl_job() {
		return pl_job;
	}

	public void setPl_job(String pl_job) {
		this.pl_job = pl_job;
	}

	public String getPl_jiesao() {
		return pl_jiesao;
	}

	public void setPl_jiesao(String pl_jiesao) {
		this.pl_jiesao = pl_jiesao;
	}

	public String getPl_photo() {
		return pl_photo;
	}

	public void setPl_photo(String pl_photo) {
		this.pl_photo = pl_photo;
	}

}