package com.qf.system.peilian.dao;

import java.util.List;
import java.util.Map;

import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.util.PageData;
import com.qf.util.entity.Page;
public interface PeilianMapper{

   int save(PageData pd);
   int delete(PageData pd);
   int edit(PageData pd);
   List<PageData> datalistPage(Page page);
   PageData findById(PageData pd);
   void deleteAll(String[] ArrayDATA_IDS);
List<Map> list(PageData pd);
int deleteByPrimaryKey(Integer plId);

int insert(Peilian record);

int insertSelective(Peilian record);

Peilian selectByPrimaryKey(Integer plId);

int updateByPrimaryKeySelective(Peilian record);

int updateByPrimaryKey(Peilian record);
void choose(PageData pd);
List<Peilian> getPeilian();
Peilian getPeilianByTimeAndUser(Map map);
List<Map> listphoto(PageData pd);
void addphoto(Map map);
List<Map> getPhotoByPeilianId(String pl_id);
Peilian2 getPeilian2(String string);
}