package com.qf.system.peilian.service;


import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface PeilianService {


     int save(Peilian peilian);
     int delete(String peilian_id);
     int edit(Peilian peilian);
     Map<String,Object> list(Page page, PageData pd);
     Peilian findById(String peilian_id);
     void deleteAll(String[] arrayDATA_IDS);
	void choose(PageData pd);
	List<Peilian> getPeilian();
	Map<String, Object> listPhoto(Page page, PageData pd);
	void addphoto(Map map);
	List<Map> getPhotoByPeilianId(String pl_id);
	Peilian2 findById2(String string);

}
