package com.qf.system.peilian.service;






import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import com.qf.system.peilian.dao.PeilianMapper;
import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.system.timer.dao.TimerMapper;
import com.qf.util.interceptor.PageHelper;
import com.qf.util.page.Page;
import com.qf.util.PageData;
import com.qf.util.common.DateUtil;


@Service("peilianService")
public class PeilianServiceImpl implements PeilianService {
	
	private PeilianMapper dao;

	public PeilianMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(PeilianMapper dao) {
		this.dao = dao;
	}
    
	private TimerMapper tdao;
    
    public TimerMapper getTdao() {
		return tdao;
	}
    @Autowired
	public void setTdao(TimerMapper tdao) {
		this.tdao = tdao;
	}
	/*
	* 新增
	*/
	@Override
	public int save(Peilian peilian){
			return dao.insert(peilian);
	}
	
	/*
	* 删除
	*/
	@Override
	public int delete(String peilian_id){
		return dao.deleteByPrimaryKey(Integer.parseInt(peilian_id));
	}
	
	/*
	* 修改
	*/
	@Override
	public int edit(Peilian peilian){
		return dao.updateByPrimaryKeySelective(peilian);
	}
	
	/*
	*列表
	*/
	@Override
	public  Map<String,Object> list(Page page,PageData pd){
			Map<String,Object> map = new HashMap<String,Object>();
			PageHelper.startPage(page); //起始位置，长度
			dao.list(pd);
			Page p  = PageHelper.endPage();
			page.setRowCount(p.getRowCount());
			page.setPageCount(p.getPageCount());
			map.put("page", page);
			map.put("list", p.getResult());
			return map;
	}
	
	
	/*
	* 通过id获取数据
	*/
	@Override
	public Peilian findById(String peilian_id){
		return dao.selectByPrimaryKey(Integer.parseInt(peilian_id));
	}
	
	/*
	* 批量删除
	*/
	@Override
	public void deleteAll(String[] list){
		dao.deleteAll(list);
	}
	@Override
	public void choose(PageData pd) {
		// TODO Auto-generated method stub
		dao.choose(pd);
	}
	@Override
	public List<Peilian> getPeilian() {
		// TODO Auto-generated method stub
		List<Peilian> list=dao.getPeilian();
		if(list!=null && list.size()>0){
			for (int i = 0; i < list.size(); i++) {
			Map map=new HashMap();
			map.put("pl_id", list.get(i).getPl_id());
			map.put("today", DateUtil.getDay());
			list.get(i).setTimer(tdao.getTimerListByPeilian(map));	
			}
		}
		return list;
	}
	@Override
	public Map<String, Object> listPhoto(Page page, PageData pd) {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String,Object>();
		PageHelper.startPage(page); //起始位置，长度
        dao.listphoto(pd);
		Page p  = PageHelper.endPage();
		page.setRowCount(p.getRowCount());
		page.setPageCount(p.getPageCount());
		map.put("page", page);
		map.put("list", p.getResult());
		return map;
	}
	@Override
	public void addphoto(Map map) {
		// TODO Auto-generated method stub
		dao.addphoto(map);
	}
	@Override
	public List<Map> getPhotoByPeilianId(String pl_id) {
		// TODO Auto-generated method stub
		return dao.getPhotoByPeilianId(pl_id);
	}
	@Override
	public Peilian2 findById2(String string) {
		// TODO Auto-generated method stub
		return dao.getPeilian2(string);
	}
	
}
