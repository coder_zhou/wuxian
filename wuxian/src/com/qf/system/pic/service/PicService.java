package com.qf.system.pic.service;


import com.qf.system.pic.model.Pic;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface PicService {


     int save(PageData pd);
     int delete(PageData pd);
     int edit(PageData pd);
     Map<String,Object> list(Page page, PageData pd);
     Pic findById(String pic_id);
     void deleteAll(String[] arrayDATA_IDS);
	List<Map> bannerlist();

}
