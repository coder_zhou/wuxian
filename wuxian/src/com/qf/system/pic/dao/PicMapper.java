package com.qf.system.pic.dao;

import java.util.List;
import java.util.Map;

import com.qf.system.pic.model.Pic;
import com.qf.util.PageData;
import com.qf.util.entity.Page;
public interface PicMapper{

   int save(PageData pd);
   int delete(PageData pd);
   int edit(PageData pd);
   List<Map> datalistPage(Page pd);
   Pic findById(PageData pd);
   void deleteAll(String[] ArrayDATA_IDS);
}