package com.qf.system.pic.model;

public class Pic{

	
	private int pic_id;  //图片ID
	
	private String picurl;  //图片路径
	
	private int param_id;  //元素id
	
	private int pic_type;  //图片类型
	
	private int addtime;  //添加时间

	public int getPic_id() {
		return pic_id;
	}

	public void setPic_id(int pic_id) {
		this.pic_id = pic_id;
	}

	public String getPicurl() {
		return picurl;
	}

	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}

	public int getParam_id() {
		return param_id;
	}

	public void setParam_id(int param_id) {
		this.param_id = param_id;
	}

	public int getPic_type() {
		return pic_type;
	}

	public void setPic_type(int pic_type) {
		this.pic_type = pic_type;
	}

	public int getAddtime() {
		return addtime;
	}

	public void setAddtime(int addtime) {
		this.addtime = addtime;
	}

}