
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `TB_PIC`
-- ----------------------------
DROP TABLE IF EXISTS `TB_PIC`;
CREATE TABLE `TB_PIC` (
 		`PIC_ID` varchar(100) NOT NULL,
		`pic_id` int(11) NOT NULL COMMENT '图片ID',
		`picurl` varchar(255) DEFAULT NULL COMMENT '图片路径',
		`param_id` int(11) NOT NULL COMMENT '元素id',
		`pic_type` int(11) NOT NULL COMMENT '图片类型',
		`addtime` int(11) NOT NULL COMMENT '添加时间',
  		PRIMARY KEY (`PIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
