package com.qf.system.menu.dao;

import java.util.List;

import com.qf.system.menu.model.Menu;
import com.qf.util.PageData;



public interface MenuMapper {
    int deleteByPrimaryKey(Integer menuId);

    int insert(Menu record);

    int insertSelective(Menu record);

    Menu selectByPrimaryKey(Integer menuId);

    int updateByPrimaryKeySelective(Menu record);

    int updateByPrimaryKey(Menu record);

	List<Menu> listAll();

	Menu getMenuById(String MENU_ID);

	PageData findMaxId(PageData pd);

	List<Menu> listAllParentMenu();

	int insertMenu(Menu menu);

	List<Menu> listSubMenuByParentId(String parentId);

	List<Menu> listAllSubMenu(Object object);

	int updateMenu(PageData pd);

	int editicon(PageData pd);

	PageData editType(PageData pd);

	void deleteMenuById(String mENU_ID);

	PageData findByUid(PageData pd);
	
	
	
	
}