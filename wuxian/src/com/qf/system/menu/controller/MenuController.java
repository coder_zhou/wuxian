package com.qf.system.menu.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qf.system.menu.model.Menu;
import com.qf.system.menu.service.MenuService;
import com.qf.util.PageData;
import com.qf.util.controller.BaseController;

@Controller
@RequestMapping(value="/qjqwmain/menu")
public class MenuController extends BaseController {
    @Resource(name="menuService")
    private MenuService service;
	
    @RequestMapping
	public ModelAndView list()throws Exception{
		ModelAndView mv = this.getModelAndView();
		try{
			List<Menu> menuList = service.listAllParentMenu();
			mv.addObject("menuList", menuList);
			mv.setViewName("/WEB-INF/system/menu/menu_list");
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		
		return mv;
	}
	
	/**
	 * 请求新增菜单页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/toAdd")
	public ModelAndView toAdd()throws Exception{
		ModelAndView mv = this.getModelAndView();
		try{
			List<Menu> menuList = service.listAllParentMenu();
			mv.addObject("menuList", menuList);
			mv.setViewName("/WEB-INF/system/menu/menu_add");
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		return mv;
	}
	/**
	 * 保存菜单信息
	 * @param menu
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/add")
	public ModelAndView add(Menu menu)throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		pd = this.getPageData();
		try{
			menu.setMenu_id(String.valueOf(Integer.parseInt(service.findMaxId(pd).get("MID").toString())+1));
			
			
			String parent_Id = menu.getParent_id();
			if(!"0".equals(parent_Id)){
				//处理菜单类型====
				pd.put("menu_id",parent_Id);
				Menu m = service.getMenuById(parent_Id);
				menu.setMenu_type(m.getMenu_type());
				//处理菜单类型====
			}
		    int i=	service.saveMenu(menu);
		    if(i>0){
			mv.addObject("msg","success");
		    }else {
		    	mv.addObject("msg","error");
		    }
		} catch(Exception e){
			logger.error(e.toString(), e);
			mv.addObject("msg","failed");
		}
		
		mv.setViewName("/WEB-INF/system/save_result");
		return mv;
		
	}/**
	 * 请求编辑菜单页面
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/toEdit")
	public ModelAndView toEdit(String menu_id)throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			pd.put("menu_id",menu_id);
		    Menu m=	 service.getMenuById(menu_id);
			List<Menu> menuList = service.listAllParentMenu();
			mv.addObject("menuList", menuList);
			mv.addObject("pd", pd);
			mv.addObject("menu",m);
			mv.setViewName("/WEB-INF/system/menu/menu_edit");
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		return mv;
	}
	
	/**
	 * 请求编辑菜单图标页面
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/toEditicon")
	public ModelAndView toEditicon(String menu_id)throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			pd.put("menu_id",menu_id);
			mv.addObject("pd", pd);
			mv.setViewName("/WEB-INF/system/menu/menu_icon");
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		return mv;
	}
	
	/**
	 * 保存菜单图标 (顶部菜单)
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/editicon")
	public ModelAndView editicon()throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		try{
			pd = this.getPageData();
			service.editicon(pd);
			mv.addObject("msg","success");
		} catch(Exception e){
			logger.error(e.toString(), e);
			mv.addObject("msg","failed");
		}
		mv.setViewName("/WEB-INF/system/save_result");
		return mv;
	}
	
	/**
	 * 保存编辑
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/edit")
	public ModelAndView edit()throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
			pd = this.getPageData();
			
			String parent_id = pd.getString("parent_id");
			if(null == parent_id || "".equals(parent_id)){
				parent_id = "0";
				pd.put("parent_id", parent_id);
			}
			
//			if("0".equals(parent_id)){
//				//处理菜单类型====
//				service.editType(pd);
//				//处理菜单类型====
//			}
		    	int i = service.edit(pd);
		    	 if(i>0){
		  		   mv.addObject("msg","success");
		  		   mv.setViewName("save_result");
		  		   return mv;
		  		   }else{
		  			   mv.addObject("msg","error");
		  			   mv.setViewName("save_result");
		  			   return mv;
		  		   }
	
	}
	
	/**
	 * 获取当前菜单的所有子菜单
	 * @param menuId
	 * @param response
	 */
	@RequestMapping(value="/sub")
	public void getSub(@RequestParam String menu_id,HttpServletResponse response)throws Exception{
		try {
			List<Menu> subMenu = service.listSubMenuByParentId(menu_id);
			JSONArray arr = JSONArray.fromObject(subMenu);
			PrintWriter out;
			
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			String json = arr.toString();
			out.write(json);
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}
	}
	
	/**
	 * 删除菜单
	 * @param menuId
	 * @param out
	 */
	@RequestMapping(value="/del")
	public void delete(@RequestParam String menu_id,PrintWriter out)throws Exception{
		
		try{
			service.deleteMenuById(menu_id);
			out.write("success");
			out.flush();
			out.close();
		} catch(Exception e){
			logger.error(e.toString(), e);
		}
		
	}
	
	
	
}
