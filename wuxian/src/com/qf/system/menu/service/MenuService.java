package com.qf.system.menu.service;

import java.util.List;

import com.qf.system.menu.model.Menu;
import com.qf.util.PageData;


/**
*
* 项目名称：qf
* 类名称：MenuService
* 类描述：菜单管理接口
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:47:57
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:47:57
* 修改备注：
* @version
*
*/
public interface MenuService {

	List<Menu> listAllSubMenu() throws Exception;

	List<Menu> listAllParentMenu() throws Exception;

	int saveMenu(Menu menu) throws Exception;

	Menu getMenuById(String MENU_ID)  throws Exception;

	PageData findMaxId(PageData pd) throws  Exception;

	void deleteMenuById(String mENU_ID) throws  Exception;

	List<Menu> listSubMenuByParentId(String mENU_ID) throws  Exception;

	int edit(PageData pd) throws  Exception;

	void editType(PageData pd) throws  Exception;

   void  editicon(PageData pd) throws  Exception;

	List<Menu> listAllMenu() throws Exception;

	PageData findByUId(PageData pd);

}
