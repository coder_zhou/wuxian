package com.qf.system.menu.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.qf.system.menu.dao.MenuMapper;
import com.qf.system.menu.model.Menu;
import com.qf.util.PageData;


/**
*
* 项目名称：qf
* 类名称：MenuServiceImpl
* 类描述：菜单管理
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:47:09
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:47:09
* 修改备注：
* @version
*
*/
@Service("menuService")
public class MenuServiceImpl implements MenuService {
	
	private MenuMapper mapper;
	public MenuMapper getMapper() {
		return mapper;
	}
    @Autowired
	public void setMapper(MenuMapper mapper) {
		this.mapper = mapper;
	}

	public Menu getMenuById(String MENU_ID) throws Exception {
		return mapper.getMenuById(MENU_ID);
		
	}
	@CacheEvict(value="menuCaChe",allEntries=true)// 清空accountCache 缓存  
	public void deleteMenuById(String MENU_ID) throws Exception {
		mapper.deleteMenuById(MENU_ID);
		
	}
	//取最大id
	public PageData findMaxId(PageData pd) throws Exception {
		return (PageData) mapper.findMaxId(pd);
		
	}
	@Cacheable(value="menuCaChe")
	public List<Menu> listAllParentMenu() throws Exception {
		return (List<Menu>) mapper.listAllParentMenu();
		
	}
	@CacheEvict(value="menuCaChe",allEntries=true)// 清空accountCache 缓存  
	public int saveMenu(Menu menu) throws Exception {
		int i=0;
		if(menu.getMenu_id()!=null && menu.getMenu_id() !=null){
			//dao.update("MenuMapper.updateMenu", menu);
		 i=  	mapper.insertMenu(menu);
		}else{
			i= 	mapper.insertMenu(menu);
		}
		return i;
	}
    @Cacheable(value="menuCaChe",key="#parentId")
	public List<Menu> listSubMenuByParentId(String parentId) throws Exception {
		return (List<Menu>) mapper.listSubMenuByParentId(parentId);
		
	}
	@Cacheable(value="menuCaChe")	
	public List<Menu> listAllMenu() throws Exception {
		List<Menu> rl = this.listAllParentMenu();
		for(Menu menu : rl){
			List<Menu> subList = this.listSubMenuByParentId(menu.getMenu_id().toString());
			menu.setSubMenu(subList);
		}
		return rl;
	}
	@Cacheable(value="menuCaCheSub")
	public List<Menu> listAllSubMenu() throws Exception{
		return (List<Menu>) mapper.listAllSubMenu(null);
		
	}
	
	/**
	 * 编辑
	 */
	@CacheEvict(value="menuCaChe",allEntries=true)// 清空accountCache 缓存  	
	public int edit(PageData pd) throws Exception {
		return  mapper.updateMenu(pd);
	}
	/**
	 * 保存菜单图标 (顶部菜单)
	 */
	@CacheEvict(value="menuCaChe",allEntries=true)// 清空accountCache 缓存  
	public void editicon(PageData pd) throws Exception {
		  mapper.editicon(pd);
	}
	
	/**
	 * 更新子菜单类型菜单
	 */
	@CacheEvict(value="menuCaChe",allEntries=true)// 清空accountCache 缓存  
	public void editType(PageData pd) throws Exception {
		 mapper.editType(pd);
	}
	@Override
	public PageData findByUId(PageData pd) {
		// TODO Auto-generated method stub
		return (PageData)mapper.findByUid(pd);
	}

}
