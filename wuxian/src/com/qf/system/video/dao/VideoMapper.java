package com.qf.system.video.dao;

import java.util.List;
import java.util.Map;

import com.qf.util.PageData;
public interface VideoMapper{

   int save(PageData pd);
   int delete(PageData pd);
   int edit(PageData pd);
   List<Map> datalistPage(PageData pd);
   PageData findById(PageData pd);
   void deleteAll(String[] ArrayDATA_IDS);
}