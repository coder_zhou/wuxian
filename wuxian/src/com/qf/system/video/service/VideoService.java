package com.qf.system.video.service;


import com.qf.system.video.model.Video;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface VideoService {


     int save(PageData pd);
     int delete(PageData pd);
     int edit(PageData pd);
     Map<String,Object> list(Page page, PageData pd);
     Map findById(PageData pd);
     void deleteAll(String[] arrayDATA_IDS);
	List<Map> listVideo();

}
