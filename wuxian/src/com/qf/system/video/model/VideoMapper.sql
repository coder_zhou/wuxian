
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `TB_VIDEO`
-- ----------------------------
DROP TABLE IF EXISTS `TB_VIDEO`;
CREATE TABLE `TB_VIDEO` (
 		`VIDEO_ID` varchar(100) NOT NULL,
		`video_photo` varchar(255) DEFAULT NULL COMMENT '视频封面',
		`video_url` varchar(255) DEFAULT NULL COMMENT '视频路径',
		`video_name` varchar(255) DEFAULT NULL COMMENT '视频名称',
		`addTime` varchar(255) DEFAULT NULL COMMENT 'addTime',
  		PRIMARY KEY (`VIDEO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
