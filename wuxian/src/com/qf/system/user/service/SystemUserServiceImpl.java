package com.qf.system.user.service;




import java.util.List;
import java.util.Map;




import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.qf.system.user.dao.SystemUserMapper;
import com.qf.system.user.model.SystemUser;
import com.qf.util.PageData;
import com.qf.util.entity.Page;



@Service("systemuserService")
public class SystemUserServiceImpl implements SystemUserServiceI {


	

	/**
	* mapper:TODO��SystemUserMapper �ӿڣ�
	*
	* @since Ver 1.1
	*/
	private SystemUserMapper mapper;
	
	
	public SystemUserMapper getMapper() {
		return mapper;
	}
	@Autowired
	public void setMapper(SystemUserMapper mapper) {
		this.mapper = mapper;
	}
	/*
	* ͨ��id��ȡ���
	*/
	public PageData findByUiId(PageData pd)throws Exception{
		return (PageData) mapper.findByUiId(pd);
	}
	/*
	* ͨ��loginname��ȡ���
	*/
	public PageData findByUId(PageData pd)throws Exception{
		return (PageData) mapper.findByUId(pd);
	}
	
	/*
	* ͨ�������ȡ���
	*/
	@Override
	public PageData findByUE(PageData pd){
		return (PageData) mapper.findByUE(pd);
				
	}
	
	/*
	* ͨ���Ż�ȡ���
	*/
	@Override
	public PageData findByUN(PageData pd){
		return (PageData) mapper.findByUN(pd);
	}
	
	/*
	* �����û�
	*/
	public void saveU(PageData pd)throws Exception{
		
		mapper.saveU(pd);
	}
	/*
	* �޸��û�
	*/
	@Override
	public void editU(PageData pd){
		mapper.editU(pd);
	}
	/*
	* ��Ƥ��
	*/
	public void setSKIN(PageData pd)throws Exception{
		mapper.setSKIN(pd);
	}
	/*
	* ɾ���û�
	*/
	@Override
	public void deleteU(PageData pd){
		mapper.deleteU(pd);
		
	}
	/*
	* ����ɾ���û�
	*/
	@Override
	public void deleteAllU(String[] USER_IDS){
		mapper.deleteAllU(USER_IDS);
	}
	/*
	*�û��б�(�û���)
	*/
	public List<PageData> listPdPageUser(Page page){
		return (List<PageData>) mapper.userlistPage(page);
	}
	
	/*
	*�û��б�(ȫ��)
	*/
	public List<PageData> listAllUser(PageData pd){
		return (List<PageData>) mapper.listAllUser(pd);
	}
	
	/*
	*�û��б�(��Ӧ���û�)
	*/
	public List<PageData> listGPdPageUser(Page page)throws Exception{
		return (List<PageData>) mapper.userGlistPage( page);
	}
	/*
	* �����û�IP
	*/
	public void saveIP(PageData pd)throws Exception{
		mapper.saveIP( pd);
	}
	
	/*
	* ��¼�ж�
	*/
	public SystemUser getUserByNameAndPwd(Map<String, String> us)throws Exception{
		return (SystemUser)mapper.getUserInfo( us);
	}
	/*
	* ���µ�¼ʱ��
	*/
	public void updateLastLogin(SystemUser u)throws Exception{
		mapper.updateLastLogin( u);
	}
	
	/*
	*ͨ��id��ȡ���
	*/
	public SystemUser getUserAndRoleById(String USER_ID) throws Exception {
		return (SystemUser) mapper.getUserAndRoleById( USER_ID);
	}
	
	/**����SystemUser ����
	* (non-Javadoc)
	* @see com.qf.system.user.service.SystemUserServiceI#saveUser(com.qf.system.user.model.SystemUser)
	*/
	@Override
	public int  saveUser(SystemUser u) throws Exception {
		// TODO Auto-generated method stub
	 return	mapper.saveUser(u);
	}
	
	/**�����û��б�
	* (non-Javadoc)
	* @see com.qf.system.user.service.SystemUserServiceI#listUser()
	*/
	@Override
	public List<PageData> listUser(PageData pd) throws Exception {
		// TODO Auto-generated method stub
		return mapper.listAllUser(pd);
	}

	



}
