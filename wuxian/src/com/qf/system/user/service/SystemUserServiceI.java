package com.qf.system.user.service;

import java.util.List;

import com.qf.system.user.model.SystemUser;
import com.qf.util.PageData;
import com.qf.util.entity.Page;






public interface SystemUserServiceI {



public PageData findByUId(PageData pd) throws Exception;

public void saveIP(PageData pd) throws Exception;

public int saveUser(SystemUser u) throws Exception;

public List<PageData> listUser(PageData pd) throws Exception;

public void deleteU(PageData pd);

public void deleteAllU(String[] arrayUSER_IDS);

public void editU(PageData pd);

public Object findByUE(PageData pd);

public Object findByUN(PageData pd);

public List<PageData> listAllUser(PageData pd);

public List<PageData> listPdPageUser(Page page);


}
