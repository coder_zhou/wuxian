package com.qf.system.user.dao;

import java.util.List;
import java.util.Map;

import com.qf.system.user.model.SystemUser;
import com.qf.util.PageData;
import com.qf.util.entity.Page;



public interface SystemUserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(SystemUser record);

    int insertSelective(SystemUser record);

    SystemUser selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(SystemUser record);

    int updateByPrimaryKey(SystemUser record);

	String findById(String userId);

	PageData findByUiId(PageData pd);

	PageData findByUId(PageData pd);

	PageData findByUE(PageData pd);

	PageData findByUN(PageData pd);

	void saveU(PageData pd);

	void setSKIN(PageData pd);

	void deleteU(PageData pd);

	void deleteAllU(String[] uSER_IDS);

	List<PageData> userlistPage(Page page);

	List<PageData> listAllUser(PageData pd);

	List<PageData> userGlistPage(Page page);

	void saveIP(PageData pd);


	void updateLastLogin(SystemUser u);

	SystemUser getUserAndRoleById(String uSER_ID);

	int saveUser(SystemUser u);


	SystemUser getUserInfo(Map<String, String> us);

	void editU(PageData pd);
}