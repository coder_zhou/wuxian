package com.qf.system.area.controller;





import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qf.system.area.model.Area;
import com.qf.system.area.service.AreaService;
import com.qf.util.common.JackJson;



/**
*
* 项目名称：qf
* 类名称：AreaInfoController
* 类描述：地区管理及查询
* 创建人：周广文
* 创建时间：2015年6月16日 下午2:40:18
* 修改人：周广文
* 修改时间：2015年6月16日 下午2:40:18
* 修改备注：
* @version
*
*/
@Controller
@RequestMapping("/area")
public class AreaInfoController {
	Logger log;
	@Resource(name="areaService")
	private AreaService service;
	
	protected Logger logger = Logger.getLogger(this.getClass()); //log4j
	
	
	/**
	* listPro(获取所有的省份)
	* TODO(获取省份)
	* @param name
	* @param @return 设定文件
	* @return ModelAndView
	* @Exception 异常对象
	* @since  CodingExample　Ver(编码范例查看) 1.1
	*/
	@RequestMapping("/listPro")
	public ModelAndView listPro(){
		ModelAndView mv=new ModelAndView();
		List<Area> list=service.getProList();
		mv.addObject("areaList",list);
		mv.setViewName("/system/area/list");
		return mv;
	}
	
	
	/**
	* listCity(获取该省下面的市)
	* TODO(接收上级对象ID:"proId")
	* TODO(根据上级ID查询下级列表)
	* TODO(返回到页面)
	* @param name
	* @param @return 设定文件
	* @return ModelAndView
	* @Exception 异常对象
	* @since  CodingExample　Ver(编码范例查看) 1.1
	*/
	@RequestMapping("/listCity")
	public ModelAndView listCity(@RequestParam("proId") int proId)
	{
		ModelAndView mv=new ModelAndView();
		List<Area> list=service.getCity(proId);
		mv.addObject("areaList",list);
		mv.setViewName("/system/area/list");
		return mv;
		
	}
    
	
	/**
	* getSon(获取下级列表)
	* TODO(接收int类型的上级ID)
	* TODO(根据上级ID查询下级列表)
	* @param name
	* @param @return 设定文件
	* @return String DOM对象
	* @Exception 异常对象
	* @since  CodingExample　Ver(编码范例查看) 1.1
	*/
	@RequestMapping("/getSon")
	public void getSon(int proId ,HttpServletRequest request ,HttpServletResponse response){
		List<Area> list=service.getCity(proId);
		response.setContentType("text/html;charset=utf-8");  
		String json= JackJson.fromObjectToJson(list);
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	
    
    /**
    * getParent(查询上级ID)
    * TODO(根据上级ID获取上级对象)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/getParent")
	public ModelAndView getParent(@RequestParam("proId") int proId){
		ModelAndView mv=new ModelAndView();
       List<Area> list=service.getParent(proId);
       mv.addObject("areaList",list);
       mv.setViewName("/system/area/list");
       return mv;
	}
    
    
    /**
    * addSon(添加下级地区)
    * TODO(接收前台传入的area 对象)
    * TODO(前台传入的area对象必须设定parent_id)
    * TODO(插入area对象)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/addSon")
    public ModelAndView addSon(Area area){
    	ModelAndView mv=new ModelAndView();
    	service.addSon(area);
    	mv.setViewName("redirect:/area/listCity?proId="+area.getParentid());
    	return mv;
    }
	
    
    /**
    * toaddSon(跳转到添加下级页面)
    * TODO(接收int类型的地区id)
    * TODO(地区ID作为parent_id 传入到页面)
    * TODO(跳转到新增页面)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/toaddSon")
    public ModelAndView toaddSon(@RequestParam("proId") int proId){
    	ModelAndView mv=new ModelAndView();
    	mv.addObject("proId",proId);
    	mv.setViewName("/WEB-INF/system/area/add");
    	return mv;
    }
    
    
    /**
    * delCity(删除地区)
    * TODO(根据ID 删除地区)
    * TODO(如果该地区有下级地区则无法删除)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/delCity")
    public ModelAndView delCity(@RequestParam("id") int id,HttpServletRequest request,HttpServletResponse response){
    	ModelAndView mv=new ModelAndView();
    	List<Area> list=service.getCity(id);
    	if(list!=null&&list.size()>0){
    		try {
				response.getWriter().write("<script>alert(\"该地区有下属地区,不能直接删除\")</script>");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		mv.setViewName("redirect:/area/listCity?proId="+id);
    		return mv;
    	}
    		service.delCity(id);
    		mv.setViewName("redirect:/area/listCity?proId="+id);
    		return mv;
    	
    	
    }
    
    
    /**
    * editCity(修改地区)
    * TODO(接收传入的area对象)
    * TODO(修改area对象)
    * TODO(跳转到地区列表)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/edit")
    public ModelAndView editCity( Area area){
    	ModelAndView mv=new ModelAndView();
    	service.editCity(area);
    	mv.setViewName("redirect:/area/listCity?proId="+area.getParentid());
    	return mv;
    }
    
    /**
    * toedit(跳转到修改页面)
    * TODO(获取传入的id)
    * TODO(查询出该id的area对象)
    * TODO(跳转到修改页面)
    * @param name
    * @param @return 设定文件
    * @return ModelAndView
    * @Exception 异常对象
    * @since  CodingExample　Ver(编码范例查看) 1.1
    */
    @RequestMapping("/toedit")
    public ModelAndView toedit(@RequestParam("id") int id){
    	ModelAndView mv=new ModelAndView();
    	Area area=service.gethere(id);
    	mv.addObject("area",area);
    	mv.setViewName("system/area/edit");
    	return mv;
    	
    }

	
}
