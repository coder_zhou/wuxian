package com.qf.system.area.service;



import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.qf.system.area.dao.AreaMapper;
import com.qf.system.area.model.Area;
import com.qf.system.area.model.AreaExample;


@Service("areaService")
public class AreaServiceImpl implements AreaService {
	
	private AreaMapper dao;

	public AreaMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(AreaMapper dao) {
		this.dao = dao;
	}
	
	/**获取省列表
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#getProList()
	*/
	@Override
	@org.springframework.cache.annotation.Cacheable(value="areaCachePro")
	public List<Area> getProList() {
		// TODO Auto-generated method stub
		AreaExample example=new AreaExample();
		example.createCriteria().andParentidEqualTo(1);
		
		return dao.selectByExample(example);
	}
	
	/**根据id获取下级
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#getCity(int)
	*/
	@Override
	@org.springframework.cache.annotation.Cacheable(value="areaCacheCity" ,key="#proId")
	public List<Area> getCity(int proId) {
		// TODO Auto-generated method stub
		AreaExample em=new AreaExample();
		em.createCriteria().andParentidEqualTo(proId);
		return dao.selectByExample(em);
	}
	
	/**根据上级ID获取上级对象
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#getParent(int)
	*/
	@Override
	@org.springframework.cache.annotation.Cacheable(value="areaCache",key="#proId")
	public List<Area> getParent(int proId) {
		// TODO Auto-generated method stub
		AreaExample em=new AreaExample();
		em.createCriteria().andAreaIdEqualTo(proId);
		return dao.selectByExample(em);
	}
	
	/**添加下级地区
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#addSon(com.qf.system.area.model.Area)
	*/
	@Override
	@CacheEvict(value="areaCache",allEntries=true)// 清空accountCache 缓存  
	public void addSon(Area area) {
		// TODO Auto-generated method stub
		dao.insert(area);
	}
	
	/**删除地区
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#delCity(int)
	*/
	@Override
	@CacheEvict(value="areaCache",allEntries=true)// 清空accountCache 缓存  
	public void delCity(int id) {
		// TODO Auto-generated method stub
		dao.deleteByPrimaryKey(id);
	}
	
	/**修改地区
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#editCity(com.qf.system.area.model.Area)
	*/
	@Override
	@CachePut(value="areaCache",key="#area.getAreaId")// 更新accountCache 缓存  
	public void editCity(Area area) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKey(area);
	}
	
	/**根据id获取地区对象
	* (non-Javadoc)
	* @see com.qf.system.area.service.AreaService#gethere(int)
	*/
	@Override
	@Cacheable(value="areaCache" ,key="#id")
	public Area gethere(int id) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(id);
	}
}
