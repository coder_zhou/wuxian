package com.qf.system.area.service;

import java.util.List;


import com.qf.system.area.model.Area;




public interface AreaService {

	List<Area> getProList();

	List<Area> getCity(int proId);

	List<Area> getParent(int proId);

	void addSon(Area area);

	void delCity(int id);

	void editCity(Area area);
	Area gethere(int id);



}
