package com.qf.system.links.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.qf.util.PageData;
import com.qf.util.entity.Page;
@Repository
public interface LinksMapper{

   int save(PageData pd);
   int delete(PageData pd);
   int edit(PageData pd);
   List<PageData> datalistPage(Page page);
   PageData findById(PageData pd);
   void deleteAll(String[] ArrayDATA_IDS);
}