package com.qf.system.links.service;


import com.qf.system.links.model.Links;
import com.qf.util.entity.Page;

import java.util.List;
import java.util.Map;

import com.qf.util.PageData;
public interface LinksService {


     int save(PageData pd)  throws Exception;
     int delete(PageData pd)  throws Exception;
     int edit(PageData pd)  throws Exception;
     List<PageData> list(Page page)  throws Exception;
    PageData  findById(PageData pd)  throws Exception;
     void deleteAll(String[] ArrayDATA_IDS)  throws Exception;
	List<Links> getIndexList()throws Exception;

}
