package com.qf.system.links.service;






import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qf.system.links.model.Links;
import com.qf.util.dao.DaoSupport;
import com.qf.util.entity.Page;
import com.qf.util.PageData;


@Service("linksService")
public class LinksServiceImpl implements LinksService {
	@Resource(name = "daoSupport")
	private DaoSupport dao;

    /*
	* 新增
	*/
	@Override
	public int save(PageData pd) throws Exception{
			return (int) dao.save("LinksMapper.save", pd);
	}
	
	/*
	* 删除
	*/
	@Override
	public int delete(PageData pd) throws Exception{
		return (int) dao.delete("LinksMapper.delete", pd);
	}

	/*
	* 修改
	*/
	@Override
	public int edit(PageData pd) throws Exception{
		return (int) dao.update("LinksMapper.edit", pd);
	}
	
	/*
	*列表
	*/
	@SuppressWarnings("unchecked")
	@Override
	public   List<PageData> list(Page page) throws Exception{
			

			return (List<PageData>) dao.findForList("LinksMapper.datalistPage", page);
	}
	
	
	/*
	* 通过id获取数据
	*/
	@Override
	public PageData findById(PageData pd) throws Exception{
		return (PageData) dao.findForObject("LinksMapper.findById", pd);
	}
	
	/*
	* 批量删除
	*/
	@Override
	public void deleteAll(String[] ArrayDATA_IDS) throws Exception{
		List<String> ls=Arrays.asList(ArrayDATA_IDS);
		dao.batchDelete("LinksMapper.deleteAll", ls);
	}
	@Override
	public List<Links> getIndexList() throws Exception {
		// TODO Auto-generated method stub
		return (List<Links>) dao.findForList("LinksMapper.IndexList", "");
	}
}
