package com.qf.system.links.model;

public class Links{

	
	private int links_id;  //ID
	
	private String links_name;  //友链名称
	
	private String links_url;  //友链路径
	
	private String links_img;  //友链图片
	
	private int links_flag;  //友链状态
	
    private Long links_addTime;     //添加时间

	public int getLinks_id() {
		return links_id;
	}

	public void setLinks_id(int links_id) {
		this.links_id = links_id;
	}

	public String getLinks_name() {
		return links_name;
	}

	public void setLinks_name(String links_name) {
		this.links_name = links_name;
	}

	public String getLinks_url() {
		return links_url;
	}

	public void setLinks_url(String links_url) {
		this.links_url = links_url;
	}

	public String getLinks_img() {
		return links_img;
	}

	public void setLinks_img(String links_img) {
		this.links_img = links_img;
	}

	public int getLinks_flag() {
		return links_flag;
	}

	public void setLinks_flag(int links_flag) {
		this.links_flag = links_flag;
	}

	public Long getLinks_addTime() {
		return links_addTime;
	}

	public void setLinks_addTime(Long links_addTime) {
		this.links_addTime = links_addTime;
	}
    

}