package com.qf.system.role.dao;

import java.util.List;

import com.qf.system.role.model.Role;
import com.qf.util.PageData;




public interface RoleMapper {
    int deleteByPrimaryKey(String roleId);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(String roleId);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

	List<Role> listAllERRoles(Object object);

	List<Role> listAllappERRoles(Object object);

	List<Role> listAllRoles(Object object);

	PageData findGLbyrid(PageData pd);

	PageData findYHbyrid(PageData pd);

	List<PageData> listAllUByRid(PageData pd);

	List<PageData> listAllAppUByRid(PageData pd);

	List<Role> listAllRolesByPId(PageData pd);

	List<PageData> listAllkefu(PageData pd);

	List<PageData> listAllGysQX(PageData pd);

	void deleteKeFuById(String rOLE_ID);

	void ROLE_ID(String rOLE_ID);

	void deleteGById(String rOLE_ID);

	void deleteRoleById(String rOLE_ID);

	Role getRoleById(String roleId);

	void updateRoleRights(Role role);

	void setAllRights(PageData pd);

	void insert(PageData pd);

	void saveKeFu(PageData pd);

	void saveGYSQX(PageData pd);

	PageData edit(PageData pd);

	PageData findObjectById(PageData pd);

	void kfqx1(PageData pd);

	void kfqx2(PageData pd);

	void fxqx(PageData pd);

	void fwqx(PageData pd);
}