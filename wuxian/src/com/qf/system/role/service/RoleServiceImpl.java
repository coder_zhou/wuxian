package com.qf.system.role.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.qf.system.role.dao.RoleMapper;
import com.qf.system.role.model.Role;
import com.qf.util.PageData;


/**
*
* 项目名称：qf
* 类名称：RoleServiceImpl
* 类描述： 权限管理
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:45:34
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:45:34
* 修改备注：
* @version
*
*/
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	
	
	private RoleMapper mapper;
	
	
	public RoleMapper getMapper() {
		return mapper;
	}

    @Autowired
	public void setMapper(RoleMapper mapper) {
		this.mapper = mapper;
	}

    @Cacheable(value="roleCaCheER")
	public List<Role> listAllERRoles() throws Exception {
		return (List<Role>) mapper.listAllERRoles(null);
		
	}
	
    @Cacheable(value="roleCaCheAPPER")
	public List<Role> listAllappERRoles() throws Exception {
		return (List<Role>) mapper.listAllappERRoles( null);
		
	}
	
	@Cacheable(value="roleCaChe")
	public List<Role> listAllRoles() throws Exception {
		return (List<Role>) mapper.listAllRoles( null);
		
	}
	
	//通过当前登录用的角色id获取管理权限数据
	@Cacheable(value="roleCaCheMU",key="#pd.getString(\"role_id\")")
	public PageData findGLbyrid(PageData pd) throws Exception {
		return (PageData) mapper.findGLbyrid( pd);
	}
	
	//通过当前登录用的角色id获取用户权限数据
	@Cacheable(value="roleCaCheY",key="#pd.getString(\"role_id\")")
	public PageData findYHbyrid(PageData pd) throws Exception {
		return (PageData) mapper.findYHbyrid( pd);
	}
	
	//列出此角色下的所有用户
	public List<PageData> listAllUByRid(PageData pd) throws Exception {
		return (List<PageData>) mapper.listAllUByRid( pd);
		
	}
	
	//列出此角色下的所有会员
	
	public List<PageData> listAllAppUByRid(PageData pd) throws Exception {
		return (List<PageData>) mapper.listAllAppUByRid( pd);
		
	}
	
	/**
	 * 列出此部门的所有下级
	 */
	@Cacheable(value="roleCaChe",key="#pd.getString(\"role_id\")")
	public List<Role> listAllRolesByPId(PageData pd) throws Exception {
		return (List<Role>) mapper.listAllRolesByPId( pd);
		
	}
	
	//列出K权限表里的数据 
	@Cacheable(value="roleCaCheKeFu")
	public List<PageData> listAllkefu(PageData pd) throws Exception {
		return (List<PageData>) mapper.listAllkefu( pd);
	}
	
	//列出G权限表里的数据 
	@Cacheable(value="roleCaCheG",key="#pd.getString(\"role_id\")")
	public List<PageData> listAllGysQX(PageData pd) throws Exception {
		return (List<PageData>) mapper.listAllGysQX( pd);
	}
	
	//删除K权限表里对应的数据
	public void deleteKeFuById(String ROLE_ID) throws Exception {
		mapper.deleteKeFuById( ROLE_ID);
	}
	
	//删除G权限表里对应的数据
	public void deleteGById(String ROLE_ID) throws Exception {
		mapper.deleteGById( ROLE_ID);
	}
	
	public void deleteRoleById(String ROLE_ID) throws Exception {
		mapper.deleteRoleById( ROLE_ID);
		
	}

	public Role getRoleById(String roleId) throws Exception {
		return (Role) mapper.getRoleById( roleId);
		
	}

	public void updateRoleRights(Role role) throws Exception {
		mapper.updateRoleRights( role);
	}
	
	
	/**
	 * 给全部子职位加菜单权限
	 */
	public void setAllRights(PageData pd) throws Exception {
		mapper.setAllRights(pd);
		
	}
	
	/**
	 * 添加
	 */
	public void add(PageData pd) throws Exception {
		mapper.insert( pd);
	}
	
	/**
	 * 保存客服权限
	 */
	public void saveKeFu(PageData pd) throws Exception {
		mapper.saveKeFu( pd);
	}
	
	/**
	 * 保存G权限
	 */
	public void saveGYSQX(PageData pd) throws Exception {
		mapper.saveGYSQX( pd);
	}
	
	
	
	/**
	 * 编辑角色
	 */
	public PageData edit(PageData pd) throws Exception {
		return (PageData)mapper.edit( pd);
	}


	@Override
	@Cacheable(value="roleCaCheId" ,key="#pd.getString(\"role_id\")")
	public PageData findObjectById(PageData pd) throws Exception{
		// TODO Auto-generated method stub
		return (PageData)mapper.findObjectById( pd);
	}


	@Override
	public void updateQx(String msg, PageData pd) throws Exception {
		// TODO Auto-generated method stub
		switch (msg) {
		case "kfqx1":
			mapper.kfqx1(pd);
			break;
		case "kfqx2":
			mapper.kfqx2(pd);
			break;
		case "fxqx":
			mapper.fxqx(pd);
			break;
		case "fwqx":
			mapper.fwqx(pd);
			break;

		default:
			break;
		}
		}

	@Override
	public void updateKFQx(String msg, PageData pd) throws Exception {
		// TODO Auto-generated method stub
		switch (msg) {
		case "kfqx1":
			mapper.kfqx1(pd);
			break;
		case "kfqx2":
			mapper.kfqx2(pd);
			break;
		case "fxqx":
			mapper.fxqx(pd);
			break;
		case "fwqx":
			mapper.fwqx(pd);
			break;

		default:
			break;
		}	
		}

	@Override
	public void gysqxc(String msg, PageData pd) throws Exception {
		// TODO Auto-generated method stub
		switch (msg) {
		case "kfqx1":
			mapper.kfqx1(pd);
			break;
		case "kfqx2":
			mapper.kfqx2(pd);
			break;
		case "fxqx":
			mapper.fxqx(pd);
			break;
		case "fwqx":
			mapper.fwqx(pd);
			break;

		default:
			break;
		}
	}

}
