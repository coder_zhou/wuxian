package com.qf.system.role.service;

import java.util.List;

import com.qf.system.role.model.Role;
import com.qf.util.PageData;


/**
*
* 项目名称：qf
* 类名称：RoleService
* 类描述： 权限管理接口
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:47:41
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:47:41
* 修改备注：
* @version
*
*/
public interface RoleService {

	PageData findObjectById(PageData pd) throws Exception;

	PageData findGLbyrid(PageData pd2) throws Exception;

	PageData findYHbyrid(PageData pd2) throws Exception;


	void saveKeFu(PageData pd) throws Exception;

	void saveGYSQX(PageData pd) throws Exception;

	void add(PageData pd) throws Exception;

	PageData edit(PageData pd) throws Exception;

	Role getRoleById(String rOLE_ID) throws Exception;

	void updateRoleRights(Role role) throws Exception;

	void setAllRights(PageData pd) throws Exception;

	void updateQx(String msg, PageData pd) throws Exception;

	List<Role> listAllRolesByPId(PageData pd) throws Exception;

	void deleteRoleById(String rOLE_ID) throws Exception;

	void deleteKeFuById(String rOLE_ID) throws Exception;

	void deleteGById(String rOLE_ID) throws Exception;

	List<PageData> listAllAppUByRid(PageData pd) throws Exception;

	List<PageData> listAllUByRid(PageData pd) throws Exception;

	void updateKFQx(String msg, PageData pd) throws Exception;

	void gysqxc(String msg, PageData pd) throws Exception;

	List<Role> listAllRoles() throws Exception;

	List<PageData> listAllkefu(PageData pd) throws Exception;

	List<PageData> listAllGysQX(PageData pd) throws Exception;

	List<Role> listAllERRoles() throws Exception;

}
