package com.qf.system.userinfo.dao;

import java.util.List;
import java.util.Map;

import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.util.PageData;
import com.qf.util.entity.Page;
public interface UserInfoMapper{

   int save(PageData pd);
   int delete(PageData pd);
   int edit(PageData pd);
   List<PageData> datalistPage(Page page);
   PageData findById(PageData pd);
   void deleteAll(String[] ArrayDATA_IDS);
   
   int deleteByPrimaryKey(Integer userId);

   int insert(UserInfo record);

   int insertSelective(UserInfo record);

   UserInfo selectByPrimaryKey(Integer userId);

   int updateByPrimaryKeySelective(UserInfo record);

   int updateByPrimaryKey(UserInfo record);
    List<Map> list(PageData pd);
	void choose(PageData pd);
	UserInfo getUserByPhone(String phone);
	int checkUser(Map<String, Object> valueMap);
	UserInfo2 getUserById(int user_id);
	UserInfo getUserByPhoneAndPwd(Map<String, Object> valueMap);
	List<Map> listphoto(PageData pd);
	void AddPhoto(Map map);
	List<Map> getPhotoByUserId(int user_id);
	UserInfo2 getUserByPhoneAndPwd2(Map<String, Object> valueMap);
	void updatePassword(Map<String, Object> valueMap);
	int updateUser(Map<String, Object> valueMap);
	int updateHead(Map<String, Object> valueMap);
	void delPhoto(Map<String, Object> valueMap);
   
}