package com.qf.system.userinfo.model;

import com.qf.system.peilian.model.Peilian;

public class UserInfo{

	
	private int user_id;  //用户ID
	
	private String phone;  //手机号
	
	private String password="";  //密码
	
	private String user_nick_name="";  //用户昵称
	
	private int statue=0;  //用户状态
	
	private String user_head_photo="";  //头像
	
	private String sex;  //用户性别
	
	private String addtime;  //添加时间
	
	private String checktime="";  //激活时间
	
	private int height;  //身高
	
	private int weight;  //体重
	
	private String job;  //工作
	
	private String message;  //个人简介
	
	private String keshi;  //到期时间
	
	private String user_name;  //用户姓名
    private Peilian peilian;
    private int keshi_num;
    
	public int getKeshi_num() {
		return keshi_num;
	}

	public void setKeshi_num(int keshi_num) {
		this.keshi_num = keshi_num;
	}

	public Peilian getPeilian() {
		return peilian;
	}

	public void setPeilian(Peilian peilian) {
		this.peilian = peilian;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser_nick_name() {
		return user_nick_name;
	}

	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public String getUser_head_photo() {
		return user_head_photo;
	}

	public void setUser_head_photo(String user_head_photo) {
		this.user_head_photo = user_head_photo;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddtime() {
		return addtime;
	}

	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}

	public String getChecktime() {
		return checktime;
	}

	public void setChecktime(String checktime) {
		this.checktime = checktime;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getKeshi() {
		return keshi;
	}

	public void setKeshi(String keshi) {
		this.keshi = keshi;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

}