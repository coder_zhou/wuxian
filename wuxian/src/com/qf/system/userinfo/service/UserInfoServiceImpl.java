package com.qf.system.userinfo.service;






import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import com.qf.system.userinfo.dao.UserInfoMapper;
import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.util.interceptor.PageHelper;
import com.qf.util.page.Page;
import com.qf.util.PageData;


@Service("userinfoService")
public class UserInfoServiceImpl implements UserInfoService {
	
	private UserInfoMapper dao;

	public UserInfoMapper getDao() {
		return dao;
	}
    @Autowired
	public void setDao(UserInfoMapper dao) {
		this.dao = dao;
	}
    /*
	* 新增
	*/
	@Override
	public int save(UserInfo user){
			return dao.insert(user);
	}
	
	/*
	* 删除
	*/
	@Override
	public int delete(String userinfo_id){
		return dao.deleteByPrimaryKey(Integer.parseInt(userinfo_id));
	}
	
	/*
	* 修改
	*/
	@Override
	public int edit(UserInfo userinfo){
		return dao.updateByPrimaryKeySelective(userinfo);
	}
	
	/*
	*列表
	*/
	@Override
	public  Map<String,Object> list(Page page,PageData pd){
			Map<String,Object> map = new HashMap<String,Object>();
			PageHelper.startPage(page); //起始位置，长度
			dao.list(pd);
			Page p  = PageHelper.endPage();
			page.setRowCount(p.getRowCount());
			page.setPageCount(p.getPageCount());
			map.put("page", page);
			map.put("list", p.getResult());
			return map;
	}
	
	
	/*
	* 通过id获取数据
	*/
	@Override
	public UserInfo findById(String userinfo_id){
		return dao.selectByPrimaryKey(Integer.parseInt(userinfo_id));
	}
	
	/*
	* 批量删除
	*/
	@Override
	public void deleteAll(String[] list){
		dao.deleteAll(list);
	}
	@Override
	public void choose(PageData pd) {
		// TODO Auto-generated method stub
	   dao.choose(pd);	
	}
	
	@Override
	public UserInfo getUserByPhone(String phone) {
		// TODO Auto-generated method stub
		return dao.getUserByPhone(phone);
	}
	@Override
	public int checkUser(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
		return dao.checkUser(valueMap);
	}
	@Override
	public UserInfo2 getUserById(int user_id) {
		// TODO Auto-generated method stub
		return dao.getUserById(user_id);
	}
	@Override
	public UserInfo getUserByPhoneAndPwd(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
		return dao.getUserByPhoneAndPwd(valueMap);
	}
	@Override
	public Map<String, Object> listPhoto(Page page, PageData pd) {
		// TODO Auto-generated method stub
		Map<String,Object> map = new HashMap<String,Object>();
		PageHelper.startPage(page); //起始位置，长度
        dao.listphoto(pd);
		Page p  = PageHelper.endPage();
		page.setRowCount(p.getRowCount());
		page.setPageCount(p.getPageCount());
		map.put("page", page);
		map.put("list", p.getResult());
		return map;
	}
	@Override
	public void addphoto(Map map) {
		// TODO Auto-generated method stub
	dao.AddPhoto(map);	
	}
	
	@Override
	public List<Map> getPhotoByUserId(int user_id) {
		// TODO Auto-generated method stub
		return dao.getPhotoByUserId(user_id);
	}
	
	@Override
	public UserInfo2 getUserByPhoneAndPwd2(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
		return dao.getUserByPhoneAndPwd2(valueMap);
	}
	@Override
	public void updatePassword(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
		dao.updatePassword(valueMap);
	}
	@Override
	public int updateUser(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
	      return   dao.updateUser(valueMap);	
	}
	@Override
	public int updateHead(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
		return dao.updateHead(valueMap);
	}
	@Override
	public void delPhoto(Map<String, Object> valueMap) {
		// TODO Auto-generated method stub
	   dao.delPhoto(valueMap);	
	}
	
}
