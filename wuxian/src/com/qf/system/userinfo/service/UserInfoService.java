package com.qf.system.userinfo.service;


import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.util.page.Page;
import java.util.List;
import java.util.Map;
import com.qf.util.PageData;
public interface UserInfoService {


     int save(UserInfo userinfo);
     int delete(String userinfo_id);
     int edit(UserInfo userinfo);
     Map<String,Object> list(Page page, PageData pd);
     UserInfo findById(String userinfo_id);
     void deleteAll(String[] arrayDATA_IDS);
	void choose(PageData pd);
	UserInfo getUserByPhone(String phone);
	int checkUser(Map<String, Object> valueMap);
	UserInfo2 getUserById(int user_id);
	UserInfo getUserByPhoneAndPwd(Map<String, Object> valueMap);
	Map<String, Object> listPhoto(Page page, PageData pd);
	void addphoto(Map map);
	List<Map> getPhotoByUserId(int user_id);
	UserInfo2 getUserByPhoneAndPwd2(Map<String, Object> valueMap);
	void updatePassword(Map<String, Object> valueMap);
	int updateUser(Map<String, Object> valueMap);
	int updateHead(Map<String, Object> valueMap);
	void delPhoto(Map<String, Object> valueMap);

}
