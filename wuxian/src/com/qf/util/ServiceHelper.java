package com.qf.util;

import com.qf.system.menu.service.MenuService;
import com.qf.system.role.service.RoleService;
import com.qf.system.user.service.SystemUserServiceI;
import com.qf.util.common.Const;








/**
*
* 项目名称：qf
* 类名称：ServiceHelper
* 类描述：获取spring 容器中的service
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:40:22
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:40:22
* 修改备注：
* @version
*
*/
public final class ServiceHelper {
	
	public static Object getService(String serviceName){
		//WebApplicationContextUtils.
		return Const.WEB_APP_CONTEXT.getBean(serviceName);
	}
	
	public static SystemUserServiceI getUserService(){
		return (SystemUserServiceI) getService("systemuserService");
	}
	
	public static RoleService  getRoleService(){
		return (RoleService) getService("roleService");
	}
	
	public static MenuService getMenuService(){
		return (MenuService) getService("menuService");
	}
}
