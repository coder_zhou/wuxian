package com.qf.util.aes;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.client.utils.URLEncodedUtils;

import com.google.gson.Gson;


public class AES {

	public static String encrypt(String cleartext) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(Constants.Aes_Key.getBytes(), "AES");
		// IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes());
		// ECB模式不需要传入IV
		Cipher ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		ecipher.init(Cipher.ENCRYPT_MODE, keySpec);
		return Base64.encodeBytes(ecipher.doFinal(cleartext.getBytes("UTF-8")));
	}

	public static String decrypt(String encrypted) throws Exception {
		SecretKeySpec keySpec = new SecretKeySpec(Constants.Aes_Key.getBytes(), "AES");
		// IvParameterSpec ivSpec = new IvParameterSpec(iv.getBytes());
		// ECB模式不需要传入IV
		Cipher ecipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		ecipher.init(Cipher.DECRYPT_MODE, keySpec);
		return new String(ecipher.doFinal(Base64.decode(encrypted)),"UTF-8");
	}
    public static void main(String []args){
    	Map map=new HashMap();
    	map.put("action", "/timer/delPhoto");
    	map.put("user_id", "2");
    	map.put("pic_id", "7");
//    	map.put("phone", "15146639681");
//    	map.put("password", "123123");
//    	map.put("action", "/timer/resetpassword");
    	map.put("pl_id", "2");
//    	map.put("phone", "13622132152");
//    	map.put("password", "123456");
//    	map.put("newpassword", "123456");
    	Gson gson=new Gson();
    	
    	
    	String  aa =gson.toJson(map);
    	try {
			System.out.println("http://localhost:8080/appRoute/route?para="+URLEncoder.encode(encrypt(aa)));
			System.out.println("http://182.92.168.214:8080/appRoute/route?para="+URLEncoder.encode(encrypt(aa)));
			
			String dd="kjJVLzbhEbP5FD60bmBr81wAm/xuUTteAdlwW5H04kNKqsYQegjvjanhKfbXKirkw68244Wqj4a92CmoA/HAoV6TdxB+qduNsoGNy4j2N3a7+JMXzEOWE13F9RKBpy9ouyHF78zUPUIrPO6/oV5DAcpo41YQwh4hZkbhz64dt9s7Mt688RrjAGs8w4T6EnccpLFR5M6jI0/jlkUgP8O23/drAF3q8ExP4ci7PLub1RpMKz7OruQVAuQwUh4Uh+mNwDeGcL5mxw8JveanGB2CZ8HJavktMg0QKXazdSIvOTVVkwmcjAum5lT5xcBEecQ26j2CRVpRCfk3Os6/z9yzipLYpSPj96z1Vu10TlWKdgBbUut5yujTD9geH0kAdGrLzfjZEq76c96NCZoMOinAHUmib2HP9ez9/x17aSnfZNrZq8krxl82y/7c0T7obpL2OdX0NLvxTcS9Qv1Vm0Zp4alrUFNPbv7uoEG1y0oPnTpESYlD5vl8heOjns+jIaTA8b+tWNfib/5/ecCuqMKZOfhC/qXbniwz+4s7l9XpSsaN3NAk1EawaDtx8UqbmzI8m2e3AuaZyDpKCENtIPjy3ITkNRmmaG3dLo6dllNbb91pu+9UOzIaHQ1O5H6hG2JeeYWFQ6GnlNtids0gyAhYrPi3iB0LVk89CiJQDDVBmGIBpaVKfLKB6iHFzZ87xwjBD19I8SEqlW9MrCwnlr7pZlVRkOhBUESMoKi+7ioEgV8/SpMuSolvtcnDiXHR5NsszxPgMq1ZBqoid15STIsjwvS0ahUeoNJtbIJkkztuuPstis7HNlqBW9rNa6yGpmr45s7j07v2iW6llAzUUdyTsQ==";
			System.out.println(decrypt(dd));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}