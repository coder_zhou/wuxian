package com.qf.util.interceptor;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.qf.system.menu.model.Menu;
import com.qf.system.user.model.SystemUser;
import com.qf.util.RightsHelper;
import com.qf.util.ServiceHelper;
import com.qf.util.common.Const;
import com.qf.util.common.Tools;



/**
*
* 项目名称：qf
* 类名称：RightsHandlerInterceptor
* 类描述： 权限拦截器
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:38:28
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:38:28
* 修改备注：
* @version
*
*/
public class RightsHandlerInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		String path = request.getServletPath();
		if(path.matches(Const.NO_INTERCEPTOR_PATH))
			return true;
		//shiro管理的session
		Subject currentUser = SecurityUtils.getSubject();  //获取当前操作用户
		Session session = currentUser.getSession();//获取当前用户session
		SystemUser systemUser = (SystemUser) session.getAttribute(Const.SESSION_USER); //获取当前用户
		String menuId = null;  
		List<Menu> subList = ServiceHelper.getMenuService().listAllSubMenu(); //获取菜单
		//循环遍历菜单
		for(Menu m : subList){
			String menuUrl = m.getMenu_url();
			if(Tools.notEmpty(menuUrl)){
				if(path.contains(menuUrl)){
					menuId = m.getMenu_id().toString();
					break;
				}else{
					String[] arr = menuUrl.split("\\.");
					String regex = "";
					if(arr.length==2){
						regex = "/?"+arr[0]+"(/.*)?."+arr[1];
						
					}else{
						regex = "/?"+menuUrl+"(/.*)?";
					}
					if(path.matches(regex)){
						menuId = m.getParent_id();
						break;
					}
				}
			}
		}
		//System.out.println(path+"===="+menuId);
		if(menuId!=null){
			//user = ServiceHelper.getUserService().getUserAndRoleById(user.getUserId());
			String roleRights = (String) session.getAttribute(Const.SESSION_ROLE_RIGHTS); //获取权限
			if(RightsHelper.testRights(roleRights, menuId)){
				return true;
			}else{
				System.out.println("用户："+systemUser.getUser_name()+"试图访问"+path+"被阻止！");
				ModelAndView mv = new ModelAndView();
				mv.setViewName("no_rights");
				throw new  ModelAndViewDefiningException(mv);
			}
		}
		return super.preHandle(request, response, handler);
	}
	
}
