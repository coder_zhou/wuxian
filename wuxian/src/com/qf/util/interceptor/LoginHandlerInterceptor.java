package com.qf.util.interceptor;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.qf.system.menu.model.Menu;
import com.qf.system.user.model.SystemUser;
import com.qf.util.RightsHelper;
import com.qf.util.common.Const;


/**
*
* 项目名称：qf
* 类名称：LoginHandlerInterceptor
* 类描述：  登录拦截器
* 创建人：周广文
* 创建时间：2015年5月13日 上午11:19:49
* 修改人：周广文
* 修改时间：2015年5月13日 上午11:19:49
* 修改备注：
* @version
*
*/
public class LoginHandlerInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		String path = request.getServletPath();
		if(path.matches(Const.NO_INTERCEPTOR_PATH)){
			return true;
		}else{
			//shiro管理的session
			Subject currentUser = SecurityUtils.getSubject();  
			Session session = currentUser.getSession();
			SystemUser systemUser = (SystemUser)session.getAttribute(Const.SESSION_USER);
			if(systemUser!=null){
				
				//判断是否拥有当前点击菜单的权限（内部过滤,防止通过url进入跳过菜单权限）
				/**
				 * 根据点击的菜单的xxx去菜单中的URL去匹配，当匹配到了此菜单，判断是否有此菜单的权限，没有的话跳转到登录页面
				 * 根据按钮权限，授权按钮(当前点的菜单和角色中各按钮的权限匹对)
				 */
				Boolean b = true;
				List<Menu> menuList = (List)session.getAttribute(Const.SESSION_allmenuList); //获取菜单列表
				path = path.substring(1, path.length());
				for(int i=0;i<menuList.size();i++){
					if(menuList.get(i).getSubMenu()!=null){
					for(int j=0;j<menuList.get(i).getSubMenu().size();j++){
						if(menuList.get(i).getSubMenu().get(j).getMenu_url().split("")[0].equals(path.split("")[0])){
							if(!menuList.get(i).getSubMenu().get(j).isHasMenu()){			
								//判断有无此菜单权限
								System.out.println("用户："+systemUser.getUser_name()+"试图访问"+path+"被阻止！");
								//shiro管理的session
								
								session.removeAttribute(Const.SESSION_USER);
								session.removeAttribute(Const.SESSION_ROLE_RIGHTS);
								session.removeAttribute(Const.SESSION_allmenuList);
								session.removeAttribute(Const.SESSION_menuList);
								session.removeAttribute(Const.SESSION_QX);
								session.removeAttribute(Const.SESSION_userpds);
								session.removeAttribute(Const.SESSION_USERNAME);
								session.removeAttribute(Const.SESSION_USERROL);
								session.removeAttribute("changeMenu");
								
								//shiro销毁登录
								Subject subject = SecurityUtils.getSubject(); 
								subject.logout();
								response.sendRedirect(request.getContextPath() + Const.LOGIN);
								return false;
							}else{																//按钮判断
								Map<String, String> map = (Map<String, String>)session.getAttribute(Const.SESSION_QX);//按钮权限
								map.remove("add");
								map.remove("del");
								map.remove("edit");
								map.remove("cha");
								String MENU_ID =  menuList.get(i).getSubMenu().get(j).getMenu_id().toString();
								String USERNAME = session.getAttribute(Const.SESSION_USERNAME).toString();	//获取当前登录者loginname
								Boolean isAdmin = "admin".equals(USERNAME);
								map.put("add", RightsHelper.testRights(map.get("adds"), MENU_ID) || isAdmin?"1":"0");
								map.put("del", RightsHelper.testRights(map.get("dels"), MENU_ID) || isAdmin?"1":"0");
								map.put("edit", RightsHelper.testRights(map.get("edits"), MENU_ID) || isAdmin?"1":"0");
								map.put("cha", RightsHelper.testRights(map.get("chas"), MENU_ID) || isAdmin?"1":"0");
								session.removeAttribute(Const.SESSION_QX);
								session.setAttribute(Const.SESSION_QX, map);	//重新分配按钮权限
							}
						}
					}
				}
				}
				return true;
			}else{
				//登陆过滤
				response.sendRedirect(request.getContextPath() + Const.LOGIN);
				return false;		
				//return true;
			}
		}
	}
	
}
