package com.qf.util.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.qf.util.common.Const;

public class UserSessionInterceptor extends HandlerInterceptorAdapter {
@Override
public boolean preHandle(HttpServletRequest request,
		HttpServletResponse response, Object handler) throws Exception {
	// TODO Auto-generated method stub
	Subject currentUser = SecurityUtils.getSubject();  
	Session session = currentUser.getSession();
	if(session.getAttribute("USER_ID")==null){
	
		if(session.getAttribute(Const.SESSIONUSERID)==null){
			System.out.println("监测到未登陆用户,分配一个sessionuserid:"+UUID.randomUUID().toString());
		session.setAttribute(Const.SESSIONUSERID, UUID.randomUUID().toString());
		}
		
	}
	return super.preHandle(request, response, handler);
}
}
