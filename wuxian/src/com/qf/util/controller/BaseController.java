package com.qf.util.controller;



import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qf.util.PageData;
import com.qf.util.PublicUtil;
import com.qf.util.UuidUtil;
import com.qf.util.entity.Page;



/**
*
* 项目名称：qf
* 类名称：BaseController
* 类描述：
* 创建人：周广文
* 创建时间：2015年5月22日 下午5:13:53
* 修改人：周广文
* 修改时间：2015年5月22日 下午5:13:53
* 修改备注：
* @version
*
*/
public class BaseController {
	
	protected Logger logger = Logger.getLogger(this.getClass());

	private static final long serialVersionUID = 6357869213649815390L;
	
	/**
	 * 得到PageData
	 */
	public PageData getPageData(){
		return new PageData(this.getRequest());
	}
	
	/**
	 * 得到ModelAndView
	 */
	public ModelAndView getModelAndView(){
		return new ModelAndView();
	}
	
	/**
	 * 得到request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		return request;
	}

	/**
	 * 得到32位的uuid
	 * @return
	 */
	public String get32UUID(){
		
		return UuidUtil.get32UUID();
	}
	
	/**
	 * 得到分页列表的信息 
	 */
	public Page getPage(){
		
		return new Page();
	}
	
	public static void logBefore(Logger logger, String interfaceName){
		logger.info("");
		logger.info("start");
		logger.info(interfaceName);
	}
	
	public static void logAfter(Logger logger){
		logger.info("end");
		logger.info("");
	}

//    @ExceptionHandler
//    public String exception(HttpServletRequest request, Exception e) {  
//        request.setAttribute("exceptionMessage", e.getMessage());  
//          
//        // 根据不同的异常类型进行不同处理
//        if(e instanceof SQLException) 
//            return "testerror";   
//        else
//            return "error";  
//    }  
}
