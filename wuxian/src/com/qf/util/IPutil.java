package com.qf.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;  
import java.util.Set;

public class IPutil {
//      public static String IPaddress ="http://192.168.1.254:8080/qf";
      
//	public static void main(String [] args){  
//        InetAddress netAddress = getInetAddress();  
//        System.out.println("host ip:" + getHostIp(netAddress));  
//        System.out.println("host name:" + getHostName(netAddress));  
//        Properties properties = System.getProperties();  
//        Set<String> set = properties.stringPropertyNames(); //获取java虚拟机和系统的信息。  
//        for(String name : set){  
//            System.out.println(name + ":" + properties.getProperty(name));  
//        }  
//    }  
	
	 public static InetAddress getInetAddress(){  
	        try{  
	            return InetAddress.getLocalHost();  
	        }catch(UnknownHostException e){  
	            System.out.println("unknown host!");  
	        }  
	        return null;  
	    }  
	    //图片路径
	    public static String getHostIp(){  
    	  try{  
	    	  InetAddress netAddress = InetAddress.getLocalHost();
	    	  if(null == netAddress){  
		            return null;  
		      }  
	          String ip = netAddress.getHostAddress(); //get the ip address  
	          if("192.168.1.254".equals(ip)){
		          return "http://192.168.1.254:8080/goods";
	          }
		      return "http://123.57.217.59/goods";  
    	  }catch(UnknownHostException e){  
		            System.out.println("unknown host!");  
          }  
          return null;  
	    }  
	    //得到ip
	    public static String getIp(){  
	    	  try{  
		    	  InetAddress netAddress = InetAddress.getLocalHost();
		    	  if(null == netAddress){  
			            return null;  
			      }  
		          String ip = netAddress.getHostAddress(); //get the ip address  
		          if("192.168.1.254".equals(ip)){
			          return "http://192.168.1.254:8080";
		          }
			      return "http://123.57.217.59";  
	    	  }catch(UnknownHostException e){  
			            System.out.println("unknown host!");  
	          }  
	          return null;  
		    }  
	    
	    public static String getHostName(InetAddress netAddress){  
	        if(null == netAddress){  
	            return null;  
	        }  
	        String name = netAddress.getHostName(); //get the host address  
	        return name;  
	    }  
}
