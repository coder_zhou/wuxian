package com.qf.util;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import com.qf.util.common.MD5;

public class testRole {

	
	/**
	 * 将字符串转成unicode
	 * 
	 * @param str
	 *            待转字符串
	 * @return unicode字符串
	 */
	public static String convert(String str) {
		str = (str == null ? "" : str);
		String tmp;
		StringBuffer sb = new StringBuffer(1000);
		char c;
		int i, j;
		sb.setLength(0);
		for (i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			sb.append("\\u");
			j = (c >>> 8); // 取出高8位
			tmp = Integer.toHexString(j);
			if (tmp.length() == 1)
				sb.append("0");
			sb.append(tmp);
			j = (c & 0xFF); // 取出低8位
			tmp = Integer.toHexString(j);
			if (tmp.length() == 1)
				sb.append("0");
			sb.append(tmp);

		}
		return (new String(sb));
	}

	/**
	 * 将unicode 字符串
	 * 
	 * @param str
	 *            待转字符串
	 * @return 普通字符串
	 */
	public static String revert(String str) {
		str = (str == null ? "" : str);
		if (str.indexOf("\\u") == -1)// 如果不是unicode码则原样返回
			return str;

		StringBuffer sb = new StringBuffer(1000);

		for (int i = 0; i < str.length() - 6;) {
			String strTemp = str.substring(i, i + 6);
			String value = strTemp.substring(2);
			int c = 0;
			for (int j = 0; j < value.length(); j++) {
				char tempChar = value.charAt(j);
				int t = 0;
				switch (tempChar) {
				case 'a':
					t = 10;
					break;
				case 'b':
					t = 11;
					break;
				case 'c':
					t = 12;
					break;
				case 'd':
					t = 13;
					break;
				case 'e':
					t = 14;
					break;
				case 'f':
					t = 15;
					break;
				default:
					t = tempChar - 48;
					break;
				}

				c += t * ((int) Math.pow(16, (value.length() - j - 1)));
			}
			sb.append((char) c);
			i = i + 6;
		}
		return sb.toString();
	}
	
	

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
//		int[] i = { 1, 2,4,3,15, 16, 17, 18 ,19,20,21,22,23};
//		RightsHelper ri = new RightsHelper();
//		System.out.println(ri.sumRights(i));
//		String[] aa={ "1","2","4","3","15", "16", "17", "18" ,"19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35"};
//        System.out.println(ri.sumRights(aa));
//        MD5 m = new MD5();
//		String passwd =m.getMD5ofStr("123456");
//		System.out.println(passwd);
//        boolean entityOrField =true;
//        int showCount =10;
//		StringBuffer sb=new StringBuffer();
//		//换页函数
//		sb.append("function nextPage(page){");
//		sb.append(" top.jzts();");
//		sb.append("	if(true && document.forms[0]){\n");
//		sb.append("		var url = document.forms[0].getAttribute(\"action\");\n");
//		sb.append("		if(url.indexOf('?')>-1){url += \"&"+(entityOrField?"currentPage":"page.currentPage")+"=\";}\n");
//		sb.append("		else{url += \"?"+(entityOrField?"currentPage":"page.currentPage")+"=\";}\n");
//		sb.append("		url = url + page + \"&" +(entityOrField?"showCount":"page.showCount")+"="+showCount+"\";\n");
//		sb.append("		document.forms[0].action = url;\n");
//		sb.append("		document.forms[0].submit();\n");
//		sb.append("	}else{\n");
//		sb.append("		var url = document.location+'';\n");
//		sb.append("		if(url.indexOf('?')>-1){\n");
//		sb.append("			if(url.indexOf('currentPage')>-1){\n");
//		sb.append("				var reg = /currentPage=\\d*/g;\n");
//		sb.append("				url = url.replace(reg,'currentPage=');\n");
//		sb.append("			}else{\n");
//		sb.append("				url += \"&"+(entityOrField?"currentPage":"page.currentPage")+"=\";\n");
//		sb.append("			}\n");
//		sb.append("		}else{url += \"?"+(entityOrField?"currentPage":"page.currentPage")+"=\";}\n");
//		sb.append("		url = url + page + \"&" +(entityOrField?"showCount":"page.showCount")+"="+showCount+"\";\n");
//		sb.append("		document.location = url;\n");
//		sb.append("	}\n");
//		sb.append("}\n");
//		System.out.println(sb.toString());
//		
		 JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
	        System.out.println(""+ToolProvider.getSystemJavaCompiler());
	        StandardJavaFileManager fileManager = compiler.getStandardFileManager(
	                null, null, null);

	        StringObject so = new StringObject(  
                    "CalculatorTest",  
                    "public class CalculatorTest {"  
                            + " public int multiply(int multiplicand, int multiplier) {"  
                            + " System.out.println(multiplicand);"  
                            + " System.out.println(multiplier);"  
                            + " return multiplicand * multiplier;" + " }" + "}");  
            Iterable<? extends JavaFileObject> files = Arrays.asList(so);  
            Iterable<String> options =  Arrays.asList("-d","D:\\work\\qjqw\\qf\\WebRoot\\WEB-INF\\classes");  
            JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager,  
                    null, options, null, files);  
            Boolean result = task.call();
            System.out.println(result);
            if (result) {
                Class clazz = Class.forName("CalculatorTest");

                Object instance = clazz.newInstance();

                Method m = clazz.getMethod("multiply", new Class[] { int.class,
                        int.class });

                Object[] o = new Object[] { 3, 2 };
                System.out.println(m.invoke(instance, o));
            } 
		
	}

}
class StringObject extends SimpleJavaFileObject {
    private String contents = null;

    public StringObject(String className, String contents) throws Exception {
        super(URI.create("string:///" + className.replace('.', '/')
                + Kind.SOURCE.extension), Kind.SOURCE);
        this.contents = contents;
    }

    public CharSequence getCharContent(boolean ignoreEncodingErrors)
            throws IOException {
        return contents;
    }
}