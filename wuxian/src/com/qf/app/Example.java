package com.qf.app;


import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.qf.app.io.rong.ApiHttpClient;
import com.qf.app.io.rong.models.ChatroomInfo;
import com.qf.app.io.rong.models.ContactNtfMessage;
import com.qf.app.io.rong.models.FormatType;
import com.qf.app.io.rong.models.GroupInfo;
import com.qf.app.io.rong.models.ImgMessage;
import com.qf.app.io.rong.models.ProfileMessage;
import com.qf.app.io.rong.models.PushMessage;
import com.qf.app.io.rong.models.PushMessageType;
import com.qf.app.io.rong.models.SdkHttpResult;
import com.qf.app.io.rong.models.TxtMessage;
import com.qf.app.io.rong.models.VoiceMessage;

public class Example {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

//		String key = "p5tvi9dst0iy4";
//		String secret = "rAqZnka3L5TvGH";

		SdkHttpResult result = null;

//		result = ApiHttpClient.getToken("402880ef4a", "asdfa",
//				"http://aa.com/a.png", FormatType.json);
//		System.out.println("gettoken=" + result);
		
//		toIds.add("id2");
//		toIds.add("id3");
//		result = ApiHttpClient.publishMessage("24567", toIds,
//				new TxtMessage("txtMessagehaha"), FormatType.json);
//		System.out.println("publishMessage=" + result);
//		result = ApiHttpClient.publishMessage("fromUserId", toIds,
//				new VoiceMessage("txtMessagehaha", 100L), FormatType.json);
//		System.out.println("publishMessage=" + result);
//		result = ApiHttpClient.publishMessage("fromUserId", toIds,
//				new ImgMessage("txtMessagehaha", "http://aa.com/a.png"),
//				FormatType.json);
//		System.out.println("publishMessage=" + result);
//		result = ApiHttpClient.publishSystemMessage("123456",
//					toIds, new ContactNtfMessage(ContactNtfMessage.ACCEPTRESPONSE,"123456","13212345678","123","456"), "pushContent",
//					"pushData", FormatType.json);
//			System.out.println("========================publishSystemMessage=" + result);
			
		
		Gson gson = new Gson();
		String user_id="00000";// 自己的id
		String target_id="56807";// 对方id
		String msg="通知";// 消息内容
		String media_url="http://www.baidu.com";// 处理连接
		String extra="aa";// 扩展内容
		
		List<String> toIds = new ArrayList<String>();
		toIds.add(target_id);
		
		PushMessage pushMessage = new PushMessage();
		pushMessage.setMsg_id(-1);
		pushMessage.setMsg_data(System.currentTimeMillis());
		pushMessage.setMsg_type(PushMessageType.MSG_CUSTOM);
		pushMessage.setMsg_from_id(user_id);
		pushMessage.setMsg_target_id(target_id);
		pushMessage.setMsg_content(msg);
		pushMessage.setMsg_media_url(media_url);
		pushMessage.setMsg_media_path("");
		pushMessage.setMsg_extra(extra);
		pushMessage.setMsg_send_status(-1);
		pushMessage.setMsg_read_status(-1);
		pushMessage.setMsg_audio_read_status(-3);
		pushMessage.setMsg_audio_length(0);
		pushMessage.setMsg_group_id(-1);
		pushMessage.setMsg_conversation_type(-1);
		pushMessage.setUser_id(user_id);

		TxtMessage textMessage=new TxtMessage(gson.toJson(pushMessage));
		result = ApiHttpClient.publishMessage(user_id, toIds,
				textMessage, "pushContent", "pushData",
				FormatType.json);
		System.out.println("publishMessageAddpush=" + result);
		
//		result = ApiHttpClient.publishSystemMessage("56807",
//				toIds, new ContactNtfMessage(ContactNtfMessage.REQUEST,"56807","56806","test"), "pushContent",
//				"pushData", FormatType.json);
//		System.out.println("========================publishSystemMessage=" + result);

		
//		result = ApiHttpClient.createGroup(toIds, "123", "qf", FormatType.json);
//		System.out.println("==result======================publishSystemMessage=" + result);
//		result = ApiHttpClient.publishSystemMessage("fromUserId",
//				toIds, new TxtMessage("txtMessagehaha"), "pushContent",
//				"pushData", FormatType.json);
//		System.out.println("publishSystemMessage=" + result);
//
//		List<ChatroomInfo> chats = new ArrayList<ChatroomInfo>();
//		chats.add(new ChatroomInfo("idtest", "name"));
//		chats.add(new ChatroomInfo("id%s+-{}{#[]", "name12312"));
//		result = ApiHttpClient.createChatroom(chats,
//				FormatType.json);
//		System.out.println("createchatroom=" + result);
//		List<String> chatIds = new ArrayList<String>();
//		chatIds.add("id");
//		chatIds.add("id%+-:{}{#[]");
//		result = ApiHttpClient.queryChatroom(chatIds,
//				FormatType.json);
//		System.out.println("queryChatroom=" + result);
//
//		result = ApiHttpClient.publishChatroomMessage(
//				"fromUserId", chatIds, new TxtMessage("txtMessagehaha"),
//				FormatType.json);
//		System.out.println("publishChatroomMessage=" + result);
//
//		result = ApiHttpClient.destroyChatroom(chatIds,
//				FormatType.json);
//		System.out.println("destroyChatroom=" + result);
//		List<GroupInfo> groups = new ArrayList<GroupInfo>();
//		groups.add(new GroupInfo("id1", "name1"));
//		groups.add(new GroupInfo("id2", "name2"));
//		groups.add(new GroupInfo("id3", "name3"));
//		result = ApiHttpClient.syncGroup("userId1", groups,
//				FormatType.json);
//		System.out.println("syncGroup=" + result);
//		result = ApiHttpClient.joinGroup("userId2", "id1",
//				"name1", FormatType.json);
//		System.out.println("joinGroup=" + result);
//		List<String> list = new ArrayList<String>();
//		list.add("userId3");
//		list.add("userId1");
//		list.add("userId3");
//		list.add("userId2");
//		list.add("userId3");
//		list.add("userId3");
//		result = ApiHttpClient.joinGroupBatch(list, "id1",
//				"name1", FormatType.json);
//		System.out.println("joinGroupBatch=" + result);
//
//		result = ApiHttpClient.publishGroupMessage("userId1",
//				chatIds, new TxtMessage("txtMessagehaha"), "pushContent",
//				"pushData", FormatType.json);
//		System.out.println("publishGroupMessage=" + result);
//
//		result = ApiHttpClient.quitGroup("userId1", "id1",
//				FormatType.json);
//		System.out.println("quitGroup=" + result);
//		result = ApiHttpClient.quitGroupBatch(list, "id1",
//				FormatType.json);
//		System.out.println("quitGroupBatch=" + result);
//		result = ApiHttpClient.dismissGroup("userIddismiss",
//				"id1", FormatType.json);
//		result = ApiHttpClient.getMessageHistoryUrl("2014112811",
//				FormatType.json);
//		System.out.println("getMessageHistoryUrl=" + result);
//
//		result = ApiHttpClient.blockUser("2", 10,FormatType.json);
//		System.out.println("blockUser=" + result);
//
//		result = ApiHttpClient.blockUser("id2", 10,FormatType.json);
//		System.out.println("blockUser=" + result);
//
//		result = ApiHttpClient.blockUser("id3", 10,FormatType.json);
//		System.out.println("blockUser=" + result);
//
//		result = ApiHttpClient.queryBlockUsers(FormatType.json);
//		System.out.println("queryBlockUsers=" + result);
//
//		result = ApiHttpClient.unblockUser("id1", FormatType.json);
//		System.out.println("unblockUser=" + result);
//
//		result = ApiHttpClient.queryBlockUsers(FormatType.json);
//		System.out.println("queryBlockUsers=" + result);
//
//		result = ApiHttpClient.unblockUser("id2", FormatType.json);
//		System.out.println("unblockUser=" + result);
//
//		result = ApiHttpClient.unblockUser("id3", FormatType.json);
//		System.out.println("unblockUser=" + result);
//
//		result = ApiHttpClient.queryBlockUsers(FormatType.json);
//		System.out.println("queryBlockUsers=" + result);
//
//		result = ApiHttpClient.checkOnline("143", FormatType.json);
//		System.out.println("checkOnline=" + result);
//		
//		List<String> toBlackIds = new ArrayList<String>();
//		toBlackIds.add("22");
//		toBlackIds.add("12");
//
//		result = ApiHttpClient.blackUser("3706", toBlackIds,
//				FormatType.json);
//		System.out.println("blackUser=" + result);
//		
//		result = ApiHttpClient.QueryblackUser("3706",FormatType.json);
//		System.out.println("QueryblackUser=" + result);
//		
//		result = ApiHttpClient.unblackUser("3706", toBlackIds,
//				FormatType.json);
//		System.out.println("unblackUser=" + result);
//		
//		result = ApiHttpClient.QueryblackUser("3706",FormatType.json);
//		System.out.println("QueryblackUser=" + result);	
//
//		result = ApiHttpClient.deleteMessageHistory("2014122811",
//				FormatType.json);
//		System.out.println("deleteMessageHistory=" + result);
//		
//		result = ApiHttpClient.refreshGroupInfo("id1", "name4",
//				FormatType.json);
//		System.out.println("refreshGroupInfo=" + result);

	}
}
