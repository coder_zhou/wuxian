package com.qf.app.apptimer.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.qf.system.peilian.model.Peilian;
import com.qf.system.peilian.model.Peilian2;
import com.qf.system.peilian.service.PeilianService;
import com.qf.system.pic.service.PicService;
import com.qf.system.timer.model.Timer;
import com.qf.system.timer.model.Timer2;
import com.qf.system.timer.model.Timer3;
import com.qf.system.timer.service.TimerService;
import com.qf.system.userinfo.model.UserInfo;
import com.qf.system.userinfo.model.UserInfo2;
import com.qf.system.userinfo.service.UserInfoService;
import com.qf.system.video.service.VideoService;
import com.qf.util.aes.AES;
import com.qf.util.common.DateUtil;
import com.qf.util.common.JackJson;
import com.qf.util.common.Tools;
@Controller
@RequestMapping("timer")
public class AppTimerController {
	
	@Resource(name="timerService")
	private TimerService timerService;
	@Resource(name="peilianService")
	private PeilianService peilianService;
	@Resource(name="userinfoService")
	private UserInfoService userinfoService;
	@Resource(name="videoService")
	private VideoService videoService;
	@Resource(name="picService")
	private PicService picService;
	private Gson gson = new Gson();
	
	  /**
     * 查询时段列表
     * @param request
     * @param response
     * @param valueMap
     * @param rm
     * @throws Exception
     */
    @RequestMapping("queryTimer")
    private void queryBuddhaList(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	List<Timer> list=timerService.getList();
        	if(list==null || list.size()==0){
        		rm.put("result","0");
        		rm.put("msg","没有课程数据");
        	}else{
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }  
	
	
	
    @RequestMapping("yuyue")
    private void yuyue(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	  SimpleDateFormat sdfTime = new SimpleDateFormat(
    			"yyyy-MM-dd HH:mm");
    	  SimpleDateFormat dd = new SimpleDateFormat(
      			"yyyy-MM-dd");
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	Timer timer=timerService.getTimerById(valueMap.get("time_id").toString());
        	String datetime=timer.getTimstr()+" "+timer.getStattime();
        	UserInfo2  user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	Timer user_timer =timerService.getTimerByUserForToday(valueMap.get("user_id").toString());
        	Date s= sdfTime.parse(datetime);
        	boolean isRight=true;
        	if(timer==null ){
        		rm.put("result","0");
        		rm.put("msg","没有课程数据");
        	}else
        		if((timer.getXueyuan_laile()+1)>timer.getXueyuan()){
        	   		rm.put("result","0");
            		rm.put("msg","此课程学员已满");
            		isRight=false;
        		}else
        		if(timer.getUserInfo()!=null&&timer.getUserInfo().size()>0){
        			for(int i=0;i<timer.getUserInfo().size();i++){
        				if(timer.getUserInfo().get(i).getUser_id()==Integer.parseInt(valueMap.get("user_id").toString())){
        					rm.put("result","0");
        	        		rm.put("msg","您已预约此课程");
        	        		isRight=false;
        				}
        			}
        		}
        	if(isRight){
        	if(((s.getTime()/1000)-(System.currentTimeMillis()/1000))<(2*60*60)){
        			System.out.println(datetime+s.getTime()/1000+":"+System.currentTimeMillis()/1000);
        			System.out.println(DateUtil.fromTimestampToDate((long) 1448640000));
        				rm.put("result","0");
    	        		rm.put("msg","课程开始前2小时内不可预约");
        			}else if(user==null){
        				rm.put("result","0");
    	        		rm.put("msg","用户数据错误");
        				}else if(user.getKeshi_num()<=0){
        					rm.put("result","0");
        	        		rm.put("msg","您已经用完所有课时了");
        				}else if(dd.parse(user.getKeshi()).before(new Date())){
        					rm.put("result","0");
        	        		rm.put("msg","您的会员已到期");
        				}else if(user_timer!=null){
        					rm.put("result","0");
        	        		rm.put("msg","您今天已经约过了");
        				}else
        	  {
  				valueMap.put("today", timer.getTimstr());
  				timerService.yuyue(valueMap);
  				Timer tt=timerService.getTimerById(valueMap.get("time_id").toString());
  				rm.put("result","1");
          		rm.put("msg","预约成功");
          		rm.put("timer", tt);
  				
  			}
        			}
        				
               
            			
//        	}	
        		
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }  
	
    @RequestMapping("queryPeilian")
    private void queryPeilian(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	List<Peilian> list=peilianService.getPeilian();
        	if(list==null || list.size()==0){
        		rm.put("result","0");
        		rm.put("msg","没有陪练数据");
        	}else{
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }  
    
    @RequestMapping("getTimer")
    private void getTimer(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
//    	Map valueMap=new HashMap();
//    	valueMap.put("time_id", "6");
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	Timer timer=timerService.getTimerById(valueMap.get("time_id").toString());
        	if(timer==null ){
        		rm.put("result","0");
        		rm.put("msg","没有该课时数据");
        	}else{
        		rm.put("result","1");
        		rm.put("timer",timer);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }  
    
    @RequestMapping("getUser")
    public void getUser(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception{
      	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	UserInfo user=userinfoService.getUserByPhone(valueMap.get("phone").toString());
        	if(user==null ){
        		rm.put("result","0");
        		rm.put("msg","您还不是本店会员");
        	}else{
        		rm.put("result","1");
        		rm.put("user",user);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }
    
    @RequestMapping("checkUser")
    private void checkUser(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
 //   	Map valueMap=new HashMap();
 //   	valueMap.put("phone", "15146639681");
//    	valueMap.put("password","123123");
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//phone
        	//password
        	UserInfo user=userinfoService.getUserByPhone(valueMap.get("phone").toString());
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","该用户不是本店会员");
        	}else{
        		valueMap.put("checkTime", DateUtil.getDay());
        		int i=userinfoService.checkUser(valueMap);
        		UserInfo2 uu=userinfoService.getUserById(user.getUser_id());
        		rm.put("result","1");
        		rm.put("user",uu);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    }  
    @RequestMapping("login")
    private void login(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
//    	Map valueMap=new HashMap();
//    	valueMap.put("phone", "15146639681");
//    	valueMap.put("password","123123");
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//phone
        	//password
        	UserInfo user=userinfoService.getUserByPhoneAndPwd(valueMap);
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户名或密码错误");
        	}else if(user.getStatue()==0){
        		rm.put("result","0");
        		rm.put("msg","该用户未激活");
        	}
        	else{
        		UserInfo2 uu=userinfoService.getUserById(user.getUser_id());
//        		Map map=new HashMap();
//        		map.put("user", uu);
//        		List<Timer3> timer=timerService.getTimerByUserId(user.getUser_id());
//        		map.put("timer", timer);
        		rm.put("result","1");
        		rm.put("list",uu);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    @RequestMapping("getPhoto")
    private void getPhoto(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
//    	Map valueMap=new HashMap();
//    	valueMap.put("phone", "15146639681");
//    	valueMap.put("password","123123");
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//phone
        	//password
        	//password
        	UserInfo2 user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","该用户不是本店会员");
        	}else{
        		valueMap.put("checkTime", DateUtil.getDay());
        		List<Map> list=userinfoService.getPhotoByUserId(user.getUser_id());
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    @RequestMapping("addPhoto")
    private void addPhoto(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
//    	Map valueMap=new HashMap();
//    	valueMap.put("user_id", "15146639681");
//    	valueMap.put("password","123123");
//    	picurl =""
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//phone
        	//password
        	//password
        	UserInfo2 user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","该用户不是本店会员");
        	}else{
        		valueMap.put("checkTime", DateUtil.getDay());
        		valueMap.put("user_id", user.getUser_id());
        		valueMap.put("addTime", System.currentTimeMillis()/1000);
        		userinfoService.addphoto(valueMap);
        		List<Map> list=userinfoService.getPhotoByUserId(user.getUser_id());
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    @RequestMapping("PlPhoto")
    private void PlPhoto(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//phone
        	//password
        	//password
        	List<Map> list=peilianService.getPhotoByPeilianId(valueMap.get("pl_id").toString());
        	if(list==null){
        		rm.put("result","0");
        		rm.put("msg","无数据");
        	}else{
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    
    @RequestMapping("resetpassword")
    private void resetpassword(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//user_id
        	//password
        	//newpassword
        	UserInfo2 user=userinfoService.getUserByPhoneAndPwd2(valueMap);
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","原始密码错误");
        	}else{
        		userinfoService.updatePassword(valueMap);
        		rm.put("result","1");
        		rm.put("msg","修改成功");
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    
    @RequestMapping("updateUser")
    private void updateUser(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	UserInfo2 user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户ID错误");
        	}else{
        		int i=  userinfoService.updateUser(valueMap);
        		if(i==1){
        			UserInfo2 u2=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));	
        		rm.put("result","1");
        		rm.put("user",u2);
        		}else{
        			rm.put("result","0");
            		rm.put("msg","修改失败");
        		}
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    @RequestMapping("updateHead")
    private void updateHead(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	UserInfo2 user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户ID错误");
        	}else{
        		int i=  userinfoService.updateHead(valueMap);
        		if(i==1){
        		rm.put("result","1");
        		rm.put("msg","修改成功");
        		}else{
        			rm.put("result","0");
            		rm.put("msg","修改失败");
        		}
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    @RequestMapping("video")
    private void video(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	List<Map> user=videoService.listVideo();
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户ID错误");
        	}else{
        		rm.put("result","1");
        		rm.put("list",user);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    @RequestMapping("getUserTimer")
    private void getUserTimer(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	List<Timer3> list=timerService.getTimerByUserId(Integer.parseInt(valueMap.get("user_id").toString()));
        	
        	if(list==null){
        		rm.put("result","0");
        		rm.put("msg","无数据");
        	}else{
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    
    @RequestMapping("getPeilian")
    private void getPeilian(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
             Peilian2 p=peilianService.findById2(valueMap.get("pl_id").toString());
        	
        	if(p==null){
        		rm.put("result","0");
        		rm.put("msg","无此陪练");
        	}else{
        		List<Map> list=peilianService.getPhotoByPeilianId(valueMap.get("pl_id").toString());
        		List<Timer2> timer=timerService.getTimerByPeilianId(valueMap.get("pl_id").toString());
        		rm.put("result","1");
        		rm.put("size",list.size());
        		rm.put("timer", timer);
        		rm.put("peilian",p);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    @RequestMapping("banner")
    private void banner(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	List<Map> user=picService.bannerlist();
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户ID错误");
        	}else{
        		rm.put("result","1");
        		rm.put("list",user);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
    
    @RequestMapping("delPhoto")
    private void delPhoto(HttpServletRequest request,HttpServletResponse response, Map<String,Object> rm) throws Exception {  
    	//传值map
    	Map<String,Object> valueMap = (Map<String,Object>)request.getAttribute("valueMap");
        try { 
        	//user_id
        	//pic_id
        	UserInfo2 user=userinfoService.getUserById(Integer.parseInt(valueMap.get("user_id").toString()));
        	if(user==null){
        		rm.put("result","0");
        		rm.put("msg","用户ID错误");
        	}else{
        		userinfoService.delPhoto(valueMap);
        		List<Map> list=userinfoService.getPhotoByUserId(user.getUser_id());
        		rm.put("result","1");
        		rm.put("list",list);
        	}
        } catch(Exception e){
        	rm.put("result","0");
    		rm.put("msg","操作失败，请联系管理员");
        	e.printStackTrace();
        }
        returnMsg(response,rm);
    } 
    
	  /**   
		 * 返回错误信息
		 * @param request   
		 * @param response
		 * @param rm
		 * @throws Exception
		 */
		private void returnMsg(HttpServletResponse response,Map<String,Object> rm) throws Exception {  
	        String json  = "";  
	        try {  
	        	
	        	
				json = JackJson.fromObjectToJson(rm);
				System.out.println("----json---->"+json);
				String resultjson = AES.encrypt(json);
	            response.setContentType("text/html;charset=utf-8");  
	            response.getWriter().write(resultjson);  
	        } catch (IOException e) {  
	        	 e.printStackTrace();
	        }  
	    }

}
