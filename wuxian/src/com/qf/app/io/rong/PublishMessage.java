package com.qf.app.io.rong;


//import io.rong.imlib.NativeObject.UserInfo;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.qf.app.io.rong.models.FormatType;
import com.qf.app.io.rong.models.PushMessage;
import com.qf.app.io.rong.models.PushMessageType;
import com.qf.app.io.rong.models.SdkHttpResult;
import com.qf.app.io.rong.models.TxtMessage;
import com.qf.util.IPutil;



public class PublishMessage {
     public static void pushMsgToPhone(String user_id,String target_id,String media_url,String extra) throws Exception{
    	 SdkHttpResult result = null;
    	 Gson gson = new Gson();
// 		String user_id="00000";// 自己的id
// 		String target_id="56807";// 对方id
 		String msg="通知";// 消息内容
// 		String media_url="http://www.baidu.com";// 处理连接
// 		String extra="aa";// 扩展内容
 		
 		List<String> toIds = new ArrayList<String>();
 		toIds.add(target_id);
 		
 		PushMessage pushMessage = new PushMessage();
 		pushMessage.setMsg_id(-1);
 		pushMessage.setMsg_data(System.currentTimeMillis());
 		pushMessage.setMsg_type(PushMessageType.MSG_CUSTOM);
 		pushMessage.setMsg_from_id(user_id);
 		pushMessage.setMsg_target_id(target_id);
 		pushMessage.setMsg_content(msg);
 		pushMessage.setMsg_media_url(media_url);
 		pushMessage.setMsg_media_path("");
 		pushMessage.setMsg_extra(extra);
 		pushMessage.setMsg_send_status(-1);
 		pushMessage.setMsg_read_status(-1);
 		pushMessage.setMsg_audio_read_status(-3);
 		pushMessage.setMsg_audio_length(0);
 		pushMessage.setMsg_group_id(-1);
 		pushMessage.setMsg_conversation_type(-1);
 		pushMessage.setUser_id(user_id);
 		
 		JSONObject jb = new JSONObject();
 		jb.put("user_icon", "http://7xkccy.com2.z0.glb.qiniucdn.com/icon_qjqw1_n.png");
 		jb.put("user_name","祈福小助手");
 		jb.put("user_id", "00000");
 		pushMessage.setMsg_user_extra(jb.toString()); //头像   名称  用户id;
 		TxtMessage textMessage=new TxtMessage(gson.toJson(pushMessage));
 		
 		result = ApiHttpClient.publishMessage(user_id, toIds,
 				textMessage, "pushContent", "pushData",
 				FormatType.json);
 		System.out.println("publishMessageAddpush=" + result);
     }
     
     public static void pushMsgToAllPhone(String user_id,List<String> toIds,String media_url,String extra) throws Exception{
    	 SdkHttpResult result = null;
    	 Gson gson = new Gson();
// 		String user_id="00000";// 自己的id
// 		String target_id="56807";// 对方id
 		String msg="通知";// 消息内容
// 		String media_url="http://www.baidu.com";// 处理连接
// 		String extra="aa";// 扩展内容
 		
// 		List<String> toIds = new ArrayList<String>();
// 		toIds.add(target_id);
 		
 		PushMessage pushMessage = new PushMessage();
 		pushMessage.setMsg_id(-1);
 		pushMessage.setMsg_data(System.currentTimeMillis());
 		pushMessage.setMsg_type(PushMessageType.MSG_CUSTOM);
 		pushMessage.setMsg_from_id(user_id);
// 		pushMessage.setMsg_target_id(target_id);
 		pushMessage.setMsg_content(msg);
 		pushMessage.setMsg_media_url(media_url);
 		pushMessage.setMsg_media_path("");
 		pushMessage.setMsg_extra(extra);
 		pushMessage.setMsg_send_status(-1);
 		pushMessage.setMsg_read_status(-1);
 		pushMessage.setMsg_audio_read_status(-3);
 		pushMessage.setMsg_audio_length(0);
 		pushMessage.setMsg_group_id(-1);
 		pushMessage.setMsg_conversation_type(-1);
 		pushMessage.setUser_id(user_id);
 		
 		JSONObject jb = new JSONObject();
 		jb.put("user_icon", "http://7xkccy.com2.z0.glb.qiniucdn.com/icon_qjqw1_n.png");
 		jb.put("user_name","祈福小助手");
 		jb.put("user_id", "00000");
 		pushMessage.setMsg_user_extra(jb.toString()); //头像   名称  用户id;
 		TxtMessage textMessage=new TxtMessage(gson.toJson(pushMessage));
 		
 		result = ApiHttpClient.publishMessage(user_id, toIds,
 				textMessage, "pushContent", "pushData",
 				FormatType.json);
 		System.out.println("publishMessageAddpush=" + result);
     }
     
     public static void main(String[] args) throws Exception {
    	 JSONObject jb = new JSONObject();
  		jb.put("icon", IPutil.getIp()+"/goods/images/showmessage/notice_relationcemetery.png");
  		jb.put("title","系统消息");
  		jb.put("content","111");
  		String url = IPutil.getIp()+"/appPushMessage/showMessage?msg_id="+314523;
    	 pushMsgToPhone("00000","56807",url,jb.toString());
	}
}
