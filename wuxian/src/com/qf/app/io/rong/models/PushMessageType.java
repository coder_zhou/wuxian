package com.qf.app.io.rong.models;

/**
 * 推送消息读的消息类型
 * 
 * @author jfc
 */
public class  PushMessageType {

	/** 文本消息 -1 */
	public static final int MSG_TXT = -1;
	/** 图像消息 -2 */
	public static final int MSG_IMAGE = -2;
	/** 音频消息 -3 */
	public static final int MSG_AUDIO = -3;
	/** 视频消息 -4 */
	public static final int MSG_VIDEO = -4;
	/** 位置消息 -5 */
	public static final int MSG_LOCATION = -5;
	/** 文件消息 -6 */
	public static final int MSG_FILE = -6;
	/** 富文本消息 -7 */
	public static final int MSG_CUSTOM = -7;

}
