package com.qf.app.io.rong.models;


import java.io.Serializable;

/**
 * 推送消息model
 * 
 * @author jfc
 */
public class PushMessage implements Serializable{

	private static final long serialVersionUID = 1L;
	private int _id;// 数据库id
	private int msg_id;// 消息id
	private long msg_data;// 消息时间
	private int msg_type;// 消息类型
	private String msg_from_id;// 发消息人
	private String msg_target_id;// 收消息人
	private String msg_content;// 消息内容 针对text消息
	private String msg_media_url;// 多媒体消息url
	private String msg_media_path;// 本地多媒体路径
	private String msg_extra;// 消息扩展
	private int msg_send_status;// 消息状态，发送中，发送失败，成功
	private int msg_read_status;// 消息读取状态 已读 未读
	private int msg_audio_read_status;// 语音读取状态，已听 未听
	private int msg_audio_length;// 语音消息 时长
	private int msg_group_id;// 消息群组ID
	private int msg_conversation_type;// 会话类型 群组 私聊
	private String user_id;// 消息属于谁
	private String msg_user_extra;//消息用户扩展

	/**
	 * 获取消息数据库ID
	 */
	public int get_id() {
		return _id;
	}

	/**
	 * 设置消息数据库ID
	 * 
	 * @param msg_id
	 *            消息ID
	 */
	public void set_id(int _id) {
		this._id = _id;
	}

	/**
	 * 获取消息ID
	 */
	public int getMsg_id() {
		return msg_id;
	}

	/**
	 * 设置消息ID
	 * 
	 * @param msg_id
	 *            消息ID
	 */
	public void setMsg_id(int msg_id) {
		this.msg_id = msg_id;
	}

	/**
	 * 获取消息时间
	 */
	public long getMsg_data() {
		return msg_data;
	}

	/**
	 * 设置消息时间
	 * 
	 * @param msg_data
	 *            消息时间
	 */
	public void setMsg_data(long msg_data) {
		this.msg_data = msg_data;
	}

	/**
	 * 获取消息类型
	 */
	public int getMsg_type() {
		return msg_type;
	}

	/**
	 * 设置消息类型
	 * 
	 * @param msg_type
	 *            消息类型
	 */
	public void setMsg_type(int msg_type) {
		this.msg_type = msg_type;
	}

	/**
	 * 获取消息发送者userId
	 */
	public String getMsg_from_id() {
		return msg_from_id;
	}

	/**
	 * 设置消息发送者ID
	 * 
	 * @param msg_from_id
	 *            消失发送者userId
	 */
	public void setMsg_from_id(String msg_from_id) {
		this.msg_from_id = msg_from_id;
	}

	/**
	 * 获取消息接收者UserId
	 */
	public String getMsg_target_id() {
		return msg_target_id;
	}

	/**
	 * 设置消息接收者UserId
	 * 
	 * @param msg_target_id
	 *            消息接收者userId
	 */
	public void setMsg_target_id(String msg_target_id) {
		this.msg_target_id = msg_target_id;
	}

	/**
	 * 获取消息内容
	 */
	public String getMsg_content() {
		return msg_content;
	}

	/**
	 * 设置消息内容
	 * 
	 * @param msg_content
	 *            消息内容 配合text消息类型
	 */
	public void setMsg_content(String msg_content) {
		this.msg_content = msg_content;
	}

	/**
	 * 获取多媒体消息网络文件url
	 */
	public String getMsg_media_url() {
		return msg_media_url;
	}

	/**
	 * 设置多媒体消息网络文件url
	 * 
	 * @param msg_media_url
	 *            网络文件url(图片、语音、视频、文件)
	 */
	public void setMsg_media_url(String msg_media_url) {
		this.msg_media_url = msg_media_url;
	}

	/**
	 * 获取多媒体消息文件本地存储路径
	 */
	public String getMsg_media_path() {
		return msg_media_path;
	}

	/**
	 * 设置多媒体文件消息本地存储路径
	 * 
	 * @param msg_media_path
	 *            多媒体文件本地存储路径
	 */
	public void setMsg_media_path(String msg_media_path) {
		this.msg_media_path = msg_media_path;
	}

	/**
	 * 获取消息扩展
	 */
	public String getMsg_extra() {
		return msg_extra;
	}

	/**
	 * 设置消息扩展
	 * 
	 * @param msg_extra
	 *            消息扩展内容
	 */
	public void setMsg_extra(String msg_extra) {
		this.msg_extra = msg_extra;
	}

	/**
	 * 获取消息发送状态
	 */
	public int getMsg_send_status() {
		return msg_send_status;
	}

	/**
	 * 设置消息发送状态
	 * 
	 * @param msg_send_status
	 *            消息发送状态
	 */
	public void setMsg_send_status(int msg_send_status) {
		this.msg_send_status = msg_send_status;
	}

	/**
	 * 获取消息读取状态
	 */
	public int getMsg_read_status() {
		return msg_read_status;
	}

	/**
	 * 设置消息读取状态
	 * 
	 * @param msg_read_status
	 *            消息读取状态
	 */
	public void setMsg_read_status(int msg_read_status) {
		this.msg_read_status = msg_read_status;
	}

	/**
	 * 获取音频消息听取状态
	 */
	public int getMsg_audio_read_status() {
		return msg_audio_read_status;
	}

	/**
	 * 设置音频消息听取状态
	 * 
	 * @param msg_audio_read_status
	 *            音频消息听取状态
	 */
	public void setMsg_audio_read_status(int msg_audio_read_status) {
		this.msg_audio_read_status = msg_audio_read_status;
	}

	/**
	 * 获取音频消息时长
	 */
	public int getMsg_audio_length() {
		return msg_audio_length;
	}

	/**
	 * 设置音频消息时长
	 * 
	 * @param msg_audio_length
	 *            音频消息时长
	 */
	public void setMsg_audio_length(int msg_audio_length) {
		this.msg_audio_length = msg_audio_length;
	}

	/**
	 * 获取消息群组id
	 */
	public int getMsg_group_id() {
		return msg_group_id;
	}

	/**
	 * 设置消息群组id
	 * 
	 * @param msg_group_id
	 *            群组id
	 */
	public void setMsg_group_id(int msg_group_id) {
		this.msg_group_id = msg_group_id;
	}

	/**
	 * 获取会话类型
	 */
	public int getMsg_conversation_type() {
		return msg_conversation_type;
	}

	/**
	 * 设置会话类型
	 * 
	 * @param msg_conversation_type
	 *            会话类型
	 */
	public void setMsg_conversation_type(int msg_conversation_type) {
		this.msg_conversation_type = msg_conversation_type;
	}
	
	
	/**
	 * 获取消息user_id
	 */
	public String getUser_id() {
		return user_id;
	}
	/**
	 * 设置消息user_id
	 * 
	 * @param user_id
	 *            消息user_id
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getMsg_user_extra() {
		return msg_user_extra;
	}

	public void setMsg_user_extra(String msg_user_extra) {
		this.msg_user_extra = msg_user_extra;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PushMessage [_id=" + _id + ", msg_id=" + msg_id + ", msg_data="
				+ msg_data + ", msg_type=" + msg_type + ", msg_from_id="
				+ msg_from_id + ", msg_target_id=" + msg_target_id
				+ ", msg_content=" + msg_content + ", msg_media_url="
				+ msg_media_url + ", msg_media_path=" + msg_media_path
				+ ", msg_extra=" + msg_extra + ", msg_send_status="
				+ msg_send_status + ", msg_read_status=" + msg_read_status
				+ ", msg_audio_read_status=" + msg_audio_read_status
				+ ", msg_audio_length=" + msg_audio_length + ", msg_group_id="
				+ msg_group_id + ", msg_conversation_type="
				+ msg_conversation_type + ", user_id=" + user_id
				+ ", msg_user_extra=" + msg_user_extra + "]";
	}

	

}
