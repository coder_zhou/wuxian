package com.qf.app.io.rong.models;

import java.util.List;

import com.qf.util.common.JackJson;

/**
 * 
 * @author guohua 2015.9.21
 * 
 */
public class ShowMessage {
	private int buttonFlag;// 按钮标志位 1.有按钮 0.没有按钮
	private List<String> btnList; // 按钮list
	private String msgTitle;// 消息标题
	private String msgTime;// 消息时间
	private String msgContent;// 消息的具体内容

	
	
	 
	public String getJson(ShowMessage sw){
		return JackJson.fromObjectToJson(sw);
	}
	
	@Override
	public String toString() {
		return "ShowMessage [buttonFlag=" + buttonFlag + ", btnList=" + btnList
				+ ", msgTitle=" + msgTitle + ", msgTime=" + msgTime
				+ ", msgContent=" + msgContent + "]";
	}

	public int getButtonFlag() {
		return buttonFlag;
	}

	public void setButtonFlag(int buttonFlag) {
		this.buttonFlag = buttonFlag;
	}

	public List<String> getBtnList() {
		return btnList;
	}

	public void setBtnList(List<String> btnList) {
		this.btnList = btnList;
	}

	public String getMsgTitle() {
		return msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

	public String getMsgTime() {
		return msgTime;
	}

	public void setMsgTime(String msgTime) {
		this.msgTime = msgTime;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

}
