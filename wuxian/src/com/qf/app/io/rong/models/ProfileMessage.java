package com.qf.app.io.rong.models;

import com.qf.app.io.rong.util.GsonUtil;


//资料通知消息
public class ProfileMessage extends Message {

	private String operation;
	private String data;
	private String extra ;

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public ProfileMessage(String operation,String data) {
		this.type = "RC:ProfileNtf";
		this.operation = operation;
		this.data = data;
	}

	public ProfileMessage(String operation,String data,String extra) {
		this(operation,data);
		this.extra = extra;
	}

	@Override
	public String toString() {
		return GsonUtil.toJson(this, ProfileMessage.class);
	}
}
